<?php

error_reporting(E_ALL ^ E_NOTICE);

/* MAIN Config - set the system path */
const PATH_SYSTEM = 'system';
const PATH_APPLICATION = 'application';
const PATH_THEMES = 'themes';
const PATH_UPLOADS = 'uploads';
/* End MAIN Config */

/* include autoload rules for libs from extra folder */
include_once(PATH_APPLICATION . '/extra/autoload.php');

/* Main autoload rules */
spl_autoload_register(function($classname) {
    $tmp = explode('\\', $classname);

    foreach ($tmp as $k => $v) {
        $tmp[$k] = lcfirst($v);

        if ($tmp[$k] == $v) {
            throw new Core\Exceptions\Fatal("$classname - is not correct name of class or namespace");
        }
    }
    
    $folder = ($tmp[0] == 'core') ? PATH_SYSTEM : PATH_APPLICATION;
    $path = $folder . '/' . implode('/', $tmp) . '.php';
    
    if (file_exists($path)){
        include_once($path);
    }
    else{
        throw new Core\Exceptions\Fatal("$classname - not found");
    }
});

date_default_timezone_set(Config::TIME_ZONE);
setlocale(LC_ALL, Config::LOCALE);

try{
    $url = new Core\Urlparams($_GET);
    $init_class = $url->admin ? 'Admin\\Init' : 'Client\\Init';
    
    $init = new $init_class($url->params, $url->get);
    $init->request();
    echo $init->output();
}
catch(Core\Exceptions\E404 $e){
    echo 'Необработанная ошибка 404 - страница не найдена.';
}
catch(Core\Exceptions\Fatal $e){
    echo 'На сервере произошла странная ошибка, админ уведомлён.';
}
catch(Core\Exceptions\Exit_now $e){
    /* silense is gold */
}
catch(Core\Exceptions\Base $e){
    echo 'Необработанная ошибка.';
}
catch(Exception $e){
    echo $e;
}

// egarden71@mail.ru	e741G741
// kp.pitomnik71@mail.ru 741ePitomnic
// mail@pitomnik71.ru	e741Pitomnik
<?php

namespace Admin\Cats;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;
    
    const IMAGE_TYPE = 'cat';
    const IMAGE_NAME = 'Для категорий';
    const IMAGE_RELATION = 'cat';

    public function init() {
        $this->on_page = 10;
        
        Events::add('Module.Images.WantSizes', function(\Admin\Images\Resizes $resizes){
            $resizes->add_size(self::IMAGE_RELATION, 'home', 350, 10000);
			$resizes->add_size(self::IMAGE_RELATION, 'middle', 390, 10000);
			$resizes->add_size(self::IMAGE_RELATION, 'slider', 660, 10000);
        });
    }

    public function manifest() {
        return [
            'routs' => [
                'cats' => [
                    'c' => 'c_cats'
                ]
            ],
            'using' => [
                'users',
                'images' => [
                    'image_type' => self::IMAGE_TYPE,
                    'image_name' => self::IMAGE_NAME,
                    'image_item' => self::IMAGE_RELATION
                ]
            ],
            'registry_privs' => [
                'cats' => 'Работа с категориями'
            ],
            'menu' => [
				'cats/page/shop' => [
                    'name' => 'Категории',
                    'icon' => 'anchor',
                    'priority' => 1,
                    'access' => 'cats'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'cats_',
            'tables' => [
                'cats' => [
                    'fields' => ['id_cat', 'id_parent', 'cat_type', 'url', 'item_url', 'name', 'description', 'num_sort'],
                    'not_empty' => ['id_parent', 'item_url', 'cat_type', 'name'],
                    'unique' => [],
                    'composite_unique' => [['cat_type', 'id_parent', 'item_url'], ['cat_type', 'name']],
                    'range' => [
                        'cat_type' => ['2', '64'],
                        'name' => ['2', '64'],
                        'item_url' => ['2', '64'],
                        'description' => ['0', '256']
                    ],
                    'labels' => [
                        'id_parent' => 'Родительская категория',
                        'cat_type' => 'Тип категории',
                        'url' => 'Полный URL адрес',
                        'item_url' => 'URL категории',
                        'name' => 'Название',
                        'description' => 'Описание',
                        'num_sort' => 'Сортировка',
                        'children_num_sort' => 'Сортировка дочерних категорий'
                    ],
                    'pk' => 'id_cat'
                ]
            ]
        ];
    }

}

<?php

namespace Admin\Cats\Api;

use Admin\Cats;

class Main {

    use \Core\Traits\Singleton;

    protected $mCats;

    protected function __construct() {
        $this->mCats = Cats\M_cats::instance();
    }

    public function all($cat_type){
        return $this->mCats->get_by_fieldset(['cat_type' => $cat_type]);
    }
    
    public function get($id_cat){
        return $this->mCats->get($id_cat);
    }
    
    public function tree($cat_type){
        return $this->mCats->cat_tree($cat_type);
    }
	public function all_price(){
        return $this->mCats->all();
    }
}

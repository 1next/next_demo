<?php

namespace Admin\Cats;
	
class M_cats extends \Core\Base\Model{
    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Tree;

    protected function __construct(){
        parent::__construct(App::instance(), 'cats');
    }
    
    public function get_types($using_me){
        $cat_types = [];
        
        foreach($using_me as $params){
            if(isset($params['cat_type'])){
                if (!isset($params['cat_name'])) {
                    $params['cat_name'] = $params['cat_type'];
                }

                $cat_types[$params['cat_type']] = $params['cat_name'];
            }
        } 
            
        return $cat_types;
    }
    
    public function cat_tree($cat_type){
        $cats = $this->first_level($cat_type);
        
        foreach($cats as $k => $c){
            $cats[$k]['children'] = $this->make_tree($c['id_cat']);
        }
        
        return $cats;
    }

    public function first_level($cat_type) {
        return $this->db->select("SELECT * FROM {$this->table} 
                                  WHERE {$this->field_id_parent}=0 AND cat_type=:cat_type
                                  ORDER BY {$this->field_num_sort} ASC",
                                  ['cat_type' => $cat_type]);
    }
}
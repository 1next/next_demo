<div>
    <?=$html->ul_tree($tree, function($item) use($html, $admin_root){
        return $html->elem('a', 1)->html($item['name'])->attrs([
            'href' => $admin_root . 'cats/edit/' .  $item['id_cat']
        ]);
    }); ?>
    
    <div class="offset-buttons">
        <a class="btn btn-primary btn pull-left" href="<?=$admin_root?>cats/add/<?=$cat_type?>">
            Добавить новую категорию &raquo;
        </a>
        <a class="btn btn-default btn" href="<?=$admin_root?>cats/sort/<?=$cat_type?>">Сортировка корневых разделов</a>
    </div>
</div>
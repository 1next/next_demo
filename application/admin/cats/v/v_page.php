<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Название</th>
                <th>Описание</th>
                <th>URL</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $cat): ?>
                <tr>
                    <td><?= $cat['name'] ?></td>
                    <td><?= $cat['description'] ?></td>
                    <td><?= $cat['url'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>images/work/<?=$other['image_rel']?>/<?= $cat['id_cat'] ?>" title="Загрузить изображения" class="admin-btn admin-btn-image"></a>
                        <a href="<?=$admin_root?>cats/edit/<?=$other['cat_type']?>/<?= $cat['id_cat'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>cats/delete/<?= $cat['id_cat'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>cats/add/<?=$other['cat_type']?>/<?= $cat['id_cat'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>

    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>cats/add/<?=$other['cat_type']?>">Добавить новую категорию &raquo;</a></p>
</div>

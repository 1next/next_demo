<div>
    <form method="POST">
        <?=$html->ul($sort, function($li, $elem) use($html){
                $span = $html->elem('span', 1)->cl('ui-icon ui-icon-arrowthick-2-n-s');
                $li->html($span . $elem['name'])->attr('data-id', $elem['id_cat'])->cl('ui-state-default');
            })->cl('sorting');
        ?>
        <?=$html->input('hidden')->idn('num_sort')?>
        <input type="submit" class="btn-sm btn btn-success" id="sortable-save-btn" value="Сохранить сортировку">
    </form>
</div>
<form method="post">
    <?=$html->input('hidden')->val($fields['cat_type'])->idn('cat_type');?>
    <?=$f->set($html->select_tree($data['tree'], $fields['id_parent']), 'id_parent')?>
    <?=$f->set($html->input()->val($fields['item_url']), 'item_url') ?>
    <?=$f->set($html->input()->val($fields['name']), 'name') ?>
    <?=$f->set($html->textarea()->val($fields['description']), 'description') ?>
    <? foreach($errors->unshown() as $err): ?>
        <p class="error"><?=$err?></p>
    <? endforeach; ?>
    <div class="submit-buttons">
        <input type="submit" value="Сохранить" class="btn btn-success">
        <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
    </div>
</form>

<?php

namespace Admin\Cats;

use Core\Traits\Actions;
use Core\Arr;
use Admin\Base\Fieldgen;
use Core\Exceptions;
use Core\Info;
use Core\Session;

class C_cats extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();

        $this->check_access('cats');
        $this->module = 'cats';
        $this->main_model = M_cats::instance();
    }

    public function action_select() {
        $cat_types = $this->main_model->get_types(Info::using_me($this->module));
        
        if(count($cat_types) == 0){
            throw new \Core\Exceptions\E404("Nobody use cats module");
        }

        $link = isset($this->get['back']) ? $this->get['back'] : 'page';
        
        $this->to_base_template['title'] = 'Выберите тип категории для работы';
        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_select", 
                                                               ['cat_types' => $cat_types, 'link' => $link]);
    }
    
    public function action_tree() {
        if (!isset($this->params[2])) {
            $this->redirect($this->root . $this->module . '/select?back=tree');
        }
        
        $cat_type = trim($this->params[2]);
        $cat_types = $this->main_model->get_types(Info::using_me($this->module));
        
        if(!isset($cat_types[$cat_type])){
            throw new \Core\Exceptions\E404("Cat type $cat_type is not registred");
        }
        
        Session::push($this->module . '_redirect', $this->module . '/tree/' . $cat_type);
        
        $this->to_base_template['title'] .= "Дерево категорий - \"{$cat_types[$cat_type]}\"";
        $tree = $this->main_model->cat_tree($cat_type);
        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_tree", ['tree' => $tree, 'cat_type' => $cat_type]);
    }

    public function action_sort() {
        if (!isset($this->params[2])) {
            $this->redirect($this->root . $this->module . '/select?back=tree');
        }
        
        $cat_type = trim($this->params[2]);
        $cat_types = $this->main_model->get_types(Info::using_me($this->module));
        
        if(!isset($cat_types[$cat_type])){
            throw new \Core\Exceptions\E404("Cat type $cat_type is not registred");
        }
        
        $errors = $this->main_model->errors();
        $this->to_base_template['title'] .= 'Сортировка корневых категорий';
        $this->add_scripts($this->module, ['init']);
        $sort = $this->main_model->first_level($cat_type);
        $f = new Fieldgen(App::instance()->labels('cats'), $errors);

        if ($this->is_post()) {
            if (!($this->main_model instanceof \Core\Base\Model)) {
                throw new Exceptions\Fatal('Bad model');
            }

            if ($this->main_model->sort($_POST['num_sort'])) {
                $this->redirect($this->root . $this->module . '/tree/' . $cat_type);
            }

            $errors = $this->main_model->errors();
        }

        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_sort",
            ['sort' => $sort, 'errors' => $errors, 'f' => $f]);
    }

    protected function page_num(){
        return isset($this->params[3]) ? (int) $this->params[3] : 1;
    }
    
    protected function settings_action_page() {
        if (!isset($this->params[2])) {
            $this->redirect($this->root . $this->module . '/select?back=page');
        }

        $cat_type = trim($this->params[2]);
        $cat_types = $this->main_model->get_types(Info::using_me($this->module));
        
        if(!isset($cat_types[$cat_type])){
            throw new \Core\Exceptions\E404("Cat type $cat_type is not registred");
        }
        
        $this->page_other_data['cat_type'] = $cat_type;
        $this->page_other_data['image_rel'] = App::IMAGE_RELATION;
        
        $this->to_base_template['title'] .= "Список категорий: страница " . $this->page_num;
        $this->main_model->pager()->url_self($this->module . '/page/' . $cat_type . '/%s')
                                  ->where("cat_type='$cat_type'");
    }

    protected function settings_action_add() {
        if (!isset($this->params[2])) {
            throw new \Core\Exceptions\E404("Please select category type");
        }

        $cat_type = trim($this->params[2]);
        $cat_types = $this->main_model->get_types(Info::using_me($this->module));
        
        if(!isset($cat_types[$cat_type])){
            throw new \Core\Exceptions\E404("Cat type $cat_type is not registred");
        }
        
        $this->to_base_template['title'] .= "Добавление категории. Тип - \"{$cat_types[$cat_type]}\"";

        if (isset($this->params[3]) && is_numeric($this->params[3])) {
            $this->add_fields_data = $this->main_model->get($this->params[3]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[3]} not found.");
            }
        } else {
            $this->add_fields_data = ['id_parent' => 0, 'is_show' => 1, 'cat_type' => $cat_type];
        }
        
        $this->add_extract = ['id_parent', 'cat_type', 'item_url', 'name', 'description', 'is_show'];
        $this->add_redirect = $this->module . '/page/' . $cat_type;
        
        $tree = $this->main_model->make_tree();
        $tree = $this->main_model->cat_tree($cat_type);
        
        array_unshift($tree, ['id_cat' => 0, 'name' => 'Без категории']);
        $this->add_other_data['tree'] = Arr::to_select_tree($tree, 'id_cat', 'name');        
        $this->add_fieldgen = new Fieldgen($this->app->labels('cats'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->edit_id = $this->params[3];
        
        $this->to_base_template['title'] .= 'Редактирование категории';
        $this->add_scripts($this->module, ['init']);

        $this->edit_extract = ['id_parent', 'item_url', 'name', 'description', 'children_num_sort'];
        
        $item = $this->main_model->get($this->edit_id);
        $tree = $this->main_model->cat_tree($item['cat_type']);
        $children = $this->main_model->get_children($this->edit_id);
        array_unshift($tree, ['id_cat' => 0, 'name' => 'Без категории']);

        $disabled = [$this->params[3]];

        $down = $this->main_model->get_down_branch($this->edit_id);
        
        if ($down) {
            foreach ($down as $item) {
                $disabled[] = $item['id_cat'];
            }
        }

        $this->edit_other_data['tree'] = Arr::to_select_tree($tree, 'id_cat', 'name');
        $this->edit_other_data['sort'] = $children;
        $this->edit_other_data['disabled'] = $disabled;
        $this->edit_other_data['cat_type'] = $cat_type;
        $this->edit_fieldgen = new Fieldgen($this->app->labels('cats'), $this->errors);
    }

    protected function settings_action_delete() {
        
    }

}

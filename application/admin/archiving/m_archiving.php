<?php

namespace Admin\Archiving;

use Config;
use ZipArchive;
use Admin\Archiving\App;

class M_archiving extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'archiving');
    }

    /**
     * Получение имен таблиц
     * @return array - Список всех таблиц БД
     */
    public function get_tables_names() {
        $res = $this->db->select("SHOW TABLES");
        $tables = [];

        foreach ($res as $table) {
            foreach ($table as $t) {
                $tables[] = $t;
            }
        }
        return $tables;
    }

    /**
     * Получение содержимого таблиц
     * @return array - Содержимое дла файла `бекапа`
     */
    public function get_tables() {
        $in_file = [];
        $tables = $this->get_tables_names();

        if (empty($tables)) {
            return $in_file;
        }

        $res = [];
        $database = Config::DATABASES;
        $in_file[] = "--\n-- База данных: `" . $database['main']['database'] . "`\n--";

        foreach ($tables as $t) {
            $header = true;
            $columns = '';
            $in_file[] = "\n\n--\n-- Дамп данных таблицы `$t`\n--\n";

            $in_file[] = "-- Структура таблицы `$t`";
            $res[$t]['structure'] = $this->db->select("SHOW CREATE TABLE `" . $t . "`");
            $in_file[] = $res[$t]['structure'][0]['Create Table'] . "\n\n";

            $res[$t]['value'] = $this->db->select("SELECT * FROM {$t}");
            $no_zpt_str = true;

            foreach ($res[$t]['value'] as $value) {
                if ($header) {
                    $no_zpt = true;

                    foreach ($value as $k => $v) {
                        if ($no_zpt) {
                            $columns .= "`$k`";
                            $no_zpt = false;
                        } else {
                            $columns .= ", `$k`";
                        }
                    }
                    $header = false;
                    $in_file[] = "INSERT INTO `$t`($columns) VALUES\n";
                }
                $columns_value = '';
                $no_zpt = true;

                foreach ($value as $k => $v) {
                    if ($no_zpt) {
                        $columns_value .= "'$v'";
                        $no_zpt = false;
                    } else {
                        $columns_value .= ", '$v'";
                    }
                }

                if ($no_zpt_str) {
                    $in_file[] = "($columns_value)";
                    $no_zpt_str = false;
                } else {
                    $in_file[] = ",\n($columns_value)";
                }
            }
        }
        return $in_file;
    }

    /**
     * Создание архива БД
     * @return string - имя файла `бекапа`
     */
    public function create_backup_db() {
        $in_file = $this->get_tables();

        if (empty($in_file)) {
            return '';
        }

        $fname = 'backup_' . date('Y-m-d_h-i-s');
        $file_sql = $fname . '.sql';

        file_put_contents(App::PATH_BACKUP . $file_sql, $in_file, FILE_APPEND | LOCK_EX);

        return $file_sql;
    }

    /**
     * Получить все архивы
     * @param string $dir - Путь для хранения `бекапа`
     * @return array - Список созданных `бекапов`
     */
    public function get_arhiv($dir = App::PATH_BACKUP) {
        $result = [];

        foreach (scandir($dir) as $item) {
            if ($item != "." && $item != "..") {
                if (!is_dir($item)) {
                    $result[] = $item;
                }
            }
        }
        return $result;
    }

    /**
     * Получение всех файлов и папок для архивирования
     * @param string $dir - Путь к файлам сайта
     * @param array $no_backup_f - Список исключенных папок
     * @param array $no_backup_e - Список исключенных расширений
     * @return array - Список всех папок и файлов
     */
    public function get_dirs($dir = '', $no_backup_f = App::NO_BACKUP_FOLDER, $no_backup_e = App::NO_BACKUP_EXT) {
        $result = [];

        if (trim($dir) != '') {
            foreach (scandir($dir) as $file) {
                if (!in_array($file, ['.', '..'])) {
                    $current = $dir . $file;
                    $ext = array_reverse(explode('.', $current));

                    if (!in_array($file, $no_backup_f) && !in_array($ext[0], $no_backup_e)) {
                        if (is_file($current)) {
                            $result[] = $current;
                        }

                        if (is_dir($current)) {
                            $result[] = $this->get_dirs($current . '/');
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Объединение многомерного массива в одномерный
     * @param array $arr - Многомерный массив
     * @return array - Одномерный массив
     */
    public function merge_arr($arr = []) {
        $result = [];
        foreach ($arr as $a) {

            if (is_array($a)) {
                $result = array_merge($result, $this->merge_arr($a));
            } else {
                $result[] = $a;
            }
        }
        return $result;
    }

    /**
     * Создание архива всего сайта
     * @param string $file_sql - Имя sql-файла
     * @param array $allfiles - Список файлов 
     * @return string - Имя созданного архива
     */
    public function create_archive($file_sql, $allfiles = []) {
        $backup_name = "backup_" . date("Y-m-d_h-i-s") . ".zip";
        $zip = new ZipArchive();

        if ($zip->open(App::PATH_BACKUP . $backup_name, ZipArchive::CREATE) === true) {
            if ($file_sql != '') {
                $zip->addFile(App::PATH_BACKUP . $file_sql, $file_sql);
            }

            foreach ($allfiles as $f) {
                $local = substr($f, 2);
                $zip->addFile($f, $local);
            }
        }
        $zip->close();

        if (file_exists(App::PATH_BACKUP . $file_sql)) {
            unlink(App::PATH_BACKUP . $file_sql);
        }

        return $backup_name;
    }

    /**
     * Удаления архива
     * @param string $file - Имя файла
     * @return boolean - Результат уладения
     */
    public function delete_old_archive($file) {

        $ext = array_reverse(explode('.', $file));

        if ($ext[0] != 'zip' and $ext[1] != 'zip') {
            return false;
        }

        $file_del = App::PATH_BACKUP . $file;

        if (!file_exists($file_del)) {
            return false;
        }
        return unlink($file_del);
    }

    /**
     * Отправка архива на почту 
     * @param type $filename - Имя архива
     * @return boolean - Результат отправки
     */
    public function archive_to_mail($filename = '') {
        if ($filename == '') {
            return false;
        }

        $to = App::BACKUP_TO_MAIL;
        $from = App::BACKUP_FROM_MAIL;
        $title = 'Архив БД ' . date("Y-m-d H:i:s");
        $content = 'Архив БД: ' . $filename;
        $boundary = "-----" . strtoupper(md5(uniqid(rand() . time() . rand(), 1)));
        $file = file_get_contents(App::PATH_BACKUP . $filename);

        $headers = "MIME-Version: 1.0;\r\n"
                . "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n"
                . "From: $from \r\n";

        $message = "--$boundary\r\n"
                . "Content-Type: text/html; charset=utf-8\r\n"
                . "Content-Transfer-Encoding: base64\r\n\r\n"
                . chunk_split(base64_encode(iconv("utf8", "utf-8", $content)))
                . "\r\n--$boundary\r\n"
                . "Content-Type: application/octet-stream; name=\"$filename\"\r\n"
                . "Content-Transfer-Encoding: base64\r\n"
                . "Content-Disposition: attachment; filename=\"$filename\"\r\n\r\n"
                . chunk_split(base64_encode($file))
                . "\r\n--$boundary--\r\n";

        return mail($to, $title, $message, $headers);
    }

}

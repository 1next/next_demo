<?php

namespace Admin\Archiving;

use Admin\Base;

class C_archiving extends Base\Controller {

    public function before() {
        parent::before();
        $this->check_access('archiving');
        $this->module = 'archiving';
        $this->main_model = M_archiving::instance();
    }

    public function action_index() {
        $this->add_styles($this->module, ['styles']);
        
        $this->to_base_template['title'] .= 'Архивирование сайта';

        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_index", [
            'arhivs' => $this->main_model->get_arhiv()
        ]);
    }

    public function action_add() {
        $this->to_base_template['title'] .= 'Архивирование сайта';

        $dirs = $this->main_model->get_dirs('./');
        $list_dirs = $this->main_model->merge_arr($dirs);

        $file_sql = $this->main_model->create_backup_db();
        $this->main_model->create_archive($file_sql, $list_dirs);

        $this->redirect($this->root . 'archiving');
    }

    public function action_delete() {
        $this->main_model->delete_old_archive($this->params[2]);
        $this->redirect($this->root . 'archiving');
    }

    public function action_send() {
        $this->main_model->archive_to_mail($this->params[2]);
        $this->redirect($this->root . 'archiving');
    }

}

<div>
    <table class="table table-hover table-bordered">
        <tbody>
            <tr>Список архивов:</tr>
            <? foreach ($arhivs as $a): ?>
                <tr>
                    <td><a href="<?= Admin\Archiving\App::PATH_BACKUP ?><?= $a ?>"><?= $a ?></a></td>
                    <td> 
                        <a href="<?= $admin_root ?>archiving/delete/<?= $a ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?= $admin_root ?>archiving/send/<?= $a ?>" title="Отправить" class="admin-btn admin-btn-archiving-send"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?= $admin_root ?>archiving/add/">Добавить архив &raquo;</a></p>
</div>
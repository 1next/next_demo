<?php

namespace Admin\Archiving;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    const PATH_BACKUP = PATH_SYSTEM . '/backup/';
    const NO_BACKUP_FOLDER = ['backup', 'updoads', '.git', 'nbproject', 'cache', 'logs', 'images', 'ckeditor', 'Twig'];
    const NO_BACKUP_EXT = ['zip'];
    const BACKUP_TO_MAIL = 'admin@mail';
    const BACKUP_FROM_MAIL = 'Модульная <CMS>';

    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'archiving' => [
                    'c' => 'c_archiving'
                ],
            ],
            'using' => [],
            'registry_privs' => [
                'archiving' => 'Архив сайта'
            ],
            'menu' => [
                'archiving' => [
                    'name' => 'Архив сайта',
                    'icon' => 'archive',
                    'priority' => 1,
                    'access' => 'archiving'
                ]
            ]
        ];
    }

    public function rules() {
        return [];
    }

}

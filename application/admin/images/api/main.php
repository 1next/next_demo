<?php

namespace Admin\Images\Api;

use Admin\Images;
use Core\Html;
use Admin\Base\Fieldgen;
use Core\Errors;

class Main extends \Core\Base\Model{

    use \Core\Traits\Singleton;

    private $mImages;
    private $mItems;
    private $table_items;
    
    protected function __construct() {
        parent::__construct(Images\App::instance(), 'index');
        $this->mImages = Images\M_images::instance();
        $this->mItems = Images\M_images_items::instance();
        $this->table_items = $this->db->prefix() . $this->app->prefix() . 'items';
    }
    
    public function add_from_base64($item, $id_item, $name, $base64){        
        $id_image = $this->mImages->upload_from_base64($item, $name, $base64);
        
        if(!$id_image){
            $this->errors = $this->mImages->errors();
            return false;
        }
        
        $max = $this->db->select("SELECT MAX(num_sort) as mx FROM {$this->table_items}
                           WHERE item=:item AND id_item=:id_item",
                           ['item' => $item, 'id_item' => $id_item])[0]['mx'];

        $num = ($max == null) ? 0 : $max + 1;
        
        $this->mItems->add([
            'id_image' => $id_image, 
            'id_item' => $id_item,
            'item' => $item,
            'is_show' => '1',
            'num_sort' => $num
        ]);
        
        return $id_image;
    }
    
    public function get_by_item($item, $id_item){
        return $this->db->select("SELECT * FROM {$this->table_items}
                                  LEFT JOIN {$this->table} using(id_image)
                                  WHERE item=:item AND id_item=:id_item
                                  ORDER BY num_sort ASC",
                                  ['item' => $item, 'id_item' => $id_item]);
    }
    
    public function sort($item, $id_item, $images_id) {
        $where = "item=:item AND id_item=:id_item AND id_image=:id_image";
        $params = ['item' => $item, 'id_item' => $id_item];
        
        for($i = 0; $i < count($images_id); $i++) {
            $params['id_image'] = (int)$images_id[$i];
            $this->db->update($this->table_items, ['num_sort' => $i], $where, $params);
        }

        return true;
    }
    
    public function get_edit_form($id_image){
        $image = $this->get($id_image);
        
        if($image == null){
            return false;
        }
        
        $html = Html::instance();
        $f = new Fieldgen(Images\App::instance()->labels('index'), new Errors());
        $f->set_theme('default');
        
        return  $html->input('hidden')->idn('id_image')->val($image['id_image']) . 
                $f->set($html->input()->val($image['title']), 'title') . 
                $f->set($html->input()->val($image['alt']), 'alt');
    }
}

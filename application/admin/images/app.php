<?php

namespace Admin\Images;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;
    
    const DIR = PATH_UPLOADS . '/';
    
    public function init() {
        $this->on_page = 20;
        
        Events::add('System.CKeditor.Plugins', function(&$plugins){
            $plugins[] = 'cms_image';
        });
    }

    public function manifest() {
        return [
            'routs' => [
                'images' => [
                    'c' => 'c_images'
                ],
                'images-ajax' => [
                    'c' => 'c_images_ajax'
                ]
            ],
            'using' => ['users'],
            'registry_privs' => [
                'images' => 'Работа с изображениями'
            ],
            'menu' => [
  
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'images_',
            'tables' => [
                'index' => [
                    'fields' => ['id_image', 'location', 'name', 'ext', 'title', 'alt', 'is_show'],
                    'not_empty' => ['name', 'ext', 'is_show'],
                    'unique' => ['name'],
                    'range' => [
                        'title' => ['3', '256']
                    ],
                    'labels' => [
                        'location' => 'Расположение',
                        'name' => 'Название',
                        'title' => 'Тайтл',
                        'alt' => 'Альт',
                        'is_show' => 'Отображать на сайте'
                    ],
                    'pk' => 'id_image'
                ],
                'items' => [
                    'fields' => ['id_image', 'id_item', 'item', 'num_sort'],
                    'not_empty' => ['id_image', 'id_item', 'item', 'num_sort'],
                    'labels' => [
                        'id_image' => 'id картинки',
                        'id_item' => 'id сущности для прикрепления',
                        'item' => 'К чему прикрепляем картинку',
                        'num_sort' => 'Сортировка'
                    ],
                    'pk' => ['id_image', 'id_item', 'item']
                ],
                'sizes' => [
                    'fields' => ['id_image', 'slug', 'width', 'height'],
                    'not_empty' => ['id_image', 'slug', 'width', 'height'],
                    'labels' => [
                        'id_image' => 'id картинки',
                        'slug' => 'Тип размера',
                        'width' => 'Высота',
                        'height' => 'Ширинаы'
                    ],
                    'pk' => ['id_image', 'slug']
                ]
            ]
        ];
    }

}

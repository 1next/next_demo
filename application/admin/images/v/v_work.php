<h3>Текущие изображения</h3>
<a href="<?=$admin_root . $back?>">Назад</a>
<div class="work-images">
    <? foreach($images as $image): ?>
    <div class="work-image<? if($image['is_show'] == 0): ?> not-show<? endif;?>" data-id="<?=$image['id_image']?>">
        <img src="/uploads/<?=$image['location'] . $image['name'] . '.' . $image['ext']?>" title="<?=$image['title']?>" alt="<?=$image['alt']?>">
        <div class="work-image-actions">
            <span class="fa fa-fw fa-pencil work-ajax-edit" title="Редактировать мета-данные"></span>
            <? if($image['is_show'] == 0): ?>
                <span class="fa fa-fw fa-eye work-ajax-show" title="Сделать видимой"></span>
            <? else: ?>
                <span class="fa fa-fw fa-eye-slash work-ajax-hide" title="Скрыть"></span>
            <? endif;?>
            <span class="fa fa-fw fa-trash work-ajax-trash" title="Удалить"></span>
        </div>
    </div>
    <? endforeach; ?>
</div>
<button type="button" class="save-sort btn btn-success">Сохранить сортировку</button>
<div class="sort-msg"></div>
<h3>Загрузка изображений</h3>
<div class="images-upload-wrapper">
    <div class="images-drop">
        <p>Перетащите сюда изображения:</p>
        <form class="images-form">
            <input type="file" multiple>
            <input type="hidden" name="id_product" value="<?=$id?>">
            <input type="hidden" name="rel" value="<?=$type?>">
        </form>
    </div>
    <div class="uploaded-holder"> 
        <div class="upload-buttons">
            <span class="for-upload-count"></span>
            <button type="button" class="upload btn btn-success">Загрузить</button>
            <button type="button" class="delete btn btn-danger">Отменить все</button>
        </div> 
        <div class="loading-messages"></div>
        <div class="dropped-files"></div>
    </div>
</div>
<div class="error-group"></div>
<div class="modal-dialog" title="Редактирование изображения"><form></form></div>
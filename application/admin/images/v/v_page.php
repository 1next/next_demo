<div class="images-list">  
    <? foreach ($data as $one): ?>
        <div class="image-item">
            <div class="image">
                <img src="uploads/<?=$one['path']?>">
            </div>
            <div class="actions">
                <a href="<?= $admin_root ?>images/edit/<?= $one['id_image'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                <a href="<?= $admin_root ?>images/delete/<?= $one['id_image'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
            </div>
        </div>
    <? endforeach ?>
    <?= $navbar ?>
</div>
<?php

namespace Admin\Images;

class M_images_sizes extends \Core\Base\Model {

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Get_by_fieldset;
    use \Core\Traits\Model\Delete_by_field;

    protected function __construct() {
        parent::__construct(App::instance(), 'sizes');
    }
}

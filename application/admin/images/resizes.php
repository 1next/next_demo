<?php

namespace Admin\Images;

/**
 * Класс для хранения зарегистрированных ресайсов изображений при загрузке
 */
class Resizes {

    use \Core\Traits\Singleton;

    /**
     * Зарегистрированные размеры изображений 
     * @var array Массив вида [module => [sizes]]
     */
    protected $data;

    protected function __construct() {
       $this->data = [];
    }
    
    /**
     * Зарегистрировать размер
     * @param string $module Модуль
     * @param string $slug Шорткод
     * @param int $w Ширина
     * @param int $h Высота
     * @param boolean $crop Вырезать жёстко по заданным $w и $h
     */
    public function add_size($module, $slug, $w, $h, $crop = false){
        if(!isset($this->data[$module])){
            $this->data[$module] = [];
        }
        
        $this->data[$module][] = [
            'slug' => $slug, 
            'w' => (int)$w, 
            'h' => (int)$h, 
            'crop' => $crop
        ];
    }
    
    /**
     * Получить зарагистрированные размеры для текущего модуля
     * @param string $module Модуль
     * @return array Зарегистрированные размеры
     */
    public function sizes($module){
        return isset($this->data[$module]) ? $this->data[$module] : [];
    }
}

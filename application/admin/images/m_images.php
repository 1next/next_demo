<?php

namespace Admin\Images;

use Core\Events;

class M_images extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected $resizes;

    protected function __construct() {
        parent::__construct(App::instance(), 'index');
        $this->resizes = Resizes::instance();
        Events::run('Module.Images.WantSizes', $this->resizes);
    }

    public function delete($pk) {
        $image = $this->get($pk);

        if ($image == null) {
            return false;
        }

        try {
            $this->db->beginTransaction();
            M_images_items::instance()->delete_by_field('id_image', [$pk]);

            $mSizes = M_images_sizes::instance();

            $files = $mSizes->get_by_fieldset(['id_image' => $pk]);

            $mSizes->delete_by_field('id_image', [$pk]);
            $res = parent::delete($pk);

            $this->db->commit();

            foreach ($files as $file) {
                $to_delete = App::DIR . $image['location'] . $image['name'] . '-' . $file['slug'] . '.' . $image['ext'];

                if (file_exists($to_delete)) {
                    unlink($to_delete);
                }
            }

            $to_delete = App::DIR . $image['location'] . $image['name'] . '.' . $image['ext'];

            if (file_exists($to_delete)) {
                unlink($to_delete);
            }

            return $res;
        } catch (Exception $e) {
            throw new Exceptions\Fatal('Delete transaction by images items was broken and rollback', $e->getCode(), $e);
        }
    }

    public function get_types($using_me) {
        $tag_types = [];

        foreach ($using_me as $params) {
            if (isset($params['image_type'])) {
                if (!isset($params['image_name'])) {
                    $params['image_name'] = $params['image_type'];
                }

                $tag_types[$params['image_type']] = $params['image_name'];
            }
        }

        return $tag_types;
    }

    public function upload_from_base64($item, $name, $value) {
        if (!$this->check_type($name)) {
            $this->errors->add('type', 'Неопознанный формат изображения');
            return false;
        }

        $rand = md5(time());
        $f1 = $rand[0];
        $f2 = $rand[1];

        if (!is_dir(App::DIR . $f1)) {
            mkdir(App::DIR . $f1);
        }

        $location = $f1 . '/' . $f2;

        if (!is_dir(App::DIR . $location)) {
            mkdir(App::DIR . $location);
        }

        $location .= '/';

        $getMime = explode('.', $name);
        $mime = strtolower(end($getMime));
        $name = mt_rand(0, 10000000);
        $filename = $name . '.' . $mime;

        while (file_exists(App::DIR . $location . $filename)) {
            $name = mt_rand(0, 10000000);
            $filename = $name . '.' . $mime;
        }

        $id = $this->add(['location' => $location, 'name' => $name, 'ext' => $mime]);
        $this->move_upload_base64($id, $item, $value, $location . $filename);
        return $id;
    }

    private function check_type($name) {
        $getMime = explode('.', $name);
        $mime = strtolower(end($getMime));
        $types = array('jpg', 'png', 'gif', 'jpeg');
        return in_array($mime, $types);
    }

    private function move_upload_base64($id_image, $item, $file, $path) {
        $data = explode(',', $file);
        $encodedData = str_replace(' ', '+', $data[1]);
        $decodedData = base64_decode($encodedData);

        $mSizes = M_images_sizes::instance();

        if (file_put_contents(App::DIR . $path, $decodedData)) {
            foreach ($this->resizes->sizes($item) as $resize) {
                $full_slug = $item . '-' . $resize['slug'];
                $resize_path = $this->resize(App::DIR . $path, $full_slug, $resize['w'], $resize['h'], $resize['crop'], $item);

                if ($resize_path !== false) {
                    $info = getimagesize($resize_path);
                    $mSizes->add([
                        'id_image' => $id_image,
                        'slug' => $full_slug,
                        'width' => $info[0],
                        'height' => $info[1]
                    ]);
                }
            }

            return true;
        }

        return false;
    }

    private function resize($src, $slug, $w, $h, $crop, $item) {
        $w1 = $w;
        $h1 = $h;

        $info = getimagesize($src);
        $wr = $info[0] / $w;
        $hr = $info[1] / $h;

        if ($wr > $hr) {
            $h = min($h, $info[1]);
            $w = ($h * $info[0]) / $info[1];

            if (!$crop && $w > $w1) {
                $w = $w1;
                $h = ($w * $info[1]) / $info[0];
            }

            $cp = 'h';
        } else {
            $w = min($w, $info[0]);
            $h = ($w * $info[1]) / $info[0];

            if (!$crop && $h > $h1) {
                $h = $h1;
                $w = ($h * $info[0]) / $info[1];
            }

            $cp = 'w';
        }

		// Пропорции
		if($item == 'cat'){
			$h = $w / 1.625;
		}
        $im = imagecreatefromstring(file_get_contents($src));
        //$im = imagecreatefrompng($src);
        $dst = imagecreatetruecolor($w, $h);

        imagealphablending($dst, false);
        imagesavealpha($dst, true);
        $col = imagecolorallocatealpha($dst, 0, 0, 0, 127);
        imagefilledrectangle($dst, 0, 0, $w, $h, $col);

        imagecopyresampled($dst, $im, 0, 0, 0, 0, $w, $h, $info[0], $info[1]);

        unset($im);
        $im = $dst;
        unset($dst);

        if ($crop) {
            $start_x = 0;
            $start_y = 0;
            $stop_x = $w;
            $stop_y = $h;

            if ($cp == 'w') {
                $start_y = abs($h1 - $h) / 2;
                $stop_y = $h - 2 * $start_y;
                $h = $h1;
                echo 1;
            } else {
                $start_x = abs($w1 - $w) / 2;
                $stop_x = $w - 2 * $start_x;
                $w = $w1;
                echo 2;
            }

            $dst = imagecreatetruecolor($w, $h);

            imagealphablending($dst, false);
            imagesavealpha($dst, true);
            $col = imagecolorallocatealpha($dst, 0, 0, 0, 127);
            imagefilledrectangle($dst, 0, 0, $w, $h, $col);

            imagecopyresampled($dst, $im, 0, 0, $start_x, $start_y, $w, $h, $stop_x, $stop_y);

            unset($im);
            $im = $dst;
            unset($dst);
        }

        $width = imagesx($im);
        $height = imagesy($im);

        $tmp = explode('.', $src);
        $ext = $tmp[count($tmp) - 1];
        unset($tmp[count($tmp) - 1]);
        $basename = implode('.', $tmp) . '-' . $slug;

        $fname = 'image' . $ext;

        if (!is_callable($fname)) {
            $ext = 'jpg';
            $fname = 'imagejpeg';
        }

        $path = $this->unique_name($basename, $ext);

        if (!$fname($im, $path)) {
            unset($im);
            return false;
        }

        unset($im);
        return $path;
    }

    private function unique_name($basename, $ext) {
        $fullname = $basename . '.' . $ext;
        $x = 1;

        while (file_exists($fullname)) {
            $fullname = $basename . '(' . $x++ . ').' . $ext;
        }

        return $fullname;
    }

}

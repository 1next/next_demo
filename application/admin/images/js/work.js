$(function () {

    var pr_im = $('.work-images');

    var images = new CMS.imagedrop('images-ajax/upload', {
        data_to_php: {
            id_item: $('.images-form input[name=id_product]').val(),
            rel: $('.images-form input[name=rel]').val()
        },
        on_success_upload: function (data) {
            $('<div/>').addClass('work-image').attr('data-id', data.id_image)
                    .append(
                            $('<img/>').attr('src', '/uploads/' + data.location + data.name + '.' + data.ext)
                            )
                    .append('<div class="work-image-actions">' +
                            '<span class="fa fa-fw fa-pencil work-ajax-edit" title="Редактировать мета-данные"></span>' +
                            '<span class="fa fa-fw fa-eye-slash work-ajax-hide" title="Скрыть"></span>' +
                            '<span class="fa fa-fw fa-trash work-ajax-trash" title="Удалить"></span>' +
                            '</div>'
                            )
                    .appendTo(pr_im);
        }
    });

    var sortable = new CMS.sortable('.work-images');
    var all_saved = true;
    var btn_save = $('.save-sort');
    var sort_msg = $('.sort-msg');
    var send = {
        id_item: $('input[name=id_product]').val(),
        rel: $('input[name=rel]').val(),
        items: []
    };

    sortable.elem.on('sortupdate', function (event, ui) {
        all_saved = false;
        sort_msg.hide();
        btn_save.fadeIn(300);
    });

    btn_save.click(function () {
        btn_save.prop('disabled', true);

        send.items = sortable.getItems();

        $.post(_PHP.admin_root + 'images-ajax/sortimages', send, function (data) {
            btn_save.hide().prop('disabled', false);

            if (data.res) {
                sort_msg.hide().html('Сортировка успешно сохранена').fadeIn(300);
            } else {
                sort_msg.hide().html('Непонятная ошибка на сервере').fadeIn(300);
            }
        }, 'json');
    });

    pr_im.on('click', '.work-image-actions .fa-trash', function () {
        var btn = $(this);

        if (btn.hasClass('clicked') || !confirm("Удаляем?")) {
            return false;
        }

        btn.addClass('clicked');
        var div = $(this).parents('.work-image').eq(0);
        var id = div.attr('data-id');

        $.post(_PHP.admin_root + 'images-ajax/delete', {id: id}, function (data) {
            if (data.res) {
                div.slideUp(300, function () {
                    $(this).remove();
                });
            } else {
                btn.removeClass('clicked');
                alert('Непонятная ошибка на сервере');
            }
        }, 'json');
    });

    pr_im.on('click', '.work-ajax-trash', function () {
        var btn = $(this);

        if (btn.hasClass('clicked') || !confirm("Удаляем?")) {
            return false;
        }

        btn.addClass('clicked');
        var div = $(this).parents('.work-image').eq(0);
        var id = div.attr('data-id');

        $.post(_PHP.admin_root + 'images-ajax/delete', {id: id}, function (data) {
            if (data.res) {
                div.slideUp(300, function () {
                    $(this).remove();
                });
            } else {
                btn.removeClass('clicked');
                alert('Непонятная ошибка на сервере');
            }
        }, 'json');
    });
    
    pr_im.on('click', '.work-ajax-hide', function () {
        var btn = $(this);

        if (btn.hasClass('clicked')) {
            return false;
        }

        btn.addClass('clicked');
        var div = $(this).parents('.work-image').eq(0);
        var id = div.attr('data-id');

        $.post(_PHP.admin_root + 'images-ajax/hide', {id: id}, function (data) {
            if (data.res) {
                btn.removeClass('work-ajax-hide').addClass('work-ajax-show')
                   .removeClass('fa-eye-slash').addClass('fa-eye')
                   .attr('title', 'Сделать видимой');
                div.addClass('not-show');
            } else {
                alert('Непонятная ошибка на сервере');
            }
            
            btn.removeClass('clicked');
        }, 'json');
    });
    
    pr_im.on('click', '.work-ajax-show', function () {
        var btn = $(this);

        if (btn.hasClass('clicked')) {
            return false;
        }

        btn.addClass('clicked');
        var div = $(this).parents('.work-image').eq(0);
        var id = div.attr('data-id');

        $.post(_PHP.admin_root + 'images-ajax/show', {id: id}, function (data) {
            if (data.res) {
                btn.removeClass('work-ajax-show').addClass('work-ajax-hide')
                   .removeClass('fa-eye').addClass('fa-eye-slash')
                   .attr('title', 'Скрыть');
                div.removeClass('not-show');
            } else {
                alert('Непонятная ошибка на сервере');
            }
            
            btn.removeClass('clicked');
        }, 'json');
    });
    
    var $modal = $('.modal-dialog');

    $modal.dialog({
        modal: true,
        draggable: false,
        autoOpen: false,
        minWidth: 480,
        maxWidth: 700,
        show: 'blind',
        hide: 'blind',
        buttons: {
            'Сохранить': function () {
                $.post(_PHP.admin_root + 'images-ajax/edit', $modal.find('form').serialize(), function (data) {
                    if(data.res){
                        $modal.dialog('close');
                    }
                    else{
                        $.each(data.errors, function(key, elem){
                            $modal.find('input[name="' + key + '"]').parent().next().html(elem);
                        });
                    }
                }, 'json');
            },
            'Отмена': function () {
                $modal.dialog('close');
            }
        }
    });
    
    pr_im.on('click', '.work-ajax-edit', function () {
        var btn = $(this);

        if (btn.hasClass('clicked')) {
            return false;
        }

        btn.addClass('clicked');
        
        var div = $(this).parents('.work-image').eq(0);
        var id = div.attr('data-id');

        $.get(_PHP.admin_root + 'images-ajax/form', {id: id}, function (data) {
            $modal.find('form').html(data.form)
            $modal.dialog('open');
            form_loading = false;
        }, 'json');
        
        btn.removeClass('clicked');
    });
});
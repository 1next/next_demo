<?php

namespace Admin\Images;

use Core\Traits\Actions;
use Core\Exceptions;
use Core\Arr;
use Admin\Base\Fieldgen;
use Core\Info;
use Core\Session;

class C_images extends \Admin\Base\Controller {

    use Actions\Page;
    use Actions\Edit;
    use Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('images');
        $this->module = 'images';
        $this->main_model = M_images::instance();
    }

    public function action_work(){
        $this->to_base_template['title'] = 'Работа с изображениями';
        $types = $this->main_model->get_types(Info::using_me($this->module));
        $type = $this->params[2];
        
        if(!isset($types[$type])){
            throw new Exceptions\E404("Image type $type is not registred");
        }
        
        if(!isset($this->params[3]) || !ctype_digit($this->params[3])){
            throw new Exceptions\E404("Bad id_item value = {$this->params[3]}");
        }
        
        $id = (int)$this->params[3];
        
        $images = Api\Main::instance()->get_by_item($type, $id);
        
        $this->add_scripts($this->module, ['work']);
        $this->add_styles($this->module, ['styles']);
        
        $this->to_base_template['content'] = $this->template($this->module . '/v/v_work', [
            'type' => $type,
            'id' => $id,
            'back' => Session::has('current_redirect') ? Session::read('current_redirect') : 'images', 
            'images' => $images
        ]);
    }
    
    protected function settings_action_page() {
        $this->to_base_template['title'] .= 'Изображения: страница ' . $this->page_num;
        
        $this->add_styles($this->module, ['styles']);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование товара';
        $this->add_scripts($this->module, ['init']);
        $this->edit_extract = ['id_cat', 'title', 'tags_id'];
        $this->edit_template = "{$this->module}/v/v_add";
        
        $tree = Cats::instance()->tree(App::CAT_TYPE);
        $this->edit_other_data['cats'] = Arr::to_select_tree($tree, 'id_cat', 'name');
        
        $this->edit_other_data['tags'] = Tags::instance()->all_by_type(App::TAG_TYPE);
        $tags_id = Tags::instance()->get_tags_id(App::TAG_RELATION, $this->edit_id);
        $this->edit_fields_data['tags_id'] = implode(',', $tags_id);
        
        $this->edit_fieldgen = new Fieldgen($this->app->labels('prototypes'), $this->errors);
    }

    protected function settings_action_delete() {
        
    }

}

<?php

namespace Admin\Images;

use Core\Input;
use Core\Arr;

class C_images_ajax extends \Admin\Base\Ajax {

    public function before() {
        parent::before();
        $this->check_access('images');
    }
    
    public function action_upload() {    
        $mImages = Api\Main::instance();
        $id = $mImages->add_from_base64(Input::post('rel'), Input::post('id_item'), Input::post('name'), Input::post('value'));
        
        if($id){            
            $this->response['res'] = true;
            $this->response['image'] = $mImages->get($id);
        }
        else{
            $this->response['res'] = false;
            $this->response['errors'] = $mImages->errors()->unshown();
        }
    }
    
    public function action_sortimages() {    
        $mImages = Api\Main::instance();
        $id = $mImages->sort(Input::post('rel'), Input::post('id_item'), $_POST['items']);
        
        if($id){            
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
        }
    }
    
    public function action_delete() {    
        $mImages = M_images::instance();
        $id = $mImages->delete(Input::post('id'));
        
        if($id){            
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
        }
    }
    
    public function action_show() {    
        $mImages = M_images::instance();
        $res = $mImages->edit(Input::post('id'), ['is_show' => '1']);
        $this->response['res'] = $res;
    }
    
    public function action_hide() {    
        $mImages = M_images::instance();
        $res = $mImages->edit(Input::post('id'), ['is_show' => '0']);
        $this->response['res'] = $res;
    }
    
    public function action_form() {    
        $mImages = Api\Main::instance();
        $res = $mImages->get_edit_form(Input::get('id'));
        
        if($res !== false){
            $this->response['res'] = true;
            $this->response['form'] = $res;
        }
        else{
            $this->response['res'] = false;
            $this->response['errorCode'] = 404;
        }
    }
    
    public function action_edit() {    
        $mImages = M_images::instance();       
        $fields = Arr::extract($_POST, ['title', 'alt']);       
        $res = $mImages->edit(Input::post('id_image'), $fields);
        
        if($res !== false){
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
            $this->response['errors'] = $mImages->errors();
        }
    }
    
    protected function set_error_template(\Core\Errors $errors) {
        
    }

}

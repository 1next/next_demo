<?php

namespace Admin\Tags;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'tags' => [
                    'c' => 'c_tags',
                ]
            ],
            'using' => ['users'],
            'registry_privs' => [
                'tags' => 'Работа с тегами'
            ],
            'menu' => [
                'tags' => [
                    'name' => 'Теги',
                    'icon' => 'tags',
                    'open' => [
                        'tags/select' => [
                            'name' => 'Выбор типа',
                            'icon' => 'map-signs'
                        ],
                        'tags/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'tags/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ]
                    ],
                    'priority' => 1,
                    'access' => 'tags'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'tags_',
            'tables' => [
                'tags' => [
                    'fields' => ['id_tag', 'url', 'tag_type', 'name', 'description'],
                    'not_empty' => ['url', 'tag_type', 'name'],
                    'unique' => [],
                    'composite_unique' => [['tag_type', 'url'], ['tag_type', 'name']],
                    'range' => [
                        'url' => ['2', '64'],
                        'name' => ['2', '64'],
                        'description' => ['0', '256']
                    ],
                    'labels' => [
                        'url' => 'URL-адрес',
                        'tag_type' => 'Тип тега',
                        'name' => 'Название',
                        'description' => 'Описание'
                    ],
                    'pk' => 'id_tag'
                ],
                'items' => [
                    'fields' => ['id_tag', 'id_item', 'item'],
                    'not_empty' => ['id_tag', 'id_item', 'item'],
                    'unique' => [],
                    'range' => [
                        'items' => ['0', '64']
                    ],
                    'labels' => [
                        'url' => 'URL-адрес',
                        'tag_type' => 'Тип тега',
                        'name' => 'Название',
                        'description' => 'Описание'
                    ],
                    'pk' => ['id_tag', 'id_item', 'item']
                ]
            ]
        ];
    }

}

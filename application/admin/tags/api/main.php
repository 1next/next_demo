<?php

namespace Admin\Tags\Api;

use Admin\Tags as Tags;
use Core\Sql;
use Core\Arr;
use Exception;
use Core\Exceptions;

class Main {

    use \Core\Traits\Singleton;

    protected $mTags;
    protected $mTagsItems;
    protected $db;

    protected function __construct() {
        $this->mTags = Tags\M_tags::instance();
        $this->mTagsItems = Tags\M_tags_items::instance();
        $this->db = Sql::instance();
    }

    /**
     * Получить все теги определённого типа
     * @param string $type Тип тега
     * @return array Массив тегов
     */
    public function all_by_type($type) {
        return $this->mTags->get_by_fieldset(['tag_type' => $type]);
    }

    /**
     * Добавить связь между сущностью и тегами
     * @param int $id_item id сущности
     * @param string $item Название сущности
     * @param array $tags_id Массив с id тегов
     */
    public function add_relations($id_item, $item, $tags_id) {
        foreach ($tags_id as $id_tag) {
            $this->mTagsItems->add(['id_tag' => $id_tag, 'item' => $item, 'id_item' => $id_item]);
        }
    }

    /**
     * Удалить связь между сущностью и тегами
     * @param int $id_item id сущности
     * @param string $item Название сущности
     */
    public function delete_relations($id_item, $item) {
        $this->mTagsItems->delete_by_fieldset([['item' => $item, 'id_item' => $id_item]]);
    }

    /**
     * Редактировать связь между сущностью и тегами
     * @param int $id_item id сущности
     * @param string $item Название сущности
     * @param array $tags_id Массив с id тегов
     */
    public function edit_relations($id_item, $item, $tags_id) {
        try{
            $this->db->beginTransaction();
            $this->delete_relations($id_item, $item);
            $this->add_relations($id_item, $item, $tags_id);
            $this->db->commit();
        } catch (Exception $e) {
            throw new Exceptions\Fatal('Edit transaction by tags items was broken and rollback', $e->getCode(), $e);
        }      
    }

    /**
     * Получить id тегов по id сущности
     * @param string $item Сущность
     * @param int $id_item Id сущности
     * @return array [id_tag1, id_tag2 ... id_tagN]
     */
    public function get_tags_id($item, $id_item) {
        $res = $this->mTagsItems->get_by_fieldset([['item' => $item, 'id_item' => $id_item]]);
        return Arr::section($res, 'id_tag');
    }

    /**
     * Получить id сущностей для конкретного тега
     * @param string $item Сущность
     * @param int $id_tag Id тега
     * @return array [id_item1, id_item2 ... id_itemN]
     */
    public function get_items_id($item, $id_tag) {
        $res = $this->mTagsItems->get_by_fieldset([['item' => $item, 'id_tag' => $id_tag]]);
        return Arr::section($res, 'id_item');
    }
    
    /**
     * Получить теги:
     *      если передан id - для одной сущности
     *      если передан массив id - для нескольких сущностей
     * @param string $item Сущнсоть
     * @param mixed $data id сущности или массив [id_item1, id_item2 ... id_itemN]
     * @return array Массив с тегами
     */
    public function get_tags($item, $data) {
        if (is_array($data)) {
            return $this->get_tags_by_items_id($item, $data);
        } else {
            return $this->get_tags_by_id_item($item, $data);
        }
    }

    /**
     * Получить id сущностей для набора тегов
     * @param string $item Сущность
     * @param array [id_tag1, id_tag2 ... id_tagN]
     * @return array [id_item1, id_item2 ... id_itemN]
     */
    public function get_items_group_id($item, $tags_id) {
        if (count($tags_id) == 0) {
            return [];
        }

        $prefix = $this->mTags->prefix() . Tags\App::instance()->prefix();
        $items_id = [];
        $in = '';

        for ($i = 0; $i < count($tags_id); $i++) {
            $items_id[$tags_id[$i]] = [];
            $in .= ($i == 0) ? (int) $tags_id[$i] : ',' . (int) $tags_id[$i];
        }

        $res = $this->db->select("SELECT * FROM {$prefix}items
                                  WHERE item=:item AND id_tag IN ($in)", ['item' => $item]
        );

        foreach ($res as $one) {
            $items_id[$one['id_tag']][] = $one['id_item'];
        }

        return $items_id;
    }
    
    private function get_tags_by_id_item($item, $id_item) {
        $prefix = $this->mTags->prefix() . Tags\App::instance()->prefix();

        return $this->db->select("SELECT * FROM {$prefix}items
                                  JOIN {$prefix}tags using(id_tag)
                                  WHERE item=:item AND id_item=:id_item", ['item' => $item, 'id_item' => $id_item]
        );
    }

    private function get_tags_by_items_id($item, $items_id) {
        if (count($items_id) == 0) {
            return [];
        }

        $prefix = $this->mTags->prefix() . Tags\App::instance()->prefix();
        $tags = [];
        $in = '';

        for ($i = 0; $i < count($items_id); $i++) {
            $tags[$items_id[$i]] = [];
            $in .= ($i == 0) ? (int) $items_id[$i] : ',' . (int) $items_id[$i];
        }

        $res = $this->db->select("SELECT * FROM {$prefix}items
                                  JOIN {$prefix}tags using(id_tag)
                                  WHERE item=:item AND id_item IN ($in)", ['item' => $item]
        );

        foreach ($res as $one) {
            $tags[$one['id_item']][] = $one;
        }

        return $tags;
    }

}

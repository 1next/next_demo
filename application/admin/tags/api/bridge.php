<?php

namespace Admin\Tags\Api;

use Admin\Tags as Tags;
use Core\Html;
use Core\Errors;
use Admin\Base\Fieldgen;
use Core\Arr;

class Bridge {

    use \Core\Traits\Singleton;

    protected $mTags;

    protected function __construct() {
        $this->mTags = Tags\M_tags::instance();
    }

    public function get_ajax_form($tag_type){
        $html = Html::instance();
        $f = new Fieldgen(Tags\App::instance()->labels('tags'), new Errors());
        $f->set_theme('default');
        
        return  $html->input('hidden')->idn('tag_type')->val($tag_type) . 
                $f->set($html->input(), 'url') . 
                $f->set($html->input(), 'name') . 
                $f->set($html->textarea(), 'description');
    }
    
    public function get($pk){
        return $this->mTags->get($pk);
    }
    
    public function add($fields){        
        $fields = Arr::extract($_POST, ['tag_type', 'url', 'name', 'description']);       
        return $this->mTags->add($fields);
    }
    
    public function errors(){
        return $this->mTags->errors()->unshown();
    }
}

<form method="post">
    <?= $f->set($html->select($data['tag_types'], $fields['tag_type']), 'tag_type') ?>
    <?= $f->set($html->input()->val($fields['url']), 'url') ?>
    <?= $f->set($html->input()->val($fields['name']), 'name') ?>
    <?= $f->set($html->textarea()->val($fields['description']), 'description') ?>
    <? foreach ($errors->unshown() as $err): ?>
        <p class="error"><?= $err ?></p>
    <? endforeach; ?>
    <div class="submit-buttons">
        <input type="submit" value="Сохранить" class="btn btn-success">
        <a class="btn btn-danger" href="<?= $admin_root . $back ?>">Закрыть без сохранения</a>
    </div>
</form>
<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Название</th>
                <th>Описание</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $tag): ?>
                <tr>
                    <td><?= $tag['name'] ?></td>
                    <td><?= $tag['description'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>tags/edit/<?= $tag['id_tag'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>tags/delete/<?= $tag['id_tag'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>tags/add/<?= $tag['id_tag'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>tags/add?default_type=<?=$other['tag_type']?>">Добавить новый тег &raquo;</a></p>
</div>
<?php

namespace Admin\Tags;
use Config;

class M_tags extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'tags');
    }

    public function delete($pk){
        try{
            $this->db->beginTransaction();
            M_tags_items::instance()->delete_by_fieldset(['id_tag' => $pk]);
            parent::delete($pk);
            $this->db->commit();
        } 
        catch (Exception $e) {
            throw new Exceptions\Fatal('Delete transaction by tags items was broken and rollback', $e->getCode(), $e);
        }
    }
    
    public function get_types($using_me){
        $tag_types = [];
        
        foreach($using_me as $params){
            if(isset($params['tag_type'])){
                if (!isset($params['tag_name'])) {
                    $params['tag_name'] = $params['tag_type'];
                }

                $tag_types[$params['tag_type']] = $params['tag_name'];
            }
        } 
            
        return $tag_types;
    }
}

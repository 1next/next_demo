<?php

namespace Admin\Tags;

use Core\Traits\Actions;
use Core\Info;
use Core\Exceptions;
use Admin\Base\Fieldgen;

class C_tags extends \Admin\Base\Controller {

    use Actions\Page;
    use Actions\Add;
    use Actions\Edit;
    use Actions\Delete;

    public function before() {
        parent::before();

        $this->check_access('tags');

        $this->module = 'tags';
        $this->main_model = M_tags::instance();
    }

    public function action_select() {
        $tag_types = $this->main_model->get_types(Info::using_me($this->module));
        
        if(count($tag_types) == 0){
            throw new \Core\Exceptions\E404("Nobody use tags module");
        }
        
        $this->to_base_template['title'] = 'Выберите тип тега для работы';
        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_select", 
                                                                ['tag_types' => $tag_types]);
    }
    
    protected function page_num(){
        return isset($this->params[3]) ? (int) $this->params[3] : 1;
    }
    
    protected function settings_action_page() {
        if (!isset($this->params[2])) {
            $this->redirect($this->root . $this->module . '/select');
        }

        $tag_type = trim($this->params[2]);
        $tag_types = $this->main_model->get_types(Info::using_me($this->module));
        
        if(!isset($tag_types[$tag_type])){
            throw new \Core\Exceptions\E404("Tag type $tag_type is not registred");
        }
       
        $this->page_other_data['tag_type'] = $tag_type;
        $this->to_base_template['title'] .= "Список тегов: \"{$tag_types[$tag_type]}\", страница " . $this->page_num;
        $this->main_model->pager()->url_self('/' . $this->module . '/page/' . $tag_type . '/%s')
                                ->where("tag_type='$tag_type'");
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление тега';

        if (isset($this->params[2]) && is_numeric($this->params[2])) {
            $this->add_fields_data = $this->main_model->get($this->params[2]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[2]} not found.");
            }
        } else {
            $this->add_fields_data = [];

            if (isset($this->get['default_type'])) {
                $this->add_fields_data['tag_type'] = $this->get['default_type'];
            }
        }

        $this->add_redirect = isset($this->get['default_type']) ? 
                              $this->module . "/page/" . $this->get['default_type'] :
                              $this->module;
        
        $this->add_extract = ['tag_type', 'url', 'name', 'description'];
        $this->add_other_data['tag_types'] = $this->main_model->get_types(Info::using_me($this->module));
        $this->add_fieldgen = new Fieldgen($this->app->labels('tags'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование тега';
        $this->edit_extract = ['tag_type', 'url', 'name', 'description'];
        $this->edit_other_data['tag_types'] = $this->main_model->get_types(Info::using_me($this->module));
        $this->edit_template = "{$this->module}/v/v_add";
        $this->edit_fieldgen = new Fieldgen($this->app->labels('tags'), $this->errors);
    }

    protected function settings_action_delete() {
        
    }

}

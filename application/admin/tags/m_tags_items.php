<?php

namespace Admin\Tags;

class M_tags_items extends \Core\Base\Model {

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Delete_by_fieldset;

    protected function __construct() {
        parent::__construct(App::instance(), 'items');
    }
}

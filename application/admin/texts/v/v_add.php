<form method="post">
    <?= $f->set($html->input()->val($fields['alias']), 'alias') ?>
    <?= $f->set($html->input()->val($fields['title']), 'title') ?>
    <?= $f->set($html->textarea()->val($fields['content']), 'content') ?>
    <?= $f->set($html->input('checkbox')->battr('checked', (isset($fields['is_show']) && $fields['is_show'] != 0)), 'is_show') ?>
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
</form>
<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Алиас</th>
                <th>Название</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $text): ?>
                <tr>
                    <td><?= $text['alias'] ?></td>
                    <td><?= $text['title'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>texts/edit/<?= $text['id_text'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>texts/delete/<?= $text['id_text'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>texts/add/<?= $text['id_text'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>texts/add/">Добавить новый текст &raquo;</a></p>
</div>
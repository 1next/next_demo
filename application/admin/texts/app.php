<?php

namespace Admin\Texts;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'texts' => [
                    'c' => 'c_texts'
                ]
            ],
            'using' => ['users'],
            'registry_privs' => [
                'texts' => 'Работа с текстами'
            ],
            'menu' => [
                'texts' => [
                    'name' => 'Тексты',
                    'icon' => 'pencil-square-o',
                    'open' => [
                        'texts/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'texts/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ]
                    ],
                    'priority' => 1,
                    'access' => 'texts'
                ]
            ]
        ];
    }

    public function rules() {

        return [
            'tables_prefix' => 'texts_',
            'tables' => [
                'texts' => [
                    'fields' => ['id_text', 'alias', 'title', 'content', 'is_show'],
                    'not_empty' => ['alias', 'title', 'content'],
                    'html_allowed' => ['content'],
                    'unique' => ['alias'],
                    'range' => [
                        'alias' => ['3', '64'],
                        'title' => ['3', '256']
                    ],
                    'labels' => [
                        'alias' => 'Название для шаблона',
                        'title' => 'Расшифровка названия',
                        'content' => 'Текст',
                        'is_show' => 'Отображать на сайте',
                    ],
                    'pk' => 'id_text'
                ]
            ]
        ];
    }

}

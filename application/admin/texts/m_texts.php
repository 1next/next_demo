<?php

namespace Admin\Texts;

class M_texts extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'texts');
    }

}

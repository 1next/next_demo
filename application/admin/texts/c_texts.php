<?php

namespace Admin\Texts;

use Core\Traits\Actions;
use Core\Exceptions;
use Admin\Base\Fieldgen;

class C_texts extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('texts');
        $this->module = 'texts';
        $this->main_model = M_texts::instance();
    }

    protected function settings_action_page() {
        $this->to_base_template['title'] .= 'Список текстов: страница ' . $this->page_num;
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление текста';

        if (isset($this->params[2]) && is_numeric($this->params[2])) {
            $this->add_fields_data = $this->main_model->get($this->params[2]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[2]} not found.");
            }
        } else {
            $this->add_fields_data = ['is_show' => 1];
        }

        $this->add_extract = ['alias', 'title', 'content', 'is_show'];
        $this->add_checkboxes_detect = ['is_show'];
        $this->add_fieldgen = new Fieldgen($this->app->labels('texts'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование текста';
        $this->edit_extract = ['alias', 'title', 'content', 'is_show'];
        $this->edit_checkboxes_detect = ['is_show'];
        $this->edit_template = "{$this->module}/v/v_add";
        $this->edit_fieldgen = new Fieldgen($this->app->labels('texts'), $this->errors);
    }

    protected function settings_action_delete() {
        
    }

}

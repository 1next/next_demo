<canvas id="modules" width="<?=$size?>" height="<?=$size?>"></canvas>

<script>
    
    /**
     * Функция для более удобного перебора объекта
     * @param {object} data
     * @param {callback} callback
     * @returns {undefined}
     */
    function forEach(data, callback){
        for(var key in data){
            if(data.hasOwnProperty(key)){
                callback(key, data[key]);
            }
        }
    }
    
    
    /**
     * Функция для рисования стрелки в конце линии
     * @param {integer} p1x Координата X начальной точки
     * @param {integer} p1y Координата Y начальнйо точки
     * @param {integer} p2x Координата X конечной точки
     * @param {integer} p2y Координата Y конечной точки
     * @param {string} color Цвет линий
     * @param {object} canv Объект (CanvasRenderingContext2D), в котором надо рисовать стрелку
     * @returns {undefined}
     */
    function drawLineWithArrow(p1x, p1y, p2x, p2y, color, canv) {
        
        var
            x1, y1, x2, y2,
            kat1 = p2x - p1x,
            kat2 = p2y - p1y,
            arrowAngle = 0.13,
            angle;
        
        canv.lineWidth = 3;
        canv.strokeStyle = color;
        
        canv.beginPath();
        canv.moveTo(p1x, p1y);
        canv.lineTo(p2x, p2y);
        canv.stroke();
        
        if (kat1 === 0) {
            if (p2y > p1y) {
                angle = Math.PI;
            }
            else {
                angle = Math.PI * 2;
            }
        }
        else if (kat2 === 0) {
            if (p2x > p1x) {
                angle = Math.PI / 2 * 3;
            }
            else {
                angle = Math.PI / 2;
            }
        }
        else {
            if (p2y > p1y) {
                angle = Math.atan(kat1 / kat2) - Math.PI;
            }
            else {
                angle = Math.atan(kat1 / kat2);
            }
            
        }
        
        x1 = p2x + Math.round( 35 * Math.sin(angle + arrowAngle) );
        y1 = p2y + Math.round( 35 * Math.cos(angle + arrowAngle) );
        
        x2 = p2x + Math.round( 35 * Math.sin(angle - arrowAngle) );
        y2 = p2y + Math.round( 35 * Math.cos(angle - arrowAngle) );
        
        canv.beginPath();
        canv.moveTo(p2x, p2y);
        canv.lineTo(x1,y1);
        canv.stroke();
        
        canv.beginPath();
        canv.moveTo(p2x, p2y);
        canv.lineTo(x2,y2);
        canv.stroke();
    }
    
    
    var
        canvas = document.getElementById('modules'),
        ctx = canvas.getContext('2d'),
        w = _PHP.size,
        h = _PHP.size,
        radius = _PHP.radius,
        using = _PHP.using;
    
    ctx.fillStyle = '#FFF';
    ctx.fillRect(0,0,w,h);
    
    ctx.fillStyle = 'red';
    ctx.strokeStyle = '#000';
    ctx.font = 'bold 20px sans-serif';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    
    
    forEach(using, function(key, item) {
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.strokeStyle = 'black';
        ctx.arc(item.coords.x, item.coords.y, radius, 0, 2*Math.PI, false);
        ctx.stroke();
        ctx.fillText(key, item.coords.x, item.coords.y, radius*2);
        
        if (item.links.length > 0) {
            forEach(item.links, function(k, v) {
                drawLineWithArrow(item.points.x, item.points.y, using[v].points.x, using[v].points.y, using[v].color, ctx);
            });
        }
        
    });
    
</script>
<?php

namespace Admin\Communications;

use Config;

class C_communications extends \Admin\Base\Controller {
    
    // Дефолтные значения
    private $canvax_size = 800;
    private $item_radius = 70;
    private $center;
    private $big_radius;
    private $small_radius;
    private $count;
    
    public function before() {
        parent::before();
        $this->check_access('communications');
        $this->module = 'communications';
    }
    
    public function action_index() {
        $this->redirect($this->module . '/admin');
    }
    
    public function action_admin() {
        $this->to_base_template['title'] .= 'Связи админских модулей';
        $this->draw('admin');
    }
    
    public function action_client() {
        $this->to_base_template['title'] .= 'Связи клиентских модулей';
        $this->draw('client');
    }
    
    private function draw(string $who) {
        
        $manifest = \Core\Info::manifest();
        
        $modules = Config::$modules[$who];
        
        $this->count = count($modules);
        
        // Угол для равномерного распределения модулей по кругу
        $angle = 2 * M_PI / $this->count;
        
        // Расчёт item_radius, чтобы не было наложения
        $this->calc_sizes();
        
        $using = [];
        $i = 0;
        foreach ($modules as $item) {
            $using[$item]['coords'] = [
                'x' => $this->center + round( $this->big_radius * sin(M_PI + $angle * $i) ),
                'y' => $this->center + round( $this->big_radius * cos(M_PI + $angle * $i) ),
            ];
            
            $using[$item]['points'] = [
                'x' => $this->center + round( $this->small_radius * sin(M_PI + $angle * $i) ),
                'y' => $this->center + round( $this->small_radius * cos(M_PI + $angle * $i) ),
            ];
            
            $using[$item]['links'] = isset($manifest['using'][$item]) ? array_keys($manifest['using'][$item]) : [];
            $using[$item]['name'] = $item;
            $using[$item]['color'] = \Core\Helpers::random_html_color();
            
            $i++;
        }
        
        $this->vars_to_js['using'] = $using;
        $this->vars_to_js['size'] = $this->canvax_size;
        $this->vars_to_js['radius'] = $this->item_radius;
        
        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_index",
                [
                    'size' => $this->canvax_size,
                ]);
        
    }
    
    private function calc_sizes() {
        $this->center = round($this->canvax_size / 2);
        $this->big_radius = $this->center - 50 - $this->item_radius;
        
        if ($this->item_radius < 50) {
            $this->item_radius = 50;
        }
        
        if ($this->canvax_size < 600) {
            $this->canvax_size = 600;
        }
        
        while ($this->count * $this->item_radius * 2 * 1.1 > 2 * M_PI * $this->big_radius) {
            if ($this->item_radius > 50) {
                $this->item_radius -= 4;
                
                $this->big_radius = $this->center - 50 - $this->item_radius;
            }
            else {
                $this->canvax_size += 50;
                
                $this->center = round($this->canvax_size / 2);
                $this->big_radius = $this->center - 50 - $this->item_radius;
            }
        }
        
        $this->small_radius = $this->center - 50 - $this->item_radius * 2;
    }
    
}
<?php

namespace Admin\Communications;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {
    
    use Singleton;
    
    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'communications' => [
                    'c' => 'c_communications'
                ]
            ],
            'registry_privs' => [
                'communications' => 'Просмотр связей'
            ],
            'menu' => [
                'communications' => [
                    'name' => 'Связи модулей',
                    'icon' => 'retweet',
                    'open' => [
                        'communications/admin' => [
                            'name' => 'Админские',
                            'icon' => 'apple'
                        ],
                        'communications/client' => [
                            'name' => 'Клиентские',
                            'icon' => 'android'
                        ],
                    ],
                    'priority' => 10,
                    'access' => 'communications'
                ]
                
            ]
        ];
    }

    public function rules() {
        return [];
    }

}

<?php

namespace Admin;


class Init extends \Core\Base\Init {

    public function __construct($params, $get) {
        parent::__construct($params, $get, 'admin');
    }

}

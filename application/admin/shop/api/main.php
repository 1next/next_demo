<?php

namespace Admin\Shop\Api;

use Admin\Shop;
use Core\Arr;

class Main extends \Core\Base\Model{

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Get_in;

    protected function __construct() {
        parent::__construct(Shop\App::instance(), 'products');
    }
    
    public function get_options_by_group($ids) {
        $options = Shop\M_products_options::instance()->get_by_ids($ids);
        $assoc = Arr::grouping($options, 'id_product');
        $res = [];
        
        foreach($assoc as $id_product => $pr2op){
            $res[$id_product] = [];
            
            foreach($pr2op as $option){
                $res[$id_product][$option['title']] = $option;
            }
        }
        
        return $res;
    }
}

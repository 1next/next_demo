<?php

namespace Admin\Shop;

class M_products_options extends \Core\Base\Model {
	

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Delete_by_fieldset;

    protected function __construct() {
        parent::__construct(App::instance(), 'pr2op');
    }

    public function get_by_ids($pk_array){
        if(empty($pk_array)){
            return [];
        }
        
        $prefix = $this->db->prefix() . $this->app->prefix();
        $to = $prefix . 'options';
        $tu = $prefix . 'units';
        
        $clear = [];
        
        foreach($pk_array as $pk){
            $clear[] = (int)$pk;
        }        
        
        $in = implode(',', $clear);
        
        $res = $this->db->select("SELECT * FROM {$this->table}
                                  LEFT JOIN $to using (id_option)
                                  LEFT JOIN $tu using (id_unit)
                                  WHERE id_product IN ($in)
        ");
        
        return $res;
    }
	
	public function get_by_ids_price($prices){
        if(empty($prices)){
            return [];
        }
        
        $prefix = $this->db->prefix() . $this->app->prefix();
        $to = $prefix . 'options';
        $tp = $prefix . 'products';
		 
        $clear = [];
        
        foreach($prices as $p){
            $clear[] = "'$p'";
        }        
        
        $in = implode(',', $clear);
  		
        $res = $this->db->select("SELECT * FROM {$this->table}
									LEFT JOIN $to using (id_option)
									LEFT JOIN $tp using (id_product)
									WHERE url IN ($in)
		");
    
		$ids = [];
		$clear = [];
		
		foreach($res as $p){
           $ids[] = $p['id_prototype'];
		}    
		
        $ids = array_unique($ids);
		        
        foreach($ids as $p){
            $clear[] = "$p";
        }        
        
        $in = implode(',', $clear);
	
		$products = $this->db->select("SELECT * FROM $tp WHERE id_product IN ($in)");
		
		foreach($res as $k =>$p2){
			foreach($products as $prod){
				if($p2['id_prototype'] == $prod['id_product']){
					$res[$k]['title_p'] = $prod['title'];
					$res[$k]['id_prototype'] = $prod['id_product'];
				}
			}         
		}  
        return $res;
    }
}

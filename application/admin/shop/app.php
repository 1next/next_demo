<?php

namespace Admin\Shop;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;
    
    const TAG_TYPE = 'shop';
    const TAG_NAME = 'Для каталога';
    const TAG_RELATION = 'shop';
    const CAT_TYPE = 'shop';
    const CAT_NAME = 'Для каталога';
    const IMAGE_TYPE = 'shop';
    const IMAGE_NAME = 'Для каталога';
    const IMAGE_RELATION = 'shop';
    
    const OPTION_TYPES = ['input' => 'Обычное поле', 'textarea' => 'Многострочный текст', 'ckeditor' => 'HTML-редактор'];
    
    public function init() {
        $this->on_page = 20;
        
        Events::add('Module.Images.WantSizes', function(\Admin\Images\Resizes $resizes){
            $resizes->add_size(self::IMAGE_RELATION, 'mini', 96, 10000);
            $resizes->add_size(self::IMAGE_RELATION, 'middle', 390, 10000);
            $resizes->add_size(self::IMAGE_RELATION, 'origin', 667, 10000);
		});
    }

    public function manifest() {
        return [
            'routs' => [
                'shop' => [
                    'c' => 'c_products',
                    'a' => 'switch'
                ],
                'shop/products' => [
                    'c' => 'c_products',
                    'a' => 'switch'
                ],
                'shop/products-ajax' => [
                    'c' => 'c_products_ajax',
                    'a' => 'switch'
                ],
                'shop/units' => [
                    'c' => 'c_units',
                    'a' => 'switch'
                ],
                'shop/options' => [
                    'c' => 'c_options',
                    'a' => 'switch'
                ],
                'shop_ajax' => [
                    'c' => 'ajax'
                ],
				'shop/price' => [
                    'c' => 'c_products',
                    'a' => 'price'
                ]
            ],
            'using' => [
                'users',
                'cats' => [
                    'cat_type' => self::CAT_TYPE,
                    'cat_name' => self::CAT_NAME
                ],
                'tags' => [
                    'tag_type' => self::TAG_TYPE,
                    'tag_name' => self::TAG_NAME,
                    'tag_item' => self::TAG_RELATION
                ],
                'images' => [
                    'image_type' => self::IMAGE_TYPE,
                    'image_name' => self::IMAGE_NAME,
                    'image_item' => self::IMAGE_RELATION
                ]
            ],
            'registry_privs' => [
                'shop' => 'Работа с каталогом'
            ],
            'menu' => [
                'shop' => [
                    'name' => 'Каталог',
                    'icon' => 'pencil-square-o',
                    'open' => [
                        'shop/products' => [
                            'name' => 'Список товаров',
                            'icon' => 'list-ul'
                        ],
                        'shop/options' => [
                            'name' => 'Опции для товаров',
                            'icon' => 'cog'
                        ],
                        'shop/units' => [
                            'name' => 'Единицы измерения',
                            'icon' => 'medium'
                        ]
                    ],
                    'priority' => 1,
                    'access' => 'shop'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'shop_',
            'tables' => [
                'products' => [
                    'fields' => ['id_product', 'id_prototype', 'id_cat', 'title'],
                    'not_empty' => ['id_cat', 'title'],
                    'html_allowed' => [],
                    'unique' => ['title'],
                    'range' => [
                        'title' => ['3', '256']
                    ],
                    'labels' => [
                        'id_prototype' => 'Прототип товара',
                        'id_cat' => 'Категория',
                        'title' => 'Название'
                    ],
                    'pk' => 'id_product'
                ],
                'units' => [
                    'fields' => ['id_unit', 'name', 'description'],
                    'not_empty' => ['name', 'description'],
                    'unique' => ['name'],
                    'range' => [
                        'name' => ['1', '32'],
                        'description' => ['4', '512']
                    ],
                    'labels' => [
                        'name' => 'Короткое название',
                        'description' => 'Подробное название'
                    ],
                    'pk' => 'id_unit'
                ],
                'options' => [
                    'fields' => ['id_option', 'url', 'title', 'opt_type'],
                    'not_empty' => ['url', 'title', 'opt_type'],
                    'unique' => ['url', 'title'],
                    'range' => [
                        'url' => ['2', '64'],
                        'title' => ['2', '256'],
                        'opt_type' => ['2', '32'],
                    ],
                    'labels' => [
                        'url' => 'URL-шорткод',
                        'title' => 'Название',
                        'opt_type' => 'Тип поля'
                    ],
                    'pk' => 'id_option'
                ],
                'pr2op' => [
                    'fields' => ['id_product', 'id_option', 'id_unit', 'value', 'in_list'],
                    'not_empty' => ['id_product', 'id_option', 'id_unit', 'value'],
                    'html_allowed' => ['value'],
                    'range' => [
                        'title' => ['2', '256'],
                    ],
                    'labels' => [
                        'id_product' => 'Продукт',
                        'id_option' => 'Опция',
                        'id_unit' => 'Единицы измерения',
                        'value' => 'Значение',
                        'in_list'=> 'Выводится в общем списке опций на странице товара'
                    ],
                    'pk' => ['id_product', 'id_option']
                ]
            ]
        ];
    }

}

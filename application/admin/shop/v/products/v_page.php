<div>
	<a class="btn btn-success" href="<?=$admin_root?>shop/price">скачать каталог</a>
	<i>(в прайсе для клиента продукты только "В наличии")</i>
    <div class="filter">
        <span class="name">Фильтр по категориям</span>
        <?= $html->select_tree($other['cats'], $other['id_cat'])->addClass('filter-cat')?>
        <? if($other['id_cat'] != null): ?>
            <a href="<?=$admin_root?>shop/products" title="Очистить фильтр" class="fa fa-ban fa-lg filter-clear"></a>
        <? endif; ?>
		
    </div>
	<table class="table table-hover table-bordered shop-products">
        <thead>
			<tr>
                <th>Название</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr>
                    <td><?= $one['title'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>shop/products/view/<?=$one['id_product'] ?>" title="Посмотреть как выглядит товар" class="admin-btn admin-btn-view"></a>
                        <a href="<?=$admin_root?>images/work/<?=$other['image_rel']?>/<?= $one['id_product'] ?>" title="Загрузить изображения" class="admin-btn admin-btn-image"></a> 
                        <a href="<?=$admin_root?>shop/products/edit/<?= $one['id_product'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <? if(empty($one['children'])): ?>
                        <a href="<?=$admin_root?>shop/products/delete/<?= $one['id_product'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <? endif; ?>
                        <a href="<?=$admin_root?>shop/products/add/<?= $one['id_product'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
                <? foreach ($one['children'] as $child): ?>
                <tr class="inner-row">
                    <td><?= $child['title'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>shop/products/view/<?=$child['id_product'] ?>" title="Посмотреть как выглядит товар" class="admin-btn admin-btn-view"></a>
                        <a href="<?=$admin_root?>images/work/<?=$other['image_rel']?>/<?= $child['id_product'] ?>" title="Загрузить изображения" class="admin-btn admin-btn-image"></a> 
                        <a href="<?=$admin_root?>shop/products/edit/<?= $child['id_product'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>shop/products/delete/<?= $child['id_product'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>shop/products/add/<?= $child['id_product'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
                <? endforeach ?>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p>
        <a class="btn btn-primary btn" href="<?=$admin_root?>shop/products/add/">
            Добавить товар &raquo;
        </a>
    </p>
</div>
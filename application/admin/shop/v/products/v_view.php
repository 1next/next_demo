<div>
    <h3>Основная информация</h3>
    <table class="table table-hover table-bordered shop-products">
        <tbody>
            <tr>
                <td>Название</td>
                <td><?=$product['title']?></td>
            </tr>
            <tr>
                <td>Категория</td>
                <td><?=$cat['name']?></td>
            </tr>
            <tr>
                <td>Прототип</td>
                <td>
                    <? if($product['id_prototype'] == 0): ?>
                        нет
                    <? else: ?>
                        <a href="<?=$admin_root?>shop/products/view/<?=$prototype['id_product']?>"><?=$prototype['title']?></a>
                    <? endif; ?>
                </td>
            </tr>
        </tbody>
    </table>
    <h4>Опции товара, отображаемые в публичном списке и при сравнении</h4>
    <table class="table table-hover table-bordered shop-products">
        <thead>
            <tr>
                <th>Опция</th>
                <th>Значение</th>
                <th>Единицы измерения</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($product['options'] as $option): ?>
                <? if($option['in_list'] == '1'): ?>
                    <tr>
                        <td><?=$option['title']?></td>
                        <td><?=$option['value']?></td>
                        <td><?=$option['name']?> (<?=$option['description']?>)</td>
                    </tr>
                <? endif; ?>
            <? endforeach ?>
        </tbody>
    </table>
    <h4>Приватные опции для ручного вывода</h4>
    <table class="table table-hover table-bordered shop-products">
        <thead>
            <tr>
                <th>Опция</th>
                <th>Значение</th>
                <th>Единицы измерения</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($product['options'] as $option): ?>
                <? if($option['in_list'] == '0'): ?>
                    <tr>
                        <td><?=$option['title']?></td>
                        <td><?=$option['value']?></td>
                        <td><?=$option['name']?> (<?=$option['description']?>)</td>
                    </tr>
                <? endif; ?>
            <? endforeach ?>
        </tbody>
    </table>
</div>
<a class="btn btn-danger" href="javascript:history.back();">Вернуться назад</a>
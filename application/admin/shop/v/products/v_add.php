<form method="post" class="product-form">
    <?= $f->set($html->select($data['prototypes'], $fields['id_prototype'], $data['prototypes_disabled']), 'id_prototype') ?>
    <?= $f->set($html->select_tree($data['cats'], $fields['id_cat']), 'id_cat')?>
    <?= $f->set($html->input()->val($fields['title']), 'title') ?>
    <?= $html->input('hidden')->idn('pr2op')->val(htmlspecialchars($fields['pr2op']))?>
    <h4>Опции товара <span id="add-option" class="fa fa-plus-circle"></span></h4>
    <table id="product-options" class="table table-bordered table-hover">
        <tr>
            <th>Название</th>
            <th>Значение</th>
            <th>Единица измерения</th>
            <th>В списке</th>
            <th>-</th>
        </tr>
    </table>
    <div class="error-group">
        <? foreach($errors->unshown() as $err): ?>
            <p class="error"><?=$err?></p>
        <? endforeach; ?>
    </div>
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
</form>
<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>Название</th>
                <th>URL</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr>
                    <td><?= $one['title'] ?></td>
                    <td><?= $one['url'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>shop/options/edit/<?= $one['id_option'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>shop/options/delete/<?= $one['id_option'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>shop/options/add/<?= $one['id_option'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p>
        <a class="btn btn-primary btn" href="<?=$admin_root?>shop/options/add/">
            Добавить опцию &raquo;
        </a>
    </p>
</div>
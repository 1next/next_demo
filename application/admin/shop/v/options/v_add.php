<form method="post">
    <?= $f->set($html->input()->val($fields['title']), 'title') ?>
    <?= $f->set($html->input()->val($fields['url']), 'url') ?>
    <?= $f->set($html->select($data['types'], $fields['opt_type']), 'opt_type') ?>
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
</form>
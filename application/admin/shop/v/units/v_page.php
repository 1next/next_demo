<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>Сокращенённое название</th>
                <th>Подробное название</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr>
                    <td><?= $one['name'] ?></td>
                    <td><?= $one['description'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>shop/units/edit/<?= $one['id_unit'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>shop/units/delete/<?= $one['id_unit'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>shop/units/add/<?= $one['id_unit'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p>
        <a class="btn btn-primary btn" href="<?=$admin_root?>shop/units/add/">
            Добавить единицу измерения &raquo;
        </a>
    </p>
</div>
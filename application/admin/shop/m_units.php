<?php

namespace Admin\Shop;

class M_units extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'units');
    }

}

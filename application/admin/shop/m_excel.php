<?php

namespace Admin\Shop;

use Admin\Cats\Api\Main as Cats;
use Client\Texts\Api\Main as M_texts;
use PHPExcel;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;

class M_excel extends \Core\Base\Model {

    use \Core\Traits\Singleton;
	private $letters = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';
	
    protected function __construct() {
        parent::__construct(App::instance(), 'excel');
    }

	public function get_header_price() {
		$mText = M_texts::instance();
		$header = [];
		
		$header['title']	= $mText->get('title_p');
		$header['address']	= $mText->get('address_p');
		
		$header['phone']	= $mText->get('phone');
		$header['phone2']	= $mText->get('phone2');
		$header['email']	= $mText->get('email');
		$header['site']		= $mText->get('site');
		
		$header['ogrn']		= $mText->get('ogrn_p');
		$header['inn']		= $mText->get('inn_p');
		$header['kpp']		= $mText->get('kpp_p');
		$header['rs']		= $mText->get('rs_p');
		
		return $header;
	}
	
	
	public function get_list_cats() {
		$cats = [];
		$cats_all = Cats::instance()->all_price();
				
		foreach($cats_all as $c){
			$cats[$c['id_cat']] = $c['name'];
		}
		return $cats;
	}

	
	public function get_list_products() {
		$prices = ['content', 'price', 'priceg', 'pricem', 'instock', 'height', 'age', 'ripening'];
		
		$list = M_products_options::instance()->get_by_ids_price($prices, 'id_option');

		$list2 = \Core\Arr::grouping($list, 'id_cat');
		
		foreach($list2 as $k => $l){
			$list2[$k] = \Core\Arr::grouping($l, 'id_prototype');
		}
		
		foreach($list2 as $k => $ls){
			foreach($ls as $kl => $l){
				$list2[$k][$kl] = \Core\Arr::grouping($l, 'id_product');
			}
		}
	
		$list = [];
		foreach($list2 as $kc => $list_cats){
			foreach($list_cats as $kpt => $list_prototypes){
				foreach($list_prototypes as $kpr => $list_products){
					$l = [];
					foreach($list_products as $opt){
						if($opt['url'] != 'content'){
		
							if($l['title'] == ''){	
								$l['title'] = $opt['title'];
							}
							$l[$opt['url']] = $opt['value'];
						}
					}
					if(!empty($l)){
						$list[$kc][$kpt][$kpr] = $l;
					}
				}
			}
		}
		return $list;
	}
	
	
	
	public function get_prototypes() {
		$pr = M_products::instance()->get_prototypes();
		$pr = \Core\Arr::grouping($pr, 'id_cat');
		$prototypes = [];
		
		foreach($pr as $cat => $pt){
			foreach($pt as $p){
				$prototypes[$cat][$p['id_product']] = $p['title'];
			}
		}

		return $prototypes;
	}
	
	public function get_ages($products) {
		$age=[];
		
		
		foreach($products as $prod){
			foreach($prod as $id_pt => $pr){
				$tmp_pr = \Core\Arr::grouping($pr, 'age');
				
				foreach($tmp_pr  as $ag => $p){
					$age[$id_pt][] = $ag;
				}
			}	
		}
		
		foreach($age as $id_pt => $ag){
			sort($ag);
			$age[$id_pt] = $ag;
				
		}
	
	
		return $age;
	}
	
	
	// Координиты ячейки	 
	public function get_cell_coords($row, $col){
		return $this->get_col_letter($col) . ($row + 1);
	}
	
	// Символ столбца
	public function get_col_letter($col){
		return $this->letters[$col];
	}
	
	//Пользовательская сортировка двумерного массива
	public function sort_price(&$arr){
		
		usort($arr, function ($v1, $v2) {
			if ($v1['title'] == $v2['title']){return 0;}
			return ($v1['title'] < $v2['title'])? -1: 1;
		}); 
	}
	
	public function get_price($admin = false) {
		$fields = ['№', 'Название', 'В наличии', 'Рост (м)', 'Цена за штуку (руб)', 'Цена за метр (руб)'];	// Лиственные, Хвойные
		$fields_2 = ['№', 'Название', 'В наличии', 'Рост (м)', 'Цена (руб)', 'Срок созревания'];	// Плодовые
		
		$name = 'kp_price_';
		$title = 'ПРАЙС-ЛИСТ от '; 
		
		if($admin){
			$name = 'kp_katalog_';
			$title = 'КАТАЛОГ от '; 
		}
				
		$name = $name . date('Y-m-d_H-i');
		$title = $title . date('d-m-Y'); 
						
		$header = $this->get_header_price();
		$cats = $this->get_list_cats();
		$prototypes = $this->get_prototypes();
		$products = $this->get_list_products();	
		
		$ages = $this->get_ages($products);	
		
	//	\Core\Helpers::preprint($ages);
		
		$xls = new PHPExcel();
		
		function cell_center($col, $row, &$xls, $list = 0){
			$letters = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';

			if(is_int($col)){
				$col = $letters[$col];
			}
			$xls->getActiveSheet($list)->getStyle($col.$row)->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		};

		function cell_color($col,$row, $color, &$xls, $list = 0){
			$letters = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';
			$xls->getActiveSheet($list)->getStyle($letters[$col].$row)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($color);
		};

		function row_merge($col1, $col2, $row, &$xls, $list = 0){
			$letters = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';
			
			if(is_int($col2)){
				$col2 = $letters[$col2];
			}
			$xls->setActiveSheetIndex($list)->mergeCells($col1.$row.':'.$col2.$row);
		};

		function cell_autosize($col, &$xls, $list = 0){
			$letters = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';
			$xls->getActiveSheet($list)->getColumnDimension($letters[$col])->setAutoSize(true);
		};

		function cell_font($col, $row, $s, &$xls, $list = 0){
			if($s == 'bold'){	$xls->getActiveSheet($list)->getCell($col.$row)->getStyle()->getFont()->setBold(1);}
			if($s == 'italic'){	$xls->getActiveSheet($list)->getCell($col.$row)->getStyle()->getFont()->setItalic(1);}
		};
		
		function cell_border($col1, $col2, $row, &$xls, $list = 0){
			$letters = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';

			if(is_int($col1)){	
				$cell1 = $letters[$col1].$row;
			} else {			
				$cell1 = $col1.$row;	
			};
			
			if(is_int($col2)){	
				$cell2 = $letters[$col2].$row;
			} else {			
				$cell2 = $col2.$row;	
			};
			
			$style = ['borders' => ['allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN]]]; 
			$xls->getActiveSheet($list)->getStyle($cell1.':'.$cell2)->applyFromArray($style);

		};
		
		function cell_set($col, $row, $val, &$xls, $list = 0){  
			$letters = 'ABCDEFGHIJKLMNOPQRSTVWXYZ';

			if(is_int($col)){	
				$cell = $letters[$col].$row;
			} else {			
				$cell = $col.$row;	
			};

			$xls->setActiveSheetIndex($list)->setCellValue($cell, $val);
		}

		$list = 0;
		
		foreach($cats as $id_cat => $cat_title){
			$price2 = false; 
			
			if($id_cat == 3 or $id_cat == 4){ 
				$price2 = true;
				$fields = $fields_2;
			}
			
			$count_fields = count($fields);
			
			$row = 2;
			cell_set('B',$row, $header['title'], $xls, $list);
						
			cell_font('B',$row, 'bold' , $xls, $list);
			row_merge('B', 'D', $row, $xls, $list);

			if(!$admin){
				cell_set('E',$row, 'Телефоны:', $xls, $list);
				cell_font('E',$row, 'italic' , $xls, $list);

				cell_set('F',$row, $header['phone'], $xls, $list);
				row_merge('F', 'G', $row++, $xls, $list);

				cell_set('B',$row, $header['address'], $xls, $list);
				row_merge('B', 'D', $row, $xls, $list);

				cell_set('F',$row, $header['phone2'], $xls, $list);
				row_merge('F', 'G', $row++, $xls, $list);

				cell_set('B',++$row, 'Р/C: '.$header['rs'], $xls, $list);
				cell_center('B',$row, $xls, $list);
				
				cell_set('E',$row, 'Эл.почта:', $xls, $list);
				cell_font('E',$row, 'italic' , $xls, $list);

				cell_set('F',$row, $header['email'], $xls, $list);
				row_merge('F', 'G', $row++, $xls, $list);

				cell_set('B',$row, 'ОГРН: '.$header['ogrn'], $xls, $list);
				cell_center('B',$row++, $xls, $list);
				
				cell_set('B',$row, 'ИНН: '.$header['inn'], $xls, $list);
				cell_center('B',$row, $xls, $list);
			}

			cell_set('E',$row, 'Сайт:', $xls, $list);
			cell_font('E',$row, 'italic' , $xls, $list);

			cell_set('F',$row, $header['site'], $xls, $list);
			row_merge('F', 'G', $row++, $xls, $list);

			cell_set('A',++$row, $title, $xls, $list);
			cell_font('A',$row, 'bold' , $xls, $list);
			cell_center('A',$row, $xls, $list);

			row_merge('A', $count_fields-1, $row++, $xls, $list);

			for($i = 0; $i < $count_fields; $i++){
				cell_autosize($i, $xls, $list);
			}
			
			$row++;
			
			for($i = 0; $i < $count_fields; $i++){
				cell_set($i,$row, $fields[$i], $xls, $list);
				cell_color($i,$row, 'AAAAAA', $xls, $list);
				cell_center($i, $row, $xls, $list);
			} 
			
			cell_border('A', $count_fields-1, $row, $xls, $list);

			$i = 1;
			
			foreach($prototypes as $pt_id_cat => $prototype){
				
				if($id_cat == $pt_id_cat){
					$xls->getActiveSheet($list)->setTitle($cat_title);
				
					cell_set(0, ++$row, $cat_title, $xls, $list);
					cell_font('A',$row, 'bold' , $xls, $list);
					cell_center('A',$row, $xls, $list);
					
					row_merge('A', 'F', $row, $xls, $list);
					cell_border('A', $count_fields-1, $row++, $xls, $list);

					foreach($prototype as $id_prototype => $prots_title){
						foreach($products as $p_id_cat => $prods){
							if($id_cat == $p_id_cat ){

								foreach($prods as $p_id_prot => $pr){
									if($id_prototype == $p_id_prot and $id_cat == $p_id_cat ){
										$this->sort_price($pr);
									
										foreach($ages as $a_id_prot => $ag){
											foreach($ag as $a){
												
												if($a_id_prot == $p_id_prot ){
								
													cell_set(0, $row, $prots_title.' '.$a, $xls, $list);
													cell_color(0,$row, 'EEEEEE', $xls, $list);
													cell_font('A',$row, 'bold' , $xls, $list);
													cell_center('A',$row, $xls, $list);

													//$price2	? $col2 = 'G' : $col2 = 'F';

													row_merge('A', 'F', $row, $xls, $list);
													cell_border('A', $count_fields-1, $row++, $xls, $list);
												

													foreach($pr as $v){
														if($a_id_prot == $p_id_prot and $a == $v['age']){
															cell_set(0, $row, $i++, $xls, $list);				cell_center('A', $row, $xls, $list);
															cell_set(1, $row, $v['title'], $xls, $list);	
															cell_set(2, $row, $v['instock'], $xls, $list);		cell_center('C', $row, $xls, $list);
															cell_set(3, $row, $v['height'], $xls, $list);		cell_center('D', $row, $xls, $list);
															cell_set(4, $row, $v['price'], $xls, $list);		cell_center('E', $row, $xls, $list);

															if($price2){ 
																cell_set(5, $row, $v['ripening'], $xls, $list);	cell_center('F', $row, $xls, $list);
															}else{
																cell_set(5, $row, $v['pricem'], $xls, $list);	cell_center('F', $row, $xls, $list);
															}
															cell_border('A', $count_fields-1, $row++, $xls, $list);
														}
													}
												}	
											}
										}	
									}
								}
							}
						}
					}
				}		
			}			
			if(count($cats) > ++$list){
				$xls->createSheet();
			}		
		}
		//*

		 // устанавливаем бордер ячейкам          
//$styleArray = ['borders' => ['allborders' => ['style' => PHPExcel_Style_Border::BORDER_THIN]]]; 
//$objWorkSheet->getStyle('A1:A2')->applyFromArray($styleArray);
//
//
//$PHPExcel_Style->getBorders()->getLeft()->applyFromArray([‘style’ =>PHPExcel_Style_Border::BORDER_DASHDOT,’color’ => [‘rgb’ => ’808080']]);
		 		// */
		
		
		\Core\Headers::send('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
		\Core\Headers::send('Cache-Control: max-age=0');
		\Core\Headers::send('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');	// Excel2007_Redirect output to a client’s web browser
		
		//$excel->getProperties()->setCreator("CMS")
									//->setLastModifiedBy("CMS");
									// ->setTitle("Прайс CMS")
									// ->setSubject("Office 2007 XLSX Test Document")
									// ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
									// ->setKeywords("office 2007 openxml php")
									// ->setCategory("Test result file");
		
		/* IE 9
		// If you're serving to IE 9, then the following may be needed
		\Core\Headers::send('Cache-Control: max-age=1'); 
		// If you're serving to IE over SSL, then the following may be needed
		\Core\Headers::send('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		\Core\Headers::send('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		\Core\Headers::send('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		\Core\Headers::send('Pragma: public'); // HTTP/1.0 //*/

		/* Excel2003
		\Core\Headers::send("Content-Type: application/x-msexcel"); // Excel2003_Redirect output to a client’s web browser
		$writter = new PHPExcel_Writer_Excel5($this->excel); //*/
		
		return $xls;

	}
}


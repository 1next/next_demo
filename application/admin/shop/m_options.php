<?php

namespace Admin\Shop;

class M_options extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'options');
    }

}

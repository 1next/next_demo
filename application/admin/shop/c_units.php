<?php

namespace Admin\Shop;

use Core\Traits\Actions;
use Core\Exceptions;
use Admin\Base\Fieldgen;

class C_units extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('shop');
        $this->module = 'shop';
        $this->main_model = M_units::instance();
    }

    public function action_switch(){
        $action = 'action_' . (isset($this->params[2]) ? $this->params[2] : 'index');
        $this->$action();
    }
    
    protected function page_num() {
        return isset($this->params[3]) ? (int) $this->params[3] : 1;
    }

    protected function settings_action_page() {
        $this->to_base_template['title'] .= 'Единицы измерения: страница ' . $this->page_num;
        $this->page_template = "{$this->module}/v/units/v_page";
        $this->main_model->pager()->url_self($this->module . '/units/page/%s');
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление единицы измерения';

        if (isset($this->params[3]) && is_numeric($this->params[3])) {
            $this->add_fields_data = $this->main_model->get($this->params[3]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[3]} not found.");
            }
        } else {
            $this->add_fields_data = [];
        }

        $this->add_redirect .= '/units';
        $this->add_template = "{$this->module}/v/units/v_add";
        $this->add_extract = ['name', 'description'];
        $this->add_fieldgen = new Fieldgen($this->app->labels('units'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование единицы измерения';
        $this->edit_id = $this->params[3];
        $this->edit_extract = ['name', 'description'];
        $this->edit_template = "{$this->module}/v/units/v_add";
        $this->edit_fieldgen = new Fieldgen($this->app->labels('units'), $this->errors);
    }

    protected function settings_action_delete() {
        $this->delete_id = $this->params[3];
    }

}

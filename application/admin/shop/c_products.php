<?php

namespace Admin\Shop;

use Core\Traits\Actions;
use Core\Exceptions;
use Core\Arr;
use Admin\Base\Fieldgen;
use Admin\Cats\Api\Main as Cats;
//use PHPExcel;
use PHPExcel_IOFactory;
//use PHPExcel_Style_Fill;
//use PHPExcel_Style_Alignment;

class C_products extends \Admin\Base\Controller {

    use Actions\Page;
	use Actions\Add;
	use Actions\Edit;
	use Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('shop');
        $this->module = 'shop';
        $this->main_model = M_products::instance();
        $this->add_styles($this->module, ['styles']);
    }

    public function action_switch(){
        if(!isset($this->params[1])){
            $this->redirect($this->root . $this->module . '/products');
        }
        
        $action = 'action_' . (isset($this->params[2]) ? $this->params[2] : 'index');
        $this->$action();
    }
    
    public function action_view(){
        $product = $this->main_model->get_compile($this->params[3]);
        
        if($product == null){
            throw new Exceptions\E404("Item with id {$this->params[3]} not found.");
        }
        
        $prototype = ($product['id_prototype'] == 0) ? [] : $this->main_model->get($product['id_prototype']);
        $cat = Cats::instance()->get($product['id_cat']);
        
        $this->to_base_template['title'] = $product['title'] . ' - предпросмотр';
        $this->to_base_template['content'] = $this->template($this->module . '/v/products/v_view', 
                                ['product' => $product, 'prototype' => $prototype, 'cat' => $cat]);
    }
    
    protected function page_num() {
        return isset($this->params[3]) ? (int) $this->params[3] : 1;
    }

    protected function settings_action_page() {
        $this->to_base_template['title'] .= 'Товары: страница ' . $this->page_num;
        $this->page_template = "{$this->module}/v/products/v_page";
        $this->page_other_data['image_rel'] = App::IMAGE_RELATION;
        
        $id_cat = $this->get['id_cat'] ? (int)$this->get['id_cat'] : null;
        $url_self = $this->module . '/products/page/%s';
        $where = "id_prototype='0'";
        
        if($id_cat !== null && $id_cat !== 0){
            $url_self .= "?id_cat=$id_cat";
            $where .= " AND id_cat='$id_cat'";
        }
        
        $this->main_model->pager()->url_self($url_self)->where($where);
        
        $this->add_scripts($this->module, ['page']);
        
        $cats = Cats::instance()->tree(App::CAT_TYPE);
        array_unshift($cats, ['id_cat' => 0, 'name' => 'нет']);
        
        $this->page_other_data['cats'] = Arr::to_select_tree($cats, 'id_cat', 'name');
        $this->page_other_data['id_cat'] = $id_cat;
        $this->vars_to_js['Module_Shop_UrlSelf'] = $this->module . '/products?id_cat={{id_cat}}';
        /*
         * $this->main_model->pager()
                         ->url_self($this->module . '/products/page/%s')
                         ->where("id_prototype='0'");
         */
    }

    protected function action_page_data_processor(&$data) {
		$this->main_model->sort_title($data);
        $data_id = Arr::section($data, 'id_product');
        $children = $this->main_model->get_in(['id_prototype' => $data_id]);
        $this->main_model->sort_title($children);
		
        $children_by_prototype = Arr::grouping($children, 'id_prototype');
        
        foreach($data as $k => $p){
            $data[$k]['children'] = isset($children_by_prototype[$p['id_product']]) ? 
                                            $children_by_prototype[$p['id_product']] : 
                                            [];
                                        
        }
    }
    
    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление товара';

        if (isset($this->params[3]) && is_numeric($this->params[3])) {
            $this->add_fields_data = $this->main_model->get($this->params[3]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[3]} not found.");
            }
            
            $product_options = M_products_options::instance()->get_by_fieldset(['id_product' => $this->params[3]]);
            
            foreach($product_options as $k => $option){
                $product_options[$k]['value'] = htmlspecialchars_decode($option['value']);
            }
            
            $this->add_fields_data['pr2op'] = json_encode($product_options);
        } else {
            $this->add_fields_data = ['pr2op' => json_encode([])];
        }
        
        $this->vars_to_js['options'] = M_options::instance()->all();
        $this->vars_to_js['units'] = M_units::instance()->all();
        
        $this->add_scripts('base', ['lib/jquery.json.min', 'ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init']);
        
        $this->add_redirect .= '/products';
        $this->add_template = "{$this->module}/v/products/v_add";
        $this->add_extract = ['id_cat', 'id_prototype', 'title', 'pr2op'];
        $this->add_other_data['cats'] = Arr::to_select_tree(Cats::instance()->tree(App::CAT_TYPE), 'id_cat', 'name');
        
        $prototypes = $this->main_model->get_by_fieldset(['id_prototype' => 0]);
        array_unshift($prototypes, ['id_product' => 0, 'title' => 'Без протопита (корневой товар)']);
        $this->add_other_data['prototypes'] = Arr::to_select($prototypes, 'id_product', 'title');
        $this->add_other_data['prototypes_disabled'] = [];
        
        $this->add_fieldgen = new Fieldgen($this->app->labels('products'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование товара';

        $this->vars_to_js['options'] = M_options::instance()->all();
        $this->vars_to_js['units'] = M_units::instance()->all();
        
        $this->add_scripts('base', ['lib/jquery.json.min', 'ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init']);
        
        $this->edit_id = $this->params[3];
        
        $product_options = M_products_options::instance()->get_by_fieldset(['id_product' => $this->edit_id]);
        
        foreach($product_options as $k => $option){
            $product_options[$k]['value'] = htmlspecialchars_decode($option['value']);
        }
        
        $this->edit_fields_data['pr2op'] = json_encode($product_options);
        
        $this->edit_extract = ['id_cat', 'id_prototype', 'title', 'pr2op'];
        $this->edit_template = "{$this->module}/v/products/v_add";
        $this->edit_other_data['cats'] = Arr::to_select_tree(Cats::instance()->tree(App::CAT_TYPE), 'id_cat', 'name');
        
        $children = $this->main_model->get_by_fieldset(['id_prototype' => $this->edit_id]);
        
        if(count($children) > 0){
            $prototypes = [['id_product' => 0, 'title' => 'Без протопита - нельзя менять, он сам прототип']];
        }
        else{
            $prototypes = $this->main_model->get_by_fieldset(['id_prototype' => 0]);
            array_unshift($prototypes, ['id_product' => 0, 'title' => 'Без протопита (корневой товар)']);
        }

        $this->edit_other_data['prototypes'] = Arr::to_select($prototypes, 'id_product', 'title');
        $this->edit_other_data['prototypes_disabled'] = [$this->edit_id];
        
        $this->edit_fieldgen = new Fieldgen($this->app->labels('products'), $this->errors);
    }

    protected function settings_action_delete() {
        $this->delete_id = $this->params[3];
    }

	protected function action_price() {
		
		$excel = M_excel::instance()->get_price(true);
			
		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$writer->save('php://output');	

		throw new Exceptions\Exit_now('1');
	}
}

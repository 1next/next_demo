<?php

namespace Admin\Shop;

use Admin\Images\Api\Main as Images;

class C_products_ajax extends \Admin\Base\Ajax {

    public function before() {
        parent::before();
        $this->check_access('shop');
    }

    public function action_switch(){
        $action = 'action_' . (isset($this->params[2]) ? $this->params[2] : 'index');
        $this->$action();
    } 

    protected function set_error_template(\Core\Errors $errors) {
        
    }

}

<?php

namespace Admin\Shop;

use Core\Arr;
use Core\Events;

class M_products extends \Core\Base\Model {

    use \Core\Traits\Singleton;

use \Core\Traits\Model\Get_in;

    protected function __construct() {
        parent::__construct(App::instance(), 'products');
    }

	public function sort_title(&$arr){
		usort($arr, function ($v1, $v2) {
			if ($v1['title'] == $v2['title']){return 0;}
			return ($v1['title'] < $v2['title'])? -1: 1;
		}); 
	}
	
	public function get_prototypes(){
		return $this->get_by_fieldset(['id_prototype' => 0],'title');
	}
	
    public function get_compile($pk) {
        $product = $this->get($pk);

        if ($product == null) {
            return null;
        }

        $ids = [$product['id_product']];
        $has_pr = false;

        if ($product['id_prototype'] != 0) {
            $ids[] = $product['id_prototype'];
            $has_pr = true;
        }

        $pr2op = M_products_options::instance()->get_by_ids($ids);

        if ($has_pr) {
            $o_by_p = Arr::grouping($pr2op, 'id_option');
            $options = [];

            foreach ($o_by_p as $option_group) {
                if (count($option_group) == 1) {
                    $options[] = $option_group[0];
                } else {
                    $child_option = ($option_group[0]['id_product'] == $pk) ? $option_group[0] : $option_group[1];
                    $parent_option = ($option_group[0]['id_product'] != $pk) ? $option_group[0] : $option_group[1];
                    $child_option['value'] = str_replace('%parent%', $parent_option['value'], $child_option['value']);
                    $options[] = $child_option;
                }
            }

            $product['options'] = $options;
        } else {
            $product['options'] = $pr2op;
        }

        return $product;
    }

    public function add($fields) {
        if (isset($fields['id_prototype']) && !$this->check_prototype($fields['id_prototype'])) {
            return false;
        }

        if (isset($fields['id_prototype']) && $fields['id_prototype'] != '0') {
            $row = $this->get($fields['id_prototype']);
            $fields['id_cat'] = $row['id_cat'];
        }

        $this->db->beginTransaction();

        $success = true;
        $id = parent::add($fields);

        if ($id > 0 && isset($fields['pr2op'])) {
            $mPr2Op = M_products_options::instance();
            $products_options = json_decode($fields['pr2op'], true);

            if (is_array($products_options)) {
                foreach ($products_options as $pr2op) {
                    $todb = Arr::extract($pr2op, ['id_option', 'id_unit', 'value', 'in_list']);
                    $todb['id_product'] = $id;
                    $res = $mPr2Op->add($todb);

                    if ($res === false) {
                        $this->errors = $mPr2Op->errors();
                        $success = false;
                        break;
                    }
                }
            } else {
                $success = false;
                $this->errors->add('pr2op', 'Получен некорректный JSON, описывающий опции продуктов.');
            }
        }

        if (!$success) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $id;
    }

    public function edit($pk, $fields) {
        if (isset($fields['id_prototype']) &&
                (!$this->check_prototype($fields['id_prototype']) ||
                !$this->check_cascade($pk, $fields['id_prototype'])
                )) {
            return false;
        }

        if (isset($fields['id_prototype']) && $fields['id_prototype'] != '0') {
            $row = $this->get($fields['id_prototype']);
            $fields['id_cat'] = $row['id_cat'];
        }

        $this->db->beginTransaction();

        $success = true;
        $result = parent::edit($pk, $fields);

        if ($result && isset($fields['pr2op'])) {
            $mPr2Op = M_products_options::instance();
            $products_options = json_decode($fields['pr2op'], true);

            if (is_array($products_options)) {
                $mPr2Op->delete_by_fieldset(['id_product' => $pk]);

                foreach ($products_options as $pr2op) {
                    $todb = Arr::extract($pr2op, ['id_option', 'id_unit', 'value', 'in_list']);
                    $todb['id_product'] = $pk;
                    $res = $mPr2Op->add($todb);

                    if ($res === false) {
                        $this->errors = $mPr2Op->errors();
                        $success = false;
                        break;
                    }
                }
            } else {
                $success = false;
                $this->errors->add('pr2op', 'Получен некорректный JSON, описывающий опции продуктов.');
            }
        }

        if (!$success) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        Events::run('System.Model.Edit.Success', [
            'table' => $this->table,
            'pk' => $pk,
            'item' => $this->get($pk)
        ]);

        return $result;
    }

    public function delete($pk) {
        $chidlren = $this->get_by_fieldset(['id_prototype' => $pk]);

        if (count($chidlren) > 0) {
            $this->errors->add('delete', 'Нельзя удалять товар, у которого есть дочерние товары. Удалите сначала их.');
            return false;
        }

        $this->db->beginTransaction();

        $mPr2Op = M_products_options::instance();
        $pr2op = $mPr2Op->delete_by_fieldset(['id_product' => $pk]);

        if (!parent::delete($pk)) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return true;
    }

    private function check_prototype($id_prototype) {
        if ($id_prototype == 0) {
            return true;
        }

        $product = $this->get($id_prototype);

        if ($product == null) {
            $this->errors->add('id_prototype', 'Такой товар не найден');
            return false;
        }

        if ($product['id_prototype'] != '0') {
            $this->errors->add('id_prototype', 'Данный товар сам является прототипом');
            return false;
        }

        return true;
    }

    private function check_cascade($pk, $id_prototype) {
        if ($id_prototype == 0) {
            return true;
        }

        if ($pk == $id_prototype) {
            $this->errors->add('id_prototype', 'Сам для себя прототип? Ну-ну.');
            return false;
        }

        $products = $this->get_by_fieldset(['id_prototype' => $pk]);

        if (count($products) > 0) {
            $this->errors->add('id_prototype', 'Нельзя. Данный товар - протопип для других.');
            return false;
        }

        return true;
    }

}

$(function () {

    var url = _PHP.Module_Shop_UrlSelf;

    $('.filter-cat').on('change', function(){
        var id = $(this).val();
        var location = url.replace('{{id_cat}}', id);
        document.location = _PHP.admin_root + location;
    });
    
});
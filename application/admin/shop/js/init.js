$(function () {

    var options = _PHP.options;
    var units = _PHP.units;

    var span = $('#add-option');
    var table = $('#product-options');
    var hidden = $('.product-form input[name=pr2op]');
    var errors = $('.error-group');

    var form_prototype = $('select[name=id_prototype]');
    var form_cat = $('select[name=id_cat]');

    var ck_id_num = 0;

    function get_field(option_type) {
        var field = {
            elem: '',
            atfer_append: function () {}
        };

        switch (option_type) {
            case 'input':
                field.elem = $('<input/>');
                break
            case 'textarea':
                field.elem = $('<textarea/>');
                break;
            case 'ckeditor':
                field.elem = $('<textarea/>');
                field.atfer_append = function () {
                    var id = 'ck_option_' + ck_id_num++;
                    this.elem.attr('id', id);
                    CMS.ckeditor(CKEDITOR, id);
                }
                break;
        }

        return field;
    }

    function add_pr2op() {
        var options_select = $('<select/>').addClass('form-control').addClass('option');

        for (var i = 0; i < options.length; i++) {
            $('<option/>').val(options[i].id_option).html(options[i].title)
                    .attr('data-field', options[i].opt_type)
                    .appendTo(options_select);
        }

        var units_select = $('<select/>').addClass('form-control').addClass('unit');

        for (var i = 0; i < units.length; i++) {
            $('<option/>').val(units[i].id_unit).html(units[i].description).appendTo(units_select);
        }

        var tr = $('<tr/>').addClass('data-pr2op');

        $('<td/>').append(options_select).appendTo(tr);
        $('<td/>').append($('<input/>').attr('type', 'text').attr('data-field', 'input')
                .addClass('form-control').addClass('value'))
                .appendTo(tr);
        $('<td/>').append(units_select).appendTo(tr);
        $('<td/>').append($('<input/>').attr('type', 'checkbox').addClass('in_list')).appendTo(tr);
        $('<td/>').append($('<span/>').addClass('fa fa-minus-square-o delete-option').attr('title', 'Удалить опцию'))
                .appendTo(tr);

        table.append(tr);
        return tr;
    }

    function controll_types(tr) {
        var type = tr.find('select.option option:selected').data('field');
        var current = tr.find('.value');

        if (type !== current.data('field')) {
            var field_obj = get_field(type);
            var field = field_obj.elem;

            if (current.data('field') == 'ckeditor') {
                var ck_id = current.attr('id');

                if (CKEDITOR.instances[ck_id]) {
                    CKEDITOR.instances[ck_id].destroy();
                }
            }

            current.before(
                    field.val(current.val())
                    .attr('type', 'text').attr('data-field', type)
                    .addClass('form-control').addClass('value')).remove();

            field_obj.atfer_append();
        }
    }

    table.on('change', 'select.option', function () {
        controll_types($(this).parents('tr').eq(0));
    });

    span.click(function () {
        var current_options = {};

        table.find('select.option').each(function () {
            current_options[$(this).val()] = true;
        });

        if ($('.data-pr2op').length < options.length) {
            var this_tr = add_pr2op();
            var stop = false;

            this_tr.find('select.option option').each(function () {
                if (!stop) {
                    if (current_options[$(this).attr('value')] === undefined) {
                        $(this).prop('selected', true);
                        stop = true;
                    }
                }
            });

            this_tr.find('input.in_list').prop('checked', true);
            controll_types(this_tr);
        } else {
            errors.hide().html($('<p/>').addClass('error').html('У нас нет столько различных опций :)')).fadeIn(300);
        }
    });

    table.on('click', 'span.delete-option', function () {
        $(this).parents('tr').eq(0).remove();
    });

    $('.product-form').submit(function (e) {
        for(var ck_id in CKEDITOR.instances){
            CKEDITOR.instances[ck_id].updateElement();
        }
        
        var pr2op = [];
        var unique = {};
        var err = 0;
        var dup_err = false;

        table.find('select.option').removeClass('shop-error');
        table.find('select.unit').removeClass('shop-error');
        table.find('.value').removeClass('shop-error');

        table.find('tr.data-pr2op').each(function () {
            var elem = {};
            var tr = $(this);
            elem.id_option = tr.find('select.option').val();
            elem.id_unit = tr.find('select.unit').val();
            elem.value = tr.find('.value').val();
            elem.in_list = tr.find('input.in_list').prop('checked') ? 1 : 0;

            if (elem.value.trim() === '') {
                tr.find('.value').addClass('shop-error');
                err++;
            }

            pr2op.push(elem);

            if (unique[elem.id_option] === undefined) {
                unique[elem.id_option] = true;
            } else {
                err++;
                dup_err = true;
                table.find('select.option:has(option[value=' + elem.id_option + ']:selected)').addClass('shop-error');
            }
        });
        
        hidden.val($.toJSON(pr2op));
        
        if (err > 0) {
            e.preventDefault();
        }

        if (dup_err) {
            errors.hide().html($('<p/>').addClass('error').html('Нельзя ставить одинаковые опции.')).fadeIn(300);
        }
    });

    function prototype_cat_controll() {
        if (form_prototype.val() != '0') {
            form_cat.prop('disabled', true).attr('title', 'Не имеет смысла - категория будет взята у прототипа.');
        } else {
            form_cat.prop('disabled', false).removeAttr('title');
        }
    }

    form_prototype.change(function () {
        prototype_cat_controll();
    });

    prototype_cat_controll();

    try {
        var init_pr2op = $.secureEvalJSON(hidden.val());

        if (init_pr2op.length > 0) {
            for (var i = 0; i < init_pr2op.length; i++) {
                var this_tr = add_pr2op();
                this_tr.find('select.option').val(init_pr2op[i].id_option);
                this_tr.find('select.unit').val(init_pr2op[i].id_unit);
                this_tr.find('input.value').val(init_pr2op[i].value);
                this_tr.find('input.in_list').prop('checked', init_pr2op[i].in_list == 1);
                controll_types(this_tr);
            }
        }
    } catch (err) {
        errors.hide().append($('<p/>').addClass('error').html('В поле опций подставлен некорректный JSON - операция загрузки текущих опций завершилась ошибкой.')).fadeIn(300);
    }
});
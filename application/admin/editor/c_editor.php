<?php

namespace Admin\Editor;

use Core\Exceptions;
use Config;
use Core\Helpers;

class C_editor extends \Admin\Base\Controller {

    private $current_theme;

    public function before() {
        parent::before();
        $this->check_access('editor');
        $this->module = 'editor';
        $this->current_theme = Helpers::join_path([PATH_THEMES, Config::CURRENT_THEME]);
    }

    protected function action_index() {
        $this->action_tree();
    }

    protected function action_tree() {
        $this->add_scripts($this->module, ['ace-min-noconflict/ace', 'humane.min', 'init']);
        $this->add_styles($this->module, ['main', 'jackedup']);

        $tree = M_editor::instance()->get_tree($this->current_theme);

        $this->to_base_template['title'] .= 'Дерево шаблонов';
        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_tree", [
            'tree' => $tree,
            'ext' => App::ALLOWED_EXT
        ]);
    }

}

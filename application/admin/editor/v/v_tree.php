<div class="row">

    <div class="col-xs-12 hidden-xs hidden-sm text-right">
        <i id="ace-full" class="fa fa-caret-square-o-left"></i>
    </div>

    <div class="col-md-4 col-xs-12 left-block">
        <?= $html->ul_tree($tree, function($item) use($html, $admin_root){

            if($item['extension']) {
                $el_type = 'a';
                $class = 'filename';
                $add_file = $html->elem('i', 1)->cl('remove-file')->render();
            } else {
                $el_type = 'div';
                $class = 'pathname fa fa-minus-square-o';
                $add_file = $html->elem('i', 1)->cl('add-file')->render();
            }

            return $html->elem($el_type, 1)->html($item['basename'] . $add_file)->attrs([
                'href' => '#',
                'data-href' => $item['path']
            ])->cl($class);

        })->id('ul-tree') ?>
    </div>

    <div class="col-md-8 col-xs-12 right-block">
        <div id="ace-editor">Select file</div>
        <button class="btn btn-success" id="editor-save" disabled>Сохранить изменения</button>
    </div>

</div>

<form class="add-file-form clearfix">
    <span>Путь: <span id="fpath"></span></span>
    <span class="add-form-close fa fa-close pull-right"></span>

    <div class="btn-group-xs select-type" data-toggle="buttons">
        Создать:
        <label class="btn btn-warning active">
            <input type="radio" name="type" id="file" autocomplete="off" checked> Файл
        </label>
        <label class="btn btn-warning">
            <input type="radio" name="type" id="dir" autocomplete="off"> Папку
        </label>
    </div>
    <?= $html->input()->attrs(['placeholder' => 'Введите имя файла'])->id('fname')->cl('form-control') ?>
    <?= $html->select($ext, 0)->cl('form-control input-sm')->id('select-ext')?>
    <input type="submit" value="Создать" class="btn btn-success btn-sm pull-right">
</form>
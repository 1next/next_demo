<form method="post">
    <div id="ace-editor">Something wrong...</div>

    <input type="hidden" name="content">
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
</form>

<script>
    var _aceData = <?=json_encode($data, true)?>;
</script>
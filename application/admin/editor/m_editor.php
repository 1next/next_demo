<?php

namespace Admin\Editor;
use Core\Helpers;
use Core\Arr;
use Config;
	
class M_editor{
    use \Core\Traits\Singleton;

    private $theme = null;
    private $allowed = [];
    private $base_path;
    private $full_path;

    protected function __construct() {
        $this->base_path = Helpers::join_path([PATH_THEMES, Config::CURRENT_THEME]);

        $tree = $this->get_tree($this->base_path);
        $list = Arr::tree_to_list($tree, 'path');

        foreach ($list as $item) {
            $path = Helpers::join_path([$this->base_path, $item]);

            if(is_file($path)) {
                $this->allowed['files'][] = ltrim($item, '/');
            } else if(is_dir($path)) {
                $this->allowed['dirs'][] = ltrim($item, '/');
            }
        }

    }

    public function get($filename) {

        if(!$this->check_file($filename)) {
            return false;
        }

        $res = file_get_contents($this->full_path);

        if($res === false) {
            return false;
        }

        return $res;
    }

    public function edit($filename, $content) {

        if(!$this->check_file($filename)) {
            return false;
        }

        if(file_put_contents($this->full_path, $content) === false) {
            return false;
        }

        return true;
    }

    public function add($fields) {

        switch ($fields['create']) {
            case 'file':
                return $this->create_file($fields);
                break;
            case 'dir':
                return $this->create_dir($fields);
                break;
            default:
                return false;
        }
    }

    private function create_file($fields) {
        $ext = App::ALLOWED_EXT;

        
        $type = $ext[$fields['type']];

        if(!$this->check_file($fields['path'], false)) {
            return false;
        }

        $full_path = Helpers::join_path([$this->base_path, $fields['path'], $fields['file']]) . ".$type";

        if (strpos($full_path, '../') !== false) {
            return false;
        }

        if(is_file($full_path)) {
            return false;
        }

        if(file_put_contents($full_path, '') === false) {
            return false;
        }

        return true;
    }

    private function create_dir($fields) {

        if(!$this->check_file($fields['path'], false)) {
            return false;
        }

        $full_path = Helpers::join_path([$this->base_path, $fields['path'], $fields['file']]);

        if (strpos($full_path, '../') !== false) {
            return false;
        }

        if(is_dir($full_path)) {
            return false;
        }

        if(mkdir($full_path, 0700) === false) {
            return false;
        }

        return true;
    }

    public function remove($filename) {

        if(!$this->check_file($filename)) {
            return false;
        }

        if(!unlink($this->full_path)) {
            return false;
        }

        return true;
    }

    public function get_tree($dir) {
        $this->theme = ($this->theme) ? $this->theme : $dir;
        $result = [];

        foreach (scandir($dir) as $item) {
            if ($item != "." && $item != "..") {

                $path = Helpers::join_path([$dir, $item]);
                $pathinfo = pathinfo($path);
                $is_dir = is_dir($path);

                if($is_dir || in_array($pathinfo['extension'], App::ALLOWED_EXT)) {
                    $one = [];
                    $one['path'] = str_replace($this->theme, '', $path);
                    $one['basename'] = $pathinfo['basename'];
                    $one['extension'] = ($pathinfo['extension']) ? $pathinfo['extension'] : null;
                    $one['children'] = [];

                    if($is_dir) {
                        $one['children'] = $this->get_tree($path);
                    }

                    $result[] = $one;
                }
            }
        }
        return $result;
    }

    private function check_file($path, $file = true) {
        $path = ltrim($path, '/');
        $type = ($file) ? 'files' : 'dirs';

        $full_path = Helpers::join_path([$this->base_path, $path]);

        if(!is_writable($full_path)) {
            return false;
        }

        if($file && !in_array(pathinfo($full_path)['extension'], App::ALLOWED_EXT)) {
            return false;
        }

        foreach ($this->allowed[$type] as $item) {
            if($item == $path) {
                $this->full_path = $full_path;
                return true;
            }
        }

        return false;
    }

}

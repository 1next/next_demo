$(function() {

    /**
     * Init
     */

    var mods = {
        js: 'javascript',
        css: 'css',
        twig: 'twig',
        html: 'html'
    };

    var notify = {
        saveSuccess: 'Файл успешно сохранен!',
        saveFail: 'Во время сохранения файла что-то пошло не так, перезагрузите страницу!',
        loadFail: 'Ошибка загрузки файла, перезагрузите страницу и попробуйте еще раз!',
        addFail: {
            file: 'Не удалось создать файл!',
            dir: 'Не удалось создать папку!'
        },
        addSuccess: {
            file: 'Файл успешно создан!',
            dir: 'Папка успешно создана!'
        },
        removeFail: 'Не удалось удалить файл!',
        removeSuccess: 'Файл успешно удален!',
        default: 'Выберите файл для редактирования'
    };

    // base
    var adminRoot = _PHP.admin_root;
    var editor = ace.edit("ace-editor");
    var request = false;
    var file;

    // ace
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setTabSize(4);

    // human msg
    humane.log(notify.default);

    /**
     * Устанавливает контент в редактор
     * @param content строка
     */
    function setContent(content) {
        editor.setValue(content);
        editor.gotoLine(0);
    }


    function humanMsg(msg) {
        humane.remove(function() {
            humane.log(msg);
        });
    }

    /**
     * Устанавливает режим подсветки по имени файла
     * @param filename строка
     */
    function setModByFile(filename) {
        var ext = filename.split('.').pop();
        var mod = mods[ext];
        editor.getSession().setMode("ace/mode/" + mod);
    }

    /**
     * Получает содержимое файла для редактирования, по AJAX
     * @param filename строка - имя файла
     */
    function getContent(filename) {

        if(request) {
            return;
        }
        request = true;

        var data = {
            'filename': filename
        };

        $.get(adminRoot + '/editor-ajax/get', data, function(res){
            request = false;

            if(res.content === false) {
                return humanMsg(notify.loadFail);
            }

            $('#ul-tree a').removeClass('active');
            $('.filename[data-href="'+ filename+'"]').addClass('active');

            $('#editor-save').removeAttr('disabled');
            setContent(res.content);
            setModByFile(filename);
            file = filename;

        }, 'json');
    }

    /**
     * Сохранить изменения в файл
     * @param content строка
     * @param filename строка - имя файла
     */
    function saveContent(content, filename) {
        $.post(adminRoot + '/editor-ajax/edit', {content: content, filename: filename}, function(res) {

            if(res.content === false) {
                return humanMsg(notify.saveFail);
            }

            humanMsg(notify.saveSuccess);

        }, 'json');
    }

    //function changeType(type) {
    //    console.log(type);
    //}


    /**
     * Обработчики событий
     */

    // Сохранить изменения
    $('#editor-save').on('click', function(e) {
        var newContent = editor.getValue();
        saveContent(newContent, file);
    });

    // Получить контент при клике на имя файла
    $('body').on('click', '.filename', function(e) {
            e.preventDefault();

            var filename = $(this).data('href');

            getContent(filename);
        })
        .on('mouseover', '.filename', function() {
            $(this).find('.remove-file').show();
        })
        .on('mouseleave', '.filename', function() {
            $(this).find('.remove-file').hide();
        });

    $( ".select-type" ).change(function() {
        var type = $( ".select-type input:checked" ).attr('id');

        if(type == 'file') {
            $('#fname').attr('placeholder', 'Введите название файла');
            $('#select-ext').show();
        } else {
            $('#fname').attr('placeholder', 'Введите название папки');
            $('#select-ext').hide();
        }



    });

    $('#ace-full').click(function() {
        $('.left-block').toggle();
        $('.right-block').toggleClass('col-md-8 col-md-12');
        $(this).toggleClass('fa-caret-square-o-left fa-caret-square-o-right');
    });

    // Скрываем/раскрываем список
    $('.pathname')
        .on('click', function() {
            var $span = $(this);

            $span.toggleClass('fa-plus-square-o fa-minus-square-o');
            $span.parent().find('ul:first').slideToggle();
        })
        .on('mouseover', function() {
            $(this).find('.add-file').show();
        })
        .on('mouseleave', function() {
            $(this).find('.add-file').hide();
        });

    $('body').on('click', '.remove-file', function(e) {
        e.stopPropagation();
        e.preventDefault();

        if(!confirm("Вы действительно хотите удалить?"))
            return;

        var $that = $(this);

        var file = $that.parent().data('href');

        $.post(adminRoot + '/editor-ajax/remove', {file: file}, function(res) {

            if(res.content === false) {
                return humanMsg(notify.removeFail);
            }

            humanMsg(notify.removeSuccess);

            $that.parent().remove();

        }, 'json');
    });


    (function(){
        $fileForm = $('.add-file-form');

        $('.add-file').on('click', function(e) {
            e.stopPropagation();

            var pathname = $(this).parent().data('href');

            $form = $fileForm.show();
            $form.find('#fpath').text(pathname);


            $(this).parent().after($form);
        });

        $('.add-form-close').on('click', function() {
            formHide();
        });

        $('.add-file-form input[type="submit"]').on('click', function(e) {
            e.preventDefault();

            var file = $fileForm.find('#fname').val();
            var type = $fileForm.find('select').val();
            var path = $fileForm.find('#fpath').text();
            var create = $('.select-type input:checked').attr('id');
            var $li = $(this).parents('li:first');

            addNewFile(path, file, type, create, $li);

        });

        function formHide() {
            $fileForm.find('#fname').val('');
            $fileForm.hide();
        }

        function addNewFile(path, file, type, create, $li) {

            var data = {
                path: path,
                file: file,
                type: type,
                create: create
            };

            $.post(adminRoot + '/editor-ajax/add', data, function(res) {

                if(res.content === false) {
                    return humanMsg(notify.addFail[create]);
                }

                humanMsg(notify.addSuccess[create]);

                formHide();

                if(create == 'file') {
                    var ext = $li.find('select option:selected').text();
                    var filename = file + '.' + ext;
                    var dataHref = path + '/' + filename;

                    var $i = '<i class="remove-file"></i>';

                    $newLi = $('<li><a href="#" class="filename active" data-href="'+dataHref+'">'+filename+ $i +'</a></li>');

                    $('#ul-tree a').removeClass('active');
                    $li.find('ul:first').append($newLi);
                    getContent(dataHref);
                } else if(create == 'dir') {
                    location.href = location.href;
                }




            }, 'json');
        }


    })();


});
<?php

namespace Admin\Editor;
use Config;
use Core\Input;
use Core\Helpers;
use Core\Arr;

class Ajax extends \Admin\Base\Ajax {

    public function before() {
        parent::before();
        $this->check_access('editor');
    }

    public function action_get() {
        $filename = Input::get('filename');
        $this->response['content'] = M_editor::instance()->get($filename);
    }

    public function action_edit() {
        $filename = Input::post('filename');
        $content = Input::post('content');

        $this->response['content'] = M_editor::instance()->edit($filename, $content);
    }

    public function action_add() {
        $fields = Arr::extract($_POST, ['file', 'type', 'path', 'create']);
        $this->response['content'] = M_editor::instance()->add($fields);
    }

    public function action_remove() {
        $filename = Input::post('file');
        $this->response['content'] = M_editor::instance()->remove($filename);
    }

    protected function set_error_template(\Core\Errors $errors) {

    }
}

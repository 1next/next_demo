<?php

namespace Admin\Editor;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;
    const ALLOWED_EXT = ['twig', 'css', 'js'];

    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'editor' => [
                    'c' => 'c_editor'
                ],
                'editor-ajax' => [
                    'c' => 'ajax'
                ]
            ],
            'using' => [],
            'registry_privs' => [
                'editor' => 'Редактирование шаблонов'
            ],
            'menu' => [
                'editor' => [
                    'name' => 'Редактор тем',
                    'icon' => 'file-code-o',
                    'priority' => 1,
                    'access' => 'editor'
                ]
            ]
        ];
    }

    public function rules() {
        return [];
    }

}

<div class="form-group">
    <label for="field-{-field-}" class="col-sm-2 control-label">{-label-}</label>
    <div class="col-sm-5">
        {-this-}
    </div>
    <div class="error error-right">
        {-error-}
    </div>
</div>
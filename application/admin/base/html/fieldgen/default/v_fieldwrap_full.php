<div class="form-group">
    <label for="field-{-field-}">{-label-}</label>
    <div>
        {-this-}
    </div>
    <div class="error error-right">
        {-error-}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10">
        <div class="checkbox">
            <label for="field-{-field-}">
                {-this-} {-label-}
            </label>
        </div>
    </div>
    <div class="error error-right">
        {-error-}
    </div>
</div>
<? if($max_page > 1):?>
<nav>
  <ul class="pagination">
    <? if($page_num <= 1): ?>
        <li class="disabled"><span aria-hidden="true">Начало</span></li>
        <li class="disabled"><span aria-hidden="true">Пред.</span></li>
    <? else: ?>
        <li><a href="<?=$admin_root . sprintf($url_self, 1)?>">Начало</a></li>
    <li><a href="<?=$admin_root . sprintf($url_self, $page_num - 1)?>">Пред.</a></li>
    <? endif; ?>
    <? for($i = $left; $i <= $right; $i++):?>
            <? if($i <1 || $i > $max_page) continue;?>
            <? if($i == $page_num): ?>
        <li class="active">
            <span><?=$i?></span>
        </li>
    <? else: ?>
    <li><a href="<?=$admin_root . sprintf($url_self, $i)?>"><?=$i?></a></li>
    <? endif; ?>
    <? endfor; ?>

    <? if($page_num * $on_page >= $count): ?>
        <li class="disabled"><span aria-hidden="true">След.</span></li>
        <li class="disabled"><span aria-hidden="true">Конец</span></li>
    <? else: ?>
    <li><a href="<?=$admin_root . sprintf($url_self, $page_num + 1)?>">След.</a></li>
    <li><a href="<?=$admin_root . sprintf($url_self, $max_page)?>">Конец</a></li>
    <? endif; ?>
  </ul>
</nav>
<? endif; ?>
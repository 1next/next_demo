<?php

namespace Admin\Base;

use Admin\Users\Api\Auth;
use Config;
use Core\Enum;
use Core\Helpers;
use Core\Exceptions;
use Core\Info;
use Core\Html;
use Core\Headers;
use Core\Events;

class Controller extends \Core\Base\Controller {

    protected $module;
    protected $main_model;
    protected $user;

    public function __construct() {
        parent::__construct();

        if (($this->user = Auth::instance()->user()) == null) {
            throw new Exceptions\E404('Unregistered user climbs in admin panel');
        }

        $this->root .= Config::ADMIN_URL . '/';
        $this->template_class->set_base(PATH_APPLICATION . '/admin/');
        $this->template_class->add_global('root', Config::BASE_URL);
        $this->template_class->add_global('admin_root', $this->root);
        $this->template_class->add_global('html', Html::instance());
        $this->to_base_template['scripts'] = [];
        $this->to_base_template['styles'] = [];
        $this->vars_to_js['admin_root'] = $this->root;
    }

    public function before() {
        $this->to_base_template['title'] = '';
        $this->to_base_template['content'] = '';
        $this->to_base_template['menu'] = Info::manifest()['menu'];
        
        $this->add_styles('base', ['bootstrap.min', 'font-awesome.min', 'jquery-ui/jquery-ui.min', 'styles']);
        $this->add_scripts('base', ['lib/jquery.min', 'lib/jquery-ui.min', 'lib/bootstrap.min', 'core/cms', 'init']);
    }

    public function render() {
        $auth = Auth::instance();
        $menu_sort_kp = [];
        $canonical = implode('/', $this->params);
        
        foreach ($this->to_base_template['menu'] as $url => $module) {
            if (isset($module['access']) && !$auth->can($module['access'])) {
                continue;
            }

            $this->to_base_template['menu'][$url]['is_active'] = Helpers::url_strpos0($canonical, $url);

            if ($this->to_base_template['menu'][$url]['is_active'] && isset($module['open'])) {
                foreach ($module['open'] as $u => $m) {
                    if (isset($m['access']) && !$auth->can($m['access'])) {
                        continue;
                    }

                    $this->to_base_template['menu'][$url]['open'][$u]['is_active'] = Helpers::url_strpos0($canonical, $u);
                }
            }

            $pr = isset($module['priority']) ? (int) $module['priority'] : 0;
            $menu_sort_kp[$pr][] = $url;
        }

        ksort($menu_sort_kp);
        $this->to_base_template['menu_sort_kp'] = array_reverse($menu_sort_kp);
        
        $ck_plugins = Events::run('System.CKeditor.Plugins', []);
        $this->vars_to_js['CKExtraPlugins'] = $ck_plugins[0];
        
        $this->to_base_template['vars_to_js'] = $this->vars_to_js;
        
        return $this->template('base/html/v_main', $this->to_base_template);
    }
    
    protected function set_content_404($e) {
        Headers::c404();
        
        $this->to_base_template['title'] = 'Страница не найдена';
        $this->to_base_template['content'] = $this->template('base/html/v_404');
        
        if (Config::WORK_MODE == Enum::WORK_MODE_DEVELOPMENT) {
            $this->to_base_template['content'] .= $this->template('base/html/v_exception', ['e' => $e]);
        }
    }
    
    protected function set_content_access($e) {
        $this->to_base_template['title'] = 'Недостаточно прав';
        $this->to_base_template['content'] = $this->template('base/html/v_access');
        
        if (Config::WORK_MODE == Enum::WORK_MODE_DEVELOPMENT) {
            $this->to_base_template['content'] .= $this->template('base/html/v_exception', ['e' => $e]);
        }
    }

    protected function check_access($priv) {
        if(!Auth::instance()->can($priv)){
            throw new Exceptions\Access('User "' . $this->user['login'] . '" does not have priv "' . $priv . '"');
        }
    }

    protected function set_error_template(\Core\Errors $errors) {
        $this->to_base_template['title'] = 'Ошибка при выполнении операции';
        $this->to_base_template['content'] = $this->template('base/html/v_some_error', ['errors' => $errors]);
    }

}

<?php

namespace Admin\Base;
use Config;
use Core\Errors;
use Core\Html;

class Fieldgen {

    private $labels;
    private $template;
    private $errors;
    private $theme;
    private $template_name;

    const PATH = PATH_APPLICATION . '/admin/base/html/fieldgen/';
    
    public function __construct($labels, Errors &$errors) {
        $this->labels = $labels;
        $this->errors = &$errors;
        $this->theme = Config::FIELDGEN_THEME;
        $this->template_name = 'v_fieldwrap.php';
        $this->template = file_get_contents(self::PATH . $this->theme . '/' . $this->template_name);
    }

    public function set_template($template) {
        $this->template_name = $template;
        $this->template = file_get_contents(self::PATH . $this->theme . '/' . $this->template_name);
        return $this;
    }
    
    public function set_theme($theme) {
        $this->theme = $theme;
        $this->template = file_get_contents(self::PATH . $this->theme . '/' . $this->template_name);
        return $this;
    }

    /**
     * 
     * @param \Core\Html\Elem Объект тега
     * @param string $key Ключ поля, который будет установлен в id и name
     * @return \Core\Html\Elem Модифицированный объект тега
     */
    public function set(Html\Elem $elem, $key) {
        if($elem instanceof Html\Input || $elem instanceof Html\Textarea || $elem instanceof Html\Select){
            $elem->addClass('form-control');
        }
        
        $wrap = str_replace('{-field-}', $key, $this->template);
        $wrap = str_replace('{-label-}', $this->labels[$key], $wrap);
        $wrap = str_replace('{-error-}', $this->errors->get($key), $wrap);
        return $elem->idn($key)->wrap($wrap);
    }

}

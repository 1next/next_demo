<?php

namespace Admin\Base;

use Admin\Users\Api\Auth;
use Core\Exceptions;
use Core\Info;
use Core\Enum;

abstract class Ajax extends \Core\Base\Controller {

    protected $to_base_template = array();
    protected $module;
    protected $user;
    protected $response;

    public function __construct() {
        parent::__construct();

        $this->response = [];
        Info::execute_mode(Enum::EXECUTE_MODE_AJAX);
        
        if (($this->user = Auth::instance()->user()) == null) {
            throw new Exceptions\E404('Unregistered user climbs in admin panel');
        }
    }

    public function before() {}

    public function render() {
        return json_encode($this->response);
    }

    protected function set_content_404($e) {
        $this->response['errorCode'] = '404';
    }
    
    protected function set_content_access($e) {
        $this->response['errorCode'] = '403';
    }

    protected function check_access($priv) {
        if(!Auth::instance()->can($priv)){
            throw new Exceptions\Access('User "' . $this->user['login'] . '" does not have priv "' . $priv . '"');
        }
    }

}

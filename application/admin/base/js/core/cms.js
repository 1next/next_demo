$.event.props.push('dataTransfer');

$.datepicker.regional['ru'] = {
    closeText: 'Закрыть',
    prevText: '&#x3c;Пред',
    nextText: 'След&#x3e;',
    currentText: 'Сегодня',
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
        'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
    dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false
};

$.datepicker.setDefaults($.datepicker.regional['ru']);

var CMS_CORE = function () {};
CMS_CORE.prototype = {};

CMS_CORE.prototype.datepicker = function (selector, format, value) {
    this.elem = $(selector);
    var f = format || "yy-mm-dd";
    var v = value || this.elem.attr('value');

    this.elem.datepicker();
    this.elem.datepicker("option", "dateFormat", f);
    this.elem.datepicker("setDate", v);
};

CMS_CORE.prototype.ckeditor = function (ck_object, id_field) {
    if ($('#' + id_field).length > 0) {
        ck_object.config.height = 400;

        ck_object.replace(id_field);

        ck_object.timestamp = (new Date().getTime());       
        ck_object.config.extraAllowedContent = 'div[data-*]; code(*)';
        
        /* ck_object.config.toolbar = 'Full'; */
        ck_object.config.removePlugins = 'save,liststyle,tabletools,scayt,menubutton,contextmenu,base64image,newpage,preview,print,find,selectall,language,forms,iframe,flash,wsc,showblocks';
        ck_object.config.extraPlugins = _PHP.CKExtraPlugins.join(',');
    }

    return ck_object;
}

CMS_CORE.prototype.dragandgrop = function (settings) {
    var defaults = {
        drag_wrapper: '.draganddrop .drag',
        drop_wrapper: '.draganddrop .drop',
        success_drop: function () {}
    };

    var params = $.extend(defaults, settings);
    var $ul = $(params.drop_wrapper).find('ul');

    var start = $ul.data('start').toString();

    if (start !== '') {
        var to_find = start.split(',');
        var $li = $(params.drag_wrapper).find('ul');

        for (var i in to_find) {
            var $one = $li.find('li[data-id=' + to_find[i] + ']');

            if ($one.length > 0) {
                $ul.append($one.clone());
            }
        }
    }

    $(params.drag_wrapper).find('ul li').draggable({
        helper: "clone"
    });

    $(params.drop_wrapper).droppable({
        drop: function (event, ui) {
            var id = ui.draggable.data('id');
            var exists = $ul.find('li[data-id=' + id + ']');

            if (exists.length > 0) {
                exists.effect('highlight', {}, 500);
            } else {
                $(this).find('ul').append(ui.draggable.clone());
                params.success_drop();
            }
        }
    });

    $ul.on('dblclick', 'li', function () {
        $(this).remove();
    });

    this.droped_items_ul = $ul;

    this.getItems = function () {
        var items = [];

        $ul.find('li').each(function () {
            items.push($(this).data('id'));
        });

        return items;
    }
}

CMS_CORE.prototype.sortable = function (selector) {
    this.elem = $(selector);

    this.elem.sortable();

    this.getItems = function () {
        var items = [];

        this.elem.find('> *').each(function () {
            items.push($(this).data('id'));
        });

        return items;
    }
}

CMS_CORE.prototype.imagedrop = function (upload_url, settings) {
    var defaults = {
        data_to_php: {},
        max_files_upload: 10,
        max_filesize: 2 * 1024 * 1024,
        drop_wrapper: '.images-upload-wrapper',
        drop_zone: '.images-drop',
        dropped_files: '.dropped-files',
        images_form: '.images-form',
        uploaded_holder: '.uploaded-holder',
        upload_buttons: '.upload-buttons',
        start_upload_button: '.upload',
        delete_all_button: '.delete',
        loading_messages: '.loading-messages',
        for_upload_count: '.for-upload-count',
        on_success_upload: function(data){}
    };

    var params = $.extend(defaults, settings);

    var wrapper = $(params.drop_wrapper);
    var dropzone = wrapper.find(params.drop_zone);
    var dropped_files = wrapper.find(params.dropped_files);
    var images_form = wrapper.find(params.images_form);
    var uploaded_holder = wrapper.find(params.uploaded_holder);
    var upload_buttons = uploaded_holder.find(params.upload_buttons);
    var start_upload_button = upload_buttons.find(params.start_upload_button);
    var delete_all_button = upload_buttons.find(params.delete_all_button);
    var loading_messages = uploaded_holder.find(params.loading_messages);
    var for_upload_count = upload_buttons.find(params.for_upload_count);

    var dataArray = [];

    dropzone.find('input[type=file]').on('change', function () {
        load(this.files);
        images_form[0].reset();
    });

    dropzone.on('drop', function (e) {
        load(e.dataTransfer.files);
    });

    dropzone.on('dragenter', function (e) {
        $(this).addClass('dragged');
        e.preventDefault();
    });

    dropzone.on('drop', function (e) {
        $(this).removeClass('dragged');
        e.preventDefault();
    });

    dropzone.on('dragover', function (e) {
        e.preventDefault();
    });

    dropped_files.on('click', "span.drop-button", function () {
        var elid = $(this).data('id');
        dataArray.splice(elid, 1);
        dropped_files.find('>.image').remove();
        addImage(-1);
    });

    delete_all_button.click(function () {
        restart();
    });

    start_upload_button.click(function () {
        upload();
    });

    function load(files) {
        if (files.length <= params.max_files_upload) {
            preview(files);
        } else {
            alert("Максимально допустимое количество изображений:  " + params.max_files_upload);
            files.length = 0;
            return;
        }
    }

    function preview(files) {
        uploaded_holder.show();

        $.each(files, function (index, file) {
            if (!files[index].type.match('image.*')) {
                alert('Не-не-не! Только изображения!');
                return false;
            }

            if ((dataArray.length + files.length) <= params.max_files_upload) {
                upload_buttons.fadeIn(500);
            } else {
                alert("Максимально допустимое количество изображений:  " + params.max_files_upload);
                return;
            }

            var fileReader = new FileReader();

            fileReader.onload = (function (file) {
                return function (e) {
                    dataArray.push({name: file.name, value: this.result});
                    addImage((dataArray.length - 1));
                };
            })(files[index]);

            fileReader.readAsDataURL(file);
        });

        return false;
    }

    function addImage(ind) {
        if (ind < 0) {
            start = 0;
            end = dataArray.length;
        } else {
            start = ind;
            end = ind + 1;
        }

        if (dataArray.length === 0) {
            upload_buttons.hide();
            uploaded_holder.hide();
        } else {
            upload_buttons.find(for_upload_count).html("Выбрано файлов: " + dataArray.length);
        }

        for (i = start; i < end; i++) {
            if (dropped_files.find('> .image').length <= params.max_files_upload) {
                dropped_files.append('<div class="image" style="background: url(' + dataArray[i].value + '); background-size: cover;"> <span class="drop-button" data-id="' + i + '">Удалить</span></div>');
            }
        }

        return false;
    }

    function restart() {
        upload_buttons.hide();
        dropped_files.find('> .image').remove();
        uploaded_holder.hide();
        dataArray.length = 0;
        loading_messages.empty();
    }

    function upload() {
        upload_buttons.hide();
        dropped_files.find('> .image').hide();        
        sendSequence(dataArray, function(){
            restart();
        });
    }
    
    function sendSequence(images, callback){
        if(images.length === 0){
            callback();
            return true;
        }
        
        var send_data = images.shift();
        
        for(var k in params.data_to_php){
            if(send_data[k] === undefined){
                send_data[k] = params.data_to_php[k];
            }
        }
        
        $.post(_PHP.admin_root + upload_url, send_data, function(data){
            var msg;
            
            if(data.res){
                msg = $('<div/>').html(data.image.name + ' - загружен успешно!');
                params.on_success_upload(data.image);
            }
            else{
                msg = $('<div/>').html(data.errors);
            }
                
            loading_messages.append(msg);
            sendSequence(images, callback);
        }, 'json');
    }
}

var CMS = new CMS_CORE();
CKEDITOR.plugins.add('cms_image', {
    onLoad: function () {
        CKEDITOR.addCss(".cke_cms_image{width: 50px; height: 50px; border: 1px solid red;}");
    },
    init: function (editor)
    {
        
    },
    afterInit: function (editor) {
        var dataProcessor = editor.dataProcessor;
        (dataProcessor = dataProcessor && dataProcessor.dataFilter) && dataProcessor.addRules({
            elements: {
                div: function (dataProcessor) {
                    if (dataProcessor.attributes['data-widget'] == 'image') {
                        fakeElem = editor.createFakeParserElement(dataProcessor, "cke_cms_image", "widget", !0);
                        fakeElem.attributes.title = 'Картинка по id';
                        return fakeElem;
                    }
                }
            }
        })
    }
});
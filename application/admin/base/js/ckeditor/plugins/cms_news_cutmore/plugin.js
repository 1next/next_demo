CKEDITOR.plugins.add('cms_news_cutmore', {
    onLoad: function () {
        CKEDITOR.addCss(".cke_cut_more{height:5px;width:100%;border-top:1px dotted red;border-bottom:1px dotted red;margin:10px 0;}");
    },
    init: function (editor)
    {
        editor.addCommand('cms_news_cutmore', {
           exec: function(editor){
               editor.insertHtml('<div data-widget="cut-more"></div>');
           } 
        });
        
        editor.ui.addButton('Cms_news_cutmore', {
            label: 'Добавить тег no-more для обрезки контента',
            command: 'cms_news_cutmore',
            icon: this.path + 'icons/cmscutmore.png',
            toolbar: 'insert'
        });
    },
    afterInit: function (editor) {
        var dataProcessor = editor.dataProcessor;
        (dataProcessor = dataProcessor && dataProcessor.dataFilter) && dataProcessor.addRules({
            elements: {
                div: function (dataProcessor) {
                    if (dataProcessor.attributes['data-widget'] == 'cut-more') {
                        var fakeElem = editor.createFakeParserElement(dataProcessor, "cke_cut_more", "widget", !0);
                        fakeElem.attributes.title = 'Отрезаем контент здесь';
                        return fakeElem;
                    }
                }
            }
        });
    }
});
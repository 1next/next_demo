<?php

namespace Admin\Reviews;

class M_reviews extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'reviews');
    }

}

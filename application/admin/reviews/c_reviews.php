<?php

namespace Admin\Reviews;

use Core\Traits\Actions;
use Core\Exceptions;
use Core\Arr;
use Admin\Base\Fieldgen;
use Admin\Cats\Api\Main as Cats;

class C_reviews extends \Admin\Base\Controller {

use Actions\Page;
use Actions\Add;
use Actions\Edit;
use Actions\Delete;

    public function before() {
        parent::before();
        
        $this->check_access('reviews');
        $this->module = 'reviews';
        $this->main_model = M_reviews::instance();
    }

    protected function page_num(){
        return isset($this->params[3]) ? (int) $this->params[3] : 1;
    }
    
    protected function settings_action_page() {
        //$filters = ['all', 'unmoderate'];
        
        if(!isset($this->params[2])){
            $this->redirect($this->root . $this->module . '/page/all');
        }
        
        if($this->params[2] == 'unmoderate'){
            $this->main_model->pager()->where("is_moderate='0'");
        }
        elseif($this->params[2] != 'all'){
            throw new Exceptions\E404("Undefined pagination mode {$this->params[2]}");
        }       
        
        $this->main_model->pager()->url_self($this->module . '/page/' . $this->params[2] . '/');        
        $this->to_base_template['title'] .= 'Список отзывов: страница №' . $this->page_num;
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление нового отзыва';

        $this->add_scripts('base', ['lib/jquery.liTranslit', 'translit_init', 'ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init']);

        if (isset($this->params[2]) && is_numeric($this->params[2])) {
            $this->add_fields_data = $this->main_model->get($this->params[2]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[2]} not found.");
            }
            

        } else {
            $this->add_fields_data = ['is_show' => 1, 'is_moderate' => 0];
        }

        $this->add_extract = ['title', 'id_cat', 'author', 'announcement', 'url', 'content', 'dt', 'is_show', 'is_moderate'];
        $this->add_checkboxes_detect = ['is_show', 'is_moderate'];
        
        $tree = Cats::instance()->tree(App::CAT_TYPE);
        $this->add_other_data['cats'] = Arr::to_select_tree($tree, 'id_cat', 'name');
        
        $this->add_fieldgen = new Fieldgen($this->app->labels('reviews'), $this->errors);
    }

    protected function settings_action_edit() {
        
        $this->to_base_template['title'] .= 'Редактирование отзыва';
        
        $this->add_scripts('base', ['ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init']);

        $this->edit_extract = ['title', 'id_cat', 'author', 'announcement', 'url', 'content', 'dt', 'is_show', 'is_moderate'];
        $this->edit_checkboxes_detect = ['is_show', 'is_moderate'];
        
        $tree = Cats::instance()->tree(App::CAT_TYPE);
        $this->edit_other_data['cats'] = Arr::to_select_tree($tree, 'id_cat', 'name');
        $this->edit_template = "{$this->module}/v/v_add";
        $this->edit_fieldgen = new Fieldgen($this->app->labels('reviews'), $this->errors);
    }

    protected function settings_action_delete() {
        
    }

}

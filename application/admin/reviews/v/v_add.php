<form method="post">
    <?= $f->set($html->select_tree($data['cats'], $fields['id_cat']), 'id_cat') ?>
    <?= $f->set($html->input()->val($fields['title']), 'title') ?>
    <?= $f->set($html->input()->val($fields['author']), 'author') ?>
    <?= $f->set($html->input()->val($fields['announcement']), 'announcement') ?>
    <?= $f->set($html->input()->val($fields['url']), 'url') ?>
    <?= $f->set($html->input()->val($fields['dt']), 'dt') ?>
    <?= $f->set_template('v_fieldwrap_full.php')
          ->set($html->textarea()->val($fields['content']), 'content') ?>
    <?= $f->set_template('v_fieldwrap_checkbox_inline.php')
          ->set($html->input('checkbox')
          ->battr('checked', (isset($fields['is_show']) && $fields['is_show'] != 0)), 'is_show')
          ->attrs(['class' => ''])
    ?>
    <?= $f->set_template('v_fieldwrap_checkbox_inline.php')
          ->set($html->input('checkbox')
          ->battr('checked', (isset($fields['is_moderate']) && $fields['is_moderate'] != 0)), 'is_moderate')
          ->attrs(['class' => ''])
    ?>
    <?=$html->input('hidden')->name('tags_id') ?>
    <div class="submit-buttons">
        <input type="submit" value="Сохранить" class="btn btn-success">
        <a class="btn btn-danger" href="<?=$admin_root . $back ?>">
            Закрыть без сохранения
        </a>
    </div>
</form>
<div class="modal-dialog" title="Добавление тега">
    
</div>
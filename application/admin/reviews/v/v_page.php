<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Заголовок</th>
                <th>URL</th>
                <th>Дата</th>
                <th>Отображение</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr <?= $one['is_show'] ? '' : 'class="danger"' ?>>
                    <td><?= $one['title'] ?></td>
                    <td><?= $one['url'] ?></td>
                    <td><?= $one['dt'] ?></td>
                    <td><?= $one['is_show'] ? 'Да' : 'Нет' ?></td>
                    <td>
                        <a href="<?=$admin_root?>reviews/edit/<?= $one['id_review'] ?>" id_newtitle="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>reviews/delete/<?= $one['id_review'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>reviews/add/<?= $one['id_review'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p>
        <a class="btn btn-primary btn" href="<?=$admin_root?>reviews/add/">
            Добавить отзыв &raquo;
        </a>
    </p>
</div>
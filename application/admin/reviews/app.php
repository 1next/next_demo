<?php

namespace Admin\Reviews;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    const CAT_TYPE = 'shop';
    const CAT_NAME = 'Для магазина';

    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'reviews' => [
                    'c' => 'c_reviews'
                ],
                'reviews_ajax' => [
                    'c' => 'ajax'
                ]
            ],
            'using' => [
                'users',
                'cats' => [
                    'cat_type' => self::CAT_TYPE,
                    'cat_name' => self::CAT_NAME
                ]
            ],
            'registry_privs' => [
                'reviews' => 'Работа с отзывами'
            ],
            'menu' => [
                'reviews' => [
                    'name' => 'Отзывы',
                    'icon' => 'star-o',
                    'open' => [
                        'reviews/page/all' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'reviews/page/unmoderate' => [
                            'name' => 'Новые',
                            'icon' => 'list-ul'
                        ],
                        'reviews/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ]
                    ],
                    'priority' => 33,
                    'access' => 'reviews'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'reviews_',
            'tables' => [
                'reviews' => [
                    'fields' => ['id_review', 'id_cat', 'title', 'author', 'announcement', 'content', 'url', 'dt', 'is_show', 'is_moderate'],
                    'not_empty' => ['title', 'id_cat', 'author', 'announcement', 'content', 'url', 'dt'],
                    'unique' => ['url', 'content'],
                    'html_allowed' => ['content'],
                    'correct_url' => ['url'],
                    'date' => ['dt'],
                    'range' => [
                        'title' => ['10', '128'],
                        'announcement' => ['50', '180'],
                        'url' => ['2', '128'],
                        'content' => ['100', '65535']
                    ],
                    'labels' => [
                        'id_cat' => 'Категория',
                        'title' => 'Название отзыва',
                        'author' => 'Автор отзыва',
                        'announcement' => 'Анонс отзыва',
                        'url' => 'URL',
                        'content' => 'Содержание',
                        'dt' => 'Дата',
                        'is_show' => 'Отображать',
                        'is_moderate' => 'Модерация'
                    ],
                    'pk' => 'id_review'
                ]
            ]
        ];
    }

}

$(function () {
    
    new CMS.datepicker('input[name=dt]'); 
    new CMS.ckeditor(CKEDITOR, 'field-content')
    var drads = new CMS.dragandgrop();

    $('input[type=submit]').click(function (e) {
        var items = drads.getItems();
        $('input[name=tags_id]').val(items.toString());
    });

    var form_loaded = false;
    var form_loading = false;

    $modal = $('.modal-dialog');
    $drag = $('.drag ul');

    $modal.dialog({
        modal: true,
        draggable: false,
        autoOpen: false,
        minWidth: 480,
        maxWidth: 700,
        show: 'blind',
        hide: 'blind',
        buttons: {
            'Сохранить': function () {
                $.post(_PHP.admin_root + 'news_ajax/add_tag', $modal.find('form').serialize(), function (data) {
                    if(data.res){
                        $modal.dialog('close');
                        $('<li/>').draggable({helper: "clone"}).html(data.tag.name).attr('data-id', data.tag.id_tag).appendTo($drag);
                        $modal.find('form').find('input, select, textarea').val('');
                    }
                    else{
                        $.each(data.errors, function(key, elem){
                            $modal.find('input[name="' + key + '"]').parent().next().html(elem);
                        });
                    }
                }, 'json');
            },
            'Отмена': function () {
                $modal.dialog('close');
            }
        }
    });

    $('.add_tag').click(function () {
        if (form_loading) {
            return;
        }

        if (!form_loaded) {
            form_loading = true;
            form_loaded = true;

            $.get(_PHP.admin_root + 'news_ajax/get_tags_form', {}, function (data) {
                var form = $('<form>').append(data.form);
                $modal.append(form).dialog('open');
                form_loading = false;
            }, 'json');
        } else {
            $modal.dialog('open');
        }
    });
});
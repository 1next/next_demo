<?php

namespace Admin\News;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    const TAG_TYPE = 'news';
    const TAG_NAME = 'Для новостей';
    const TAG_RELATION = 'news';
    const CAT_TYPE = 'news';
    const CAT_NAME = 'Для новостей';

    public $visible_states = ['Отображается в ленте', 'Доступна только по ссылке',
                              'Доступ для администраторов', 'Скрыта от всех'];

    public function init() {
        Events::add('System.CKeditor.Plugins', function(&$plugins){
            $plugins[] = 'cms_news_cutmore';
        });
    }

    public function manifest() {
        return [
            'routs' => [
                'news' => [
                    'c' => 'c_news'
                ],
                'news_ajax' => [
                    'c' => 'ajax'
                ]
            ],
            'using' => [
                'users',
                'tags' => [
                    'tag_type' => self::TAG_TYPE,
                    'tag_name' => self::TAG_NAME,
                    'tag_item' => self::TAG_RELATION
                ],
                'cats' => [
                    'cat_type' => self::CAT_TYPE,
                    'cat_name' => self::CAT_NAME
                ]
            ],
            'registry_privs' => [
                'news' => 'Работа с новостями'
            ],
            'menu' => [
                'news' => [
                    'name' => 'Новости',
                    'icon' => 'newspaper-o',
                    'open' => [
                        'news/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'news/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ]
                    ],
                    'priority' => 33,
                    'access' => 'news'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'news_',
            'tables' => [
                'news' => [
                    'fields' => ['id_new', 'id_cat', 'title', 'url', 'content', 'dt', 'visible'],
                    'not_empty' => ['title', 'id_cat', 'url', 'content', 'dt'],
                    'unique' => ['url'],
                    'html_allowed' => ['content'],
                    'correct_url' => ['url'],
                    'date' => ['dt'],
                    'range' => [
                        'title' => ['2', '255'],
                        'url' => ['2', '128'],
                        'content' => ['2', '65535']
                    ],
                    'labels' => [
                        'title' => 'Заголовок',
                        'id_cat' => 'Категория',
                        'url' => 'URL',
                        'content' => 'Содержание',
                        'dt' => 'Дата',
                        'visible' => 'Отображение',
                    ],
                    'pk' => 'id_new'
                ]
            ]
        ];
    }

}

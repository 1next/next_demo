<?php

namespace Admin\News;
use Admin\Tags\Api\Main as Tags;

class M_news extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'news');
    }

    public function add($fields){
        $res = parent::add($fields);
        
        if($res){
            $tags_id = ($fields['tags_id'] == '') ? [] : explode(',', $fields['tags_id']);
            Tags::instance()->add_relations($res, App::TAG_RELATION, $tags_id);
        }
        
        return $res;
    }
    
    public function edit($pk, $fields){
        $res = parent::edit($pk, $fields);
        
        if($res){
            $tags_id = ($fields['tags_id'] == '') ? [] : explode(',', $fields['tags_id']);
            Tags::instance()->edit_relations($pk, App::TAG_RELATION, $tags_id);
        }
        
        return $res;
    }
    
    public function delete($pk){
        Tags::instance()->delete_relations($pk, App::TAG_RELATION);
        parent::delete($pk);
    }
}

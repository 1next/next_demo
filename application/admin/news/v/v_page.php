<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Заголовок</th>
                <th>URL</th>
                <th>Дата</th>
                <th>Отображение</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr>
                    <td><?= $one['title'] ?></td>
                    <td><?= $one['url'] ?></td>
                    <td><?= $one['dt'] ?></td>
                    <td><?= $other['visible_states'][$one['visible']]?></td>
                    <td>
                        <a href="<?=$admin_root?>news/edit/<?= $one['id_new'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>news/delete/<?= $one['id_new'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>news/add/<?= $one['id_new'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p>
        <a class="btn btn-primary btn" href="<?=$admin_root?>news/add/">
            Добавить новость &raquo;
        </a>
    </p>
</div>
<form method="post">
    <?= $f->set($html->select_tree($data['cats'], $fields['id_cat']), 'id_cat')?>
    <?= $f->set($html->input()->val($fields['title']), 'title') ?>
    <?= $f->set($html->input()->val($fields['url']), 'url') ?>
    <?= $f->set($html->input()->val($fields['dt']), 'dt') ?>
    <?= $f->set_template('v_fieldwrap_full.php')
          ->set($html->textarea()->val($fields['content']), 'content') ?>
    <?=$f->set_template('v_fieldwrap.php')
        ->set($html->select($data['visible_states'], $fields['visible']), 'visible')?>
    <?=$html->input('hidden')->name('tags_id') ?>
    <h4>Выберите теги</h4>
    <div class="draganddrop">
        <div class="drop">
            <ul data-start="<?=$fields['tags_id']?>"></ul>
        </div>
        <div class="drag">            
            <span class="add_tag" title="Добавить тег">+</span>
            <?=$html->ul($data['tags'], function($li, $item){
                $li->html($item['name'])->attr('data-id', $item['id_tag']);
            });?>
        </div>
    </div>
    <div class="submit-buttons">
        <input type="submit" value="Сохранить" class="btn btn-success">
        <a class="btn btn-danger" href="<?=$admin_root . $back ?>">
            Закрыть без сохранения
        </a>
    </div>
</form>
<div class="modal-dialog" title="Добавление тега"></div>
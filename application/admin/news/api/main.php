<?php

namespace Admin\News\Api;

use Admin\News as News;
use Core\Sql;

class Main {

    use \Core\Traits\Singleton;

    protected $mNews;
    protected $db;

    protected function __construct() {
        $this->mNews = News\M_news::instance();
        $this->db = Sql::instance();
    }

    public function all_visible() {
        return $this->mNews->get_by_fieldset(['visible' => '0']);
    }

}

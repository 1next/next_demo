<?php

namespace Admin\News;

use Core\Traits\Actions;
use Core\Exceptions;
use Core\Arr;
use Admin\Base\Fieldgen;
use Admin\Tags\Api\Main as Tags;
use Admin\Cats\Api\Main as Cats;

class C_news extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();

        $this->check_access('news');

        $this->module = 'news';
        $this->main_model = M_news::instance();
    }

    protected function settings_action_page() {
        $this->to_base_template['title'] .= 'Список новостей: страница ' . $this->page_num;
        $this->page_other_data['visible_states'] = $this->app->visible_states;
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление новости';

        $this->add_scripts('base', ['lib/jquery.liTranslit', 'translit_init', 'ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init']);

        if (isset($this->params[2]) && is_numeric($this->params[2])) {
            $this->add_fields_data = $this->main_model->get($this->params[2]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[2]} not found.");
            }
            
            $tags_id = Tags::instance()->get_tags_id(App::TAG_RELATION, $this->params[2]);
            $this->add_fields_data['tags_id'] = implode(',', $tags_id);
        } else {
            $this->add_fields_data = ['is_show' => 1, 'tags_id' => ''];
        }

        $this->add_extract = ['title', 'id_cat', 'url', 'content', 'dt', 'visible', 'tags_id'];
        
        $this->add_other_data['tags'] = Tags::instance()->all_by_type(App::TAG_TYPE);
        $this->add_other_data['cats'] = Arr::to_select_tree(Cats::instance()->tree(App::CAT_TYPE), 'id_cat', 'name');
        $this->add_other_data['visible_states'] = $this->app->visible_states;
        
        $this->add_fieldgen = new Fieldgen($this->app->labels('news'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование новости';

        $this->add_scripts('base', ['ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init']);

        $this->edit_extract = ['title', 'id_cat', 'url', 'content', 'dt', 'visible', 'tags_id'];
        
        $this->edit_other_data['tags'] = Tags::instance()->all_by_type(App::TAG_TYPE);
        $this->edit_other_data['cats'] = Arr::to_select_tree(Cats::instance()->tree(App::CAT_TYPE), 'id_cat', 'name');
        $this->edit_other_data['visible_states'] = $this->app->visible_states;
        
        $tags_id = Tags::instance()->get_tags_id(App::TAG_RELATION, $this->edit_id);
        $this->edit_fields_data['tags_id'] = implode(',', $tags_id);

        $this->edit_template = "{$this->module}/v/v_add";
        $this->edit_fieldgen = new Fieldgen($this->app->labels('news'), $this->errors);
    }

    protected function settings_action_delete() {
        
    }

}

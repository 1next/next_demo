<?php

namespace Admin\News;

use Admin\Tags\Api\Bridge as Tags;

class Ajax extends \Admin\Base\Ajax {

    public function before() {
        parent::before();
        $this->check_access('news');
    }

    public function action_add_tag() {    
        $mTags = Tags::instance();
        
        if($id = $mTags->add($_POST)){            
            $this->response['res'] = true;
            $this->response['tag'] = $mTags->get($id);
        }
        else{
            $this->response['res'] = false;
            $this->response['errors'] = $mTags->errors();
        }
    }

    public function action_get_tags_form() {        
        $this->response['form'] = Tags::instance()->get_ajax_form(App::TAG_TYPE);
    }

    protected function set_error_template(\Core\Errors $errors) {
        
    }

}

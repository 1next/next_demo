<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Название</th>
                <th>Права</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $role): ?>
                <tr>
                    <td><?= $role['description'] ?></td>
                    <td>
                        <ul>
                            <?php
                            $privs = explode(',', $role['privs']);

                            foreach ($privs as $name):
                                ?>
                                <li><?= $other['privs'][$name] ?></li>
                            <? endforeach; ?>
                        </ul>
                    </td>
                    <td>
                        <a href="<?=$admin_root?>users/roles/edit/<?= $role['id_role'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>users/roles/delete/<?= $role['id_role'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>users/roles/add/<?= $role['id_role'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?=$navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>users/roles/add/">Добавить новую роль &raquo;</a></p>
</div>  
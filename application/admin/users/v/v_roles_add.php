<form method="post">
    <?= $f->set($html->input()->val($fields['description']), 'description') ?>
    <?= $f->set($html->select($data['privs'], is_array($fields['privs']) ? $fields['privs'] : explode(',', $fields['privs']))->battr('multiple'), 'privs')->name('privs[]') ?>
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
</form>
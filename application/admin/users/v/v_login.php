<!DOCTYPE html>
<html>
    <head>
        <base href="<?= \Config::BASE_URL ?>">
        <title><?= $title ?></title>
        <meta charset="utf-8">
        <style>
            html,body{min-height: 100%; height:100%; overflow: hidden; background: #999;}
            form{width: 400px; margin: 0 auto; margin-top: 20%;}
            form input{width: 100%; padding: 5px;}
            form input[type=checkbox]{width: auto;}
            .error{color: #ff0;}
        </style>
    </head>
    <body>
        <form method="post">
            <h1><?= $title ?></h1>
            <p class="error">
                <? if ($error): ?>
                    Неправильные логин или пароль
                <? endif; ?>
            </p>
            <label>
                Логин:<br>
                <input name="login" type="text">
            </label>
            <br>
            <label>
                Пароль:<br> 
                <input name="password" type="password">
            </label>
            <br>
            <label><input type="checkbox" name="remember"> запомнить меня</label><br><br>
            <input type="submit" class="btn btn-success" value="Войти">
        </form>
    </body>
</html>



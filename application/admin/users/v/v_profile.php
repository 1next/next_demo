<form method="post">
    <?= $f->set($html->input()->val($fields['name']), 'name') ?>
    <?= $f->set($html->input('password'), 'old_password') ?>
    <?= $f->set($html->input('password'), 'password') ?>
    <?= $f->set($html->input('password'), 'repeat_password') ?>
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="javascript:history.back()">Закрыть без сохранения</a>
</form>
<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Имя</th>
                <th>Логин</th>
                <th>Права доступа</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $user): ?>
                <tr>
                    <td><?= $user['name'] ?></td>
                    <td><?= $user['login'] ?></td>
                    <td><?= $user['description'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>users/edit/<?= $user['id_user'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>users/delete/<?= $user['id_user'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>users/add/">Добавить нового пользователя &raquo;</a></p>
</div>
<form method="post">
    <?= $f->set($html->input()->val($fields['name']), 'name') ?>
    <?= $f->set($html->input()->val($fields['login']), 'login') ?>
    <?= $f->set($html->input('password')->val($fields['password']), 'password') ?>
    <?= $f->set($html->select($data['roles'], $fields['id_role']), 'id_role') ?>
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
</form>
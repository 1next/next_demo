<?php

namespace Admin\Users;

class M_users extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'users');
    }

    public function edit($pk, $fields) {
        if ($fields['password'] == '') {
            unset($fields['password']);
        }

        return parent::edit($pk, $fields);
    }

}

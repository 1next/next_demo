<?php

namespace Admin\Users;

use Core\Arr;
use Admin\Base\Fieldgen;
use Core\Traits\Actions;
use Core\Crypt;
use Core\Exceptions;

class C_users extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();
        $this->module = 'users';
        $this->main_model = M_users::instance();
    }

    protected function settings_action_page() {
        $this->check_access('users');
        $this->to_base_template['title'] .= 'Список пользователей: страница ' . $this->page_num;
        $this->main_model->pager()->left_join($this->main_model->prefix() . 
                                            $this->app->prefix($this->module) . 
                                            'roles using(id_role)');
    }

    protected function settings_action_add() {
        $this->check_access('users');
        $this->to_base_template['title'] .= 'Добавление пользователя';
        $this->add_extract = ['name', 'login', 'password', 'id_role'];
        $this->add_fields_data = ['id_role' => 1];
        $this->add_other_data['roles'] = Arr::to_select(M_roles::instance()->all(), 'id_role', 'description');
        $this->add_fieldgen = new Fieldgen($this->app->labels('users'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->check_access('users');
        $this->to_base_template['title'] .= 'Редактирование пользователя';
        $this->edit_extract = ['name', 'login', 'password', 'id_role'];
        $this->edit_other_data['roles'] = Arr::to_select(M_roles::instance()->all(), 'id_role', 'description');
        $this->edit_fieldgen = new Fieldgen($this->app->labels('users'), $this->errors);
    }

    protected function settings_action_delete() {
        $this->check_access('users');
    }

    public function action_profile(){
        $this->to_base_template['title'] .= $this->user['login'] . ' - редактирование профиля';        
        
        $form = [
            'name' => 'Имя',
            'password' => 'Новый пароль',
            'repeat_password' => 'Повторите пароль',
            'old_password' => 'Текущий пароль'
        ];
        
        $errors = new \Core\Errors();
        
        if($this->is_post()){
            $send = Arr::extract($_POST, array_keys($form));
            
            if(Crypt::hash($send['old_password']) != $this->user['password']){
                $errors->add('old_password', 'Неверный текущий пароль');
            }
            
            if(trim($send['password']) == ''){
                $errors->add('password', 'Не может быть пустым');
            }            
            elseif($send['password'] != $send['repeat_password']){
                $errors->add('password', 'Пароли не совпадают');
                $errors->add('repeat_password', 'Пароли не совпадают');
            }
            
            if($errors->count() == 0){
                $edit = Arr::extract($send, ['name', 'password']);
                
                if($this->main_model->edit($this->user['id_user'], $edit)){
                    $this->redirect($this->root . 'desktop');
                }
                else{
                    $errors = $this->main_model->errors();
                }
            }
        }
        
        $fields = Arr::extract($this->user, ['name']);

        $this->to_base_template['content'] = $this->template($this->module . '/v/v_profile', [
            'f' => new Fieldgen($form, $errors),
            'fields' => $fields
        ]);
    }
}

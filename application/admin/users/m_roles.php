<?php

namespace Admin\Users;

class M_roles extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'roles');
    }

    public function add($fields) {
        $fields['privs'] = is_array($fields['privs']) ? implode(',', $fields['privs']) : '';
        $res = parent::add($fields);

        if (!$res) {
            return false;
        }

        return true;
    }

    public function edit($pk, $fields) {
        $fields['privs'] = is_array($fields['privs']) ? implode(',', $fields['privs']) : '';
        $res = parent::edit($pk, $fields);

        if (!$res) {
            return false;
        }

        return true;
    }

}

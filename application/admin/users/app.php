<?php

namespace Admin\Users;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'users' => [
                    'c' => 'c_users'
                ],
                'login' => [
                    'c' => 'c_login'
                ],
                'users/roles' => [
                    'c' => 'c_roles',
                    'a' => 'index'
                ],
                'profile' => [
                    'c' => 'c_users',
                    'a' => 'profile'
                ]
            ],
            'using' => [],
            'registry_privs' => [
                'users' => 'Работа с пользователями'
            ],
            'menu' => [
                'users' => [
                    'name' => 'Пользователи',
                    'icon' => 'users',
                    'open' => [
                        'users/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'users/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ],
                        'users/roles' => [
                            'name' => 'Права доступа',
                            'icon' => 'universal-access'
                        ]
                    ],
                    'access' => 'users'
                ],
                'profile' => [
                    'name' => 'Мой профиль',
                    'icon' => 'user'
                ],
                'login?out=1' => [
                    'name' => 'Выход',
                    'icon' => 'sign-out',
                    'priority' => -1000
                ],
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'users_',
            'tables' => [
                'users' => [
                    'fields' => ['id_user', 'login', 'password', 'name', 'id_role'],
                    'not_empty' => ['login', 'password', 'name', 'id_role'],
                    'unique' => ['login'],
                    'hash' => ['password'],
                    'range' => [
                        'login' => [3, 32]
                    ],
                    'min_length' => [
                        'password' => 5
                    ],
                    'labels' => [
                        'login' => 'Логин',
                        'password' => 'Пароль',
                        'name' => 'Имя',
                        'id_role' => 'Права доступа'
                    ],
                    'pk' => 'id_user'
                ],
                'sessions' => [
                    'fields' => ['id_session', 'id_user', 'token', 'dt_start', 'dt_last', 'last_ip', 'last_agent', 'is_active'],
                    'not_empty' => ['id_user', 'token', 'dt_start', 'dt_last', 'last_ip', 'last_agent', 'is_active'],
                    'unique' => ['token'],
                    'labels' => [],
                    'pk' => 'id_session'
                ],
                'roles' => [
                    'fields' => ['id_role', 'description', 'privs'],
                    'not_empty' => ['description', 'privs'],
                    'unique' => ['description'],
                    'labels' => [
                        'description' => 'Название роли',
                        'privs' => 'Права доступа'
                    ],
                    'pk' => 'id_role'
                ]
            ]
        ];
    }

}

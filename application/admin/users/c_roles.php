<?php

namespace Admin\Users;

use Core\Info;
use Core\Exceptions;
use Admin\Base\Fieldgen;
use Core\Traits\Actions;

class C_roles extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('users');
        $this->module = 'users';
        $this->main_model = M_roles::instance();
    }

    protected function action_index() {
        $action = 'action_' . (isset($this->params[2]) ? $this->params[2] : 'page');
        $this->$action();
    }

    protected function settings_action_page() {
        $this->to_base_template['title'] .= 'Список ролей: страница ' . $this->page_num;
        $this->page_template = "{$this->module}/v/v_roles_page";
        $this->page_other_data['privs'] = Info::manifest()['registry_privs'];
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление роли';
        $this->add_other_data['privs'] = Info::manifest()['registry_privs'];

        if (isset($this->params[3]) && is_numeric($this->params[3])) {
            $this->add_fields_data = $this->main_model->get($this->params[3]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[3]} not found.");
            }
        }

        $this->add_extract = ['description', 'privs'];
        $this->add_template = "{$this->module}/v/v_roles_add";
        $this->add_redirect = $this->module . '/roles';
        $this->add_fieldgen = new Fieldgen($this->app->labels('roles'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->edit_id = $this->params[3];
        $this->to_base_template['title'] .= 'Редактирование роли';
        $this->edit_other_data['privs'] = Info::manifest()['registry_privs'];
        $this->edit_extract = ['description', 'privs'];
        $this->edit_template = "{$this->module}/v/v_roles_add";
        $this->edit_redirect = $this->module . '/roles';
        $this->edit_fieldgen = new Fieldgen($this->app->labels('roles'), $this->errors);
    }

    protected function settings_action_delete() {
        $this->delete_id = $this->params[3];
        $this->delete_redirect = $this->module . '/roles';
    }

}

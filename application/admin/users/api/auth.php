<?php

namespace Admin\Users\Api;

use Admin\Users as Users;
use Core\Exceptions;

class Auth {

    use \Core\Traits\Singleton;

    protected $user = -1;
    protected $privs = -1;

    public function user() {
        if ($this->user != -1) {
            return $this->user;
        }

        $uid = Users\M_sessions::instance()->uid();

        if ($uid == null) {
            return null;
        }

        return ($this->user = Users\M_users::instance()->get($uid));
    }

    public function can($priv) {
        if (!isset(\Core\Info::manifest()['registry_privs'][$priv])) {
            throw new Exceptions\Fatal('unregistred priv: ' . $priv);
        }

        if ($this->privs === -1) {
            $this->privs = [];
            $user = $this->user();

            if ($user == null) {
                return false;
            }

            $role = Users\M_roles::instance()->get($user['id_role']);
            $privs = explode(',', $role['privs']);

            foreach ($privs as $p) {
                $this->privs[$p] = true;
            }
        }

        return isset($this->privs[$priv]);
    }

}

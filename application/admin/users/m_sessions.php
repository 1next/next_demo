<?php

namespace Admin\Users;

use Core\Session;
use Core\Cookie;
use Core\Crypt;
use Core\Logs;
use Core\Input;

class M_sessions extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected $hash_confimation = 8;
    protected $uid;
    protected $sid;
    protected $token;

    protected function __construct() {
        parent::__construct(App::instance(), 'sessions');
    }

    public function add($fields) {
        if (!isset($fields['id_user'])) {
            return false;
        }

        $fields['token'] = $this->generate_token($fields['id_user']);
        $fields['dt_start'] = date("Y-m-d H:i:s");
        $fields['dt_last'] = date("Y-m-d H:i:s");
        $fields['last_ip'] = Input::server('REMOTE_ADDR');
        $fields['last_agent'] = Input::server('HTTP_USER_AGENT');

        $res = parent::add($fields);

        if (!$res) {
            return false;
        }

        Session::push('users_auth_key', $fields['token']);

        if ($fields['remember']) {
            Cookie::push('users_auth_key', $fields['token']);
        }

        return true;
    }

    public function uid() {
        if ($this->uid != null) {
            return $this->uid;
        }

        $token = Session::read('users_auth_key');

        if ($token == null) {
            $token = Cookie::read('users_auth_key');

            if ($token == null) {
                return null;
            } else {
                Session::push('users_auth_key', $token);
            }
        }

        $res = $this->get_by_fieldset(['token' => $token]);

        if ($res[0] == null || $res[0]['is_active'] == 0) {
            if ($res[0] == null && !$this->verify_token($token)) {
                Logs::error('Some moron use auth token that is not verify. Maybe it is attack.');
            }

            return null;
        }

        $this->sid = $res[0]['id_session'];
        $this->uid = $res[0]['id_user'];
        return $this->uid;
    }

    public function logout() {
        $uid = $this->uid();
        
        if ($uid != null && $this->sid != null) {
            $this->edit($this->sid, ['is_active' => 0]);
        }

        Session::slice('users_auth_key');
        Cookie::slice('users_auth_key');
        $this->sid = null;
        $this->uid = null;
    }

    private function generate_token($id_user) {
        $hash = Crypt::light($id_user, $this->hash_confimation);
        $point_start = mt_rand(0, 32 - $this->hash_confimation);

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
        $token = "";
        $clen = strlen($chars) - 1;

        while (strlen($token) < $point_start) {
            $token .= $chars[mt_rand(0, $clen)];
        }

        $token .= $hash;

        while (strlen($token) < 32) {
            $token .= $chars[mt_rand(0, $clen)];
        }

        return $token;
    }

    private function verify_token($token) {
        $ban = true;
        $users = M_users::instance()->all();

        foreach ($users as $u) {
            $pattern = Crypt::light($u['id_user'], $this->hash_confimation);

            if (strpos($token, $pattern) !== false) {
                $ban = false;
                break;
            }
        }

        return !$ban;
    }

}

<?php

namespace Admin\Users;

use Config;
use Core\Crypt;
use Core\Input;

class C_login extends \Core\Base\Controller {

    protected $title;
    protected $error;

    public function before() {
        $this->template_class->set_base(PATH_APPLICATION . '/admin/');
    }

    public function action_index() {
        $this->title = 'Авторизация';
        $this->error = false;
        $data = [];

        if ($this->is_post()) {
            $data['remember'] = (int)(Input::post('remember') != null);
            $res = M_users::instance()->get_by_fieldset(['login' => Input::post('login')]);

            if ($res[0] == null || (Crypt::hash(Input::post('password')) != $res[0]['password'])) {
                $this->error = true;
            } else {
                $data['id_user'] = $res[0]['id_user'];

                if (M_sessions::instance()->add($data)) {
                    $this->redirect($this->root . Config::ADMIN_URL . '/texts');
                }

                $this->error = true;
            }
        }
        else {
            if (isset($this->get['out'])) {
                M_sessions::instance()->logout();
            } elseif (M_sessions::instance()->uid() != null) {
                $this->redirect('/' . Config::ADMIN_URL . '/texts');
            }
        }
    }

    public function render() {
        return $this->template('users/v/v_login', ['title' => $this->title, 'error' => $this->error]);
    }

    protected function set_content_404($e) {
        
    }

    protected function set_content_access($e) {
        
    }

    protected function set_error_template(\Core\Errors $errors) {
        
    }

}

<?php

namespace Admin\Gallery;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    const IMAGE_TYPE = 'gallery';
    const IMAGE_NAME = 'Для галерей';
    const IMAGE_RELATION = 'gallery';
    
    public function init() {
        /*Events::add('System.CKeditor.Plugins', function(&$plugins){
            $plugins[] = 'cms_gallery';
        });*/
    }

    public function manifest() {
        return [
            'routs' => [
                'gallery' => [
                    'c' => 'c_gallery'
                ],
                'gallery-ajax' => [
                    'c' => 'c_ajax'
                ]
            ],
            'using' => [
                'users',
                'images' => [
                    'image_type' => self::IMAGE_TYPE,
                    'image_name' => self::IMAGE_NAME,
                    'image_item' => self::IMAGE_RELATION
                ]
            ],
            'registry_privs' => [
                'gallery' => 'Работа с галереями'
            ],
            'menu' => [
                'gallery' => [
                    'name' => 'Галереи фогографий',
                    'icon' => 'pencil-square-o',
                    'open' => [
                        'gallery/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'gallery/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ]
                    ],
                    'priority' => 1,
                    'access' => 'gallery'
                ]
            ]
        ];
    }

    public function rules() {

        return [
            'tables_prefix' => 'galleries_',
            'tables' => [
                'index' => [
                    'fields' => ['id_gallery', 'shortcode', 'name'],
                    'not_empty' => ['shortcode', 'name'],
                    'unique' => ['shortcode'],
                    'range' => [
                        'shortcode' => ['2', '64'],
                        'name' => ['3', '256']
                    ],
                    'labels' => [
                        'shortcode' => 'Код для встраивания',
                        'name' => 'Название'
                    ],
                    'pk' => 'id_gallery'
                ]
            ]
        ];
    }

}

<?php

namespace Admin\Gallery;

use Admin\Images\Api\Main as Images;

class c_ajax extends \Admin\Base\Ajax {

    public function before() {
        parent::before();
        $this->check_access('gallery');
    }

    public function action_upload() {
        $mImages = Images::instance();

        if($id = $mImages->add_from_base64(App::IMAGE_RELATION, $_POST['id_item'], $_POST['name'], $_POST['value'])){
            $this->response['res'] = true;
            $this->response['image'] = $mImages->get($id);
        }
        else{
            $this->response['res'] = false;
            $this->response['errors'] = $mImages->errors()->unshown();
        }
    }

    public function action_sortimages() {
        $mImages = Images::instance();

        if($id = $mImages->sort(App::IMAGE_RELATION, $_POST['id_item'], $_POST['items'])){
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
        }
    }

    public function action_getallgalleries() {
        $this->response['galleries'] = M_gallery::instance()->all();
    }


    protected function set_error_template(\Core\Errors $errors) {

    }

}
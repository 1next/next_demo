<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Алиас</th>
                <th>Название</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr>
                    <td><?= $one['shortcode'] ?></td>
                    <td><?= $one['name'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>gallery/edit/<?= $one['id_gallery'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>images/work/<?=$other['image_relation']?>/<?= $one['id_gallery'] ?>" title="Загрузить изображения" class="admin-btn admin-btn-image"></a> 
                        <a href="<?=$admin_root?>gallery/delete/<?= $one['id_gallery'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>gallery/add/<?= $one['id_gallery'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>gallery/add/">Добавить новую галерею &raquo;</a></p>
</div>
<?php

namespace Admin\Gallery;

use Core\Traits\Actions;
use Core\Exceptions;
use Admin\Base\Fieldgen;
use Admin\Images\Api\Main as Images;

class C_gallery extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('gallery');
        $this->module = 'gallery';
        $this->main_model = M_gallery::instance();
    }

    protected function settings_action_page() {
        $this->to_base_template['title'] .= 'Список галерей: страница ' . $this->page_num;
        $this->page_other_data['image_relation'] = App::IMAGE_RELATION;
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление галереи';

        if (isset($this->params[2]) && is_numeric($this->params[2])) {
            $this->add_fields_data = $this->main_model->get($this->params[2]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[2]} not found.");
            }
        } else {
            $this->add_fields_data = ['is_show' => 1];
        }

        $this->add_extract = ['shortcode', 'name'];
        $this->add_checkboxes_detect = ['is_show'];
        $this->add_fieldgen = new Fieldgen($this->app->labels('index'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование галереи';
        $this->edit_extract = ['shortcode', 'name'];
        $this->edit_checkboxes_detect = ['is_show'];
        $this->edit_template = "{$this->module}/v/v_add";
        $this->edit_fieldgen = new Fieldgen($this->app->labels('index'), $this->errors);
    }

    protected function settings_action_delete() {
        
    }

}

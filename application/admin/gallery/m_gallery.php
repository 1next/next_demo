<?php

namespace Admin\Gallery;

class M_gallery extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'index');
    }

}

<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Шотркод</th>
                <th>Название</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $menu): ?>
                <tr>
                    <td><?= $menu['shortcode'] ?></td>
                    <td><?= $menu['title'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>menu/edit/<?= $menu['id_menu'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>menu/delete/<?= $menu['id_menu'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>menu/add/<?= $menu['id_menu'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>menu/add/">Добавить новое меню &raquo;</a></p>
</div>
<form method="post">
    <?= $f->set($html->input()->val($fields['shortcode']), 'shortcode')?>
    <?= $f->set($html->input()->val($fields['title']), 'title')?>
    <?=$html->input('hidden')->idn('items_id')->val($fields['items_id'])?>
    <h4>Выбирете страницы</h4>
    <div class="draganddrop">
        <div class="drop">
            <ul data-start="<?=$fields['items_id']?>"></ul>
        </div>
        <div class="drag">
            <?=$html->ul($data['pages'], function($li, $item){
                $li->html($item['title'])->attr('data-id', $item['id_page']);
            });?>
        </div>
    </div>
    <input type="submit" value="Сохранить" class="btn btn-success">
    <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
</form>
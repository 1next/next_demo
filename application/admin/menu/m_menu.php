<?php

namespace Admin\Menu;

use Core\Arr;

class M_menu extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected $mItems;

    protected function __construct() {
        parent::__construct(App::instance(), 'menu');
        $this->mItems = M_menu_items::instance();
    }

    public function get_items_string($id_menu){
        $items = $this->mItems->get_items($id_menu);
        $arr = Arr::section($items, 'id_item');
        return implode(',', $arr);
    }
    
    public function add($fields) {
        $res = parent::add($fields);
        
        if($res && isset($fields['items_id'])){
            $this->set_items($res, $fields['items_id']);
        }
        
        return $res;
    }
    
    public function edit($pk, $fields) {
        $res = parent::edit($pk, $fields);
        
        if($res && isset($fields['items_id'])){
            $this->set_items($pk, $fields['items_id']);
        }
        
        return $res;
    }
    
    public function delete($pk) {
        $this->db->beginTransaction();
        $res = parent::delete($pk);
        $this->mItems->delete_by_fieldset(['id_menu' => $pk]);
        $this->db->commit();
        return $res;
    }
    
    protected function set_items($id_menu, $items){
        $this->db->beginTransaction();
        
        $this->mItems->delete_by_fieldset(['id_menu' => $id_menu]);
        $elems = explode(',', $items);
        
        for($i = 0; $i < count($elems); $i++){
            $this->mItems->add([
                'id_menu' =>  $id_menu,
                'id_item' => (int)$elems[$i],
                'num_sort' => $i
            ]);
        }
        
        $this->db->commit();
    }
}

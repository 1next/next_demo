<?php

namespace Admin\Menu;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'menu' => [
                    'c' => 'c_menu'
                ]
            ],
            'using' => ['users', 'pages'],
            'registry_privs' => [
                'menu' => 'Работа с менюшками'
            ],
            'menu' => [
                'menu' => [
                    'name' => 'Меню',
                    'icon' => 'bars',
                    'open' => [
                        'menu/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'menu/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ]
                    ],
                    'priority' => 1,
                    'access' => 'menu'
                ]
            ]
        ];
    }

    public function rules() {

        return [
            'tables_prefix' => 'menu_',
            'tables' => [
                'menu' => [
                    'fields' => ['id_menu', 'shortcode', 'title'],
                    'not_empty' => ['shortcode', 'title'],
                    'html_allowed' => [],
                    'unique' => ['shortcode'],
                    'range' => [
                        'shortcode' => ['3', '64'],
                        'title' => ['3', '256']
                    ],
                    'labels' => [
                        'shortcode' => 'Шорткод',
                        'title' => 'Расшифровка названия'
                    ],
                    'pk' => 'id_menu'
                ],
                'items' => [
                    'fields' => ['id_menu', 'id_item', 'num_sort'],
                    'not_empty' => ['id_menu', 'id_item', 'num_sort'],
                    'unique' => [],
                    'labels' => [],
                    'pk' => ['id_menu', 'id_item']
                ]
            ]
        ];
    }

}

$(function(){
    
    $drags = new CMS.dragandgrop({
        success_drop: function(){
            $drags.droped_items_ul.sortable('refresh');
        }
    });
    
    $drags.droped_items_ul.sortable();
    
    $('input[type="submit"]').click(function(e){
        var items = $drags.getItems();
        $('input[name="items_id"]').val(items.toString());
    });
    
});
<?php


namespace Admin\Menu;

use Admin\Base;
use Core\Traits\Actions;
use Admin\Pages\Api\Main as Pages;

class C_menu extends Base\Controller{
    
    use Actions\Page;
    use Actions\Add;
    use Actions\Edit;
    use Actions\Delete;
    
    public function before() {
        parent::before();
        $this->check_access('menu');
        $this->module = 'menu';
        $this->main_model = M_menu::instance();
    }

    public function settings_action_page() {
        $this->to_base_template['title'] .= 'Список меню';
    }
    
    public function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление меню';
        $this->add_scripts($this->module, ['init']);
        
        $pages = Pages::instance();
        
        if(isset($this->params[2]) && is_numeric($this->params[2])){
            $this->add_fields_data = $this->main_model->get($this->params[2]);
            $this->add_fields_data['items_id'] = $this->main_model->get_items_string($this->params[2]);
        }
        else{
            $this->add_fields_data = ['items_id' => ''];
        }
        
        $this->add_other_data['pages'] = $pages->all();
        $this->add_extract = ['shortcode', 'title', 'items_id'];
        $this->add_fieldgen = new Base\Fieldgen($this->app->labels('menu'), $this->errors);
    }
    
    public function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование меню';
        $this->add_scripts($this->module, ['init']);
        
        $this->edit_other_data['pages'] = Pages::instance()->all();
        $this->edit_fields_data = ['items_id' => $this->main_model->get_items_string($this->params[2])];
        
        $this->edit_extract = ['shortcode', 'title', 'items_id'];
        $this->edit_template = "{$this->module}/v/v_add";
        $this->edit_fieldgen = new Base\Fieldgen($this->app->labels('menu'), $this->errors);
    }


    public function settings_action_delete() {
        
    }
}

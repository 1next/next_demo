<?php

namespace Admin\Menu;

class M_menu_items extends \Core\Base\Model {

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Delete_by_fieldset;

    protected function __construct() {
        parent::__construct(App::instance(), 'items');
    }

    public function get_items($id_menu){
        return $this->db->select("SELECT * FROM {$this->table}
                                  WHERE id_menu=:id_menu
                                  ORDER BY num_sort ASC
                                ", ['id_menu' => $id_menu]);
    }
}

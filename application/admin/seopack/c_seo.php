<?php

namespace Admin\Seopack;

use Config;
use Core\Exceptions;
use Core\Errors;
use Admin\Base\Fieldgen;

use Core\Arr;
use Admin\Pages\Api\Main as Pages;
use Admin\Cats\Api\Main as Cats;
use Admin\Shop\Api\Main as Shop;

class C_Seo extends \Admin\Base\Controller {

    use \Core\Traits\Actions\Page;

use \Core\Traits\Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('seopack');
        $this->module = 'seopack';
        $this->main_model = M_index::instance();
        $this->to_base_template['title'] = 'SEO: ';
        $this->errors = new Errors();
    }

    public function action_research() {
        $this->to_base_template['title'] .= 'обновление урлов';

        try {
            $collector = Extra\Collector::instance();
            $urls = $collector->get_urls();

            foreach ($urls as $url) {
                $res = $this->main_model->get_by_fieldset([['item' => $url['item'], 'id_item' => $url['id_item']]]);

                if (empty($res)) {
                    $this->main_model->add($url);
                }
            }

            $this->redirect($this->root . $this->module);
        } catch (Exceptions\E404 $e) {
            throw new Exception('SEO - bad extra collector', $e->getCode(), $e);
        }
    }

    public function action_clear() {
        $this->to_base_template['title'] .= 'удаление неиспользуемых урлов';

        try {
            $collector = Extra\Collector::instance();

            $urls = [];

            foreach ($collector->get_urls() as $one) {
                $urls[] = $one['url'];
            }

            foreach ($this->main_model->all() as $k => $value) {
                if(!in_array($value['url'], $urls)) {
                    $this->main_model->delete($value['id_index']);
                }
            }

            $this->redirect($this->root . $this->module);
        } catch (Exceptions\E404 $e) {
            throw new Exception('SEO - bad extra collector', $e->getCode(), $e);
        }
    }

    public function action_sitemap() {
        $this->to_base_template['title'] .= 'генерация карты сайта';

        try {
            $index = M_index::instance()->all();

            $pattern = "
    <url>
        <loc>%s</loc>
        <lastmod>%s</lastmod>
        <changefreq>%s</changefreq>
        <priority>%s</priority>
    </url>";

            $res = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

            foreach ($index as $one) {
                if ($one['in_sitemap'] == '1') {
                    $res .= sprintf($pattern,
                            Config::MAIN_PROTOCOL . '://' . Config::DOMAIN . Config::BASE_URL . $one['url'],
                            $this->main_model->make_lastmod($one['lastmod']),
                            $this->main_model->make_changefreq($one['changefreq']),
                            $this->main_model->make_priority($one['priority']));
                }
            }

            $res .= '
</urlset>';
            file_put_contents('sitemap.xml', $res);

            $this->redirect($this->root . $this->module);
        } catch (Exceptions\E404 $e) {
            throw new Exception('SEO - bad extra collector', $e->getCode(), $e);
        }
    }

    protected function settings_action_page() {
		$this->to_base_template['title'] .= ' индексируемые URL-ы';
        $this->page_other_data['priority'] = App::PRIORITY;
        $this->page_other_data['changefreq'] = App::CHANGEFREQ;
        $this->page_other_data['f'] = new Fieldgen($this->app->labels('index'), $this->errors);

        $this->add_styles($this->module, ['styles']);
        $this->add_scripts($this->module, ['init']);
    }

    protected function settings_action_delete() {

    }
}

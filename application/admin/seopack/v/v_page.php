<?php
    $f = $other['f'];
?>
<div class="seopack-list">
    <p>
        <a class="btn btn-success" href="<?= $admin_root ?>seopack/research/">Найти неучтённые адреса</a>
        <a class="btn btn-primary" href="<?= $admin_root ?>seopack/sitemap/">Сгенерировать карту сайта</a>
        <a class="btn btn-danger btn-delete" href="<?= $admin_root ?>seopack/clear/">Удалить неиспользуемые адреса</a>
        <a class="btn btn-warning" href="<?= $root ?>sitemap.xml" target="_blank">Посмотреть карту сайта</a>
    </p>
    <hr>
    <? foreach ($data as $one): ?>
        <div class="seopack-one">
            <div class="info">
                <div class="url">
                    <a href="<?=$root . $one['url']?>" target="_blank"><?=$root . $one['url']?></a>
                </div>
                <div>
                    <span class="item"><?=$one['item']?></span>, id =
                    <span class="item-id"><?=$one['id_item']?></span>
                </div>
                <div class="actions">
                    <a href="<?= $admin_root ?>seopack/delete/<?= $one['id_index'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                </div>
            </div>
            <div class="form" data-id="<?=$one['id_index']?>">
                <?= $f->set($html->select($other['priority'], $one['priority']), 'priority')?>
                <?= $f->set($html->select($other['changefreq'], $one['changefreq']), 'changefreq')?>
                <?= $f->set($html->input()->val($one['title']), 'title') ?>
                <?= $f->set($html->input()->val($one['h1']), 'h1') ?>
                <?= $f->set($html->input()->val($one['keywords']), 'keywords') ?>
                <?= $f->set($html->textarea()->val($one['description']), 'description') ?>
                <?= $f->set($html->input('checkbox')->battr('checked', ($one['is_index'] != 0)), 'is_index') ?>
                <?= $f->set($html->input('checkbox')->battr('checked', ($one['is_follow'] != 0)), 'is_follow') ?>
                <?= $f->set($html->input('checkbox')->battr('checked', ($one['in_sitemap'] != 0)), 'in_sitemap') ?>
            </div>
            <div class="lastmod">
                <span>Last-modify:</span>
                <span><?=date("Y-m-d H:i:s", $one['lastmod'])?></span>
                <button type="button" class="btn btn-success" data-id="<?=$one['id_index']?>">
                    <span class="fa fa-refresh"></span>
                </button>
            </div>
        </div>
    <? endforeach ?>
    <?= $navbar ?>
</div>
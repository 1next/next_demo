<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>Откуда</th>
                <th>Куда</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr>
                    <td><?= $one['url_from'] ?></td>
                    <td><?= $one['url_to'] ?></td>
                    <td>
                        <a href="<?=$admin_root?>seopack/redirects/edit/<?= $one['id_redirect'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>seopack/redirects/delete/<?= $one['id_redirect'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>seopack/redirects/add/<?= $one['id_redirect'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
    <br>
    <p>
        <a class="btn btn-primary btn" href="<?=$admin_root?>seopack/redirects/add/">
            Добавить редирект &raquo;
        </a>
    </p>
</div>
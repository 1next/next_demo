<?php

namespace Admin\Seopack;

class M_index extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'index');
    }

    public function make_lastmod($time){
        return date("Y-m-d", $time) . 'T' .  date("H:i:s", $time) . '+03:00';
    }
    
    public function make_changefreq($num){
        $ch = App::CHANGEFREQ;
        return $ch[$num];
    }
    
    public function make_priority($num){
        $ch = App::PRIORITY;
        return $ch[$num];
    }
}

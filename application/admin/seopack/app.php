<?php

namespace Admin\Seopack;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;
    
    const CHANGEFREQ = ['0' => 'always', '1' => 'hourly', '2' => 'daily', '3' => 'weekly', '4' => 'monthly', '5' => 'yearly'];
    const PRIORITY = ['0' => '1', '1' => '0.9', '2' => '0.8', '3' => '0.7', '4' => '0.6', '5' => '0.5'];

    public function init() {
        $this->on_page = 100;
        
        Events::add('System.Model.Edit.Success', function($info){
            try{
                Extra\Collector::instance()->on_update($info);
            }
            catch(Exception $e){
                /*  чтобы не рушить процесс обновления другого модуля */
            }
        });
    }

    public function manifest() {
        return [
            'routs' => [
                'seopack' => [
                    'c' => 'c_seo'
                ],
                'seopack-ajax' => [
                    'c' => 'c_seo_ajax'
                ],
                'seopack/redirects' => [
                    'c' => 'c_redirects',
                    'a' => 'switch'
                ],
            ],
            'using' => ['users'],
            'registry_privs' => [
                'seopack' => 'СЕО продвижение'
            ],
            'menu' => [
                'seopack' => [
                    'name' => 'СЕО продвижение',
                    'icon' => 'search',
                    'open' => [
                        'seopack/page' => [
                            'name' => 'URL-ы',
                            'icon' => 'list-ul'
                        ]
						/*,
                        'seopack/redirects' => [
                            'name' => 'Редиректы',
                            'icon' => 'arrow-right'
                        ]//*/
                    ],
                    'priority' => 1,
                    'access' => 'seopack'
                ]
            ]
        ];
    }

    public function rules() {

        return [
            'tables_prefix' => 'seopack_',
            'tables' => [
                'index' => [
                    'fields' => ['id_index', 'item', 'id_item', 'url', 'priority', 'changefreq', 'lastmod', 'is_index', 
                                 'is_follow', 'in_sitemap', 'title', 'h1', 'keywords', 'description'],
                    'not_empty' => ['item', 'id_item', 'url', 'priority', 'changefreq', 'lastmod', 'is_index', 'is_follow', 'in_sitemap'],
                    'unique' => ['url'],
                    'composite_unique' => [['item', 'id_item']],
                    'range' => [
                        'item' => ['2', '64'],
                        'title' => ['0', '128'],
                        'h1' => ['0', '64'],
                        'keywords' => ['0', '200'],
                        'description' => ['0', '180']
                    ],
                    'labels' => [
                        'item' => 'Сущность',
                        'id_item' => 'ID сущности',
                        'priority' => 'Приоритет url-а',
                        'changefreq' => 'Частота изменения',
                        'lastmod' => 'Последнее изменение',
                        'is_index' => 'Разрешить индексацию',
                        'is_follow' => 'Разрешить переходы по ссылкам',
                        'in_sitemap' => 'Включён в карту сайта',
                        'title' => 'Тайтл страницы',
                        'h1' => 'Заголовок первого уровня',
                        'keywords' => 'Meta keywords',
                        'description' => 'Meta description'
                    ],
                    'pk' => 'id_index'
                ],
                'redirects' => [
                    'fields' => ['id_redirect', 'url_from', 'url_to'],
                    'not_empty' => ['url_from', 'url_to'],
                    'unique' => ['url_from'],
                    'range' => [
                        'url_from' => ['1', '255'],
                        'url_to' => ['1', '256']
                    ],
                    'labels' => [
                        'url_from' => 'Старый адрес',
                        'url_to' => 'Новый адрес'
                    ],
                    'pk' => 'id_redirect'
                ]
            ]
        ];
    }

}

$(function () {

    $('.seopack-list').on('change', 'input, textarea, select', function(e){
       var field = $(this);
       var msg = field.parents('.form-group').eq(0).find('.error');

       var obj = {
           name: field.attr('name'),
           value: (field.attr('type') == 'checkbox') ? (field.prop('checked') ? 1 : 0) : field.val(),
           id_index: field.parents('.form').eq(0).data('id')
       }

       $.post(_PHP.admin_root + 'seopack-ajax/save', obj, function(data){
            if(data.res){
                msg.css({opacity: 0}).addClass('success').html('сохранено').animate({opacity: 1}, 300);
            }
            else{
                msg.css({opacity: 0}).removeClass('success').html(data.error).animate({opacity: 1}, 300);
            }
        }, 'json');

    });

    $('.seopack-list').on('click', '.lastmod button', function(e){
       var btn = $(this);
       var span = btn.find('span').addClass('fa-spin');
       var target = btn.prev();

       $.post(_PHP.admin_root + 'seopack-ajax/lastmod', {id_index: btn.data('id')}, function(data){
            span.removeClass('fa-spin');

            if(data.res){
                target.hide().html(data.dt).fadeIn(300);
            }
            else{
                target.hide().html('error').fadeIn(300);
            }
        }, 'json');

    });

    $('.btn-delete').click(function(e){
      if(!confirm("Вы уверены, что хотите удалить неиспользуемые адреса")){
        e.preventDefault();
      }
    });

});
<?php

namespace Admin\Seopack\Extra;

use Admin\Seopack;
use Core\Arr;
use Admin\Pages\Api\Main as Pages;
use Admin\Cats\Api\Main as Cats;
use Admin\Shop\Api\Main as Shop;
//use Admin\News\Api\Main as News;
//use Admin\Tags\Api\Main as Tags;

class Collector extends Seopack\Collector{

    use \Core\Traits\Singleton;

    public function get_urls() {
        $this->urls = [];
        $time = time();
		
		
		$mShop = Shop::instance();
		
		$products = $mShop->all();
        $ids = Arr::section($products, 'id_product');
        
		$options = $mShop->get_options_by_group($ids);
        $pages = Pages::instance()->get_in(['visible' => ['0', '1']]);

		foreach($pages as $one){
		
			$url = explode('/', $one['url']);
			$this->pack('page', $one['id_page'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['title'], $one['title']);
		
			if($one['url'] == 'katalog'){
				foreach($products as $one){
					$cat = Cats::instance()->get($one['id_cat']);
					$url_str = $cat['url'] . '/' . $options[$one['id_product']]['url-страницы']['value'];
					$url = explode('/', $url_str);
					$this->pack('product', $one['id_product'], $url_str, count($url), count($url), $time, 1, 1, 1, $one['title'], $one['title']);
				} 
			}
			
		}
	
		$url_str = 'sitemap';
        $url = explode('/', $url_str);
        $this->pack('sitemap', 1, $url_str, count($url), count($url), $time, 1, 1, 1);
		
		
		
		
		
    /*    
        $pages = Pages::instance()->get_in(['visible' => ['0', '1']]);
        
        foreach($pages as $one){
            $url = explode('/', $one['url']);
            $this->pack('page', $one['id_page'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['title'], $one['title']);
        }
        
        $mCats = Cats::instance();
        $cats = $mCats->all('shop');
        
        foreach($cats as $one){
            $url = explode('/', $one['url']);
            $this->pack('cat', $one['id_cat'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['name'], $one['description']);
        }
        
        $mShop = Shop::instance();
        $products = $mShop->get_by_fieldset(['id_prototype' => 0]);
        $ids = Arr::section($products, 'id_product');
        $options = $mShop->get_options_by_group($ids);
        
        foreach($products as $one){
            $cat = $mCats->get($one['id_cat']);
            $url_str = $cat['url'] . '/' . $options[$one['id_product']]['url-страницы']['value'];
            $url = explode('/', $url_str);
            $this->pack('product', $one['id_product'], $url_str, count($url), count($url), $time, 1, 1, 1, $one['title'], $one['title']);
        }   
        
        $url_str = 'sitemap';
        $url = explode('/', $url_str);
        $this->pack('sitemap', 1, $url_str, count($url), count($url), $time, 1, 1, 1);
//*/
        return $this->urls;
    }   
	
	public function get_urls1() {
        $this->urls = [];
        $time = time();
        
        $pages = Pages::instance()->get_in(['visible' => ['0', '1']]);
        
        foreach($pages as $one){
            $url = explode('/', $one['url']);
            $this->pack('page', $one['id_page'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['title'], $one['title']);
        }
        
        $mCats = Cats::instance();
        $cats = $mCats->all('shop');
        
        foreach($cats as $one){
            $url = explode('/', $one['url']);
            $this->pack('cat', $one['id_cat'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['name'], $one['description']);
          //  $this->pack('cat.sale-page', $one['id_cat'], $one['url'] . '/skidki', count($url), count($url), $time, 0, 0, 0, $one['name'], $one['description']);
        }
        
        $mShop = Shop::instance();
        $products = $mShop->get_by_fieldset(['id_prototype' => 0]);
        $ids = Arr::section($products, 'id_product');
        $options = $mShop->get_options_by_group($ids);
        
        foreach($products as $one){
            $cat = $mCats->get($one['id_cat']);
            $url_str = $cat['url'] . '/' . $options[$one['id_product']]['url-страницы']['value'];
            $url = explode('/', $url_str);
            $this->pack('product', $one['id_product'], $url_str, count($url), count($url), $time, 1, 1, 1, $one['title'], $one['title']);
        }   
        
        $url_str = 'sitemap';
        $url = explode('/', $url_str);
        $this->pack('sitemap', 1, $url_str, count($url), count($url), $time, 1, 1, 1);
        
		/*
        // блог 
       // $this->pack('blog-flow', 0, 'blog', 1, 1, $time, 1, 1, 1, 'Блог', 'Блог');
        // добавить постраничку в поток
        
        $cats = $mCats->all('news');
        
        foreach($cats as $one){
            $one['url'] = 'blog/' . $one['url'];
            $url = explode('/', $one['url']);
       //     $this->pack('cat', $one['id_cat'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['name'], $one['name']);
        }
        // добавить постраничку в категориях
        
        $tags = Tags::instance()->all_by_type('news');
        
        foreach($tags as $one){
            $one['url'] = 'blog/@' . $one['url'];
            $url = explode('/', $one['url']);
      //      $this->pack('tag', $one['id_tag'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['name'], $one['name']);
        }
        // добавить постраничку в тегах
        
        $news = News::instance()->all_visible();
        $news_cats = Arr::to_select($cats, 'id_cat', 'url');
        
        foreach($news as $one){
            $one['url'] = 'blog/' . $news_cats[$one['id_cat']] . '/' . $one['url'];
            $url = explode('/', $one['url']);
       //     $this->pack('blog-post', $one['id_new'], $one['url'], count($url), count($url), $time, 1, 1, 1, $one['title'], $one['title']);
        }
        //*/
        return $this->urls;
    }

    public function on_update($info) {
        $item = null;
        $url = null;
        $id_item = $info['pk'];
        
        if($info['table'] == 'lavr_pages_pages'){
            $item = 'page';
            $url = $info['item']['url'];
        }
        elseif($info['table'] == 'lavr_cats_cats'){
            $item = 'cat';
            $url = $info['item']['url'];
        }
        elseif($info['table'] == 'lavr_shop_products'){
            $item = 'product';
            
            $mShop = Shop::instance();
            $mCats = Cats::instance();
            $product = $mShop->get($id_item);
            
            if($product['id_prototype'] != 0){
                $id_item = $product['id_prototype'];
            }
            
            $options = $mShop->get_options_by_group([$id_item]);
            $cat = $mCats->get($product['id_cat']);
            $url = $cat['url'] . '/' . $options[$id_item]['url-страницы']['value'];
        }
        
        if($item != null && $url != null){
            $mIndex = Seopack\M_index::instance();
            
            $index = $mIndex->get_by_fieldset([['item' => $item, 'id_item' => $id_item]]);
            $last_url = $index[0]['url'];
            
            if($index[0] != null){
                $mIndex->edit($index[0]['id_index'], [
                    'url' => $url,
                    'lastmod' => time()
                ]);
                
                if($item === 'cat' && $last_url != $url){
                    $mShop = Shop::instance();
                    $products = $mShop->get_by_fieldset(['id_cat' => $id_item]);
                    $ids = Arr::section($products, 'id_product');
                    $options = $mShop->get_options_by_group($ids);
                    
                    foreach($products as $pr){
                        $index = $mIndex->get_by_fieldset([['item' => 'product', 'id_item' => $pr['id_product']]]);
                        
                        if($index[0] != null){
                            $pr_url = $url . '/' . $options[$pr['id_product']]['url-страницы']['value'];
                            
                            $mIndex->edit($index[0]['id_index'], [
                                'url' => $pr_url,
                                'lastmod' => time()
                            ]);
                        }
                    }
                }
            }
        }
    }

}

<?php

namespace Admin\Seopack;

use Core\Input;

class C_seo_ajax extends \Admin\Base\Ajax {

    public function before() {
        parent::before();
        $this->check_access('seopack');
    }

    public function action_save() {    
        $mIndex = M_index::instance();
        
        $id_index = Input::post('id_index');
        $name = Input::post('name');
        $value = Input::post('value');
        
        if($mIndex->edit($id_index, [$name => $value])){            
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
            $this->response['error'] = $mIndex->errors()->get($name);
        }
    }
    
    public function action_lastmod() {    
        $mIndex = M_index::instance();
        
        $id_index = Input::post('id_index');
        $lastmod = time();

        if($mIndex->edit($id_index, ['lastmod' => $lastmod])){            
            $this->response['res'] = true;
            $this->response['dt'] = date('Y-m-d H:i:s', $lastmod);
        }
        else{
            $this->response['res'] = false;
            $this->response['error'] = $mIndex->errors()->get($name);
        }
    }
    
    protected function set_error_template(\Core\Errors $errors) {
        
    }

}

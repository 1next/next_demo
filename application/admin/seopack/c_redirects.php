<?php

namespace Admin\Seopack;

use Config;
use Core\Exceptions;
use Core\Errors;
use Admin\Base\Fieldgen;
use Core\Traits\Actions;

class C_redirects extends \Admin\Base\Controller {

    use Actions\Page;

use Actions\Add;

use Actions\Edit;

use Actions\Delete;

    public function before() {
        parent::before();
        $this->check_access('seopack');
        $this->module = 'seopack';
        $this->main_model = M_redirects::instance();
    }

    public function action_switch(){
        $action = 'action_' . (isset($this->params[2]) ? $this->params[2] : 'index');
        $this->$action();
    }
    
    protected function settings_action_page() {
        $this->page_template = $this->module . '/v/redirects/v_page';
        $this->to_base_template['title'] .= 'SEO редиректы: страница ' . $this->page_num;
    }

    protected function settings_action_add() {
        $this->to_base_template['title'] .= 'Добавление редиректа';

        if (isset($this->params[3]) && is_numeric($this->params[3])) {
            $this->add_fields_data = $this->main_model->get($this->params[3]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[3]} not found.");
            }
        } else {
            $this->add_fields_data = [];
        }

        $this->add_redirect .= '/redirects';
        $this->add_template = $this->module . '/v/redirects/v_add';
        $this->add_extract = ['url_from', 'url_to'];
        $this->add_fieldgen = new Fieldgen($this->app->labels('redirects'), $this->errors);
    }

    protected function settings_action_edit() {
        $this->to_base_template['title'] .= 'Редактирование редиректа';
        $this->edit_id = $this->params[3];
        $this->edit_extract = ['url_from', 'url_to'];
        $this->edit_template = $this->module . '/v/redirects/v_add';
        $this->edit_fieldgen = new Fieldgen($this->app->labels('redirects'), $this->errors);
    }

    protected function settings_action_delete() {
        $this->delete_id = $this->params[3];
    }

}

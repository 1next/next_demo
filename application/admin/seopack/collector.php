<?php

namespace Admin\Seopack;

abstract class Collector{

    protected $urls;

    abstract public function get_urls();
    abstract public function on_update($info);

    protected function pack($item, $id_item, $url, $priority, $changefreq, $lastmod, $is_index, 
                            $is_follow, $in_sitemap, $title = null, $h1 = null, $keywords = null, $description = null){
        $this->urls[] = [
            'item' => $item,
            'id_item' => $id_item,
            'url' => $url,
            'priority' => $priority,
            'changefreq' => $changefreq,
            'lastmod' => $lastmod,
            'is_index' => $is_index,
            'is_follow' => $is_follow,
            'in_sitemap' => $in_sitemap,
            'title' => $title,
            'h1' => $h1,
            'keywords' => $keywords,
            'description' => $description,
        ];
    }

    
}

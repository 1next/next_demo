<div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th class="numberlist">Заголовок</th>
                <th>Полный URL</th>
                <th>Базовый шаблон</th>
                <th>Внутренний шаблон</th>
                <th>Видимость</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $page): ?>
                <tr
                    <?= $page['visible'] == 1 ? 'class="info"' : '' ?>
                    <?= $page['visible'] == 2 ? 'class="warning"' : '' ?>
                    <?= $page['visible'] == 3 ? 'class="danger"' : '' ?>
                    >
                    <td><?= $page['title'] ?></td>
                    <td><?= $page['url'] ?></td>
                    <td><?= $page['base_template'] ?></td>
                    <td><?= $page['inner_template'] ?></td>
                    <td>
                        <? if($page['visible'] == 0): ?>
                        Видна всем
                        <? elseif($page['visible'] == 1) :?>
                        Не видна в поиске
                        <? elseif($page['visible'] == 2) :?>
                        Только админу
                        <? elseif($page['visible'] == 3) :?>
                        Не видна
                        <? endif; ?>
                    
                    </td>
                    <td>
                        <a href="<?=$admin_root?>pages/edit/<?= $page['id_page'] ?>" title="Редактировать" class="admin-btn admin-btn-edit"></a> 
                        <a href="<?=$admin_root?>pages/delete/<?= $page['id_page'] ?>" title="Удалить" class="admin-btn admin-btn-delete del"></a>
                        <a href="<?=$admin_root?>pages/add/<?= $page['id_page'] ?>" title="Добавить на основе текущего" class="admin-btn admin-btn-copy"></a>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>

    <?= $navbar ?>
    <br>
    <p><a class="btn btn-primary btn" href="<?=$admin_root?>pages/add/">Добавить новую страницу &raquo;</a></p>
</div>

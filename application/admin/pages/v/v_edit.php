<form method="post">
    <?=$f->set($html->select_tree($data['tree'], $fields['id_parent'], $data['disabled']), 'id_parent')?>
    <?=$f->set($html->input()->val($fields['title']), 'title') ?>
    <?=$f->set($html->input()->val($fields['item_url']), 'item_url') ?>
    <?=$f->set($html->select_f($data['base_templates'], $fields['base_template']), 'base_template') ?>
    <?=$f->set($html->select_f($data['inner_templates'], $fields['inner_template']), 'inner_template') ?>
    
    <?= $f->set_template('v_fieldwrap_full.php')
          ->set($html->textarea()->val($fields['content']), 'content') ?>
    
    <?=$f->set_template('v_fieldwrap.php')
        ->set($html->select($data['visible'], [$fields['visible']]), 'visible')?>
    
    <? 
        if(count($data['sort']) > 0){
            echo 
            $f->set_template('v_fieldwrap.php')->set_theme('default')
             ->set($html->ul($data['sort'], function($li, $elem) use ($html){
                $span = $html->elem('span', 1)->cl('ui-icon ui-icon-arrowthick-2-n-s');
                $li->html($span . $elem['title'])->attr('data-id', $elem['id_page'])->cl('ui-state-default');
            })->cl('list-unstyled sorting'), 'num_sort');
        }
    ?>
    <?=$html->input('hidden')->idn('num_sort')?>
    <? foreach($errors->unshown() as $err): ?>
        <p class="error"><?=$err?></p>
    <? endforeach; ?>
    <div class="submit-buttons">
        <input type="submit" value="Сохранить" class="btn btn-success">
        <a class="btn btn-danger" href="<?=$admin_root . $back ?>">Закрыть без сохранения</a>
    </div>
</form>

<div>
    <?= $html->ul_tree($tree, function($item) use($html, $admin_root){
        return $html->elem('a', 1)->html($item['title'])->attrs([
            'href' => $admin_root . 'pages/edit/' .  $item['id_page']
        ]);
    }) ?>
    
    <div class="offset-buttons">
        <a class="btn btn-primary btn"href="<?=$admin_root?>pages/add/">
            Добавить новую страницу &raquo;
        </a>
    </div>
</div>
<?php

namespace Admin\Pages;

use Config;
use Core\Traits\Actions;
use Core\Arr;
use Core\Helpers;
use Core\Exceptions;
use Core\Dir;
use Admin\Base\Fieldgen;

class C_pages extends \Admin\Base\Controller{
    use Actions\Page;
    use Actions\Add;
    use Actions\Edit;
    use Actions\Delete;
    
    public function before(){
        parent::before();
        
        $this->check_access('pages');
        $this->module = 'pages';
        $this->main_model = M_pages::instance();
    }
    
    public function action_tree(){
        $this->to_base_template['title'] .= 'Дерево страниц';
        $tree = $this->main_model->make_tree();
        $this->to_base_template['content'] = $this->template("{$this->module}/v/v_tree", ['tree' => $tree]);
    }
    
    protected function settings_action_page(){
        $this->errors = $this->main_model->errors();
        $this->to_base_template['title'] .= 'Список страниц: страница ' . $this->page_num;
    }
    
    protected function settings_action_add(){
        $this->to_base_template['title'] .= 'Добавление страницы';
        
        $this->add_scripts('base', ['lib/jquery.liTranslit', 'translit_init', 'ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init']);

        if (isset($this->params[2]) && is_numeric($this->params[2])) {
            $this->add_fields_data = $this->main_model->get($this->params[2]);
            
            if($this->add_fields_data === null){
                throw new Exceptions\E404("Item with id {$this->params[2]} not found.");
            }
        } else {
            $this->add_fields_data = ['id_parent' => 0];
        }
        
        $this->add_extract = ['id_parent', 'item_url', 'title', 'base_template', 'inner_template', 
                               'seo_title', 'seo_keywords', 'seo_description', 'content', 'visible'];  
        
        $visible = ['Видна всем и в поиске', 'Видна всем, но не в поиске',
            'Только для админа', 'Не видна'];

        $tree = $this->main_model->make_tree();
        array_unshift($tree, ['id_page' => 0, 'title' => 'Без категории']);
        $this->add_other_data['tree'] = Arr::to_select_tree($tree, 'id_page', 'title');
        $this->add_other_data['visible'] = $visible;
        
        $this->add_other_data['base_templates'] = Dir::ls(PATH_THEMES . '/' . Config::CURRENT_THEME . '/base_templates', '/\.(twig)$/i', false);
        $this->add_other_data['inner_templates'] = Dir::ls(PATH_THEMES . '/' . Config::CURRENT_THEME . '/inner_templates', '/\.(twig)$/i', false);
        
        $this->add_fieldgen = new Fieldgen($this->app->labels('pages'), $this->errors);
    }
    
    protected function settings_action_edit(){
        $this->to_base_template['title'] .= 'Редактирование страницы';
        
        $this->add_scripts('base', ['ckeditor/ckeditor']);
        $this->add_scripts($this->module, ['init', 'sort']);
        
        $this->edit_extract = ['id_parent', 'item_url', 'title', 'base_template', 'inner_template', 
                               'seo_title', 'seo_keywords', 'seo_description', 'content', 'visible', 'num_sort'];        
        
        $tree = $this->main_model->make_tree();
        $children = $this->main_model->get_children($this->params[2]);
        array_unshift($tree, ['id_page' => 0, 'title' => 'Без категории']);
        
        $disabled = [$this->params[2]];
        
        $down = $this->main_model->get_down_branch($this->params[2]);
        if ($down){
            foreach ($down as $item) {
                $disabled[] = $item['id_page'];
            }
        }
        
        $visible = ['Видна всем и в поиске', 'Видна всем, но не в поиске',
            'Только для админа', 'Не видна'];
        
        $this->edit_other_data['base_templates'] = Dir::ls(PATH_THEMES . '/' . Config::CURRENT_THEME . '/base_templates', '/\.(twig)$/i', false);
        $this->edit_other_data['inner_templates'] = Dir::ls(PATH_THEMES . '/' . Config::CURRENT_THEME . '/inner_templates', '/\.(twig)$/i', false);
        $this->edit_other_data['tree'] = Arr::to_select_tree($tree, 'id_page', 'title');
        $this->edit_other_data['visible'] = $visible;
        $this->edit_other_data['sort'] = $children;
        $this->edit_other_data['disabled'] = $disabled;
        $this->edit_fieldgen = new Fieldgen($this->app->labels('pages'), $this->errors);
    }
    
    protected function settings_action_delete(){}
}

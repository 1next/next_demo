<?php

namespace Admin\Pages\Api;

use Admin\Pages;

class Main extends \Core\Base\Model{

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Get_in;

    protected function __construct() {
        parent::__construct(Pages\App::instance(), 'pages');
    }
    
}

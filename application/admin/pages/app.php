<?php

namespace Admin\Pages;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;
    
    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'pages' => [
                    'c' => 'c_pages'
                ]
            ],
            'using' => [
                'users'
            ],
            'registry_privs' => [
                'pages' => 'Работа со страницами'
            ],
            'menu' => [
                'pages' => [
                    'name' => 'Страницы',
                    'icon' => 'columns',
                    'open' => [
                        'pages/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ],
                        'pages/tree' => [
                            'name' => 'Дерево',
                            'icon' => 'tree'
                        ],
                        'pages/add' => [
                            'name' => 'Добавить',
                            'icon' => 'plus-square-o'
                        ]
                    ],
                    'priority' => 99,
                    'access' => 'pages'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'pages_',
            'tables' => [
                'pages' => [
                    'fields' => ['id_page', 'id_parent', 'url', 'item_url', 'title', 'content', 
                        'num_sort', 'visible', 'base_template', 'inner_template'
                    ],
                    'not_empty' => ['title', 'item_url', 'visible', 'base_template', 
                        'inner_template', 'content'],
                    'html_allowed' => ['content'],
                    'unique' => [],
                    'composite_unique' => [['id_parent', 'item_url']],
                    'correct_url' => ['url', 'item_url'],
                    'range' => [
                        'title' => ['2', '128'],
                        'url' => ['2', '255'],
                        'item_url' => ['2', '128'],
                        'content' => ['0', '65535']
                    ],
                    'labels' => [
                        'id_parent' => 'Родительская страница',
                        'url' => 'Полный URL адрес',
                        'item_url' => 'URL страницы',
                        'title' => 'Заголовок',
                        'content' => 'Содержание',
                        'visible' => 'Видимость',
                        'num_sort' => 'Порядок сортировки дочерних элементов',
                        'base_template' => 'Базовый шаблон',
                        'inner_template' => 'Внутренний шаблон'
                    ],
                    'pk' => 'id_page'
                ]
            ]
        ];
    }

}

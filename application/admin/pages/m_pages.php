<?php

namespace Admin\Pages;
	
class M_pages extends \Core\Base\Model{
    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Tree;

    protected function __construct(){
        parent::__construct(App::instance(), 'pages');
    }
}
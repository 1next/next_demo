<div class="orders-list">
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>Информация</th>
                <th>Товары</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($data as $one): ?>
                <tr data-id-order="<?=$one['id_order']?>">
                    <td>
                        <table class="table table-hover table-bordered">
                            <tr>
                                <td>№ заказа</td>
                                <td><?=$one['id_order']?></td>
                            </tr>
                            <tr>
                                <td>Создан</td>
                                <td><?=$one['dt_open']?></td>
                            </tr>
                            <tr>
                                <td>Имя</td>
                                <td><?=$one['name']?></td>
                            </tr>
                            <tr>
                                <td>Телефон</td>
                                <td><?=$one['phone']?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?=$one['email']?></td>
                            </tr>
                            <tr>
                                <td>Адрес</td>
                                <td><?=$one['delivery']?></td>
                            </tr>
                            <tr>
                                <td>Коммент</td>
                                <td><?=$one['comment']?></td>
                            </tr>
                            <tr>
                                <td>Статус</td>
                                <td>
                                    <?=$html->select($other['statuses'], $one['status'])->idn('status')?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="table table-hover table-bordered">
                            <tr>
                                <th>Наименование</th>
                                <th>Цена</th>
                                <th>Кол-во</th>
                                <th>Скидка</th>
                                <th>Итого</th>
                            </tr>
                            <? $sum = 0;
                            foreach ($other['details'][$one['id_order']] as $po): 
                               $sum += $po['sum'];
                                ?>
                                <tr>
                                    <td><?=$other['products'][$po['id_product']][0]['title']?></td>
                                    <td><?=$po['price']?></td>
                                    <td><?=$po['cnt']?></td>
                                    <td><?=$po['sale']?></td>
                                    <td><?=$po['sum']?></td>
                                </tr>
                            <? endforeach ?>
                            <tr>
                                <td colspan="4">Сумма к оплате</td>
                                <td><?=$sum?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    <?= $navbar ?>
</div>
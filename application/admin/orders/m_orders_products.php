<?php

namespace Admin\Orders;

class M_orders_products extends \Core\Base\Model {

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Get_in;

    protected function __construct() {
        parent::__construct(App::instance(), 'products');
    }

}

$(function () {

    $('.orders-list').on('change', 'input, textarea, select', function(e){
       var field = $(this);
       var tr = field.parents('tr[data-id-order]').eq(0);

       var obj = {
           name: field.attr('name'),
           value: (field.attr('type') == 'checkbox') ? (field.prop('checked') ? 1 : 0) : field.val(),
           id_order: tr.data('id-order')
       }
       
       $.post(_PHP.admin_root + 'orders-ajax/save', obj, function(data){
            if(data.res){
                ;
            }
            else{
                alert(data.error);
            }
        }, 'json');

    });
    
});
<?php

namespace Admin\Orders;

use Core\Input;

class C_orders_ajax extends \Admin\Base\Ajax {

    public function before() {
        parent::before();
        $this->check_access('orders');
    }

    public function action_save() {    
        $mOrders = M_orders::instance();
        
        $id_index = Input::post('id_order');
        $name = Input::post('name');
        $value = Input::post('value');
        
        if($mOrders->edit($id_index, [$name => $value])){            
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
            $this->response['error'] = $mIndex->errors()->get($name);
        }
    }
    
    protected function set_error_template(\Core\Errors $errors) {
        
    }

}

<?php

namespace Admin\Orders;

use Core\Traits\Actions;
use Core\Arr;
use Admin\Shop\Api\Main as Shop;

class C_orders extends \Admin\Base\Controller {

    use Actions\Page;

    public function before() {
        parent::before();
        $this->check_access('orders');
        $this->module = 'orders';
        $this->main_model = M_orders::instance();
    }

    protected function settings_action_page() {
        $this->main_model->pager()->order_by('id_order DESC');
        $this->to_base_template['title'] .= 'Заказы: страница ' . $this->page_num;
    }

    protected function action_page_data_processor($data){
        $this->add_scripts($this->module, ['init']);
        
        $orders_id = Arr::section($data, 'id_order');
        
        $mPO = M_orders_products::instance();
        $mShop = Shop::instance();
        
        $pr2order = $mPO->get_in(['id_order' => $orders_id]);
        $products_id = Arr::section($pr2order, 'id_product');
        $products = $mShop->get_in(['id_product' => $products_id]);
        
        $this->page_other_data['details'] = Arr::grouping($pr2order, 'id_order');
        $this->page_other_data['products'] = Arr::grouping($products, 'id_product');
        $this->page_other_data['statuses'] = ['Новый', 'Принят', 'Нет связи', 'Отменён', 'Выполнен'];
    }
}

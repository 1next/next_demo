<?php

namespace Admin\Orders;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    public function init() {
        $this->on_page = 20;
    }

    public function manifest() {
        return [
            'routs' => [
                'orders' => [
                    'c' => 'c_orders'
                ],
                'orders-ajax' => [
                    'c' => 'c_orders_ajax'
                ]
            ],
            'using' => ['users'],
            'registry_privs' => [
                'orders' => 'Обработка заказов'
            ],
            'menu' => [
                'orders' => [
                    'name' => 'Обработка заказов',
                    'icon' => 'credit-card',
                    'open' => [
                        'orders/page' => [
                            'name' => 'Список',
                            'icon' => 'list-ul'
                        ]
                    ],
                    'priority' => 1,
                    'access' => 'orders'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'orders_',
            'tables' => [
                'index' => [
                    'fields' => ['id_order', 'id_client', 'phone', 'email', 'name', 'comment', 'status'],
                    'not_empty' => ['phone', 'name'],
                    'range' => [
                        'phone' => ['7', '15'],
                        'name' => ['2', '256']
                    ],
                    'email' => ['email'],
                    'max_length' => [
                        'email' => 256,
                        'comment' => 4096
                    ],
                    'pk' => 'id_order'
                ],
                'products' => [
                    'fields' => ['id_order', 'id_product', 'price', 'cnt', 'sale', 'why_sale', 'sum'],
                    'not_empty' => ['id_order', 'id_product', 'price', 'cnt', 'sale', 'sum'],
                    'pk' => ['id_order', 'id_product']
                ]
            ]
        ];
    }

}

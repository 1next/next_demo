<?php

use Core\Enum;

class Config {
    
    const MAIN_PROTOCOL = 'http';
    const DOMAIN = 'сайт.рф';
    const BASE_URL = '/';
     
    const HOST = self::MAIN_PROTOCOL . '://' . self::DOMAIN;
    
    const ADMIN_URL = 'a123';
    const CURRENT_THEME = 'eg';
	const FAST_STYLE = 'media/css/fast.min.css';

    const WORK_MODE = Enum::WORK_MODE_RPODUCTION; //WORK_MODE_DEVELOPMENT; // DEV | PROD
    const ERROR_LOG_MOD = Enum::ERROR_OUTPUR_FILE; // STANDART | OUR FILE
    const ERROR_LOG_DIR = PATH_SYSTEM . '/logs/';

    const DATABASES = [
        'main' => [
            'type' => 'mysql',
            'server' => 'localhost',
            'user' => 'root',
            'password' => '',
            'database' => 'eg',
            'prefix' => 'next_'
        ]
    ];

    const DIR_CACHE_WRAP = PATH_SYSTEM . '/cache';
    const DIR_CACHE_MANIFEST = 'manifest';
    const FIELDGEN_THEME = 'next';
    const HASH_METHOD = 'sha256';
    const HASH_KEY = '123'; 
    const HASH_LIGHT_METHOD = 'sha256';
    const HASH_LIGHT_KEY = '123';
    const CRYPT_KEY =  [1, 2, 3]; 
    const TIME_ZONE = 'Europe/Moscow';
    const LOCALE = 'ru_RU.UTF8';
    const DEFAULT_ON_PAGE = 10;

    public static $modules = [
        'admin' => [
            'users', 'pages', 'seopack','texts', 'tags', 'cats', 'rateloger',
			'menu', 'shop', 'images', 'gallery', 'htmlcompressor', 'orders'
			// 'editor','news','communications', 
        ],
        'client' => [
            'base', 'texts', 'pages', 'menu', 'cats', 'images', 'shop', 
			'breadcrumbs', 'seopack', 'rateloger', 'php', 'token', 'client','orders'
			 
			// 'news', 'tags''subscribe',
        ]
    ];
    
    public static $admin_validation_messages = [
        'not_empty' => 'Поле не должно быть пустым',
        'illegal_entry' => 'В этом поле недопустимая запись',
        'unique' => 'Такое значение уже есть, нужно уникальное',
        'composite_unique' => 'Для группы полей ":labels" такая связка значений уже есть',
        'max_length' => 'Не более ":param_1" символов',
        'min_length' => 'Не менее ":param_1" символов',
        'exact_length' => 'Нужно ровно ":param_1" символов',
        'equals' => '":label_1" и ":label_2" не совпадают',
        'email' => 'Впишите правильный адрес электронной почты',
        'email_domain' => 'Это поле быть адресом электронной почты',
        'correct_url' => 'Здесь присутствуют недопустимые символы',
        'phone' => 'Не менее :param_1 цифр',
        'date' => 'Здесь должна быть дата',
        'range' => 'В поле должно быть от ":param_1" до ":param_2" символов',
        'authorized' => 'Неправильные пароль или логин!!'
    ];

}

<?php

namespace Client\Rateloger;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;
use Core\Info;
use Core\Enum;

class App extends Base\App {

    use Singleton;
    
    private static $starttime;
    
    public function init() {
        Events::add('System.Init.Done', function() {
            self::$starttime = microtime(true);
        });
        
        Events::add('System.Output.BeforePrint', function(&$html) {  
            if(Info::execute_mode() == Enum::EXECUTE_MODE_SIMPLE){
                $html .= "\n<!--" . (microtime(true) - self::$starttime) . '-->';
            }
        }, 0);
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [];
    }

}

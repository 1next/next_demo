<?php

namespace Client\Base;

use Config;
use Core\Enum;
use Core\Headers;
use Extra\Template;

class Controller extends \Core\Base\Controller {
    
    
    public function __construct() {
        parent::__construct();
        $this->template_class = Template::get_variant(PATH_APPLICATION  . '/client/');
    }

    public function before() {
        $this->vars_to_js['root'] = $this->root;
        $this->template_class->add_global('root', $this->root);       
        $this->template_class->add_global('theme_root', $this->root . PATH_THEMES . '/' . Config::CURRENT_THEME);
    }

    public function render() {
        $this->to_base_template['vars_to_js'] = [];
		$this->to_base_template['fast_style'] = file_get_contents(PATH_THEMES . '/' . Config::CURRENT_THEME. '/' .Config::FAST_STYLE);
        
        foreach($this->vars_to_js as $k => $v){
            $this->to_base_template['vars_to_js'][$k] = json_encode($v);
        }
        
        return $this->template('base_templates/v_main', $this->to_base_template);
    }

    protected function set_content_404($e) {
        Headers::c404();
        
        $this->to_base_template['title'] = 'Страница не найдена';
        $this->to_base_template['content'] = $this->template('base/v_404');
        
        if (Config::WORK_MODE == Enum::WORK_MODE_DEVELOPMENT) {
            $this->to_base_template['content'] .= $this->template('base/v_exception', ['e' => $e]);
        }
    }
    
    protected function set_content_access($e) {
        
    }
    
    protected function set_error_template(\Core\Errors $errors) {
        $this->to_base_template['title'] = 'Ошибка при выполнении операции';
        $this->to_base_template['content'] = $this->template('base/v_some_error', ['errors' => $errors]);
    }
}

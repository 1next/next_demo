<?php

namespace Client\Base;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    public function init() {

    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [];
    }

}

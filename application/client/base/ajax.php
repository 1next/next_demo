<?php

namespace Client\Base;

use Config;
use Core\Info;
use Core\Enum;
use Extra\Template;

abstract class Ajax extends \Core\Base\Controller {

    protected $to_base_template = array();
    protected $user;
    protected $response;

    public function __construct() {
        parent::__construct();

        $this->response = [];
        Info::execute_mode(Enum::EXECUTE_MODE_AJAX);
        
        $this->template_class = Template::get_variant(PATH_APPLICATION  . '/client/');
        $this->template_class->add_global('root', $this->root);       
        $this->template_class->add_global('theme_root', $this->root . PATH_THEMES . '/' . Config::CURRENT_THEME);
    } 

    public function before() {}

    public function render() {
        return json_encode($this->response);
    }

    protected function set_content_404($e) {
        $this->response['errorCode'] = '404';
    }
    
    protected function set_content_access($e) {
        $this->response['errorCode'] = '403';
    }

    protected function set_error_template(\Core\Errors $errors) {
    }
}

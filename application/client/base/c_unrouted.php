<?php

namespace Client\Base;

use Core\Exceptions;

class C_unrouted extends Controller{
    
    public function action_index(){
        throw new Exceptions\E404('404');
    }

}

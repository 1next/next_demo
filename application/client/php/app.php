<?php

namespace Client\Php;

use Config;
use Core\Base;
use Core\Events;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;
    
    public function init() {
        Events::add('TemplateClass.wantGlobals', function(\Core\Template $template_class){
            $template_class->add_global('PHP', Functions::instance());
        });
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [
            'tables_prefix' => 'images_',
            'tables' => [
                'index' => [
                    'pk' => 'id_image'
                ],
                'items' => [
                    'pk' => ['id_image', 'id_item', 'item']
                ]
            ]
        ];
    }

}

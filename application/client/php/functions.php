<?php

namespace Client\Php;

use Config;

class Functions{

    use \Core\Traits\Singleton;
    
    private $alloweds;
    
    protected function __construct() {
        $this->alloweds = [
            'date', 'strtotime', 'sprintf', 'in_array'
        ];
    }

    public function __call($name, $arguments) {
        if(in_array($name, $this->alloweds)){
            return $name(...$arguments);
        }
        
        return false;
    }
    
    public function get_style($path){
        return file_get_contents(PATH_THEMES . '/' . Config::CURRENT_THEME . '/' . $path . '.css');
    }
    
    public function string_quuee(...$strings){
        foreach($strings as $str){
            if($str != ''){
                return $str;
            }
        }
        
        return '';
    }
}

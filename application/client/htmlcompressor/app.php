<?php

namespace Client\Htmlcompressor;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;
use Core\Info;
use Core\Enum;

class App extends Base\App {

    use Singleton;

    public function init() {
        Events::add('System.Output.BeforePrint', function(&$html) {
            if(Info::execute_mode() == Enum::EXECUTE_MODE_SIMPLE){
                $html = preg_replace('|[\s]+|s', ' ', $html);
            }
        });
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [];
    }

}

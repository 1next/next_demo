<?php

namespace Client\Shop;

use Core\Exceptions;
use Core\Input;
use Core\Arr;
use Core\Mailer\Simple as Email;
use Client\Cats\Api\Main as Cats;
use Client\Images\Api\Main as Images;
use Client\Orders\Api\Main as Orders;
use Client\Orders\Api\Products as OrdersProducts;

class C_ajax extends \Client\Base\Ajax {

    public function action_cart(){
        $this->response['res'] = true;
        $this->response['cart'] = count(Api\Cart::instance()->get()['products_id']);
    }
    
    public function action_to_cart(){
        $id_product = Input::post('id_product');
        $cnt = Input::post('cnt');
        
        if($id_product == null || $cnr = null){
            throw new Exceptions\E404('Странные параметры для добавления товара');
        }
        
        $mCart = Api\Cart::instance();
        
        if($mCart->add($id_product, $cnt)){
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
            $this->response['errors'] = $mCart->errors()->unshown();
        }
    }
    
    public function action_cart_remove(){
        $id_product = Input::post('id_product');
        
        if($id_product == null){
            throw new Exceptions\E404('Странные параметры для удаления товара из корзины');
        }
        
        $mCart = Api\Cart::instance();
        
        if($mCart->remove($id_product)){
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
            $this->response['errors'] = $mCart->errors()->unshown();
        }
    }
    
    public function action_order(){
        $mCart = Api\Cart::instance();
        $cart = $mCart->get();
        
        $this->response['res'] = false;
        
        if(count($cart['products']) < 1){
            $this->response['errors'] = ['Ваша корзина пуста'];
            return;
        }

        $form = Arr::extract($_POST, ['name', 'phone', 'delivery', 'email', 'comment']);
        
        $mOrders = Orders::instance();       
        $id_order = $mOrders->add($form);
        
        if($id_order === false){
            $this->response['errors'] = $mOrders->errors()->unshown();
            return;
        }
        
        $mOrderProducts = OrdersProducts::instance(); 
        $mShop = M_shop::instance();
        
        $products_id = Arr::section($cart['products'], 'id_product');
        $options = $mShop->get_options_by_group($products_id);
        
        $products_output = [];
        
        foreach($cart['products'] as $one){
            $price = $options[$one['id_product']]['Цена']['value'];
            $sum = $price * $one['cnt'];
            
            $to_add = [
                'id_order' => $id_order,
                'id_product' => $one['id_product'],
                'price' => $price,
                'cnt' => $one['cnt'],
                'sale' => 0,
                'sum' => $sum
            ];
            
            $mOrderProducts->add($to_add);
            $products_output[$one['id_product']] = $to_add;
        }
        
        $mCart->clear();
        
        $products_info = $mShop->get_in(['id_product' => $products_id]);

        $message = $this->template('shop/email/v_order_to_admin', [
            'order' => $mOrders->get($id_order),
            'products_output' => $products_output,
            'products_info' => $products_info
        ]);
        
        $email = Email::instance();
        $email->mailer->From = 'auto@holiday-shop.ru';
        $email->mailer->FromName = 'Holiday Shop';
        $email->send("Новый заказ №$id_order", 'order@holiday-shop.ru', $message);
        
        $this->response['res'] = true;
    }
    
    public function action_filter(){
        if(!isset($_POST['id_cat'])){
            throw new Exceptions\E404('Неправильный формат данных');
        }
        
        if(!isset($_POST['filters'])){
            $_POST['filters'] = [];
        }
        
        $id_cat = Input::post('id_cat');
        $filters = $_POST['filters'];
        
        $cat = Cats::instance()->get($id_cat);
        
        if($cat == null){
            throw new Exceptions\E404('Такой категории нет');
        }
        
        $mShop = M_shop::instance();
        
        $products = $mShop->get_by_filter($cat['id_cat'], $filters);
        /* $products = $mShop->get_by_filter_next($cat['id_cat'], $filters); */
        
        $ids = Arr::section($products, 'id_product');

        $children = $mShop->get_in(['id_prototype' => $ids]);
        $pr_children = Arr::grouping($children, 'id_prototype');

        $ch_ids = Arr::section($children, 'id_product');

        $options = $mShop->get_options_by_group(array_merge($ids, $ch_ids));
        $settings = App::FILTER_KEYS;
        
        $filters_build = $mShop->to_filters($options, $settings[$cat['id_cat']]);
        $filters_assoc = [];
        
        foreach($filters as $id_option => $filter){
            $filters_assoc[$id_option] = [];
               
            foreach($filter as $value){
                $filters_assoc[$id_option][$value] = true;
            }
        }
        
        $images = Images::instance()->get_by_items(App::IMAGE_RELATION, $ids);
        $pr_images = Arr::grouping($images, 'id_item');
        
        $this->response['filters'] = $this->template('shop/template-parts/v_filter', [
           'filters' => $filters_build,
           'filters_checked' => $filters_assoc
        ]);
        
        $this->response['products'] = $this->template('shop/template-parts/v_items', [
            'cats' => [$cat['id_cat'] => $cat],
            'products' => $products,
            'children' => $pr_children,
            'options' => $options,
            'images' => $pr_images,
        ]);
        
    }

}

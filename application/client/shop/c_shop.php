<?php

namespace Client\Shop;

use Client\Breadcrumbs\Api\Main as Breadcrumbs;
use Client\Cats\Api\Main as Cats;
use Client\Images\Api\Main as Images;
//use Client\Pages\Api\Main as Pages;
use Core\Arr;
use Core\Input;
use Core\Exceptions;
use Admin\Shop\M_excel as M_excel;
use PHPExcel_IOFactory;
use Config;


class C_shop extends \Client\Base\Controller {

    private $id_product;
    private $cat;
	private $url_activ;
	private $url;
  //  private $cat_sale_only = false;

    public function routed_home($url) {
        return $url === '';
    }

    public function routed_item($url) {
        $tmp = explode('/', $url);
        $ctmp = count($tmp);

        if ($ctmp < 2) {
            return false;
        }
		$this->url = $url;
		
        $product_url = $tmp[$ctmp - 1];
        unset($tmp[$ctmp - 1]);
        $cat_url = implode('/', $tmp);
		
		$this->url_activ['url'] = $product_url;
		
		$cat = Cats::instance()->get_by_fieldset([['cat_type' => App::SHOP_TYPE, 'url' => $cat_url]])[0];
        $product = M_shop::instance()->get_by_url($cat['id_cat'], $product_url);
	
		if(empty($product)){	
			$product = M_shop::instance()->get_by_url_child($cat['id_cat'], $product_url);
		}
		
		$product_id = M_shop::instance()->get_by_fieldset([['id_cat' => $cat['id_cat'], 'id_product' => $product_url]])[0];
		
	
		if ($product != null) {
            $this->id_product = $product['id_product'];
            $this->cat = $cat;
            return true;
        }
		elseif ($product_id != null) {
			$this->id_product = $product_id['id_product'];
            $this->cat = $cat;
            return true;
		}

        return false;
    }
	
	public function routed_cat($url) {
		$this->url = $url;
        $this->cat = Cats::instance()->get_by_fieldset([['cat_type' => App::SHOP_TYPE, 'url' => $url]])[0];
        return $this->cat != null;
    }
    
   	public function action_home() {
		$mShop = M_shop::instance();
		
        $cats = Cats::instance()->all(App::SHOP_TYPE);
		$cats = $mShop->image_to_cats($cats);
		$cats = $mShop->count_to_cats($cats);
	
		$col = $mShop->get_cols_cats(count($cats));

		$this->to_base_template['topmenu'] = 1;
		$this->to_base_template['col'] = $col;
		$this->to_base_template['col'] = $mShop->get_cols_cats(count($cats));
		$this->to_base_template['title'] = 'Питомник "Каланчиновское подворье" г.Ефремов';
		$this->to_base_template['content'] = $this->template('inner_templates/v_main', [
            'cats' => $cats
        ]);
    }
	
	public function action_katalog() {
		$mShop = M_shop::instance();

		$products = $mShop->all();
		$cats_menu = $mShop->get_menu($products);
		
		$cats = Cats::instance()->all(App::SHOP_TYPE);
		$cats_pr = M_shop::instance()->image_to_cats($cats);
		
		$this->to_base_template['topmenu'] = 2;
		$this->to_base_template['title'] = "Каталог растений";
		
		$mShop = M_shop::instance();
		$cat = $this->cat;
		$products = $mShop->get_by_fieldset([['id_cat' => $cat['id_cat'], 'id_prototype' => 0]]);
		$ids = Arr::section($products, 'id_product');
		
		$children = $mShop->get_in(['id_prototype' => $ids]);
		$mShop->sort_product($children);
		$ch_ids = Arr::section($children, 'id_product');
		
		$options = $mShop->get_options_by_group(array_merge($ids, $ch_ids));
		
		$this->to_base_template['content'] = $this->template('inner_templates/v_catalog', [
			'cats_pr' => $cats_pr,
			'cats_menu' => $cats_menu,
			'options' => $options,
			'url_activ' =>  $this->url_activ,
        ]);	
	}	

	public function action_cat() {
       
		$mShop = M_shop::instance();
		
		$products = $mShop->get_by_fieldset(['id_prototype' => 0]);
		$cats_menu = $mShop->get_menu($products);
		
		$cat = $this->cat;
		$this->vars_to_js['id_cat'] = $cat['id_cat'];

		Breadcrumbs::instance()->add($cat['url'], $cat['name']);
		  
		$products = $mShop->get_by_fieldset([['id_cat' => $cat['id_cat'], 'id_prototype' => 0]]);
		$ids = Arr::section($products, 'id_product');
		
		$children = $mShop->get_in(['id_prototype' => $ids]);
		$mShop->sort_product($children);

		$pr_children = Arr::grouping($children, 'id_prototype');
		$ch_ids = Arr::section($children, 'id_product');
		
		$options = $mShop->get_options_by_group(array_merge($ids, $ch_ids));
      	
		$ids = array_merge($ids, $ch_ids);
		$images = Images::instance()->get_by_items(App::IMAGE_RELATION, $ids);
		$pr_images = Arr::grouping($images, 'id_item');
		
		$this->to_base_template['topmenu'] = 2;
        $this->to_base_template['title'] = $cat['name'];
        $this->to_base_template['content'] = $this->template('inner_templates/v_catalog', [
            'cats' => [$cat['id_cat'] => $cat],
			'options' => $options,
			'images' => $pr_images,
			'children' => $pr_children,
			'products' => $products,
			'cats_menu' => $cats_menu,
			'url_activ' =>  $this->url_activ,
			'cat_url' =>  $this->cat['url'],
			'breadcrumbs' => Breadcrumbs::instance()->get()
        ]);	
    }    
	
	public function action_item() {
		  
		$mShop = M_shop::instance();
		
		$products = $mShop->get_by_fieldset(['id_prototype' => 0]);
		$cats_menu = $mShop->get_menu($products);		
		
		$cat = $this->cat;
		$this->vars_to_js['id_cat'] = $cat['id_cat'];
		
		$products = $mShop->get_by_fieldset([['id_cat' => $cat['id_cat'], 'id_product' => $this->id_product]]);
		$ids = Arr::section($products, 'id_product');

		$children = $mShop->get_in(['id_prototype' => $ids]);
			
		$filter = Input::get('filter');
		$subfilter = Input::get('subfilter');	
		
		if($filter != '' or $subfilter != ''){
			$childs = $mShop->apply_filter($children, $filter, $subfilter);
			
			$this->url_activ['ripening'] = $subfilter;
			$this->url_activ['color'] = $filter;
		
			if(!empty($childs)){
				$children = $childs;
			}
		}
 
		$mShop->sort_product($children);
		
        $pr_children = Arr::grouping($children, 'id_prototype');
		$ch_ids = Arr::section($children, 'id_product');

        $options = $mShop->get_options_by_group(array_merge($ids, $ch_ids));
      
		$ids = array_merge($ids, $ch_ids);
		$images = Images::instance()->get_by_items(App::IMAGE_RELATION, $ids);
		
        $pr_images = Arr::grouping($images, 'id_item');
		
		$product = $mShop->compile_by_prototype($this->id_product);
		
		Breadcrumbs::instance()->add($cat['url'], $cat['name']);
		  
		if(empty($children)){
			$prototype = $mShop->get_url_prototype($this->id_product);
			
			if(!empty($prototype)){
				Breadcrumbs::instance()->add($cat['url'].'/'.$prototype['value'], $prototype['title']);
			}
		}
		  
		$crumb = $product['title'];
		
		if($filter != ''){		$crumb .= ' / '.$filter;		}
		if($subfilter != ''){	$crumb .= ' / '.$subfilter;	}
		
		Breadcrumbs::instance()->add('#', $crumb);
			
		$this->to_base_template['topmenu'] = 2;
        $this->to_base_template['title'] = $products[0]['title'];
        $this->to_base_template['content'] = $this->template('inner_templates/v_catalog', [
            'cats' => [$cat['id_cat'] => $cat],
            'products' => $products,
            'children' => $pr_children,
			'images' => $pr_images,
			'options' => $options,
			'cats_menu' => $cats_menu,
			'url_activ' =>  $this->url_activ,
			'cat_url' =>  $this->cat['url'],
			'breadcrumbs' => Breadcrumbs::instance()->get()
		 ]);	
    }    
   
	public function action_search() {
        $q = Input::get('q');
		
        if ($q == '') {
            throw new Exceptions\E404('q is empty');
        }

        $this->to_base_template['title'] = "Поиск: \"$q\"";

        $mShop = M_shop::instance();

        $products = $mShop->find($q);

        if (empty($products)) {
			$this->to_base_template['topmenu'] = 5;
            $this->to_base_template['content'] = $this->template_class->render('shop/search/v_nothing', [
                'q' => $q
            ]);
		} else {
            $ids = Arr::section($products, 'id_product');

            $children = $mShop->get_in(['id_prototype' => $ids]);
            $pr_children = Arr::grouping($children, 'id_prototype');

            $ch_ids = Arr::section($children, 'id_product');

            $options = $mShop->get_options_by_group(array_merge($ids, $ch_ids));

            $images = Images::instance()->get_by_items('shop', $ids);
            $pr_images = Arr::grouping($images, 'id_item');

            $cats_id = Arr::section($products, 'id_cat');
            $cats = Cats::instance()->get_in(['id_cat' => $cats_id]);
			
			$this->to_base_template['topmenu'] = 5;
            $this->to_base_template['content'] = $this->template_class->render('shop/search/v_results', [
                'products' => $products,
                'children' => $pr_children,
                'options' => $options,
                'images' => $pr_images,
                'cats' => $cats,
				'q' => $q
            ]);
        }
    }

	protected function action_price() {
	
		$excel =  M_excel::instance()->get_price();
		
		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$writer->save('php://output');	

		throw new Exceptions\Exit_now('1');
	}
}

<style>
    table, table td, table th{
        border: 1px solid #000;
        border-collapse: collapse;
    }
    
    table{
        border-radius: 5px;
    }
    
    table td, table th{
        padding: 10px;
    }
</style>
<h1>Поступил новый заказ</h1>
<h2>Информация о заказе</h2>
<table>
    <tbody>
        <tr>
            <td>№ заказа</td>
            <td><?=$order['id_order']?></td>
        </tr>
        <tr>
            <td>Дата</td>
            <td><?=$order['dt_open']?></td>
        </tr>
        <tr>
            <td>Имя</td>
            <td><?=$order['name']?></td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td><?=$order['phone']?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?=$order['email']?></td>
        </tr>
        <tr>
            <td>Адрес доставки</td>
            <td><?=$order['delivery']?></td>
        </tr>
        <tr>
            <td>Комментарий</td>
            <td><?=$order['comment']?></td>
        </tr>
    </tbody>
</table>
<h2>Товары</h2>
<table>
    <tbody>
        <tr>
            <th>Название</th>
            <th>Цена</th>
            <th>Кол-во</th>
            <th>Скидка</th>
            <th>Итого</th>
        </tr>
        <? foreach($products_info as $product): 
            $out = $products_output[$product['id_product']]; 
            ?>
        <tr>
            <td><?=$product['title']?></td>
            <td><?=$out['price']?></td>
            <td><?=$out['cnt']?></td>
            <td><?=$out['sale']?></td>
            <td><?=$out['sum']?></td>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
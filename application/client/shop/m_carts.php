<?php

namespace Client\Shop;

class M_carts extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'carts');
    }

}

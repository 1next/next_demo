<?php

namespace Client\Shop;

class M_products_options extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'pr2op');
    }

    public function get_by_ids($pk_array){
        if(empty($pk_array)){
            return [];
        }
        
        $prefix = $this->db->prefix() . $this->app->prefix();
        $to = $prefix . 'options';
        $tu = $prefix . 'units';
        
        $clear = [];
        
        foreach($pk_array as $pk){
            $clear[] = (int)$pk;
        }        
        
        $in = implode(',', $clear);
        
        $res = $this->db->select("SELECT * FROM {$this->table}
                                  LEFT JOIN $to using (id_option)
                                  LEFT JOIN $tu using (id_unit)
                                  WHERE id_product IN ($in)
        ");
        
        return $res;
    }
}

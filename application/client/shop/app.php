<?php

namespace Client\Shop;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    const SHOP_TYPE = 'shop';
    const SHOP_NAME = 'Для магазина';
    const IMAGE_TYPE = 'shop';
    const IMAGE_NAME = 'Для магазина';
    const IMAGE_RELATION = 'shop';
    const FILTER_KEYS = [
        1 => ['Страна', 'Производитель', 'Цвет', 'Количество мест'],
        2 => ['Тип', 'Матрас'],
        3 => ['Длина', 'Ширина'],
        4 => ['Длина'],
        5 => ['Длина', 'Ширина'],
        6 => ['Длина'],
        8 => ['Ширина'],
        9 => ['Тип', 'Высота', 'Цвет']
    ];
    const SALES_PAGE = [1];

    public function init() {
        Events::add('System.Init.Done', function() {
            Api\Cart::instance();
        });
        
        Events::add('System.Unrouted', function(&$run) {
            $c = new C_shop();
            
            if(empty($run['params']) && $c->routed_home('')){
                $run['c'] = $c;
                $run['a'] = 'action_home';
                $run['module'] = 'shop';
                return false;
            }
            
            if ($c->routed_cat(implode('/', $run['params']))) {
                $run['c'] = $c;
                $run['a'] = 'action_cat';
                $run['module'] = 'shop';
                return false;
            }

            if ($c->routed_item(implode('/', $run['params']))) {
                $run['c'] = $c;
                $run['a'] = 'action_item';
                $run['module'] = 'shop';
                return false;
            }
        }, 100);

        Events::add('Module.Seopack.Robots', function(\Client\Seopack\Robots $robots) {
            $robots->add('Disallow', '/shop/cart');
            $robots->add('Disallow', '/shop-ajax');
            $robots->add('Disallow', '/search?*');
            $robots->add('Disallow', '/search/?*');
        });
    }

    public function manifest() {
        return [
            'routs' => [
                'shop-ajax' => [
                    'c' => 'c_ajax'
                ],
                'search' => [
                    'c' => 'c_shop',
                    'a' => 'search'
                ],
                'shop/cart' => [
                    'c' => 'c_shop',
                    'a' => 'cart'
                ],
				'katalog' => [
                    'c' => 'c_shop',
                    'a' => 'katalog'
                ],
				'price' => [
                    'c' => 'c_shop',
                    'a' => 'price'
                ]
				
            ],
            'using' => [
                'cats' => [
                    'cat_type' => self::SHOP_TYPE,
                    'cat_name' => self::SHOP_NAME
                ],
                'images' => [
                    'image_type' => self::IMAGE_TYPE,
                    'image_name' => self::IMAGE_NAME,
                    'image_item' => self::IMAGE_RELATION
                ],
                'seopack',
                'token',
                'orders'
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'shop_',
            'tables' => [
                'products' => [
                    'pk' => 'id_product'
                ],
                'carts' => [
                    'fields' => ['id_cart', 'id_token'],
                    'not_empty' => ['id_token'],
                    'pk' => 'id_cart'
                ],
                'cart2pr' => [
                    'fields' => ['id_cart', 'id_product', 'cnt'],
                    'range' => [
                        'cnt' => ['1', '100'],
                    ],
                    'not_empty' => ['cnt'],
                    'pk' => ['id_cart', 'id_product']
                ]
            ]
        ];
    }

}

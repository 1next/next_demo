<?php

namespace Client\Shop\Api;

use Client\Shop;
use Core\Errors;
use Client\Token\Api\Token;

class Cart {

    use \Core\Traits\Singleton;

    protected $cart;
    protected $isReal;
    protected $mCarts;
    protected $mCartProducts;
    protected $errors;

    protected function __construct() {
        $this->cart = null;
        $this->isReal = false;
        $this->mCarts = Shop\M_carts::instance();
        $this->mCartProducts = Shop\M_carts_products::instance();
        $this->errors = new Errors();
        $this->find();
    }

    public function get() {
        return ($this->isReal && $this->cart !== null) ? $this->cart : $this->fake();
    }

    public function add($id_product, $cnt) {
        if(!$this->isReal){
            $this->init();
        }
        
        $options = Shop\M_shop::instance()->get_options_by_group([$id_product])[$id_product];

        if (!isset($options['Цена'])) {
            $this->errors->add(0, "Этот товар нельзя положить в корзину!\n\nВы пытаетесь добавить в корзину товар, у которого не указана цена.");
            return false;
        }

        $cnt = (int) $cnt;

        if ($cnt < 1) {
            $this->errors->add(0, "Количество товара не может быть меньше 1 :)");
            return false;
        }

        $exist = $this->mCartProducts->get([$this->cart['id_cart'], $id_product]);

        if ($exist != null) {
            $res = $this->mCartProducts->edit([$this->cart['id_cart'], $id_product], ['cnt' => $cnt]);
        } else {
            $res = $this->mCartProducts->add([
                'id_cart' => $this->cart['id_cart'],
                'id_product' => $id_product,
                'cnt' => $cnt
            ]);
        }

        if (!$res) {
            $this->errors->add(0, 'Неизветсная ошибка при добавлении товара в корзину');
        }

        return $res;
    }

    public function remove($id_product) {
        if(!$this->isReal){
            return false;
        }
        
        $this->mCartProducts->delete([$this->cart['id_cart'], $id_product]);
        return true;
    }

    public function clear() {
        if(!$this->isReal){
            return false;
        }
        
        $this->mCartProducts->delete_by_fieldset(['id_cart' => $this->cart['id_cart']]);
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    protected function find(){
        $token = Token::instance()->get();
        
        if($token !== null){
            $res = $this->mCarts->get_by_fieldset(['id_token' => $token]);

            if (!empty($res)) {
                $this->cart = $res[0];
                
                $this->cart['products'] = $this->mCartProducts->get_by_fieldset(['id_cart' => $this->cart['id_cart']]);
                $this->cart['products_id'] = [];

                foreach ($this->cart['products'] as $one) {
                    $this->cart['products_id'][$one['id_product']] = $one['cnt'];
                }
                
                $this->isReal = true;
            }
        }
    }

    protected function init() {
        $token = Token::instance()->init()->get();

        if ($this->cart === null) {
            $id = $this->mCarts->add(['id_token' => $token]);
            $this->cart = $this->mCarts->get($id);
        }

        $this->cart['products'] = $this->mCartProducts->get_by_fieldset(['id_cart' => $this->cart['id_cart']]);
        $this->cart['products_id'] = [];

        foreach ($this->cart['products'] as $one) {
            $this->cart['products_id'][$one['id_product']] = $one['cnt'];
        }
        
        $this->isReal = true;
    }

    protected function fake(){
        return [
            'id_cart' => 0,
            'id_token' => 0,
            'products' => [],
            'products_id' => []
        ];
    }
}

<?php

namespace Client\Shop;

use Client\Shop\App;
use Client\Shop\M_products_options;
use Core\Arr;
use Client\Cats\M_cats as M_cats;

class M_shop extends \Core\Base\Model {

    use \Core\Traits\Singleton;

use \Core\Traits\Model\Get_in;

use \Core\Traits\Model\Make_fieldset;

    private $table_p2o;
    private $table_options;
    private $table_units;

    protected function __construct() {
        parent::__construct(App::instance(), 'products');
        $this->table_p2o = $this->db->prefix() . $this->app->prefix() . 'pr2op';
        $this->table_options = $this->db->prefix() . $this->app->prefix() . 'options';
        $this->table_units = $this->db->prefix() . $this->app->prefix() . 'units';
    }

    public function get_by_url_child($id_cat, $url) {
        $res = $this->db->select("SELECT * FROM {$this->table}
                          LEFT JOIN {$this->table_p2o} using (id_product)
                          LEFT JOIN {$this->table_options} using (id_option)
                          WHERE id_cat=:id_cat AND {$this->table_options}.url='url'
                                AND {$this->table_p2o}.value=:url", ['url' => $url, 'id_cat' => $id_cat]
        );

        return $res[0];
    }  
	
	public function get_by_url($id_cat, $url) {
        $res = $this->db->select("SELECT * FROM {$this->table}
                          LEFT JOIN {$this->table_p2o} using (id_product)
                          LEFT JOIN {$this->table_options} using (id_option)
                          WHERE id_prototype='0' AND id_cat=:id_cat AND {$this->table_options}.url='url'
                                AND {$this->table_p2o}.value=:url", ['url' => $url, 'id_cat' => $id_cat]
        );

        return $res[0];
    }
	
	private function get_by_option($ids) {
		$res = $this->db->select("SELECT id_product, op.url, p2o.value FROM {$this->table} 
                        LEFT JOIN {$this->table_p2o} p2o using (id_product)
                        LEFT JOIN {$this->table_options} op using (id_option)
						WHERE id_product IN ($ids) AND op.url IN ('color','ripening','age')");	 
		
		$result = Arr::grouping($res, 'id_product');
		 
		$options = [];
		
		foreach ($result as $k => $res) {
			foreach ($res as $r) {
				$options[$k][$r['url']] = $r['value'];		
			}
		}
		return $options;
    }
	
	private function get_by_filters($ids) {
		$children = $this->db->select("SELECT id_prototype, id_product, op.url, p2o.value FROM {$this->table} 
                        LEFT JOIN {$this->table_p2o} p2o using (id_product)
                        LEFT JOIN {$this->table_options} op using (id_option)
						WHERE id_prototype IN ($ids) AND op.url IN ('color','ripening','age')");	

			
		
		$children = Arr::grouping($children, 'id_product');	
		
						
		$tmp = [];
		
		foreach ($children as $k => $res) {
					
			foreach ($res as $r) {
				if($tmp[$k]['id_prototype'] == ''){
					$tmp[$k]['id_prototype'] = $r['id_prototype'];
				}
				if($tmp[$k]['id_product'] == ''){
					$tmp[$k]['id_product'] = $r['id_product'];
				}
				$tmp[$k][$r['url']] = $r['value'];		
			}
		}

		$options = Arr::grouping($tmp, 'id_prototype');	
	
		$filter = [];
		
		foreach ($options as $k => $opts) {
			foreach ($opts as $v) {
				if($v['color'] != '' and $v['ripening'] != ''){
					if($filter[$k][$v['color']] == 'color'){
						$filter[$k][$v['color']] = [];
					}
					
					$filter[$k][$v['color']][] = $v['ripening'];
					$filter[$k][$v['color']] = array_unique($filter[$k][$v['color']]);	
					sort($filter[$k][$v['color']]);
					
				}elseif($v['age'] != '' and $v['ripening'] != ''){
					if($filter[$k][$v['age']] == 'age'){
						$filter[$k][$v['age']] = [];
					}
					$filter[$k][$v['age']][] = $v['ripening'];
					$filter[$k][$v['age']] = array_unique($filter[$k][$v['age']]);	
					sort($filter[$k][$v['age']]);
					
				}else{
					if($v['color'] != ''){
						$filter[$k][$v['color']] = 'color';
					}
					if($v['ripening'] != ''){
						$filter[$k][$v['ripening']] = 'ripening';
					}
					if($v['age'] != ''){
						$filter[$k][$v['age']] = 'age';
					}
				}
			}
		}
		
		foreach ($filter as $k => $f) {
			if(count($f)>1){
				ksort($filter[$k]);
			}
		}
		return $filter;
	}
	
	public function get_menu($products) {
		
	
		$products = $this->db->select("SELECT id_cat, id_product, p.title, op.url, p2o.value FROM {$this->table} p
                        LEFT JOIN {$this->table_p2o} p2o using (id_product)
                        LEFT JOIN {$this->table_options} op using (id_option)
						WHERE id_prototype = 0");	 
		
		$products = Arr::grouping($products, 'id_cat');
		
		$ids = '';
		
		foreach ($products as $k => $pr){
			foreach ($pr as $p){
				//if($k == 3){
					if($ids != ''){
						$ids .= ',';
					}
					$ids .= $p['id_product'];
				//}
			}
		} 
			
		$filter = $this->get_by_filters($ids);	
	
	// \Core\Helpers::preprint($products);
		
		$tmp =[];
		
		foreach ($products as $id_cat => $prod){
			
			foreach ($prod as $pr){
				if($pr['url'] == 'url'){
					$tmp[$id_cat][$pr['title']]['url'] = $pr['value'];
				}
				foreach ($filter as $id => $f){
					if($id == $pr['id_product']){
						$tmp[$id_cat][$pr['title']]['filters'] =  $f;
							
					}
				}
			}
		}

		foreach ($tmp as $k=>$p){
			ksort($tmp[$k]);
		}
		
				
		$products = $tmp;
					
		$cats = M_cats::instance()->all();
	
		$menu = [];
		$tmp = [];
		
		foreach ($cats as $cat){
			foreach ($products as $id_cat => $prod){
				if($cat['id_cat'] == $id_cat){
					$tmp['name'] = $cat['name'];
					$tmp['url'] = $cat['url'];
					$tmp['products'] = $prod;
				}
			}
			$menu[] = $tmp;	
			
		}
				
		return $menu;	
	}
	

		
	public function apply_filter($children, $filter, $subfilter) {
		
		if($filter == '' and $subfilter == ''){
			return [];
		}
		
		$ids = '';
		
		foreach ($children as $c) {
			if($ids != ''){
				$ids .= ',';
			}
			$ids .= $c['id_product'];
		}
		
		$options = $this->get_by_option($ids);

		$ids = [];
		
		foreach ($options as $k => $v) {
		
			
			if($filter != '' and $subfilter != ''){
				if($filter == $v['color'] and $subfilter == $v['ripening']){
					$ids[] = $k;
				}
				if($filter == $v['age'] and $subfilter == $v['ripening']){
					$ids[] = $k;
				}
				
			}else{
				if($subfilter == $v['ripening'] and $subfilter != '' and $filter == ''){
					$ids[] = $k;	
				}
				if($filter == $v['color'] and $filter != '' and $subfilter == ''){
					$ids[] = $k;
				}
				if($filter == $v['age'] and $filter != '' and $subfilter == ''){
					$ids[] = $k;
				}
			}
		 }
		$ids = array_unique($ids);
		
		$childs = [];
		
		foreach ($children as $c) {
			foreach ($ids as $id) {
				if($c['id_product'] == $id){
					$childs[] = $c;
				}
			}
		}	
		return $childs;
	}
  
    public function get_by_filter($id_cat, $filters) {
        if(empty($filters)){
            return $this->get_by_fieldset([['id_cat' => $id_cat, 'id_prototype' => 0]]);
        }
        
        $sql = "SELECT DISTINCT pr1.*
                FROM {$this->table} pr1
                LEFT JOIN {$this->table} pr2 ON (pr2.id_prototype = pr1.id_product)
                LEFT JOIN {$this->table_p2o} p1 ON (pr1.id_product = p1.id_product OR pr2.id_product = p1.id_product)";

        $i = 2;        
        $params_pdo = ['id_cat' => $id_cat];        
        
        foreach ($filters as $id_option => $values) {
            $alias = 'p' . $i;
            $pdo_id_option = $alias . '_id_option';
            
            $params_pdo[$pdo_id_option] = $id_option;
            $values_pairs = [];
            
            for($j = 0; $j < count($values); $j++){
                $pdo_value = "value_{$i}_{$j}";
                $values_pairs[] = "$alias.value = :$pdo_value";
                $params_pdo[$pdo_value] = $values[$j];
            }
            
            $in = implode(' OR ', $values_pairs);
            
            $sql .= " INNER JOIN {$this->table_p2o} $alias ON (
                    (pr1.id_product = $alias.id_product OR pr2.id_product = $alias.id_product) AND 
                    $alias.id_option = :$pdo_id_option AND ($in)
                    )";
            
            $i++;
        }

        $sql .= ' WHERE pr1.id_prototype = 0 AND pr1.id_cat=:id_cat';
        
        return $this->db->select($sql, $params_pdo);
    }
    
    public function get_by_filter_next($id_cat, $filters) {
        /*
         * CREATE VIEW `next_shop_view` as
            SELECT p.`id_product`,
            p.`id_prototype`,
            p.`id_cat`,
            p.`title`,
            p1.`id_product` AS id_product1,
            p1.`id_prototype` AS id_prototype1,
            p2o.`id_option`,
            p2o.`value`
            FROM `next_shop_products` AS p
            LEFT JOIN `next_shop_products` AS p1 ON p1.`id_prototype` = p.`id_product`
            LEFT JOIN `next_shop_pr2op` AS p2o ON (p.`id_product` = p2o.`id_product` OR p1.`id_product` = p2o.`id_product`)
         */
        
        if(empty($filters)){
            return $this->get_by_fieldset([['id_cat' => $id_cat, 'id_prototype' => 0]]);
        }
        
        $sql_forward = '';
        $sql_back = " AND id_prototype = '0' AND id_cat=:id_cat";

        $params_pdo = ['id_cat' => $id_cat]; 
        $cnt = count($filters);
        $i = 0;
        
        foreach ($filters as $id_option => $values) {
            $pdo_id_option = $i . '_id_option';
            $params_pdo[$pdo_id_option] = $id_option;
            
            $values_pairs = [];
            
            for($j = 0; $j < count($values); $j++){
                $pdo_value = "value_{$i}_{$j}";
                $values_pairs[] = "value = :$pdo_value";
                $params_pdo[$pdo_value] = $values[$j];
            }
            
            $in = implode(' OR ', $values_pairs);
            
            $fields = ($i === 0) ? 'DISTINCT id_product, id_prototype, id_cat, title' : 'DISTINCT id_product';
            $sql_forward .= "SELECT $fields FROM lavr_shop_view WHERE";
            $sql_back = " id_option=:$pdo_id_option AND ($in)" . $sql_back;
            
            if($i < $cnt - 1){
                $sql_forward .= ' id_product IN(';
                $sql_back = ') AND' . $sql_back;
            }
            
            $i++;
        }
        
        $sql = $sql_forward . $sql_back;
        return $this->db->select($sql, $params_pdo);
    }

    public function compile_by_prototype($pk) {
        $product = $this->get($pk);

        if ($product == null) {
            return null;
        }

        $ids = [$product['id_product']];
        $children = $this->get_by_fieldset(['id_prototype' => $pk]);

        foreach ($children as $child) {
            $child['options'] = [];
            $product['children'][$child['id_product']] = $child;
            $ids[] = $child['id_product'];
        }

        $pr2op = M_products_options::instance()->get_by_ids($ids);
        $product['options'] = [];

        foreach ($pr2op as $k => $o) {
            if ($o['id_product'] == $pk) {
                $product['options'][$o['title']] = $o;
            } else {
                $product['children'][$o['id_product']]['options'][$o['title']] = $o;
            }
        }

        if (!empty($product['children'])) {
            foreach ($product['children'] as $k => $child) {
                foreach ($child['options'] as $ko => $option) {
                    if (isset($product['options'][$option['title']])) {
                        $str = str_replace('%parent%', $product['options'][$option['title']]['value'], $option['value']);
                        $product['children'][$k]['options'][$ko]['value'] = $str;
                    }
                }
            }
        }

        /* $o_by_p = Arr::grouping($pr2op, 'id_option');
          $options = [];

          foreach ($o_by_p as $option_group) {
          if (count($option_group) == 1) {
          $options[] = $option_group[0];
          } else {
          $child_option = ($option_group[0]['id_product'] == $pk) ? $option_group[0] : $option_group[1];
          $parent_option = ($option_group[0]['id_product'] != $pk) ? $option_group[0] : $option_group[1];
          $child_option['value'] = str_replace('%parent%', $parent_option['value'], $child_option['value']);


          $product['children'][$option_group[0]['id_product']]['options'][] = $child_option;
          }
          } */

        return $product;
    }

    public function get_options_by_group($ids, $key = 'title') {
        $options = M_products_options::instance()->get_by_ids($ids);
        $assoc = Arr::grouping($options, 'id_product');
        $res = [];

        foreach ($assoc as $id_product => $pr2op) {
            $res[$id_product] = [];

            foreach ($pr2op as $option) {
                $res[$id_product][$option[$key]] = $option;
            }
        }

        return $res;
    }

    /**
     * 
     * @param array $pr2op Массив вида результата работы get_options_by_group
     */
    public function to_filters($pr2op, $allowed) {
        if (empty($allowed)) {
            return [];
        }

        $filters = [];

        foreach ($allowed as $f) {
            $filters[$f] = [
                'values' => []
            ];
        }

        foreach ($pr2op as $id_product => $options) {
            foreach ($options as $option) {
                $title = $option['title'];

                if (in_array($title, $allowed)) {
                    if (!isset($filters[$title]['option'])) {
                        $filters[$title]['option'] = $option;
                    }

                    if (!isset($filters[$title]['values'][$option['value']])) {
                        $filters[$title]['values'][$option['value']] = 0;
                    }

                    $filters[$title]['values'][$option['value']] ++;
                }
            }
        }

        foreach ($filters as $title => $arr) {
            if (count($arr['values']) == 0) {
                unset($filters[$title]);
                continue;
            }

            asort($filters[$title]['values']);
            $filters[$title]['values'] = array_reverse($filters[$title]['values'], true);
        }

        return $filters;
    }

    public function find($string) {
        $table = $this->db->prefix() . 'shop_products';
        return $this->db->select("SELECT * FROM $table WHERE title LIKE :title", ['title' => "%$string%"]);
    }

    public function same_products(int $id_cat, int $id_product, int $cnt){
        $products = $this->db->select("SELECT * FROM {$this->table} WHERE 
                                                    id_prototype = 0 AND
                                                    id_cat = :id_cat AND 
                                                    id_product > :id_product
                                                    ORDER BY id_product
                                                    LIMIT $cnt",
                                        [
                                            'id_cat' => $id_cat,
                                            'id_product' => $id_product
                                        ]);
        
        $dif = $cnt - count($products);
        
        if($dif > 0){
            $take_ids = Arr::section($products, 'id_product');
            $take_ids[] = $id_product;
            $not_in = implode(',', $take_ids);
            
            $dop_products = $this->db->select("SELECT * FROM {$this->table} WHERE 
                                                    id_prototype = 0 AND
                                                    id_cat = :id_cat AND 
                                                    id_product NOT IN ($not_in)
                                                    ORDER BY id_product
                                                    LIMIT $dif",
                                        [
                                            'id_cat' => $id_cat
                                        ]);
            
            $products = array_merge($products, $dop_products);
        }
        
        return $products;
    }
	
	public function image_to_cats($cats) {
        $table = $this->db->prefix() . 'images_items';
		$table_join = $this->db->prefix() . 'images_index';
		$tmp = [];
		
		foreach ($cats as $c) {
			$q = $this->db->select("SELECT * FROM `$table` AS i JOIN `$table_join` AS i2 ON i.id_image = i2.id_image "
					. "WHERE i.item = 'cat' AND i.id_item = ".$c['id_cat']);
			
			//\Core\Helpers::preprint($q[0]); 
			
			if(!empty($q[0]['id_image']) ){	
				$c['id_image']	= $q[0]['id_image'];
				$c['title']		= $q[0]['title'];
				$c['alt']		= $q[0]['alt'];
				$c['image']		= $q[0]['location'].$q[0]['name'];
				$c['ext_img']	= '.'.$q[0]['ext'];
				
				/*
				$c['image_slider']		= $q[0]['location'].$q[0]['name'].'-cat-slider.'.$q[0]['ext'];
				$c['image_home']		= $q[0]['location'].$q[0]['name'].'-cat-home.'.$q[0]['ext'];
				//$c['image_middle']		= $q[0]['location'].$q[0]['name'].'-cat-middle.'.$q[0]['ext'];
				$c['image_shop_mini']	= $q[0]['location'].$q[0]['name'].'-shop-mini.'.$q[0]['ext'];
				$c['image_shop_middle'] = $q[0]['location'].$q[0]['name'].'-shop-middle.'.$q[0]['ext'];
				$c['image_shop_origin'] = $q[0]['location'].$q[0]['name'].'-shop-origin.'.$q[0]['ext'];
				*/
			}
			$tmp[] = $c;
		}
		$cats = $tmp;
		$tmp = null;
		
		return $cats;
    }
	
	public function count_to_cats($cats) {
		$table = $this->db->prefix() . 'shop_products';
		foreach ($cats as &$c) {
			$q = $this->db->select("SELECT count(id_product) AS count FROM `$table` 
										WHERE id_cat = ".$c['id_cat']);

			$c['count']	= $this->count_end($q[0]['count']);
		}
		
		return $cats;
    }
	
	public function count_end($num) {
		$number = substr($num, -2);

		// 11 - 14
		if($number > 10 and $number < 15)    {
			$term = "й";
		}
		else{ 
			$number = substr($number, -1);

			if($number == 0) {$term = "й";}
			if($number == 1 ) {$term = "е";}
			if($number > 1 ) {$term = "я";}
			if($number > 4 ) {$term = "й";}
		}

		return $num.' наименовани'.$term;
	}
			
	public function product_to_cats111($cats) {
		$table = $this->db->prefix() . 'cats_cats';
		$cats_pr = [];
				
		//	\Core\Helpers::preprint($cats);	
		foreach ($cats as $k => $cat){
			$q = $this->db->select("SELECT * FROM `$table` WHERE `id_cat` = ". $k);
			$cats_pr[$k]['name'] = $q[0]['name'];
			$cats_pr[$k]['url'] = $q[0]['url'];
		
			$pr = [];
			
			$res = $this->db->select("SELECT * FROM {$this->table}
							  LEFT JOIN {$this->table_p2o} using (id_product)
							  LEFT JOIN {$this->table_options} using (id_option)
							  WHERE id_prototype='0' AND id_cat=:id_cat ", ['id_cat' => $k]
			);

		   foreach ($res as $r){
				foreach ($cat as $c){
					if($r['id_product'] == $c['id_product'] ){
						$pr[$r['value']] = $c['title']; // id_product
					}
				}
			}	
			$cats_pr[$k]['products'] = $pr;		
		}
	
		sort($cats_pr);
				
		//\Core\Helpers::preprint($cats_pr);	
		return $cats_pr;
	}
	
	//Пользовательская сортировка двумерного массива
	public function sort_product(&$arr){
		
		usort($arr, function ($v1, $v2) {
			if ($v1['title'] == $v2['title']){return 0;}
			return ($v1['title'] < $v2['title'])? -1: 1;
		}); 
		
		return $arr;
	}
			
	public function get_url_prototype($id_product){
		
		 $prod = $this->db->select("SELECT * FROM {$this->table}
                           WHERE id_product=:id_product", ['id_product' => $id_product]);
		 
		  $prototype = $this->db->select("SELECT * FROM {$this->table}
                          LEFT JOIN {$this->table_p2o} using (id_product)
                          WHERE id_product=:id_product", ['id_product' => $prod[0]['id_prototype']]
        );
								
        return $prototype[0];
		
	}
	
	public function get_cols_cats($count){
		$col = '';
		$s3 = $count % 3;
		$s4 = $count % 4;
		
		if($s3 == 0 and $s4 != 0){
			$col = 3;
		}elseif($s3 == 1 and $s4 == 1){
			$col = 3;
		}elseif($s3 == 2 and $s4 == 1){
			$col = 3;
		}elseif($s3 == 2 and $s4 == 2){
			$col = 3;
		}	
		
        return $col;
		
	}
}

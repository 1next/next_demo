<?php

namespace Client\Shop;

class M_carts_products extends \Core\Base\Model {

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Delete_by_fieldset;

    protected function __construct() {
        parent::__construct(App::instance(), 'cart2pr');
    }

}

<?php

namespace Client\Menu;

use Core\Traits\Singleton;
use Core\Sql;
use Extra\Template;

class Renderer{

    use Singleton;
    
    protected $template_class;
    protected $db;

    protected function __construct(){
        $this->template_class = Template::get_variant(PATH_APPLICATION . '/client/');
        $this->db = Sql::instance();
    }
    
    public function replace($code, $template){
        $t_menu = $this->db->prefix() . 'menu_menu';
        $t_items = $this->db->prefix() . 'menu_items';
        $t_pages = $this->db->prefix() . 'pages_pages';
        
        $items = $this->db->select("SELECT p.title, p.url FROM $t_menu as m
                                    LEFT JOIN $t_items as mi using(id_menu)
                                    LEFT JOIN $t_pages as p ON p.id_page = mi.id_item 
                                    WHERE shortcode=:shortcode
                                    ORDER BY mi.num_sort ASC", ['shortcode' => $code]);
        
        return $this->template_class->render('menu/' . $template, ['items' => $items]);
    }

}

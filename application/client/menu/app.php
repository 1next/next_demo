<?php

namespace Client\Menu;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    public function init() {
        Events::add('System.Output.BeforePrint', function(&$html){
            $html = preg_replace_callback('|<widget menu-template="(.+?)">(.+?)</widget>|', function($matches){
                return Renderer::instance()->replace($matches[2], $matches[1]);
            }, $html);
        });
    }

    public function manifest() {
        return [
            'using' => ['pages']
        ];
    }

    public function rules() {
        return [];
    }

}

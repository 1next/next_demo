<?php

namespace Client\Token;

use Core\Base;
use Core\Events;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;
    
    const TOKEN_COOKIE_NAME = 'token-for-all-or-not-for-all';
    
    public function init() {
        Events::add('System.Init.Done', function(){
            Api\Token::instance();
        });
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [
            'tables_prefix' => 'token_',
            'tables' => [
                'index' => [
                    'fields' => ['id_token', 'token', 'dt'],
                    'not_empty' => ['token'],
                    'unique' => ['token'],
                    'token' => [
                        'item' => ['64', '64'],
                    ],
                    'pk' => 'id_token'
                ]
            ]
        ];
    }

}

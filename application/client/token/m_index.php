<?php

namespace Client\Token;

class M_index extends \Core\Base\Model{

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'index');
    }

}

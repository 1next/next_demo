<?php

namespace Client\Token\Api;

use Client\Token as Module;
use Core\Cookie;
use Core\Helpers;

class Token{

    use \Core\Traits\Singleton;

    protected $token_cookie_name;
    protected $token;
    protected $mIndex;
    
    protected function __construct(){
        $this->token_cookie_name = Module\App::TOKEN_COOKIE_NAME;
        $this->token = null;
        $this->mIndex = Module\M_index::instance();
        $this->find();
    }

    public function get($key = 'id_token'){
        return ($this->token === null) ? null : $this->token[$key];
    }

    public function init(){  
        if($this->token === null){
            $fields = ['token' => Helpers::random_string(64)];
            $i = 0;
            
            while($i++ < 10){
                $id = $this->mIndex->add($fields);
                
                if($id){
                    $this->token = $this->mIndex->get($id);
                    Cookie::push($this->token_cookie_name, $this->token['token']);
                    break;
                }
                else{
                    $fields['token'] = Helpers::random_string(64);
                }
            }
        }
        
        return $this;
    }
    
    protected function find(){
        $hash = Cookie::has($this->token_cookie_name) ? Cookie::read($this->token_cookie_name) : null;
        
        if($hash != null){
            $res = $this->mIndex->get_by_fieldset(['token' => $hash]);
            
            if(!empty($res)){
                $this->token = $res[0];
            }
        }
    }
}

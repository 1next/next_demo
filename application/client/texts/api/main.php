<?php

namespace Client\Texts\Api;

use Core\Sql;
use Core\Arr;

class Main{

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\All;
    
    private $db;
    private $table;
    private $cache;
    
    protected function __construct() {
        $this->db = Sql::instance();
        $this->table = $this->db->prefix()  . 'texts_texts';
        $this->cache = null;
    }

    public function get($key){
        if($this->cache === null){
            $texts = $this->all();
            $this->cache = Arr::to_select($texts, 'alias', 'content');
        }
        
        return isset($this->cache[$key]) ? $this->cache[$key] : '';
    }
}

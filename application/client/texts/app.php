<?php

namespace Client\Texts;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    public function init() {
        Events::add('TemplateClass.wantGlobals', function(\Core\Template $template_class){
            $template_class->add_global('Texts', Api\Main::instance());
        });
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [];
    }

}

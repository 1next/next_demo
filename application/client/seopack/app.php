<?php

namespace Client\Seopack;

use Config;
use Core\Base;
use Core\Traits\Singleton;
use Core\Events;
use Core\Input;
use Core\Headers;
use Core\Exceptions;
use Core\Enum;

class App extends Base\App {

    use Singleton;

    const ALLOWED_304 = true;
    const REDIRECT_TO_MAIN_PROTOCOL = true;
    const STATIC_CONTENT_VERSION = 10;

    public function init() {
        Events::add('System.Init.Done', function($params, $get) {
            
            if(self::REDIRECT_TO_MAIN_PROTOCOL){
                $https = Input::server('HTTP_X_HTTPS') == '1';
                
                if($https xor Config::MAIN_PROTOCOL == 'https'){
                    Headers::c301();
                    Headers::send('Location: ' . Config::HOST . Input::server('REQUEST_URI'));
                    throw new Exceptions\Exit_now('protocol redirect 301');
                }
            }
            
            $seo = Api\Data::instance();
            $url = implode('/', $params);
            
            if($url == ''){
                $url = 'home';
            }
            
            $r301 = M_redirects::instance()->get_by_fieldset(['url_from' => $url])[0] ?? null;
            
            if($r301 != null){
                Headers::c301();
                Headers::send('Location: ' . Config::HOST . Config::BASE_URL . $r301['url_to']);
                throw new Exceptions\Exit_now('handle redirect 301');
            }
            
            if($seo->load($url) && self::ALLOWED_304 && Config::WORK_MODE == Enum::WORK_MODE_RPODUCTION){
                $lastModified = $seo->lastmod();
                $etagFile = md5(Input::server('REQUEST_URI') . $lastModified);
                
                $ms = Input::server('HTTP_IF_MODIFIED_SINCE');
                $et = Input::server('HTTP_IF_NONE_MATCH');
                
                $ifModifiedSince = ($ms != null) ? $ms : false;
                $etagHeader = ($et != null) ? $et : false;

                if (@strtotime($ifModifiedSince) >= $lastModified || $etagHeader == $etagFile) {
                    Headers::send(Input::server('SERVER_PROTOCOL') . ' 304 Not Modified');
                    $exit = true;
                }

                if (!$exit){
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified) . " GMT");
                }
                    
                Headers::send("Etag: $etagFile");
                Headers::send('Cache-Control: public, max-age=3600');

                if ($exit){
                    throw new Exceptions\Exit_now('not modify');
                }
            }
            
            $seo->system['canonical'] = Config::HOST;
            
            if($url != ''){
                $seo->system['canonical'] .= Config::BASE_URL . $url;
            }
        });

        Events::add('TemplateClass.wantGlobals', function(\Core\Template $template_class) {
            $template_class->add_global('SEO', Api\Data::instance());
            $template_class->add_global('static_content_version', self::STATIC_CONTENT_VERSION);
        });

    }

    public function manifest() {
        return [
            'routs' => [
                'robots.txt' => [
                    'c' => 'c_seo',
                    'a' => 'robots'
                ],
                'sitemap' => [
                    'c' => 'c_seo',
                    'a' => 'sitemap'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'seopack_',
            'tables' => [
                'index' => []
            ]
        ];
    }

}

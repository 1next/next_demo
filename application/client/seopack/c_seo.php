<?php

namespace Client\Seopack;

use Core\Events;
use Core\Headers;
use Core\Exceptions;
use Core\Html;
use Config;

class C_seo extends \Client\Base\Controller {

    public function action_robots() {
        $robots = Robots::instance();
        $root = Config::MAIN_PROTOCOL . '://' . Config::DOMAIN;

        $robots->add('User-agent', '*');
        $robots->add_empty_line();
        $robots->add('Host', $root);
        $robots->add('Sitemap', $root . Config::BASE_URL . 'sitemap.xml');
        $robots->add_empty_line();

        Events::run('Module.Seopack.Robots', $robots);


        Headers::send('Content-Type: text/plain');
        echo $robots->out();
        throw new Exceptions\Exit_now('this robots.txt');
    }

    public function action_sitemap() {
        $mIndex = M_index::instance();
        $urls = $mIndex->get_by_fieldset(['in_sitemap' => '1']);
	//	$urls = $mIndex->get_by_fieldset(['in_sitemap' => '1'], 'item, id_item ASC');
        $map = $mIndex->structure($urls);
        
        $html = Html::instance();
        
        $f = function($item) use ($html){
            return $html->elem('a', 1)->html($item['h1'])->attrs([
                'href' => $item['url'],
                'title' => $item['title'],
                'target' => '_blank'
            ]);
        };

		$sitemap = $html->ul_tree($map, $f)->addClass('sitemap')->render();

		$this->to_base_template['topmenu'] = 5;
       // $this->to_base_template['content'] = $sitemap;
				
		$this->to_base_template['content'] = $this->template('inner_templates/v_sitemap', [
			'sitemap' => $sitemap
        ]);	
		
    }

}

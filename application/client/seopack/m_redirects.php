<?php

namespace Client\Seopack;

class M_redirects extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'redirects');
    }
    
}

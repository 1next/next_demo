<?php

namespace Client\Seopack;

class M_index extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'index');
    }

    public function structure($urls){
        $map = [];
        $dictionary = [];

        foreach ($urls as $one) {
            $url = $one['url'];
            $dictionary[$url] = $one;
        }
        
        foreach ($urls as $one) {
            $url = $one['url'];
            $dictionary[$url] = $one;

            $tmp = explode('/', $url);
            $aim = & $map;
            $queue = [];
            $cnt = count($tmp);

            for ($i = 0; $i < $cnt; $i++) {
                $part = $tmp[$i];
                $queue[] = $part;

                if (!is_array($aim[$part])) {
                    $aim[$part] = [];
                    $aim[$part]['children'] = [];
                }

                if ($i === $cnt - 1) {
                    $aim[$part] = array_merge($aim[$part], $one);
                }

                $aim = & $aim[$part]['children'];
            }
        }
        
        return $map;
    }
}

<?php

namespace Client\Seopack\Api;

use Client\Seopack;

class Data{
    
    use \Core\Traits\Singleton;
    
    public $system;
    private $row;
    private $model;

    protected function __construct() {
        $this->system = [];
        $this->row = [];
        $this->model = Seopack\M_index::instance();
    }
    
    public function load($url){
        $index = $this->model->get_by_fieldset(['url' => $url]);
        
        if(!empty($index[0])){
            $this->row = $index[0];
            return true;
        }
        
        return false;
    }
    
    public function __call($name, $args) {
        return (isset($this->row[$name]) && $this->row[$name] != '') ? $this->row[$name] : null;
    }
}
<?php

namespace Client\Seopack;

class Robots{

    use \Core\Traits\Singleton;
    
    private $lines;

    private function __construct() {
        $this->lines = [];
    }
    
    public function add($name, $value){
        $this->lines[] = "$name: $value";
    }
    
    public function add_empty_line(){
        $this->lines[] = '';
    }
    
    public function out(){
        return implode("\n", $this->lines);
    }
}

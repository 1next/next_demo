<?php

namespace Client\Orders;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;
    
    public function init() {
        
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [
            'tables_prefix' => 'orders_',
            'tables' => [
                'index' => [
                    'fields' => ['id_order', 'id_client', 'phone', 'delivery', 'email', 'name', 'comment'],
                    'not_empty' => ['phone', 'name', 'delivery'],
                    'range' => [
                        'phone' => ['7', '15'],
                        'name' => ['2', '256']
                    ],
                    'email' => ['email'],
                    'max_length' => [
                        'email' => 256,
                        'delivery' => 512,
                        'comment' => 4096
                    ],
                    'pk' => 'id_order'
                ],
                'products' => [
                    'fields' => ['id_order', 'id_product', 'price', 'cnt', 'sale', 'why_sale', 'sum'],
                    'not_empty' => ['id_order', 'id_product', 'price', 'cnt', 'sale', 'sum'],
                    'pk' => ['id_order', 'id_product']
                ]
            ]
        ];
    }

}

<?php

namespace Client\Orders\Api;
use Client\Orders\App;

class Products extends \Core\Base\Model{

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'products');
    }

}

<?php

namespace Client\Images;

class M_images extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'index');
    }

    public function get($pk){
        $img = parent::get($pk);
        
        if($img !== null){
            $img['path'] = $img['location'] . $img['name'] . '.' . $img['ext'];
        }
        
        return $img;
    }
}

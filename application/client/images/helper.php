<?php

namespace Client\Images;

use Config;

class Helper{

    public $root_images;
    
    public function __construct() {
        $this->root_images = Config::BASE_URL . PATH_UPLOADS . '/';
    }
    
    public function path($path, $slug = null){
        $res = $this->root_images;

        if($path === null) {
            return App::NO_IMG_PATH;
        }
        
        if($slug == null){
            $res .= $path;
        }
        else{
            $tmp = explode('.', $path);
            $num = count($tmp) - 1;
            $ext = $tmp[$num];
            unset($tmp[$num]);
            $res .= implode('.', $tmp) . '-' . $slug . '.' . $ext;
        }
        
        return $res;
    }
}

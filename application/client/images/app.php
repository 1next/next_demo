<?php

namespace Client\Images;

use Config;
use Core\Base;
use Core\Events;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;

    const NO_IMG_PATH = '/uploads/no_image.png';//'/uploads/no_image_mini.png';
    
    public function init() {
        Events::add('TemplateClass.wantGlobals', function(\Core\Template $template_class){
            $template_class->add_global('ImageHelper', new Helper());
        });
        
        Events::add('System.Output.BeforePrint', function(&$html){
            $html = preg_replace_callback('|<div data-id="(.+?)" data-size="(.+?)" data-widget="image">(.+?)</div>|', 
                function($matches){
                    return Renderer::instance()->replace($matches[1], $matches[2]);
                }, $html);
        });
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [
            'tables_prefix' => 'images_',
            'tables' => [
                'index' => [
                    'pk' => 'id_image'
                ],
                'items' => [
                    'pk' => ['id_image', 'id_item', 'item']
                ]
            ]
        ];
    }

}

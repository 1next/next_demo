<?php

namespace Client\Images;

use Core\Traits\Singleton;
use Core\Sql;
use Extra\Template;

class Renderer{

    use Singleton;
    
    protected $template_class;
    protected $db;

    protected function __construct(){
        $this->template_class = Template::get_variant(PATH_APPLICATION . '/client/');
        $this->db = Sql::instance();
    }
    
    public function replace($id, $size){
        $image = M_images::instance()->get($id);
        
        if($image === null){
            return '';
        }
        
        return $this->template_class->render('images/v_image', [
            'img' => $image,
            'size' => $size
        ]);
    }

}

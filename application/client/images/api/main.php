<?php

namespace Client\Images\Api;

use Client\Images;

class Main extends \Core\Base\Model{

    use \Core\Traits\Singleton;

    private $table_items;
    
    protected function __construct() {
        parent::__construct(Images\App::instance(), 'index');
        $this->table_items = $this->db->prefix() . $this->app->prefix() . 'items';
    }

    public function get_by_item($item, $id_item){
        return $this->db->select("SELECT *, CONCAT(location, name, '.', ext) as path
                                  FROM {$this->table_items}
                                  LEFT JOIN {$this->table} using(id_image)
                                  WHERE item=:item AND id_item=:id_item AND is_show='1'
                                  ORDER BY num_sort ASC",
                                  ['item' => $item, 'id_item' => $id_item]);
    }

    public function get_by_items($item, $items_id){
        if(empty($items_id)){
            return [];
        }
        
        $clear_id = [];
        
        foreach($items_id as $id){
            $clear_id[] = (int)$id;
        }
        
        $in = implode(',', $clear_id);
        
        $res = $this->db->select("SELECT *, CONCAT(location, name, '.', ext) as path
                                  FROM {$this->table_items}
                                  LEFT JOIN {$this->table} using(id_image)
                                  WHERE item=:item AND id_item IN ($in) AND is_show='1'
                                  ORDER BY num_sort ASC",
                                  ['item' => $item]);
                                  
        return $res;                          
    }
}

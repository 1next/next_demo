<?php

namespace Client\Images;

class M_images_items extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'items');
    }
}

<?php

namespace Client\Breadcrumbs;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    public function init() {
        Events::add('System.Template.Base.BeforeRender', function(&$to_base_template, &$vars_to_js){
            $to_base_template['breadcrumbs'] = Api\Main::instance()->get();
        });
        
        Api\Main::instance()->add('katalog', '<span class="fa fa-home"></span>Каталог');
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [];
    }

}

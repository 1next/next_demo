<?php

namespace Client\Breadcrumbs\Api;

use Core\Traits\Singleton;

class Main {

    use Singleton;

    private $crumbs;

    protected function __construct() {
        $this->crumbs = [];
    }

    public function add($url, $content) {
        $this->crumbs[] = ['url' => $url, 'content' => $content];
    }
    
    public function get(){
        return $this->crumbs;
    }
}

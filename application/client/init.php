<?php

namespace Client;

class Init extends \Core\Base\Init {

    public function __construct($params, $get) {
        parent::__construct($params, $get, 'client');
    }

}

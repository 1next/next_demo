<?php

namespace Client\Cats\Api;

use Client\Cats;

class Main {

    use \Core\Traits\Singleton;

    protected $mCats;

    protected function __construct() {
        $this->mCats = Cats\M_cats::instance();
    }

    public function all($cat_type){
        return $this->mCats->get_by_fieldset(['cat_type' => $cat_type]);
    }
    
    public function tree($cat_type){
        return $this->mCats->cat_tree($cat_type);
    }
    
    public function get($id_cat){
        return $this->mCats->get($id_cat);
    }
    
    public function get_by_fieldset($params){
        return $this->mCats->get_by_fieldset($params);
    }
    
    public function get_in($params){
        $cats = $this->mCats->get_in($params);
        $res = [];
        
        foreach($cats as $one){
            $res[$one['id_cat']] = $one;
        }
        
        return $res;
    }

    public function get_up_branch($pk) {
        return $this->mCats->get_up_branch($pk);
    }
    
    public function get_down_branch($pk) {
        return $this->mCats->get_down_branch($pk);
    }
}

<?php

namespace Client\Cats;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    public function init() {
        Events::add('System.Template.Base.BeforeRender', function(&$to_base_template, &$js_vars){
            $to_base_template['cats_list'] = Api\Main::instance()->all('shop');
        });
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [
            'tables_prefix' => 'cats_',
            'tables' => [
                'cats' => [
                    'pk' => 'id_cat'
                ]
            ]
        ];
    }

}

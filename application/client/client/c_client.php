<?php

namespace Client\Client;

class C_client extends \Client\Base\Controller {

    public function action_login() {
        $this->to_base_template['title'] = 'Авторизация в личном кабинете';
        $this->to_base_template['content'] = $this->template('client/v_login');
    }

}

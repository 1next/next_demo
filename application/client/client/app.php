<?php

namespace Client\Client;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    public function init() {
        Events::add('Module.Seopack.Robots', function(\Client\Seopack\Robots $robots) {
            $robots->add('Disallow', '/subscribe');
        });
    }

    public function manifest() {
        return [
            'routs' => [
                'client' => [
                    'c' => 'c_client'
                ]
            ]
        ];
    }

    public function rules() {
        return [];
    }

}

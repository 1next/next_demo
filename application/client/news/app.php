<?php

namespace Client\News;

use Core\Base;
use Core\Traits\Singleton;
use Config;
use Core\Events;

class App extends Base\App {

    use Singleton;

    const BLOG_ROUT_URL = 'blog';
    const BLOG_FLOW_URL = 'flow';
    const BLOG_NAME = 'Статьи и новости';
    const ON_PAGE = 5;
    const PAGE_NUM_PATTERN = "/^(\d)$/";
    const PAGE_NUM_LINK = "%s";
    const TAG_TYPE = 'news';
    const TAG_NAME = 'Для новостей';
    const TAG_RELATION = 'news';
    const CAT_TYPE = 'news';
    const CAT_NAME = 'Для новостей';
    
    public function init() {
        Events::add('TemplateClass.wantGlobals', function(\Core\Template $template_class){
            $template_class->add_global('root_blog', Config::BASE_URL . self::BLOG_ROUT_URL);
        });
        
        Events::add('System.Output.BeforePrint', function(&$html){
            $html = preg_replace_callback('|<div data-widget="cut-more">(.+?)</div>|', function($matches){
                return '';
            }, $html);
        });
    }

    public function manifest() {
        return [
            'routs' => [
                self::BLOG_ROUT_URL => [
                    'c' => 'c_news',
                    'a' => 'index'
                ]
            ],
            'using' => [
                'breadcrumbs',
                'tags' => [
                    'tag_type' => self::TAG_TYPE,
                    'tag_name' => self::TAG_NAME,
                    'tag_item' => self::TAG_RELATION
                ],
                'cats' => [
                    'cat_type' => self::CAT_TYPE,
                    'cat_name' => self::CAT_NAME
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'news_',
            'tables' => [
                'news' => [
                    'pk' => 'id_new'
                ]
            ]
        ];
    }

}

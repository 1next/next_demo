<?php

namespace Client\News;

use Client\Breadcrumbs\Api\Main as Breadcrumbs;
use Client\Cats\Api\Main as Cats;
use Client\Tags\Api\Main as Tags;
use Core\Exceptions;
use Core\Arr;

class C_news extends \Client\Base\Controller{
    private $cat;
    private $cat_page;
    private $tag;
    private $item;
    
    public function action_index(){
        $params = array_slice($this->params, 1);
        $url = implode('/', $params);
        
        Breadcrumbs::instance()->add(App::BLOG_ROUT_URL, App::BLOG_NAME);
        
        if(empty($params) || $params[0] === 'flow'){
            $this->action_flow();
        }
        elseif($this->routed_tag($url)){
            $this->action_tag();
        }
        elseif($this->routed_cat($url)){
            $this->action_cat();
        }
        elseif($this->routed_item($url)){
            $this->action_item();
        }
        else{
            throw new Exceptions\E404('blog item not found on site');
        }
    }
    
    public function routed_cat($url){
        $mNews = M_news::instance();
        $mCats = Cats::instance();
        
        $tmp = explode('/', $url);
        
        $page_num = false;
        $cnt = count($tmp);
        
        if($cnt > 1){
            $page_num = $mNews->parse_page_num($tmp[$cnt - 1]);
        }
        
        if($page_num !== false){
            unset($tmp[$cnt - 1]);
            $url = implode('/', $tmp);
            $this->cat = $mCats->get_by_fieldset([['cat_type' => App::CAT_TYPE, 'url' => $url]])[0] ?? null;
            $this->cat_page = $page_num;
        }
        else{
            $this->cat = $mCats->get_by_fieldset([['cat_type' => App::CAT_TYPE, 'url' => $url]])[0] ?? null;
            $this->cat_page = 1;
        }
        
        return $this->cat != null;
    }
    
    public function routed_tag($url){
        $cnt = count($this->params);
        
        if($url[0] == '@' && ($cnt == 2 || $cnt == 3)){
            $tag_url = substr($this->params[1], 1);
            
            $this->tag = Tags::instance()->get_by_url(App::TAG_TYPE, $tag_url);
            return $this->tag !== null;
        }
        
        return false;
    }
    
    public function routed_item($url){
        $tmp = explode('/', $url);
        $ctmp = count($tmp);

        $post_url = $tmp[$ctmp - 1]; 
        unset($tmp[$ctmp - 1]);
        $cat_url = implode('/', $tmp);
        $id_cat = 0;

        if($cat_url != ''){
            $cat = Cats::instance()->get_by_fieldset([['cat_type' => App::CAT_TYPE, 'url' => $cat_url]])[0] ?? null;
            
            if($cat == null){
                throw new Exceptions\E404('blog item not found on site');
            }
            
            $this->cat = $cat;
            $id_cat = $cat['id_cat'];
        }
        else{
            $this->cat = ['id_cat' => 0];
        }
        
        $this->item = M_news::instance()->get_by_url($id_cat, $post_url);
        return $this->item != null;
    }
    
    public function action_flow(){
        $this->to_base_template['title'] = 'Блог';
        $mNews = M_news::instance();
        
        $page_num = isset($this->params[2]) ? $mNews->parse_page_num($this->params[2]) : 1;

        if($page_num === false){
            throw new Exceptions\E404("page_num is unvalid");
        }
        
        $news = $this->base_pagination_settings()
                     ->page_num($page_num)
                     ->url_self(App::BLOG_ROUT_URL . '/' . App::BLOG_FLOW_URL . '/' . App::PAGE_NUM_LINK)
                     ->page();
        
        $content = $this->pack_news_flow($news);
        
        if($page_num > 1){
            Breadcrumbs::instance()->add('#', 'Cтраница №' . $page_num);
            $this->to_base_template['title'] .= ' - страница ' . $page_num;
        }
        
        $this->pack_template($content);
    }
    
    public function action_cat(){
        $this->breadcrumbs_cat_research($this->cat);
        $this->to_base_template['title'] = 'Категория "' . $this->cat['name'] . '"';

        $mNews = M_news::instance();
        $page_num = $this->cat_page;
        
        $down_branch = Cats::instance()->get_down_branch($this->cat['id_cat']);
        $cats_id = Arr::section($down_branch, 'id_cat');
        $cats_id[] = $this->cat['id_cat'];
        $in = implode(',', $cats_id);
        
        $news = $this->base_pagination_settings()
                      ->add_where("AND id_cat IN ($in)")
                      ->page_num($page_num)
                      ->url_self(App::BLOG_ROUT_URL . '/' . $this->cat['url'] . '/' . App::PAGE_NUM_LINK)
                      ->page();
        
        $content = $this->pack_news_flow($news);
        
        if($page_num > 1){
            Breadcrumbs::instance()->add('#', 'Cтраница №' . $page_num);
            $this->to_base_template['title'] .= ' - страница ' . $page_num;
        }
        
        $this->pack_template($content);
    }
    
    public function action_tag(){
        $this->to_base_template['title'] = 'Поиск по тегу: "' . $this->tag['name'] . '"';
        Breadcrumbs::instance()->add(App::BLOG_ROUT_URL . '/@' . $this->tag['url'], '@' . $this->tag['name']);
        
        $mNews = M_news::instance();
        
        $page_num = isset($this->params[2]) ? $mNews->parse_page_num($this->params[2]) : 1;

        if($page_num === false){
            throw new Exceptions\E404("page_num is unvalid");
        }

        $news = $this->base_pagination_settings()
                      ->join('lavr_tags_items t ON id_new = id_item')
                      ->add_where("AND t.id_tag=:id_tag")
                      ->page_num($page_num)
                      ->url_self(App::BLOG_ROUT_URL . '/@' . $this->tag['url'] . '/' . App::PAGE_NUM_LINK)
                      ->bind('id_tag', $this->tag['id_tag'])
                      ->page();
        
        $content = $this->pack_news_flow($news);
        
        if($page_num > 1){
            Breadcrumbs::instance()->add('#', 'Cтраница №' . $page_num);
            $this->to_base_template['title'] .= ' - страница ' . $page_num;
        }
        
        $this->pack_template($content);
    }
    
    public function action_item(){
        $item = $this->item;
        
        $this->breadcrumbs_cat_research($this->cat);
        Breadcrumbs::instance()->add('#', $item['title']);
        
        $this->to_base_template['title'] = $item['title'];
        $content = $this->template('news/v_item', ['item' => $item]);
        $this->pack_template($content);
    }

    private function breadcrumbs_cat_research($cat){
        if($cat['id_cat'] == 0){
            return;
        }
        
        $up_branch = Cats::instance()->get_up_branch($cat['id_cat']);
        $mBread = Breadcrumbs::instance();
        
        foreach($up_branch as $up_cat){
            $mBread->add(App::BLOG_ROUT_URL . '/' . $up_cat['url'], $up_cat['name']);
        }
        
        $mBread->add(App::BLOG_ROUT_URL . '/' . $cat['url'], $cat['name']);
    }
    
    private function pack_news_flow($news){
        $mNews = M_news::instance();
        $navparams = $mNews->pager()->navparams();
        
        if(empty($news) && $navparams['page_num'] != 1){
            throw new Exceptions\E404('page_num great than max_page num');
        }

        $mNews->introAll($news);
        $mTags = Tags::instance();
        
        $news_id = Arr::section($news, 'id_new');
        $tags = $mTags->get_by_group(App::TAG_RELATION, $news_id);
        $cats = Cats::instance()->all(App::CAT_TYPE);
        
        $navbar = $this->template('base/v_navbar', $navparams);
        
        return $this->template('news/v_flow', [
            'news' => $news,
            'tags' => $tags,
            'cats' => Arr::to_select($cats, 'id_cat', 'url'),
            'navbar' => $navbar
        ]);
    }
    
    private function pack_template($content){
        $cats = Cats::instance()->tree(App::CAT_TYPE);
        $tags = Tags::instance()->all(App::TAG_TYPE);

        if(isset($this->cat['id_cat'])){
            $up_branch = Cats::instance()->get_up_branch($this->cat['id_cat']);
            $active_cats = Arr::section($up_branch, 'id_cat');
            $active_cats[] = $this->cat['id_cat'];
        }
        else{
            $active_cats = [];
        }
        
        $this->to_base_template['content'] = $this->template('news/v_base', [
            'content' => $content,
            'cats' => $cats,
            'active_cats' => $active_cats,
            'tags' => $tags,
            'active_tag' => $this->tag['id_tag'] ?? 0,
        ]);
    }
    
    private function base_pagination_settings(){
        $dt = date("Y-m-d");
        
        return M_news::instance()->pager()
                                 ->where("visible='0' AND dt <= '$dt'")
                                 ->order_by('dt DESC')
                                 ->on_page(App::ON_PAGE);
    }
}
<?php

namespace Client\News;

class M_news extends \Core\Base\Model{

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'news');
    }
    
    public function get_by_url(int $id_cat, $url){
        $res = $this->get_by_fieldset([['id_cat' => $id_cat, 'url' => $url]]);
        return $res[0] ?? null;
    }

    public function introAll(&$items){
        foreach($items as $k => $one){
            $this->introOne($items[$k]);
        }
    }
    
    public function introOne(&$item){
        $item['intro'] = $this->intro($item['content']);
    }
    
    public function intro($content){
        if(($i = strpos($content, '<div data-widget="cut-more">')) !== false){
            $content = substr($content, 0, $i);
        }
        
        return $content;
    }
    
    public function parse_page_num($str){
        $matches = [];
        
        if(preg_match(App::PAGE_NUM_PATTERN, $str, $matches)){
            return $matches[1];
        }
        
        return false;
    }
}

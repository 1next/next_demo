<?php

    $html = \Core\Html::instance();

    echo $html->ul_tree($cats, function($item) use ($html, $root_blog){
        return $html->elem('a', 1)->html($item['name'])->attr('href', $root_blog . '/' . $item['url']);
    });
    
?>

<?php

namespace Client\Devsecret;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;
use Core\Input;
use Core\Exceptions;

class App extends Base\App {

    use Singleton;
    
    const KEY_NAME = 'devsecretkey';
    const KEY_SECRET = 'abc';
    
    public function init() {
        Events::add('System.Init.Done', function() {
            $key = Input::get(self::KEY_NAME);
            
            if($key != self::KEY_SECRET){
                throw new Exceptions\Exit_now('no devsecret key');
            }
        });
        
        Events::add('System.Output.BeforePrint', function(&$html) {  
            $html = preg_replace_callback('|<a(.+?)href="(.+?)"|', function($matches){
                return '<a' . $matches[1] . 'href="' . $matches[2] . '?' . self::KEY_NAME . '=' . self::KEY_SECRET . '"';
            }, $html);
        }, -100);
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [];
    }

}

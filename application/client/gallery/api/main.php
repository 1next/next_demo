<?php

namespace Client\Gallery\Api;

use Client\Gallery;
use Client\Images\Api\Main as Images;

class Main extends \Core\Base\Model{

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(Gallery\App::instance(), 'index');
    }

    public function get_by_code($code) {
        $id_gallery = $this->get_by_fieldset(['shortcode' => $code])[0]['id_gallery'];
        return Images::instance()->get_by_items(Gallery\App::ITEM_TYPE, [$id_gallery]);
    }
}

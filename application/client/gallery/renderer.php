<?php

namespace Client\Gallery;

use Core\Traits\Singleton;
use Core\Sql;
use Extra\Template;
use Client\Images\Api\Main as Images;

class Renderer{

    use Singleton;
    
    protected $template_class;
    protected $db;

    protected function __construct(){
        $this->template_class = Template::get_variant(PATH_APPLICATION . '/client/');
        $this->db = Sql::instance();
    }
    
    public function replace($id, $template){
        $images = Images::instance()->get_by_items(App::ITEM_TYPE, [$id]);
        return $this->template_class->render('gallery/v_' . $template, ['images' => $images]);
    }

}

<?php

namespace Client\Gallery;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    const ITEM_TYPE = 'gallery';

    public function init() {
        Events::add('System.Output.BeforePrint', function(&$html){
            $html = preg_replace_callback('|<widget widget-type="gallery-(.+?)">(.+?)</widget>|', function($matches){
                return Renderer::instance()->replace($matches[2], $matches[1]);
            }, $html);
        });
    }

    public function manifest() {
        return [
            'using' => ['pages']
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'galleries_',
            'tables' => [
                'index' => [
                    'pk' => 'id_gallery'
                ]
            ]
        ];
    }

}

<?php

namespace Client\Tags;

use Core\Sql;

class M_tags{

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Query_select;
    use \Core\Traits\Model\Get_by_fieldset;

    private $db;
    private $table;
    
    protected function __construct() {
        $this->db = Sql::instance();
        $this->table = $this->db->prefix()  . 'tags_tags';
    }
}

<?php

namespace Client\Tags;

use Core\Sql;

class M_tags_items{

    use \Core\Traits\Singleton;

    private $db;
    private $table;
    
    protected function __construct() {
        $this->db = Sql::instance();
        $this->table = $this->db->prefix()  . 'tags_items';
    }
    
    public function table(){
        return $this->table;
    }
}

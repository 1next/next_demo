<?php

namespace Client\Tags\Api;
use Client\Tags;
use Core\Arr;

class Main{

    use \Core\Traits\Singleton;

    private $mTags;
    private $mTagsItems;
    
    protected function __construct() {
        $this->mTags = Tags\M_tags::instance();
        $this->mTagsItems = Tags\M_tags_items::instance();
    }

    public function all($type){
        return $this->mTags->query_select()->where("tag_type=:tag_type")
                    ->bind('tag_type', $type)->select();
    }
    
    public function get_by_url($type, $url){
        $tag = $this->mTags->get_by_fieldset([['tag_type' => $type, 'url' => $url]]);
        return $tag[0] ?? null;
    }
    
    public function get_by_group($rel, $ids){
        if(empty($ids)){
            return [];
        }
        
        $clean_id = [];
        
        foreach($ids as $id){
            $clean_id[] = (int)$id;
        }
        
        $in = implode(',', $clean_id);
        
        $res = $this->mTags->query_select()
                    ->join($this->mTagsItems->table() . ' ti using(id_tag)')
                    ->where("ti.item=:rel AND ti.id_item IN ($in)")
                    ->bindAll(['rel' => $rel])
                    ->select();
        
        return Arr::grouping($res, 'id_item');
    }
}

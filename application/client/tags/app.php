<?php

namespace Client\Tags;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;
    
    public function init() {
        
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [
            'tables_prefix' => 'tags_',
            'tables' => [
                'tags' => [
                    'pk' => 'id_tag'
                ],
                'items' => [
                    'pk' => ['id_tag', 'id_item', 'item']
                ]
            ]
        ];
        
    }

}

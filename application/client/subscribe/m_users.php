<?php

namespace Client\Subscribe;

class M_users extends \Core\Base\Model {

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(App::instance(), 'users');
    }
    
}

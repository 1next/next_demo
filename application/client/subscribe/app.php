<?php

namespace Client\Subscribe;

use Core\Base;
use Core\Traits\Singleton;

class App extends Base\App {

    use Singleton;
    
    public function init() {
        
    }

    public function manifest() {
        return [
            'routs' => [
                'subscribe' => [
                    'c' => 'c_ajax',
                    'a' => 'subscribe'
                ]
            ]
        ];
    }

    public function rules() {
        return [
            'tables_prefix' => 'subscribe_',
            'tables' => [
                'users' => [
                    'fields' => ['id_user', 'email', 'name', 'status'],
                    'not_empty' => ['email'],
                    'unique' => ['email'],
                    'email' => ['email'],
                    'max_length' => [
                        'email' => 128,
                        'name' => 256
                    ],
                    'pk' => 'id_user'
                ]
            ]
        ];
    }

}

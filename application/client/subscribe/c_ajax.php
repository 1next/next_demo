<?php

namespace Client\Subscribe;

use Core\Arr;

class C_ajax extends \Client\Base\Ajax {

    public function action_subscribe(){
        $mUsers = M_users::instance();
        $data = Arr::extract($_POST, ['email']);
        
        if($mUsers->add($data)){
            $this->response['res'] = true;
        }
        else{
            $this->response['res'] = false;
            $this->response['errors'] = $mUsers->errors()->unshown();
        }
    }
    
}

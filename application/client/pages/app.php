<?php

namespace Client\Pages;

use Core\Base;
use Core\Traits\Singleton;
use Core\Events;

class App extends Base\App {

    use Singleton;

    const HOME_URL = 'home';
    
    public function init() {
        Events::add('System.Unrouted', function(&$run){
            $c = new C_pages();
            
            if($c->routed(implode('/', $run['params']))){
                $run['c'] = $c;
                $run['a'] = 'action_page';
                $run['module'] = 'pages';
                return false;
            }
        });
    }

    public function manifest() {
        return [];
    }

    public function rules() {
        return [
            'tables_prefix' => 'pages_',
            'tables' => [
                'pages' => [
                    'pk' => 'id_page'
                ]
            ]
        ];
    }

}

<?php

namespace Client\Pages\Api;

use Client\Pages;

class Main extends \Core\Base\Model{

    use \Core\Traits\Singleton;

    protected function __construct() {
        parent::__construct(Pages\App::instance(), 'pages');
    }

}

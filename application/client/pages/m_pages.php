<?php

namespace Client\Pages;

use Core\Sql;

class M_pages{

    use \Core\Traits\Singleton;
    use \Core\Traits\Model\Get_by_fieldset;
    
    private $db;
    private $table;
    
    protected function __construct() {
        $this->db = Sql::instance();
        $this->table = $this->db->prefix()  . 'pages_pages';
    }

    public function get_by_url($url){
        if($url === ''){
            $url = App::HOME_URL;
        }
        
        $res = $this->get_by_fieldset([['visible' => '0', 'url' => $url]]);
        return isset($res[0]) ? $res[0] : null;
    }
    
    public function home_url(){
        return App::HOME_URL;
    }
}

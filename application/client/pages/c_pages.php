<?php

namespace Client\Pages;

use Core\Headers;
use Client\Breadcrumbs\Api\Main as Breadcrumbs;

class C_pages extends \Client\Base\Controller{
    private $page;
    
    public function routed($url){
        $mPages = M_pages::instance();

        if($url == $mPages->home_url()){
            Headers::c301();
            $this->redirect('/');
        }
        
        $this->page = $mPages->get_by_url($url);
        
        return $this->page != null;
    }
    
    public function action_page(){
        $page = $this->page;
        $mPages = M_pages::instance();
        
        if($page['url'] != $mPages->home_url()){
            Breadcrumbs::instance()->add($page['url'], $page['title']);
        }
        if($page['url'] == 'o-kompanii'){
			$this->to_base_template['topmenu'] = 3;
		}
		
		if($page['url'] == 'kontakty'){
			$this->to_base_template['topmenu'] = 4;
		}
        $this->to_base_template['title'] = $page['title'];
        $this->to_base_template['content'] = $this->template('inner_templates/' . $page['inner_template'], ['page' => $page]);
    }

}
<?php

/* 
 * Set autload rules for extra libs here
 */

spl_autoload_register(function($classname) {   
    if(strpos($classname, 'Twig_') === false && strpos($classname, 'PHPExcel') === false){
        return;
    }
    
    $file = str_replace('_', '/', $classname);
    $path = PATH_APPLICATION . '/extra/' . $file . '.php';
    
    if (file_exists($path)){
        include_once($path);
    }
    else{
        throw new Core\Exceptions\Fatal("$classname - not found by extra libs autoloader");
    }
});
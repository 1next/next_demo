<?php

namespace Extra;
use Config;

class Template extends \Core\Template{
    private $twig;
   
    protected function __construct($base_template_path) {
        $loader = new \Twig_Loader_Filesystem(PATH_THEMES . '/' . Config::CURRENT_THEME);
        
        $this->twig = new \Twig_Environment($loader, array(
            'cache' => Config::DIR_CACHE_WRAP . '/twig_cache/' . Config::CURRENT_THEME,
            'auto_reload' => true,
            'autoescape' => false
        ));
               
        parent::__construct($base_template_path);
    }
    
    public function add_global($name, $value){
        parent::add_global($name, $value);
        $this->twig->addGlobal($name, $value);
    }
    
    public function render($fileName, $vars = []) {
        try{
            $content = $this->twig->render($fileName . '.twig', $vars);
        }
        catch(\Twig_Error_Loader $e){
            $parts = explode('/', $fileName);
            $module = $parts[0];
            unset($parts[0]);
            
            $content = parent::render($module . '/view_stubs/' . implode('/', $parts), $vars);
        }
        
        return $content;
    }

}

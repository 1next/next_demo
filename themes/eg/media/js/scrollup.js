window.onload = function(){
	
	var items = document.querySelectorAll('.text');

	for(var i = 0; i < items.length; i++){
		items[i].onclick = function(){
			var content = this.querySelector('.maxcontent');
			var txt = this.querySelector('.mincontent');
			
			if(content.style.display == 'block'){
				content.style.display = 'none';
				txt.style.display = 'block';
			}else{
				content.style.display = 'block';
				txt.style.display = 'none';
			}
		}
	}
	
	
	var scrollUp = document.getElementById('scrollup'); // найти элемент

	scrollUp.onmouseover = function() { // добавить прозрачность
		scrollUp.style.opacity=0.3;
		scrollUp.style.filter  = 'alpha(opacity=30)';
	};

	scrollUp.onmouseout = function() { //убрать прозрачность
		scrollUp.style.opacity = 0.5;
		scrollUp.style.filter  = 'alpha(opacity=50)';
	};

	scrollUp.onclick = function() { //обработка клика
		window.scrollTo(0,0);
	};

// show button

	window.onscroll = function () { // при скролле показывать и прятать блок
		if ( window.pageYOffset > 400 ) {
			scrollUp.style.display = 'block';
		} else {
			scrollUp.style.display = 'none';
		}
	};
	
	
}
<?php

namespace Core;
use Core\Html as Elems;

class Html{
    use \Core\Traits\Singleton;
    
    public function elem($tag_name, $pattern_type){
        return new Elems\Elem($tag_name, $pattern_type);
    }
    
    public function input($type = 'text'){
        $input = new Elems\Input();
        $input->attr('type', $type);
        return $input;
    }
    
    // вернуть textarea
    public function textarea(){
        return new Elems\Textarea();
    }
    
    public function select($options = [], $selected = null, $disabled = null){
        $select = new Elems\Select();
        $inner_html = '';
        
        foreach($options as $v => $h){
            $option = new Elems\Option();
            $option->val($v)->html($h);
            
            if ($selected !== null && ((is_array($selected) && in_array($v, $selected)) ||
                    (!is_array($selected) && $v == $selected))) {
                $option->battr('selected');
            }

            if ($disabled !== null && ((is_array($disabled) && in_array($v, $disabled)) ||
                    (!is_array($disabled) && $v == $disabled))) {
                $option->battr('disabled');
            }

            $inner_html .= $option->render();
        }
        
        return $select->html($inner_html);
    }
    
    /**
     * Генерация списка Select произвольной функцией
     * @param array $items Любой массив
     * @param callable $f Функция для логики извлечения данных из массива
     * @return Elems\Select
     */
    public function select_f($items = [], $selected = null, $f = null){
        if($f === null){
            $f = function(Elems\Option $option, $elem) use ($selected){
                $option->html($elem)->attr('value', $elem);
                
                if($selected !== null & $elem == $selected){
                    $option->battr('selected');
                }
            };
        }
            
        $select = new Elems\Select();
        $inner_html = '';
        
        foreach($items as $item){
            $option = new Elems\Option();
            $f($option, $item);           
            $inner_html .= $option->render();
        }
        
        return $select->html($inner_html);
    }
    
    public function select_tree($options = [], $selected = null, $disabled = null, $level_offset = 0){
        $select = new Elems\Select();
        $inner_html = $this->select_tree_print_options($options, $selected, $disabled, $level_offset);
        
        return $select->html($inner_html);
    }

    /**
     * Генерация списка Ul
     * @param array $items Любой массив
     * @param callable $f Функция для логики извлечения данных из массива
     * @return Elems\Ul
     */
    public function ul($items = [], $f = null){
        if($f === null){
            $f = function(Elems\Li $li, $elem){
                $li->html($elem);
            };
        }
            
        $ul = new Elems\Ul();
        $inner_html = '';
        
        foreach($items as $item){
            $li = new Elems\Li();
            $f($li, $item);           
            $inner_html .= $li->render();
        }
        
        return $ul->html($inner_html);
    }
    
    /**
     * Генерация дерева категорий в виде списка UL со ссылками на редактирование элементов
     * @param array $tree Массив получаемый функцией модели make_tree()
     * @param type $id_key Название ключа primary key
     * @param type $name_key Название ключа с заголовком (именем) элемента
     * @param type $module Название модуля
     * @param callable $f Функция для логики извлечения данных из массива
     * @return string
     */
    public function ul_tree($tree, $f = null) {
        if($f === null){
            $f = function($item){
                return $item;
            };
        }
        
        $lis = [];

        foreach ($tree as $item) {
            $inner_html = $f($item);
            
            if (isset($item['children']) && count($item['children'] > 0)){
                $inner_html .= $this->ul_tree($item['children'], $f);
            }
                
            $lis[] = $inner_html;

        }

        return $this->ul($lis);
    }
        
    private function select_tree_print_options($options = [], $selected = null, $disabled = null, $level_offset = 0){
        $inner_html = '';
        
        foreach($options as $o){
            $option = new Elems\Option();
            
            for ($i = 0; $i < $level_offset; $i++) {
                $o['html'] = '&nbsp;' . $o['html'];
            }

            $option->val($o['value'])->html($o['html']);
            
            if ($selected !== null && ((is_array($selected) && in_array($o['value'], $selected)) ||
                    (!is_array($selected) && $o['value'] == $selected))) {
                $option->battr('selected');
            }

            if ($disabled !== null && ((is_array($disabled) && in_array($o['value'], $disabled)) ||
                    (!is_array($disabled) && $o['value'] == $disabled))) {
                $option->battr('disabled');
            }

            $inner_html .= $option->render();
            
            if (isset($o['children'])) {
                $inner_html .= $this->select_tree_print_options($o['children'], $selected, $disabled, $level_offset + 5);
            }
        }
        
        return $inner_html;
    }
    
}

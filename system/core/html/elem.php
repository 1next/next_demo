<?php

namespace Core\Html;

class Elem {

    protected $tag_name = 'div';
    protected $attrs = [];
    protected $bool_attrs = [];
    protected $inner_html = '';
    protected $patterns = ['<{-tag_name-}{-attrs-}>', '<{-tag_name-}{-attrs-}>{-inner_html-}</{-tag_name-}>'];
    protected $pattern_type = 1; // 0 - одиночный, 1 - парный - устанавливают дети в конструкторе
    protected $wrap;

    public function __construct($tag_name, $pattern_type) {
        $this->tag_name = $tag_name;
        $this->pattern_type = $pattern_type;
    }

    public function html($val) {
        $this->inner_html = $val;
        return $this;
    }

    public function val($val) {
        $this->attrs['value'] = $val;
        return $this;
    }

    public function name($val) {
        $this->attrs['name'] = $val;
        return $this;
    }

    public function id($val) {
        $this->attrs['id'] = $val;
        return $this;
    }

    public function idn($val) {
        $this->attrs['name'] = $val;
        $this->attrs['id'] = 'field-' . $val;
        return $this;
    }

    public function cl($val) {
        $this->attrs['class'] = $val;
        return $this;
    }
    
    public function addClass($val) {
        if(!isset($this->attrs['class'])){
            $this->attrs['class'] = $val;
        }
        else{
            $this->attrs['class'] .= ' ' . $val;
        }
        
        return $this;
    }
    
    public function attr($name, $val) {
        $this->attrs[$name] = $val;
        return $this;
    }

    public function attrs($pairs) {
        if (is_array($pairs)) {
            foreach ($pairs as $k => $v) {
                $this->attrs[$k] = $v;
            }
        }

        return $this;
    }

    public function battr($name, $true = true) {
        if ($true) {
            $this->bool_attrs[] = $name;
        }

        return $this;
    }

    public function battrs($arr) {
        if (is_array($arr)) {
            foreach ($arr as $attr)
                $this->bool_attrs[] = $attr;
        }

        return $this;
    }

    //
    // {-this-} - место для вставки текущего тега
    //
    public function wrap($text) {
        $this->wrap = $text;
        return $this;
    }

    public function render() {
        $attrs_temp = '';

        foreach ($this->attrs as $name => $value)
            $attrs_temp[] = "$name=\"$value\"";

        foreach ($this->bool_attrs as $attr)
            $attrs_temp[] = $attr;

        $attrs_str = empty($attrs_temp) ? '' : ' ' . implode(' ', $attrs_temp);

        $pattern = $this->patterns[$this->pattern_type];
        $pattern = str_replace('{-tag_name-}', $this->tag_name, $pattern);
        $pattern = str_replace('{-attrs-}', $attrs_str, $pattern);

        if ($this->pattern_type == 1)
            $pattern = str_replace('{-inner_html-}', $this->inner_html, $pattern);

        if ($this->wrap !== null) {
            $pattern = str_replace('{-this-}', $pattern, $this->wrap);
        }

        return $pattern;
    }

    public function __toString() {
        return $this->render();
    }

}

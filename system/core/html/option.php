<?php

namespace Core\Html;

class Option extends Elem {

    public function __construct() {
        $this->tag_name = 'option';
        $this->pattern_type = 1;
    }

}

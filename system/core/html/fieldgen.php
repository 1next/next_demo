<?php

namespace Core\Html;

class Fieldgen {

    private $template;

    public function __construct() {
        $this->template = '{-this-}';
    }

    public function get() {
        return $this->template;
    }

}

<?php

namespace Core\Html;

class Input extends Elem {

    public function __construct() {
        $this->tag_name = 'input';
        $this->pattern_type = 0;
    }

}

<?php

namespace Core\Html;

class Ul extends Elem {

    public function __construct() {
        $this->tag_name = 'ul';
        $this->pattern_type = 1;
    }

}

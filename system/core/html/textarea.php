<?php

namespace Core\Html;

class Textarea extends Elem {

    public function __construct() {
        $this->tag_name = 'textarea';
    }

    //
    // Переопределить, потому что значение textarea хранится между тегов
    //
    public function val($value) {
        $this->inner_html = $value;
        return $this;
    }

}

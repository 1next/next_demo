<?php

namespace Core\Html;

class Li extends Elem {

    public function __construct() {
        $this->tag_name = 'li';
        $this->pattern_type = 1;
    }

}

<?php

namespace Core\Html;

class Select extends Elem {

    public function __construct() {
        $this->tag_name = 'select';
        $this->pattern_type = 1;
    }

}

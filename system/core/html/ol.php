<?php

namespace Core\Html;

class Ol extends Elem {

    public function __construct() {
        $this->tag_name = 'ol';
        $this->pattern_type = 1;
    }

}
<?php

namespace Core\Exceptions;
use Core\Logs;

/**
 * Базове исключение в системе
 */
class Base extends \Exception {

    /**
     * Создать исключение и записать информацию в логи
     * @param string $message Текст сообщения
     * @param int $code Код сообщения
     * @param \Exception $previous Предыдущее исключение. Используется при создания цепочки исключений. 
     */
    public function __construct($message = null, $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
        Logs::error($this);
    }
}

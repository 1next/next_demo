<?php

namespace Core;
use Config;

class Validation {

    private $db;      // соединение с базой
    private $table;   // имя талицы
    private $rules;   // правила, подгружаемые из rules
    private $errors;  // массив с константами ошибк валидации
    private $final_object; // массив, сформированный из переданного, но с очищенными данными
    //
    // params:
    // 		$table - имя талицы для поиска в rules_map.php
    // 		$rules - правила, подгружаемые из rules
    //		

    public function __construct(\Core\Sql $db, $table, $rules) {
        $this->db = $db;
        $this->table = $table;
        $this->rules = $rules;
        $this->errors = [];
        $this->final_object = [];
        
        // автоматически заявляем первичные ключи как уникальные поля
        $unique_type = is_array($this->rules['pk']) ? 'composite_unique' : 'unique';
        
        if(!isset($this->rules[$unique_type])){
            $this->rules[$unique_type] = [];
        }

        if(!in_array($this->rules['pk'], $this->rules[$unique_type])){
            $this->rules[$unique_type][] = $this->rules['pk'];
        }
    }
    
    //
    // returns:
    //		массив с ошибками
    //
    public function errors() {
        $errors = new Errors();
        $messages = Config::$admin_validation_messages;
        $labels = $this->rules['labels'];
        
        foreach ($this->errors as $i) {
            $message = $messages[$i[0]];

            if (isset($i[1])) {
                if(is_array($i[1])){
                    $str_r = [];
                    
                    foreach($i[1] as $key){
                        $str_r[] = $labels[$key];
                    }
                    
                    $message = str_replace(':labels', implode(', ', $str_r), $message);
                }
                else{
                    $message = str_replace(':label_1', $labels[$i[1]], $message);
                }
            }

            if (isset($i[2])) {
                if (is_numeric($i[2])) {
                    $message = str_replace(':param_1', $i[2], $message);
                } else {
                    $message = str_replace(':label_2', $labels[$i[2]], $message);
                }
            }
            if (isset($i[3])) {
                $message = str_replace(':param_2', $i[3], $message);
            }
            /* //раскомментировать для исспользования
              //здесь можно добавить еще параметр
              if(isset($i[4]))
              $message = str_replace(':param_3', $i[4],  $message); */

            $key = is_array($i[1]) ? implode(',', $i[1]) : $i[1];
            $errors->add($key, $message);
        }
        
        return $errors;
    }
    
    //
    // params:
    // 		$obj - массив с формы. Ключи - поля в базе, значения - значения :)
    // 		$pk - первичный ключ при редактировании записи. При добавлении равен null.
    //
    public function execute($obj, $pk = null) {
        $pair_for_unique = [];
        $clean_obj = [];

        if(!isset($this->rules['html_allowed'])){
            $this->rules['html_allowed'] = [];
        }
        
        foreach ($obj as $key => $value) {
            if (in_array($key, $this->rules['fields'])) {
                $value = trim($value);
                $count = iconv_strlen($value, 'UTF-8');
                
                if (!in_array($key, $this->rules['html_allowed'])) {
                    $value = htmlspecialchars($value);
                    $clean_obj[] = [$key => $value];
                }

                //все представленные здесь правила описаны в файле messages.php
                if ($value == '') {
                    if (isset($this->rules['not_empty']) && in_array($key, $this->rules['not_empty'])) {
                        $this->errors[] = ['not_empty', $key];
                    }
                } else {
                    if (isset($this->rules['range']) && isset($this->rules['range'][$key]) && ($count < $this->rules['range'][$key][0] || $count > $this->rules['range'][$key][1])) {
                        $this->errors[] = ['range', $key, $this->rules['range'][$key][0], $this->rules['range'][$key][1]];
                    } elseif (isset($this->rules['exact_length']) && isset($this->rules['exact_length'][$key]) && $count != $this->rules['exact_length'][$key]) {
                        $this->errors[] = ['exact_length', $key, $this->rules['exact_length'][$key]];
                    } elseif (isset($this->rules['min_length']) && isset($this->rules['min_length'][$key]) && $count < $this->rules['min_length'][$key]) {
                        $this->errors[] = ['min_length', $key, $this->rules['min_length'][$key]];
                    } elseif (isset($this->rules['max_length']) && isset($this->rules['max_length'][$key]) && $count > $this->rules['max_length'][$key]) {

                        $this->errors[] = ['max_length', $key, $this->rules['max_length'][$key]];
                    } elseif (isset($this->rules['date']) && in_array($key, $this->rules['date']) && strtotime($value) == false) {
                        $this->errors[] = ['date', $key];
                    } elseif (isset($this->rules['equals']) && isset($this->rules['equals'][$key])) {
                        $required = $obj[$key];
                        $val = $obj[$this->rules['equals'][$key]];

                        if (!$this->equals($val, $required))
                            $this->errors[] = ['equals', $this->rules['equals'][$key], $key];
                    }
                    elseif (isset($this->rules['phone']) && isset($this->rules['phone'][$key]) && !$this->phone($value, $this->rules['phone'][$key])) {
                        $this->errors[] = array('phone', $key, $this->rules['phone'][$key]);
                    } elseif (isset($this->rules['correct_url']) && in_array($key, $this->rules['correct_url']) && preg_match("/[^a-zA-Zа-яА-ЯёЁ0-9_\-\+]+msi/", $value)) {
                        $this->errors[] = array('correct_url', $key);
                    } elseif (isset($this->rules['email']) && in_array($key, $this->rules['email']) && !$this->email($value)) {

                        $this->errors[] = array('email', $key);
                    } elseif (isset($this->rules['email_domain']) && in_array($key, $this->rules['email_domain']) && !$this->email_domain($value)) {

                        $this->errors[] = array('email_domain', $key);
                    } elseif (isset($this->rules['unique']) && in_array($key, $this->rules['unique'])) {
                        $pair_for_unique[$key] = $value;
                    }
                    /* rules extentions */
                }

                if (isset($this->rules['hash']) && in_array($key, $this->rules['hash'])) {
                    $value = Crypt::hash($value);
                }

                $this->final_object[$key] = $value;
            }
        }

        if (!empty($pair_for_unique)) {
            $list = $this->unique($pair_for_unique, $pk);

            foreach ($pair_for_unique as $key => $value) {
                foreach ($list as $list_one) {
                    foreach ($list_one as $list_key => $list_val) {
                        if ($key == $list_key && $value == $list_val)
                            $this->errors[] = array('unique', $key);
                    }
                }
            }
        }
        
        if(isset($this->rules['composite_unique'])){
            foreach($this->rules['composite_unique'] as $composite){
                if($this->composite_fields_ok($composite)){
                    if(!$this->composite_unique($composite, $pk)){
                        $this->errors[] = array('composite_unique', $composite);
                    }
                }
            }
        }
    }

    private function email($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    private function email_domain($email) {
        return (bool) checkdnsrr(preg_replace('/^[^@]++@/', '', $email), 'MX');
    }

    private function unique($pairs, $pk = null) {
        $query_pairs = [];

        foreach ($pairs as $key => $value) {
            $query_pairs[] = "$key=:$key";
        }

        $query = implode(' OR ', $query_pairs);

        if ($pk == null) {
            $result = $this->db->select("SELECT * FROM $this->table WHERE $query", $pairs);
        } else {           
            $filter = $this->pk_where($pk);
            $result = $this->db->select("SELECT * FROM $this->table 
                                               WHERE ($query)
                                               AND {$filter['where']}", 
                                               array_merge($pairs, $filter['params']));
        }

        return $result;
    }

    private function composite_fields_ok($composite){
        foreach($composite as $key){
            if(!isset($this->final_object[$key])){
                return false;
            }
        }
        
        return true;
    }
    
    private function composite_unique($composite, $pk = null) {
        $query_pairs = [];
        $params = [];
        
        foreach ($composite as $field) {
            $query_pairs[] = "$field=:$field";
            $params[$field] = $this->final_object[$field];
        }

        $query = implode(' AND ', $query_pairs);

        if ($pk == null) {
            $result = $this->db->select("SELECT * FROM $this->table WHERE $query", $params);
        } else {
            $filter = $this->pk_where($pk);
            $result = $this->db->select("SELECT * FROM $this->table
                                               WHERE ($query)
                                               AND {$filter['where']}", 
                                               array_merge($params, $filter['params']));
        }
        
        return count($result) == 0;
    }
    
    //
    // returns:
    //		true || false - всё хорошо или всё плохо
    //
    public function good() {
        return count($this->errors) == 0;
    }

    //
    // returns:
    //		массив с проверенными и отредактированными полями для базы
    //
    public function getObj() {
        if (count($this->errors) == 0) {
            return $this->final_object;
        }

        return [];
    }

    public function replaceObj() {
        return $this->final_object;
    }

    private function phone($number, $length) {
        $number = preg_replace('/\D+/', '', $number);
        return strlen($number) >= $length;
    }

    private function equals($value, $required) {
        return ($value === $required);
    }

    /**
     * Сформировать where для исключения текущего pk 
     * @param mixed $pk int для простого || [int1 ... intN] для составного
     * @return array [0 => SQL WHERE, 1 => параметры для подготовленного запроса]
     * @throws Exceptions\Fatal
     */
    protected function pk_where($pk) {
        $res = ['where' => '', 'params' => []];

        if (is_array($pk) xor is_array($this->rules['pk'])) {
            throw new Exceptions\Fatal('Moron style given by: is_array($pk) xor is_array($this->pk)');
        }
        
        if (count($pk) != count($this->rules['pk'])) {
            throw new Exceptions\Fatal('Moron style given by: count($pk) != count($this->pk)');
        }

        if (is_array($pk)) {
            $arr = array();

            for ($i = 0; $i < count($this->rules['pk']); $i++) {
                $arr[] = "{$this->rules['pk'][$i]}<>:pk_where$i";
                $res['params']["pk_where$i"] = $pk[$i];
            }

            $res['where'] = implode(' AND ', $arr);
        } else {
            $res['where'] = "{$this->rules['pk']}<>:pk_where0";
            $res['params']['pk_where0'] = $pk;
        }

        return $res;
    } 
}

<?php

namespace Core;

use Core\Sql;
use Core\Exceptions;

class Pagination extends Select_builder{

    protected $on_page;        // количество записей на странице
    protected $url_self;       // url адрес от корня без номера с страницы (/pages/all/)
    protected $page_num;       // текущая страница

    //
    // Инициализировать свойства объекта
    //
    public function __construct($table, Sql $db) {
        parent::__construct($table, $db);
        $this->on_page = 10;
        $this->url_self = '';
        $this->page_num = 1;
    }

    //
    // Получить список записей из таблицы
    //
    public function page() {
        $shift = ($this->page_num - 1) * $this->on_page;

        if ($shift < 0) {
            throw new Exceptions\Fatal('Items shift less than zero');
        }

        return $this->db->select("SELECT {$this->fields} 
                                  FROM {$this->concatenation()} 
                                  LIMIT $shift, {$this->on_page}", $this->bind);
    }

    //
    // Получить количество записей в таблице
    //
    public function count() {
        $res = $this->db->select("SELECT count(*) as cnt FROM {$this->concatenation()}", $this->bind);
        return $res[0]['cnt'];
    }

    //
    // Сформировать данные для Template (v_navbar.php)
    //
    public function navparams() {
        $count = $this->count();
        $max_page = ceil($count / $this->on_page);
        $left = $this->page_num - 2;
        $right = $this->page_num + 2;

        while ($left <= 0) {
            $left++;
            $right++;
        }

        while ($right > $max_page) {
            $left--;
            $right--;
        }

        return [
            'on_page' => $this->on_page,
            'count' => $count,
            'left' => $left,
            'right' => $right,
            'max_page' => $max_page,
            'page_num' => $this->page_num,
            'url_self' => $this->url_self
        ];
    }
    
    public function page_num($i){
        $this->page_num = $i;
        return $this;
    }
    
    public function on_page($n){
        $this->on_page = $n;
        return $this;
    }
    
    public function url_self($url_self){
        $this->url_self = $url_self;
        return $this;
    }
}

<?php

namespace Core\Base;

use Core\Sql;
use Core\Pagination;
use Core\Exceptions;
use Core\Errors;

/**
 * Базовый класс модели
 */
abstract class Model {   
    use \Core\Traits\Model\All;

use \Core\Traits\Model\Get;

use \Core\Traits\Model\Add;

use \Core\Traits\Model\Edit;

use \Core\Traits\Model\Delete;

use \Core\Traits\Model\Get_by_fieldset;

use \Core\Traits\Model\Query_select;
    
    /**
     * Объект для создания постраничной навигации
     * @var Core\Pagination 
     */
    protected $pager;
    /**
     * Объект с настройками модуля
     * @var Core\Base\App 
     */
    protected $app;
    /**
     * Полное имя таблицы [c префиксом]
     * @var string 
     */
    protected $table;
    /**
     * Короткое имя таблицы [без префикса]
     * @var string 
     */
    protected $table_name;
    /**
     * Поле первичного ключа [массив, если ключ составной]
     * @var midex string || array
     */
    protected $pk;
    /**
     * Объект для работы с базой данных
     * @var Core\Sql 
     */
    protected $db;
    /**
     * Последняя добавленная или отредактированная запись
     * @var array 
     */
    protected $last_valid_obj;
    /**
     * Объект для хранения ошибок валидации данных
     * @var Core\Errors 
     */
    protected $errors;

    /**
     * Создать новую модель
     * @param \Core\Base\App App-класс модуля
     * @param string Название таблицы в БД
     * @param \Core\Sql Класс для работы с БД (по умолчанию null)
     * @param \Core\Errors Класс для хранения ошибок (по умолчанию null)
     */
    protected function __construct(App $app, $table, Sql $db = null) {
        if($db == null){
            $db = Sql::instance();
        }

        $this->app = $app;        
        $this->db = $db;       
        $this->table_name = $table;
        $this->table = $this->prefix() . $this->app->prefix() . $this->table_name;
        $this->pk = $this->app->table_rules($table)['pk'] ?? null;               
        $this->errors = new Errors();
    }

    /**
     * Получить информацию об ошибках
     * @return Core\Errors Объект с ошибками
     */
    public function errors() {
        return $this->errors;
    }

    /**
     * Получить имя основной таблицы модели
     * @return string Название таблицы
     */
    public function table(){
        return $this->table;
    }
    
    /**
     * Получить префикс таблиц используемой базы
     * @return string Префикс
     */
    public function prefix(){
        return $this->db->prefix();
    }
    
    /**
     * Получить модуль постраничной навигации для данной модели
     * @return Core\Pagination Pager
     */
    public function pager(){
        if($this->pager == null){
            $this->pager = new Pagination($this->table, $this->db);
        }
        
        return $this->pager;
    }
    
    /**
     * Сформировать where для SQL по переданному pk 
     * @param midex $pk int для простого || [int1 ... intN] для составного
     * @return array [0 => SQL WHERE, 1 => параметры для подготовленного запроса]
     */
    protected function pk_where($pk) {
        $res = ['where' => '', 'params' => []];

        if (is_array($pk) xor is_array($this->pk)) {
            throw new Exceptions\Fatal('Moron style given by: is_array($pk) xor is_array($this->pk)');
        }
        
        if (count($pk) != count($this->pk)) {
            throw new Exceptions\Fatal('Moron style given by: count($pk) != count($this->pk)');
        }

        if (is_array($pk)) {
            $arr = array();

            for ($i = 0; $i < count($this->pk); $i++) {
                $arr[] = "{$this->pk[$i]}=:pk_where$i";
                $res['params']["pk_where$i"] = $pk[$i];
            }

            $res['where'] = implode(' AND ', $arr);
        } else {
            $res['where'] = "{$this->pk}=:pk_where0";
            $res['params']['pk_where0'] = $pk;
        }

        return $res;
    }
 
}

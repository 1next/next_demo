<?php

namespace Core\Base;

use Config;
use Core\Enum;
use Core\Info;
use Core\Exceptions;
use Core\Events;

/**
 * Базовый инициализирующий класс
 */
abstract class Init {
    /**
     * Массив элементов из ЧПУ
     * @var array 
     */
    protected $params;
    /**
     * Аналог массива $_GET
     * @var array 
     */
    protected $get;
    /**
     * admin || client
     * @var string 
     */
    protected $folder;
    /**
     * Ответ от контроллера
     * @var string 
     */
    protected $output;
    /**
     * Список активных модулей из Config для текущей папки
     * @var array 
     */
    protected $modules;

    /**
     * Создать новый инициализирующий класс
     * @param array $params Массив элементов из ЧПУ
     * @param array $get Аналог массива $_GET
     * @param string $folder admin || client
     */
    public function __construct($params, $get, $folder) {
        $this->params = $params;
        $this->get = $get;
        $this->folder = $folder;
        $this->modules = Config::$modules[$folder];
        $this->output = '';
        Info::execute_mode(Enum::EXECUTE_MODE_SIMPLE);
    }

    /**
     * Выполнить запрос
     */
    public function request() {
        $manifest_compile = $this->load_modules();
        Info::manifest($manifest_compile);

        $routs = $manifest_compile['routs'];
        $c = $a = $module = false;

        if (count($this->params) > 1) {
            $full = $this->params[0] . '/' . $this->params[1];
            $find = $routs[$full] ?? $routs[$this->params[0]] ?? null;
        } else {
            $find = $routs[$this->params[0] ?? ''] ?? null;
        }

        if ($find != null && is_array($find)) {
            $module = $find['module'];
            $c = $find['c'];
            $a = $find['a'] ?? $this->params[1] ?? 'index';
            
            Info::rout_module($module);

            $c_name = '\\' . ucfirst($this->folder) . '\\'
                    . ucfirst($module) . '\\'
                    . ucfirst($c);

            $action = 'action_' . $a;
            $controller = new $c_name();
            $this->output = $controller->go($action, $this->params, $this->get);
        } elseif($this->folder == 'admin'){
            $this->folder = 'client';
            /* а админские мб убрать с событий - очистить все подписки на события */
            $this->modules = Config::$modules[$this->folder];
            $manifest_compile = $this->load_modules();
            $controller = new \Client\Base\C_unrouted();
            $this->output = $controller->go('action_index', $this->params, $this->get);
        }
        else{
            $start = [
                'c' => null,
                'a' => null,
                'module' => null,
                'params' => $this->params,
                'get' => $this->get
            ];
            
            $run = Events::run('System.Unrouted', $start)[0];
            
            if($run['c'] != null && $run['c'] instanceof Controller){
                Info::rout_module($run['module']);
                $this->output = $run['c']->go($run['a'], $this->params, $this->get);
            }
            else{
                $controller = new \Client\Base\C_unrouted();
                $this->output = $controller->go('action_index', $this->params, $this->get);
            }
        }
        
    }

    /**
     * Вернуть ответ от контроллера
     * @return string Ответ
     */
    public function output() {
        return $this->output;
    }

    /**
     * Произвести загрузку всех активных модулей
     * @return array Скомпилированный манифест системы
     * @throws Exceptions\Fatal Фатальная ошибка, если нет модуля, который используется другим модулемы
     */
    protected function load_modules() {
        $folder = $this->folder;
        $apps = $this->init_modules($folder, $this->modules);        
        Events::run('System.Init.Done', $this->params, $this->get);
        
        $cache_path = Config::DIR_CACHE_WRAP . '/' . Config::DIR_CACHE_MANIFEST .
                                               '/' . $folder . '.cache';
        $manifest_compile = ['routs' => [], 'menu' => [], 'registry_privs' => []];
        
        if (Config::WORK_MODE == Enum::WORK_MODE_RPODUCTION && file_exists($cache_path)) {
            $manifest_compile = unserialize(file_get_contents($cache_path));
        } else {
            $modules_using = [];
            
            foreach ($this->modules as $m) {      
                $manifest = $apps[$m]->manifest();
                $compile_keys = ['routs', 'menu', 'registry_privs'];

                foreach ($compile_keys as $ck) {
                    if (isset($manifest[$ck]) && is_array($manifest[$ck])) {
                        foreach ($manifest[$ck] as $k => $v) {
                            if (is_array($v)) {
                                $v['module'] = $m;
                            }

                            $manifest_compile[$ck][$k] = $v;
                        }
                    }
                }
                
                if (isset($manifest['using'])){
                    $using = [];
                    
                    foreach($manifest['using'] as $left => $right){
                        if(is_string($left)){
                            $using[$left] = $right;
                        }
                        else {
                            $using[$right] = [];
                        }
                    }
                    
                    $modules_using[$m] = $using;
                }
            }
            
            $modelus_using_by = [];
            
            foreach ($modules_using as $name => $needs) {
                foreach($needs as $one => $params){
                    if (!in_array($one, $this->modules)) {
                        throw new Exceptions\Fatal("module $name needs in module $one");
                    }

                    if (!isset($modelus_using_by[$one])) {
                        $modelus_using_by[$one] = [];
                    }

                    $modelus_using_by[$one][$name] = $params;
                }
            }

            $manifest_compile['using'] = $modules_using;
            $manifest_compile['using_by'] = $modelus_using_by;

            if (Config::WORK_MODE == Enum::WORK_MODE_RPODUCTION) {
                if (!is_dir(Config::DIR_CACHE_WRAP . '/' . Config::DIR_CACHE_MANIFEST)) {
                    mkdir(Config::DIR_CACHE_WRAP . '/' . Config::DIR_CACHE_MANIFEST, true);
                }

                file_put_contents($cache_path, serialize($manifest_compile));
            }
        }
        
        return $manifest_compile;
    }
    
    /**
     * Инициализировать модули и получить их App-классы
     * @param string $folder Тип модуля: admin || client
     * @param array $modules Массив с названиями модулей
     * @return array \Core\Base\App Массив с App-классами модулей
     * @throws Exceptions\Fatal Если App модуля не наследник Core\Base\App
     */
    protected function init_modules($folder, $modules){
        $apps = [];
        
        foreach ($modules as $m) {
            // опрашиваем init у модулей
            $classname = ucfirst($folder) . '\\' . ucfirst($m) . '\\App';
            $app = call_user_func([$classname, 'instance']);

            if (!($app instanceof App)) {
                throw new Exceptions\Fatal("$classname not instanceof App Interface");
            }

            $app->init();
            $apps[$m] = $app;
        }
        
        return $apps;
    }
}

<?php

namespace Core\Base;
use Config;

/**
 * Базовый класс для описания настроек модуля
 */
abstract class App{
    /**
     * Количество записей на странице
     * @var int 
     */
    public $on_page = Config::DEFAULT_ON_PAGE;
    /**
     * Инициализировать модуль
     */
    public abstract function init();
    /**
     * Получить манифест модуля
     */
    public abstract function manifest();
    /**
     * Получить правила модуля для базы данных
     */
    public abstract function rules();
    
    /**
     * Получить названия полей для таблицы
     * @param string $table Название таблицы
     * @return array Названия полей
     */
    public function labels($table){
        $rules = $this->rules();
        return $rules['tables'][$table]['labels'] ?? [];
    }
    
    /**
     * Получить префикс таблиц текущего модуля
     * @return string Префикс
     */
    public function prefix(){
        return $this->rules()['tables_prefix'] ?? '';
    }
    
    /**
     * Получить правила для конкретной таблицы
     * @param string $table Название таблицы
     * @return array Массив с правилами
     */
    public function table_rules($table){
        return $this->rules()['tables'][$table] ?? [];
    }
}

<?php

namespace Core\Base;

use Config;
use Core\Template;
use Core\Exceptions;
use Core\Events;
use Core\Helpers;
use Core\Headers;
use Core\Input;

/**
 * Базовый конроллер системы
 */
abstract class Controller {
    /**
     * Массив с параметрами из ЧПУ
     * @var array 
     */
    protected $params;
    /**
     * Массив с параметрами из $_GET
     * @var array 
     */
    protected $get;
    /**
     * App-класс текущего модуля
     * @var Core\Base\App 
     */
    protected $app;
    /**
     * Класс для генерации шаблонов
     * @var Core\Template 
     */
    protected $template_class;
    /**
     * Путь до корня для текущего режима работы
     * @var string 
     */
    protected $root;
    /**
     * Массив с параметрами для передачи в базовый шаблон
     * @var array 
     */
    protected $to_base_template;
    /**
     * Массив с параметрами для передачи в javascript
     * @var array 
     */
    protected $vars_to_js;

    /**
     * Генерация ответа для текущего запроса
     */
    protected abstract function render();
    /**
     * Дополнительные настройки между вызовом конструктора и action_*
     */
    protected abstract function before();
    /**
     * Настроить показ ошибки 404
     */
    protected abstract function set_content_404($e);
    /**
     * Настроить показ ошибки нехватки прав доступа
     */
    protected abstract function set_content_access($e);
    /**
     * Настроить показ произвольного набора ошибок
     */
    protected abstract function set_error_template(\Core\Errors $errors);

    /**
     * Создать новый контроллер
     */
    public function __construct() {
        $this->template_class = Template::get_variant('/');
        $appname = Helpers::parse_classname(get_class($this))['namespace'] . '\\' . 'App';
        $this->app = $appname::instance();
        $this->root = Config::BASE_URL;
        $this->to_base_template = [];
        $this->vars_to_js = [];
    }
    
    /**
     * Выполнить текущий запрос
     * @param string $action Метод текущей страницы
     * @param array $params Массив с параметрами из ЧПУ
     * @param array $get Массив с параметрами из $_GET
     * @return string Ответ
     * @throws \Core\Exceptions\E404 Ошибка 404
     * @throws \Core\Exceptions\Access Ошибка прав доступа
     * @throws \Exception Необработанное системное исключение
     */
    public function go($action, $params, $get) {
        try{
            $this->params = $params;
            $this->get = $get;
            $this->before();
            $this->$action();
        }
        catch(Exceptions\E404 $e){            
            $this->set_content_404($e);
        }
        catch(Exceptions\Access $e){            
            $this->set_content_access($e);
        }
        catch(\Exception $e){
            throw $e;
        }
        
        $res = Events::run('System.Template.Base.BeforeRender', $this->to_base_template, $this->vars_to_js);
        $this->to_base_template = $res[0];
        $this->vars_to_js = $res[1];
        
        $output = $this->render();
        $res = Events::run('System.Output.BeforePrint', $output);
        return $res[0];
    }

    /**
     * Стандартная заглушка для создания 404-ой ошибки при отсутствии action_*
     * @param string $name Название метода
     * @param array $params Параметры, с которыми был вызван метод
     * @throws Core\Exceptions\E404 Ошибка 404
     */
    public function __call($name, $params) {
        throw new Exceptions\E404('Undefined page action. Name: ' . $name . 
                                  ', params: ' . implode(',', $params) . '.');
    }
    
    /**
     * Узнать, запрошена ли страница методом GET
     * @return boolean Метод GET
     */
    protected function is_get() {
        return Input::server('REQUEST_METHOD') == 'GET';
    }

    /**
     * Узнать, запрошена ли страница методом POST
     * @return boolean Метод POST
     */
    protected function is_post() {
        return Input::server('REQUEST_METHOD') == 'POST';
    }

    /**
     * Сгенерировать шаблон
     * @param string $fileName Путь до шаблона
     * @param array $vars Переменные для шаблона
     * @return string Сгенерированный код
     */
    protected function template($fileName, $vars = array()) {
        return $this->template_class->render($fileName, $vars);
    }

    /**
     * Выполнить редирект на указанный адрес
     * Если первый символ /, то редирект идёт относительно константы Config::BASE_URL
     * @param string $url
     */
    protected function redirect($url) {
        if ($url[0] == '/') {
            $url = Config::BASE_URL . substr($url, 1);
        }

        Headers::send("location: $url");
        exit();
    }

    /**
     * Добавить скрипты для шаблона
     * @param string $folder Папка модуля
     * @param array $scripts Массив с путями до скриптов без расширения
     */
    protected function add_scripts($folder, $scripts){
        if (!isset($this->to_base_template['scripts'][$folder])) {
            $this->to_base_template['scripts'][$folder] = [];
        }

        foreach ($scripts as $script) {
            $this->to_base_template['scripts'][$folder][] = $script;
        }
    }

    /**
     * Удалить скрипты из шаблона
     * @param string $folder Папка модуля
     * @param array $scripts Массив с путями до скриптов без расширения
     */
    protected function remove_scripts($folder, $scripts){
        foreach ($scripts as $script) {
            if(($key = array_search($script, $this->to_base_template['scripts'][$folder])) !== false) {
                unset($this->to_base_template['scripts'][$folder][$key]);
            }
        }
    }
    
    /**
     * Добавить стили для шаблона
     * @param string $folder Папка модуля
     * @param array $styles Массив с путями до стилей без расширения
     */
    protected function add_styles($folder, $styles){
        if (!isset($this->to_base_template['styles'][$folder])) {
            $this->to_base_template['styles'][$folder] = [];
        }

        foreach ($styles as $style) {
            $this->to_base_template['styles'][$folder][] = $style;
        }
    }

    /**
     * Удалить стили из шаблона
     * @param string $folder Папка модуля
     * @param array $styles Массив с путями до стилей без расширения
     */
    protected function remove_styles($folder, $styles){
        foreach ($styles as $style) {
            if(($key = array_search($style, $this->to_base_template['styles'][$folder])) !== false) {
                unset($this->to_base_template['styles'][$folder][$key]);
            }
        }
    }
}

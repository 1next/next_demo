<?php

namespace Core;

use Core\Sql;
use Core\Exceptions;

/**
 * Класс для генерации SQL-запросов типа Select
 */
class Select_builder {

    /**
     * Объект текущего соединения с БД
     * @var Core\Sql 
     */
    protected $db;
    /**
     * Столбцы для выборки
     * @var array 
     */
    protected $fields;
    /**
     * Настройки выборки
     * @var array 
     */
    protected $query = [
        'table' => '',
        'join' => '',
        'left_join' => '',
        'right_join' => '',
        'where' => '',
        'group_by' => '',
        'having' => '',
        'order_by' => '',
        'limit' => ''
    ];
    /**
     * Массив для безопасной подстановки данных в запрос
     * @var array 
     */
    protected $bind;
    /**
     * Создать новый конструктор select-запросов
     * @param string $table Основная таблица
     * @param Sql $db Объект соединения с БД
     */
    public function __construct($table, Sql $db) {
        $this->db = $db;
        $this->query['table'] = $table;
        $this->fields = '*';
        $this->bind = [];
    }

    /**
     * Получить строки из БД согласно текущим настройкам
     * @return array Итоговая выборка
     */
    public function select() {
        return $this->db->select("SELECT {$this->fields} FROM {$this->concatenation()}", $this->bind);
    }
	
    public function add_where($str){
        $this->query['where'] .= ' ' . $str;
        return $this;
    }
    
    /**
     * Заглушка для установки полей и элементов массива query
     * @param string $name Имя
     * @param string $args Значение
     * @return $this
     * * @throws Exceptions\Fatal Обращение к недопустимой sql-команде
     */
    public function __call($name, $args) {
        if(!isset($this->query[$name])){
            throw new Exceptions\Fatal("undefined SQL-setting $name with value $args");
        }

        $this->query["$name"] .= strtoupper(str_replace('_', ' ', $name)) . ' ' . $args[0] . ' ';
        return $this;
    }

    /**
     * Очистить один из установленных параметров
     * @param string $field Имя ключа
     * @return $this
     */
    public function clear($field) {
        if (isset($this->query[$field])) {
            $this->query[$field] = '';
        }

        return $this;
    }

    /**
     * Установить нужные столбцы
     * @param array $fields Столбцы, извлекаемые при запросе
     * @return $this
     */
    public function fields($fields = ['*']) {
        if (!empty($fields)) {
            $this->fields = implode(',', $fields);
        }

        return $this;
    }
    /**
     * Добавить параметр для экранирования
     * @param string $name Маска
     * @param mixed $value Значение
     * @return $this
     */
    public function bind($name, $value){
        $this->bind[$name] = $value;
        return $this;
    }
    /**
     * Установить новые параметры для экранирования
     * @param array $params Ассоциативный массив [маска => значение ...]
     * @return $this
     */
    public function bindAll(array $params){
        $this->bind = $params;
        return $this;
    }
    /**
     * Собрать строку запроса из массива
     * @return type
     */
    protected function concatenation() {
        return implode(' ', $this->query);
    }

}

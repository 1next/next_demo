<?php

namespace Core;

/**
 * Класс для хранения ошибок
 */
class Errors {
    /**
     * Массив с ошибками
     * @var array 
     */
    protected $errors = [];

    /**
     * Добавить ошибку
     * @param string $key Ключ ошибки [обычно равен id поля на форме для показа рядом]
     * @param string $message Сообщение
     */
    public function add($key, $message) {
        $this->errors[$key] = ['text' => $message, 'shown' => false];
    }

    /**
     * Получить ошибку по ключу
     * @param string $key Ключ ошибки [обычно равен id поля на форме для показа рядом]
     * @return mixed Сообщение об ошибке || null 
     */
    public function get($key) {
        if(!isset($this->errors[$key])){
            return null;
        }
        
        $this->errors[$key]['shown'] = true;
        return $this->errors[$key]['text'];
    }

    /**
     * Получить ошибки, которые ещё не были получены методом get
     * @return array Массив с ошибками
     */
    public function unshown(){
        $unshown = [];
        
        foreach($this->errors as $k => $err){
            if(!$err['shown']){
                $err['shown'] = true;
                $unshown[$k] = $err['text'];
            }
        }
        
        return $unshown;
    }
    
    /**
     * Получить общее количество ошибок
     * @return int Количество ошибок
     */
    public function count(){
        return count($this->errors);
    }
}


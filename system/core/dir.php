<?php

namespace Core;


class Dir {
    
    public static function ls($dir, $pattern = '/\.*$/i', $ext = true){
        $files = scandir($dir);
        $res = [];
        
        foreach($files as $f){
            if(preg_match($pattern, $f)){
                $res[] = $ext ? $f : substr($f, 0, strlen($f) - strlen(strrchr($f, '.')));
            }
        }
        
        return $res;
    }
}

<?php

namespace Core;

/**
 * Класс для раболты с данными хранимыми в сессии PHP
 */
class Session {
    /**
     * Запущена ли сессия
     * @var boolean 
     */
    private static $is_started = false;
    
    /**
     * Добавить данные
     * @param string $name Ключ
     * @param mixed $value Значение
     */
    public static function push($name, $value) {
        self::session_start();
        $_SESSION[$name] = $value;
    }

    /**
     * Прочитать данные по ключу
     * @param string $name Ключ
     * @return mixed Значение || null
     */
    public static function read($name) {
        self::session_start();
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    /**
     * Прочитать и удалить данные по ключу
     * @param string $name Ключ
     * @return mixed Значение || null
     */
    public static function slice($name) {
        self::session_start();
        $value = null;
        
        if(isset($_SESSION[$name])){
            $value = $_SESSION[$name];
            unset($_SESSION[$name]);
        }
        
        return $value;
    }

    /**
     * Проверить наличие данных по ключу
     * @param string $name Ключ
     * @return boolean Данные найдены
     */
    public static function has($name) {
        self::session_start();
        return isset($_SESSION[$name]);
    }

    /**
     * Удалить данные по ключу
     * @param array $names Массив с ключами
     */
    public static function kick($names) {
        self::session_start();
        
        foreach ($names as $name) {
            if(isset($_SESSION[$name])){
                unset($_SESSION[$name]);
            }
        }
    }
    
    /**
     * Запустить сессию при необходимости
     */
    private static function session_start(){
        if(!self::$is_started){
            session_start();
            self::$is_started = true;
        }
    }
}

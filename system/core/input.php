<?php

namespace Core;

/**
 * Класс для получения данных из ненадёжных глобальных массивов
 */
class Input{
    /**
     * Получить данные из массива $_GET
     * @param string $key Ключ
     * @param int $filter Стандартная константа PHP, по умолчанию FILTER_DEFAULT 
     * @return mixed Строка по ключу || null, если ключ не найден
     */
    public static function get($key, $filter = FILTER_DEFAULT){
        return filter_input(INPUT_GET, $key, $filter);
    }
    
    /**
     * Получить данные из массива $_POST
     * @param string $key Ключ
     * @param int $filter Стандартная константа PHP, по умолчанию FILTER_DEFAULT 
     * @return mixed Строка по ключу || null, если ключ не найден
     */
    public static function post($key, $filter = FILTER_DEFAULT){
        return filter_input(INPUT_POST, $key, $filter);
    }
    
    /**
     * Получить данные из массива $_COOKIE
     * @param string $key Ключ
     * @param int $filter Стандартная константа PHP, по умолчанию FILTER_DEFAULT 
     * @return mixed Строка по ключу || null, если ключ не найден
     */
    public static function cookie($key, $filter = FILTER_DEFAULT){
        return filter_input(INPUT_COOKIE, $key, $filter);
    }
    
    /**
     * Получить данные из массива $_SERVER
     * @param string $key Ключ
     * @param int $filter Стандартная константа PHP, по умолчанию FILTER_DEFAULT 
     * @return mixed Строка по ключу || null, если ключ не найден
     */
    public static function server($key, $filter = FILTER_DEFAULT){
        return filter_input(INPUT_SERVER, $key, $filter);
    }
    
    /**
     * Получить данные из массива $_ENV
     * @param string $key Ключ
     * @param int $filter Стандартная константа PHP, по умолчанию FILTER_DEFAULT 
     * @return mixed Строка по ключу || null, если ключ не найден
     */
    public static function env($key, $filter = FILTER_DEFAULT){
        return filter_input(INPUT_ENV, $key, $filter);
    }
}


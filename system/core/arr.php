<?php

namespace Core;

/**
 * Класс для полезных операций с массивами
 */
class Arr{
    /**
     * Получить набор ключей из массива
     * @param array $array Исходный массив
     * @param array $fields Массив необходимых ключей
     * @return array Итоговый массив
     */
    public static function extract($array, $fields){
        $final = array();

        foreach($array as $k => $v){
            if (in_array($k, $fields)) {
                $final[$k] = $v;
            }
        }
    
        foreach($fields as $field){
            if (!isset($final[$field])) {
                $final[$field] = '';
            }
        }	

        return $final;
    }
    
    /**
     * Сделать срез двумерного массива в одномерный по внутреннему ключу
     * @param array $array Двумерный массив
     * @param string $key Ключ
     * @return array [elem1, elem2 ... elemN]
     */
    public static function section($array, $key){
        $res = [];
        
        foreach($array as $one){
            $res[] = $one[$key];
        }
        
        return $res;
    }
    
    /**
     * Сделать срез двумерного массива в ассоциативный для быстрого поиска
     * @param array $array Двумерный массив
     * @param string $key Ключ для среза из двумерного массива
     * @return array [elem1 => true, elem2 => true ... elemN => true]
     */
    public static function section_assoc($array, $key){
        $res = [];
        
        foreach($array as $one){
            $res[$one[$key]] = true;
        }
        
        return $res;
    }
    
    /**
     * Сгруппировать двумерный массив в трёхмерный по вложенному ключу
     * @param array $arr Стандартный двумерный массив ответа от базы данных
     * @param string $key Ключ внутреннего массива для группировки
     * @return array Сгруппированный массив
     */
    public static function grouping($arr, $key){
        $res = [];
        
        foreach($arr as $row){
            if(!isset($res[$row[$key]])){
                $res[$row[$key]] = [];
            }
            
            $res[$row[$key]][] = $row;
        }
        
        return $res;
    }


    /**
     * Преобразовать двумерный массив для HTML::select
     * @param array $array Двумерный массив из базы данных
     * @param string $value_key Столбец, отправляемый в value
     * @param string $html_key Столбец, отправляемый внутрь тега option
     * @return array Преобразованный массив
     */
    public static function to_select($array, $value_key, $html_key){
        $final = array();
        
        foreach ($array as $one) {
            $final[$one[$value_key]] = $one[$html_key];
        }

        return $final;
    }
    
    /**
     * Преобразовать дерево для HTML::select__tree
     * @param array $tree Дерево из базы данных
     * @param string $value_key Столбец, отправляемый в value
     * @param string $html_key Столбец, отправляемый внутрь тега option
     * @return array Преобразованное дерево
     */
    public static function to_select_tree($tree, $value_key, $html_key){
        $final = array();
        
        foreach($tree as $one){
            $option = ['value' => $one[$value_key], 'html' => $one[$html_key]];
            
            if (isset($one['children']) && !empty($one['children'])) {
                $option['children'] = self::to_select_tree($one['children'], $value_key, $html_key);
            }

            $final[] = $option;
        }
        
        return $final;
    }

    /**
     * Преобразовать дерево в список нужных значений [& - просто так, для экономии памяти]
     * @param array $tree Дерево
     * @param string $key ключ с нужным значением
     * @param string $child внутренний массив
     * @return array список значений
     */
    public static function tree_to_list(&$tree, $key, $child = 'children'){
        $final = [];

        foreach($tree as $one){

            if(isset($one[$key])) {
                $final[] = $one[$key];
            }

            if (isset($one[$child]) && !empty($one[$child])) {
                $inner_list = self::tree_to_list($one[$child], $key);
                $final = array_merge($final, $inner_list);
            }

        }

        return $final;
    }
}


<?php

namespace Core;

/**
 * Класс реестр глобальных событий системы
 */
class Events {
    /**
     * Менеджер обработки событий
     * @var \Core\Event_manager 
     */
    protected static $event_manager;

    /**
     * Подписаться на событие
     * @param string $name Название
     * @param callable $callback Функция
     * @param int $priority Приоритет выполнения
     */
    public static function add($name, $callback, $priority = 10){
        if (self::$event_manager === null) {
            self::$event_manager = new Event_manager;
        }

        self::$event_manager->listen($name, $callback, $priority);
    }
    
    /**
     * Запустить событие
     * @param string $name Название
     * @param array $params Параметры
     */
    public static function run($name, ...$params){
        if (self::$event_manager === null) {
            self::$event_manager = new Event_manager;
        }

        return self::$event_manager->fire($name, $params);
    }
}

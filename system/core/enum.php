<?php

namespace Core;

/**
 * Класс с системными перечислениями
 */
class Enum {
    /**
     * Режимы работы: в разработке
     */
    const WORK_MODE_DEVELOPMENT = 0;
    
    /**
     * Режимы работы: боевой
     */
    const WORK_MODE_RPODUCTION = 1;
    
    /**
     * Режимы логирования ошибок для функции error_log: в системный регистратор PHP
     */
    const ERROR_OUTPUR_STANDART = 0;
    
    /**
     * Режимы логирования ошибок для функции error_log: в произвольный файл
     */
    const ERROR_OUTPUR_FILE = 3;

    /**
     * Типы запроса к серверу: обычный запрос
     */
    const EXECUTE_MODE_SIMPLE = 0;
    
    /**
     * Типы запроса к серверу: AJAX запрос
     */
    const EXECUTE_MODE_AJAX = 1;
    
    /**
     * Типы запроса к серверу: запрос виджета
     */
    const EXECUTE_MODE_WIDGET = 2;
    
}


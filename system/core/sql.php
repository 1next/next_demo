<?php

namespace Core;

use PDO;
use Config;
use Exception;
use Core\Exceptions;

/**
 * Класс для работы с базой данных
 */
class Sql {

    use \Core\Traits\Sets;

    /**
     * Экземляр PDO-соединения с базой данных
     * @var \PDO 
     */
    private $db;
    /**
     * Массив с переданными настройками для соединения с базой данных
     * @var array 
     */
    private $info;

    /**
     * Получить экземпляр базы
     * @param string $type Тип базы из конфига (по умолчанию main)
     * @return Core\Sql Абстракция работы с БД
     */
    public static function instance($type = null) {
        if ($type === null) {
            $type = 'main';
        }

        return static::get_variant($type);
    }

    /**
     * Создать экземпляр базы
     * @param Core\Sql $type Абстракция работы с БД
     * @throws Exceptions\Fatal Проблемы с подключением к базе
     */
    protected function __construct($type) {
        try {
            $databases = Config::DATABASES;
            
            if(!isset($databases[$type]) || !is_array($databases[$type])){
                throw new Exceptions\Fatal('Undefined connection ' . $type);
            }
            
            $this->info = $databases[$type];
            
            $this->db = new PDO($this->info['type'] . ':host=' . $this->info['server'] . ';dbname=' .
                    $this->info['database'], $this->info['user'], $this->info['password'], [
                        PDO::ATTR_PERSISTENT => true,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    ]
            );

            $db_charset = isset($this->info['charset']) ? $this->info['charset'] : 'UTF8';
            $this->db->exec('SET NAMES ' . $db_charset);
        } catch (Exception $e) {
            throw new Exceptions\Fatal('PDO could not connect to database', $e->getCode(), $e);
        }
    }

    /**
     * Выбрать данные из базы
     * @param string $query Текст запроса
     * @param array $params Параметры запроса ['id' => 5]
     * @return array Ответ от базы
     * @throws Exceptions\Fatal Проблемы с выполнением запроса
     */
    public function select($query, $params = []) {
        try {
            $q = $this->db->prepare($query);
            $q->execute($params);
            return $q->fetchAll();
        } catch (Exception $e) {
            throw new Exceptions\Fatal('Bad select query ' . $query, $e->getCode(), $e);
        }
    }

    /**
     * Добавить строку в таблицу
     * @param string $table Таблица
     * @param array $object ['field1' => 'value1', 'field2' => 'value2' ... 'fieldN' => 'valueN']
     * @return int Последний добавленный id (0, если у таблицы нет автоинкремента)
     * @throws Exceptions\Fatal Проблемы с выполнением запроса
     */
    public function insert($table, $object) {
        $columns = array();

        foreach ($object as $key => $value) {

            $columns[] = $key;
            $masks[] = ":$key";

            if ($value === null) {
                $object[$key] = 'NULL';
            }
        }

        $columns_s = implode(',', $columns);
        $masks_s = implode(',', $masks);

        $query = "INSERT INTO $table ($columns_s) VALUES ($masks_s)";

        try {
            $q = $this->db->prepare($query);
            $q->execute($object);
            return $this->db->lastInsertId();
        } catch (Exception $e) {
            throw new Exceptions\Fatal('Bad insert query ' . $query, $e->getCode(), $e);
        }
    }

    /**
     * Отредактировать строки в таблице
     * @param string $table Таблица
     * @param array $object ['field1' => 'value1', 'field2' => 'value2' ... 'fieldN' => 'valueN']
     * @param string $where SQL WHERE с масками для подготовленного запроса
     * @param array $params Параметры для подготовленного запроса ['param1' => 'value1' ... 'paramN' => 'valueN'] 
     * @return int Количество затронутых строк
     * @throws Exceptions\Fatal Проблемы с выполнением запроса
     */
    public function update($table, $object, $where, $params = []) {
        $sets = array();

        foreach ($object as $key => $value) {

            $sets[] = "$key=:$key";

            if ($value === NULL) {
                $object[$key] = 'NULL';
            }
        }

        $sets_s = implode(',', $sets);
        $query = "UPDATE $table SET $sets_s WHERE $where";

        try {
            $q = $this->db->prepare($query);
            $q->execute(array_merge($object, $params));
            return $q->rowCount();
        } catch (Exception $e) {
            throw new Exceptions\Fatal('Bad update query ' . $query, $e->getCode(), $e);
        }
    }

    /**
     * Удалить строки из таблицы
     * @param string $table Таблица
     * @param string $where SQL WHERE с масками для подготовленного запроса
     * @param array $params Параметры для подготовленного запроса ['param1' => 'value1' ... 'paramN' => 'valueN'] 
     * @return int Количество удалённых строк
     * @throws Exceptions\Fatal Проблемы с выполнением запроса
     */
    public function delete($table, $where, $params = []) {
        $query = "DELETE FROM $table WHERE $where";

        try {
            $q = $this->db->prepare($query);
            $q->execute($params);
            return $q->rowCount();
        } catch (Exception $e) {
            throw new Exceptions\Fatal('Bad delete query ' . $query, $e->getCode(), $e);
        }
    }

    /**
     * Начать выполнение транзакции
     */
    public function beginTransaction(){
        $this->db->beginTransaction();
    }
    
    /**
     * Принять и завершить транзакцию
     */
    public function commit(){
        $this->db->commit();
    }
    
    /**
     * Откатить действия транзакции. Обычно выполняется автоматически при выбрасывании исключения PDO
     */
    public function rollback(){
        $this->db->rollBack();
    }
    
    /**
     * Получить префикс всех таблиц данной базы
     * @return string Префикс
     */
    public function prefix(){
        return isset($this->info['prefix']) ? $this->info['prefix'] : '';
    }
}

<?php

namespace Core;

use Config;
use Core\Exceptions;

/**
 * Класс для анализа параметров url для массива $_GET и ЧПУ
 */
class Urlparams {
    /**
     * Параметры из ЧПУ 
     * @var array
     */
    public $params; 
    /**
     * Считанный массив $_GET без ЧПУ-добавки cmsquerystring
     * @var array 
     */
    public $get;
    /**
     * Режим работы системы: админский или клиентский
     * @var boolean 
     */
    public $admin;

    /**
     * Обработать параметры URL
     * @param array $get Аналог массива $_GET с обязательным параметром cmsquerystring
     * @throws Exceptions\Fatal Фатальная ошибка если не передан ключ cmsquerystring
     */
    public function __construct($get) {
        if(!isset($get['cmsquerystring'])){
            throw new Exceptions\Fatal('htaccess or widget did not give cmsquerystring parameter');
        }
        
        $uri = $get['cmsquerystring'];
        
        $this->params = array();
        $this->get = array();
        $this->admin = false;

        $info = explode('/', $uri);
        $info_last_index = count($info) - 1;
        
        if($info[$info_last_index] == ''){
            unset($info[$info_last_index]);
        }
        
        $this->params = $info;        
        $first = $this->params[0] ?? '';

        if ($first == Config::ADMIN_URL) {
            $this->admin = true;
            unset($this->params[0]);
            $this->params = array_values($this->params);
        }

        foreach($get as $k => $v){
            if($k != 'cmsquerystring'){
                $this->get[$k] = $v;
            }
        }
    }

}

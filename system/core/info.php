<?php

namespace Core;

/**
 * Класс - реестр информации в системе
 */
class Info {
    /**
     * Кеш для информационных полей
     * @var array 
     */
    private static $cache = [];

    /**
     * Получить либо установить скомпилированный манифест
     * @param array $manifest Манифест
     * @return array Манифест
     */
    public static function manifest($manifest = null) {
        if ($manifest === null) {
            return self::$cache['manifest'];
        }

        self::$cache['manifest'] = $manifest;
        return $manifest;
    }

    /**
     * Получить или установить имя модуля, роут которого активен
     * @param string $name Имя модуля
     * @return string Имя модуля
     */
    public static function rout_module($name = null) {
        if ($name === null) {
            return self::$cache['rout_module'];
        }

        self::$cache['rout_module'] = $name;
        return $name;
    }

    /**
     * Возваращет список модулей, использующих данный
     * @param string $module Имя модуля
     * @return array Массив, использующих его модулей
     */
    public static function using_me($module) {
        if (self::$cache['manifest'] === null) {
            return [];
        }

        if (!isset(self::$cache['manifest']['using_by'][$module])) {
            return [];
        }

        return self::$cache['manifest']['using_by'][$module];
    }

    /**
     * Получить или установить режим запуска системы: обычный, ajax либо виджет
     * @param int $mode Режим запуска Enum::EXECUTE_MODE_*
     * @return int Константа запуска Enum::EXECUTE_MODE_*
     */
    public static function execute_mode($mode = NULL){
        if($mode !== null){
            self::$cache['execute_mode'] = $mode;
        }
        
        return self::$cache['execute_mode'];
    }
}

<?php

namespace Core;
use Config;
use Core\Input;

/**
 * Класс для ведения логов
 */
class Logs{
    /**
     * Записать лог о произошедшей ошибке
     * @param mixed $msg Строка || объект с __toString
     */
    public static function error($msg){
        self::write($msg, Config::ERROR_LOG_DIR);
    }

    /**
     * Записать лог в соответствии с настройками системы
     * @param string $msg Строка || объект с __toString
     * @param string $dir Путь до папки с файлами логов [для Config::ERROR_LOG_MOD = 3]
     */
    private static function write($msg, $dir){
        if(!is_dir($dir)){
            mkdir($dir, true);
        }
        
        $str = date('Y-m-d H:i:s') . "\n" . Input::server('REMOTE_ADDR') . "\n" . $msg;
        error_log($str . "\n------------------------------------------------------------------\n",
                  Config::ERROR_LOG_MOD, $dir . date("Y_m_d") . '.log');
    }
    
}

<?php

namespace Core;

/**
 * Класс для отправки HTTP-заголовков
 */
class Headers{
    /**
     * Выполнить отправку произвольного заголовка
     * @param string $str Заголовок
     */
    public static function send($str){
        header($str);
    }
    
    /**
     * Отправить код 301 - перемещана надолго
     */
    public static function c301(){
        self::send('HTTP/1.1 301 Moved Permanently');
    }
    
    /**
     * Отправить код 304 - изменения отсутствуют
     */
    public static function c304(){
        self::send('HTTP/1.1 304 Not Modified');
    }
    
    /**
     * Отправить код 404 - страница не найдена
     */
    public static function c404(){
        self::send('HTTP/1.1 404 Not Found');
    }
    
    /**
     * Отправить код 500 - ошибка сервера
     */
    public static function c500(){
        self::send('HTTP/1.1 500 Internal Server Error');
    }
}


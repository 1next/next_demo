<?php

namespace Core;

use Core\Exceptions;

/**
 * Менеджер для обработки событий
 */
class Event_manager {
    /**
     * Массив с зарегистрированными событиями и обработчиками
     * @var array 
     */
    protected $listeners = [];

    /**
     * Выполнить событие
     * @param string $event Имя события
     * @param array $arguments Параметры для обработчиков
     * @return array Массив с изменёнными параметрами, после выполения обработчиков
     */
    public function fire($event, $arguments) {
        if (isset($this->listeners[$event]) && is_array($this->listeners[$event])) {
            ksort($this->listeners[$event]);

            foreach (array_reverse($this->listeners[$event]) as $callback_arr) {
                foreach ($callback_arr as $callback) {
                    if($callback(...$arguments) === false){
                        break 2;
                    }
                }
            }
        }
        
        return $arguments;
    }

    /**
     * Добавить обработчик события
     * @param string $event Имя события
     * @param callable $callback Фуцнкция-обработчик
     * @param int $priority Приоритет выполнения
     * @throws Exceptions\Fatal Фатальная ошибка, если обработчик не callable
     */
    public function listen($event, $callback, $priority = 10) {
        if (!is_callable($callback)) {
            throw new Exceptions\Fatal("Not callable subscribe to event $event");
        }

        $this->listeners[$event][$priority][] = $callback;
    }

}


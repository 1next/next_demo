<?php

namespace Core\Traits\Actions;
use Core\Exceptions;
use Core\Session;

trait Page {

    protected $page_template = '';
    protected $page_navbar_template = '';
    protected $page_num;
    protected $page_other_data = [];

    public function action_index() {
        $this->action_page();
    }

    public function action_page() {
        $pager = $this->main_model->pager();
        
        // проверяем качество модели
        if (!($pager instanceof \Core\Pagination)) {
            throw new Exceptions\Fatal('Bad model');
        }

        // ставим стандартные настройки
        $this->page_num = $this->page_num();
        $pager->page_num($this->page_num)->url_self($this->module . '/page/%s');
        $this->page_template = "{$this->module}/v/v_page";
        $this->page_navbar_template = 'base/html/v_navbar';
        $pager->on_page($this->app->on_page);
        
        // даём возможность переопределить их
        $this->settings_action_page();
        
        // рендерим согласно настройкам
        if ($this->page_num < 1) {
            throw new Exceptions\E404('Page number less than 1');
        }

        $data = $pager->page();

        // если данных нет и страница не первая - косяк
        if (empty($data) && $this->page_num != 1) {
            throw new Exceptions\E404('Page nubmber great than maximum number');
        }
        
        // даём возможность обработать полученные данные
        $this->action_page_data_processor($data);
        
        $navparams = $pager->navparams();
        $navbar = $this->template($this->page_navbar_template, $navparams);

        Session::push($this->module . '_redirect', sprintf($navparams['url_self'], $navparams['page_num']));
        Session::push('current_redirect', sprintf($navparams['url_self'], $navparams['page_num']));

        $this->to_base_template['content'] = $this->template($this->page_template, [
            'data' => $data,
            'navbar' => $navbar,
            'other' => $this->page_other_data
        ]);
    }

    protected function page_num() {
        return isset($this->params[2]) ? (int) $this->params[2] : 1;
    }

    protected abstract function settings_action_page();
    protected function action_page_data_processor(&$data){}
}

<?php

namespace Core\Traits\Actions;
use Core\Exceptions;
use Core\Session;

/**
 * Action delete для контролера
 */
trait Delete {
    /**
     * Куда отправить после успешного удаления
     * @var string URL-переадресации
     */
    protected $delete_redirect = '/';
    /**
     * ID удаляемой записи [по умолчанию $this->params[2]]
     * @var mixed int || array <- обычный пк, либо составной  
     */
    protected $delete_id = '';

    /**
     * Страница удаления сущности
     * @throws Exceptions\Fatal Модель не реализует нужные методы
     */
    public function action_delete() {
        // ставим стандартные настройки
        $this->delete_id = $this->params[2];
        $this->delete_redirect = Session::has($this->module . '_redirect') ?
                                 Session::read($this->module . '_redirect') :
                                 '/' . $this->module;

        // даём возможность переопределить их
        $this->settings_action_delete();

        // рендерим согласно настройкам
        if (!($this->main_model instanceof \Core\Base\Model)) {
            throw new Exceptions\Fatal('Bad model');
        }

        if($this->main_model->delete($this->delete_id)){
            $this->redirect($this->root . $this->delete_redirect);
        }
        else{
            $this->set_error_template($this->main_model->errors());
        }
    }

    /**
     * Настроить метод action_delete
     */
    protected abstract function settings_action_delete();
}

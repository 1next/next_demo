<?php

namespace Core\Traits\Actions;
use Core\Exceptions;
use Core\Arr;

/**
 * Action add для контролера
 */
trait Add {

    /**
     * Название шаблона
     * @var string Шаблон
     */
    protected $add_template = '';
    /**
     * Labels из rules app-класса
     * @var array Массив названий полей
     */
    protected $add_labels = [];
    /**
     * Поля с/для формы
     * @var array Ассоциативный массив
     */
    protected $add_fields_data = [];
    /**
     * Произвольные данные для передачи в шаблон
     * @var array Ассоциативный массив
     */
    protected $add_other_data = [];
    /**
     * Превращение чекбоксов в 0 || 1
     * @var array Массив с именами полей-чекбоксов
     */
    protected $add_checkboxes_detect = [];
    /**
     * Обёртка для генерации HTML
     * @var Core\Base\Fieldgen Генератор полей
     */
    protected $add_fieldgen;
    /**
     * Куда отправить после успешного добавления
     * @var string URL-переадресации 
     */
    protected $add_redirect;
    /**
     * Поля, которые мы готовы принять с формы
     * @var array Массив с названиями полей
     */
    protected $add_extract = [];

    /**
     * Страница добавления сущености
     * @throws Exceptions\Fatal Модель не реализует нужные методы
     * @throws Exceptions\E404 Поля для формы не являются массивом
     */
    public function action_add() {
        // ставим стандартные настройки
        $this->errors = $this->main_model->errors();
        $this->add_template = "{$this->module}/v/v_add";
        $this->add_redirect = $this->module;

        // даём возможность переопределить их
        $this->settings_action_add();

        // рендерим согласно настройкам
        if ($this->is_post()) {
            if (!($this->main_model instanceof \Core\Base\Model)) {
                throw new Exceptions\Fatal('Bad model');
            }

            foreach ($this->add_checkboxes_detect as $f) {
                $_POST[$f] = (int) isset($_POST[$f]);
            }

            $fields = Arr::extract($_POST, $this->add_extract);
            
            if ($this->main_model->add($fields)) {
                $this->redirect($this->root . $this->add_redirect);
            }

            $this->errors = $this->main_model->errors();
        }
        else {
            $fields = $this->add_fields_data;
            
            if ($fields === null) {
                throw new Exceptions\E404('Bad start $fields in add action');
            }
        }
       
        $this->to_base_template['content'] = $this->template($this->add_template, [
            'fields' => $fields,
            'f' => $this->add_fieldgen,
            'data' => $this->add_other_data,
            'errors' => $this->errors,
            'back' => $this->add_redirect
        ]);
    }

    /**
     * Настроить метод action_add
     */
    protected abstract function settings_action_add();
}

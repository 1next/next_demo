<?php

namespace Core\Traits\Actions;
use Core\Exceptions;
use Core\Session;
use Core\Arr;

trait Edit {

    protected $edit_template = '';
    protected $edit_labels = [];
    protected $edit_fields_data = [];
    protected $edit_other_data = [];
    protected $edit_checkboxes_detect = [];
    protected $edit_fieldgen;
    protected $edit_id;
    protected $edit_redirect = '/';
    protected $edit_extract = [];

    public function action_edit() {
        // ставим стандартные настройки
        $this->errors = $this->main_model->errors();
        $this->edit_template = "{$this->module}/v/v_edit";
        $this->edit_id = $this->params[2];
        $this->edit_redirect = Session::has($this->module . '_redirect') ?
                Session::read($this->module . '_redirect') :
                '/' . $this->module;

        // даём возможность переопределить их
        $this->settings_action_edit();
        
        // рендерим согласно настройкам
        if ($this->is_post()) {
            if (!($this->main_model instanceof \Core\Base\Model)) {
                throw new Exceptions\Fatal('Bad model');
            }

            foreach ($this->edit_checkboxes_detect as $f) {
                $_POST[$f] = (int) isset($_POST[$f]);
            }

            $fields = Arr::extract($_POST, $this->edit_extract);
            
            if ($this->main_model->edit($this->edit_id, $fields)) {
                $this->redirect($this->root . $this->edit_redirect);
            }

            $this->edit_fields_data = $fields;
            $this->errors = $this->main_model->errors();
        }
        else{
            $fields = $this->main_model->get($this->edit_id);
           
            if ($fields == null) {
                throw new Exceptions\E404('This item not found in database');
            }
            else{
                $this->edit_fields_data = array_merge($this->edit_fields_data, $fields);
            }
        }

        $this->to_base_template['content'] = $this->template($this->edit_template, [
            'fields' => $this->edit_fields_data,
            'f' => $this->edit_fieldgen,
            'data' => $this->edit_other_data,
            'errors' => $this->errors,
            'back' => $this->edit_redirect
        ]);
    }

    protected abstract function settings_action_edit();
}

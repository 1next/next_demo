<?php

namespace Core\Traits;

trait Singleton {

    public static $instance;

    public static function instance() {
        if (static::$instance == null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct() {
        
    }

    final protected function __clone() {
        
    }

    final protected function __sleep() {
        
    }

    final protected function __wakeup() {
        
    }

}

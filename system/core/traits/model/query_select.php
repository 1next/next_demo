<?php

namespace Core\Traits\Model;

use Core\Select_builder;

trait Query_select {

    public function query_select() {
        return new Select_builder($this->table, $this->db);
    }

}

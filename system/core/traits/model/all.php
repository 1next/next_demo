<?php

namespace Core\Traits\Model;

trait All {

    public function all() {
        return $this->db->select("SELECT * FROM {$this->table}");
    }

}

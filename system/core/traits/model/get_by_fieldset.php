<?php

namespace Core\Traits\Model;

trait Get_by_fieldset {    
    use Make_fieldset{
        Make_fieldset::make_fieldset as get_by_fieldset_make_fieldset;
    }
    
    public function get_by_fieldset($obj, $sorting = null) {
        $filter = $this->get_by_fieldset_make_fieldset($obj);
        $query = "SELECT * FROM {$this->table} WHERE {$filter['where']}";
        
        if($sorting !== null){
            $query .= ' ORDER BY ' . $sorting;
        }
        
        return $this->db->select($query, $filter['params']);
    }

}

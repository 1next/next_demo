<?php

namespace Core\Traits\Model;
use Core\Validation;
use Core\Events;

trait Edit {

    public function edit($pk, $fields) {
        
        $valid = new Validation($this->db, $this->table, $this->app->table_rules($this->table_name));
        $valid->execute($fields, $pk);

        if ($valid->good()) {
            $this->last_valid_obj = $valid->getObj();
            $filter = $this->pk_where($pk);
            $this->db->update($this->table, $this->last_valid_obj, $filter['where'], $filter['params']);
            
            Events::run('System.Model.Edit.Success', [
                'table' => $this->table,
                'pk' => $pk,
                'item' => $this->get($pk)
            ]);
            
            return true;
        }

        $this->errors = $valid->errors();
        return false;
    }

}

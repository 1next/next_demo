<?php

namespace Core\Traits\Model;

trait Get {

    public function get($pk) {
        $filter = $this->pk_where($pk);
        $res = $this->db->select("SELECT * FROM {$this->table} WHERE {$filter['where']}", $filter['params']);
        return $res[0];
    }

}

<?php

namespace Core\Traits\Model;

trait Get_in {

    public function get_in($obj, $mode = 'OR') {
        if (empty($obj)) {
            return [];
        }
        
        $pairs = [];
        $values = [];
        $i = 0;
       
        foreach($obj as $key => $items) {
            if(empty($items)){
                continue;
            }
            
            $arr_in = [];
            
            for($j = 0; $j < count($items); $j++){
                $arr_in[] = ":field_{$i}_{$j}";
                $values["field_{$i}_{$j}"] = $items[$j];
            }
            
            $in = implode(',', $arr_in);
            $pairs[] = "$key in ($in)";
            $i++;
        }
        
        if (empty($pairs)) {
            return [];
        }
        
        $where = implode(" $mode ", $pairs);
        $res = $this->db->select("SELECT * FROM {$this->table} WHERE $where", $values);
        return $res;
    }

}

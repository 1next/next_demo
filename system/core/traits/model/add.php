<?php

namespace Core\Traits\Model;
use Core\Validation;

trait Add {

    public function add($fields) {
        
        $valid = new Validation($this->db, $this->table, $this->app->table_rules($this->table_name));
        $valid->execute($fields);
        
        if ($valid->good()) {
            $this->last_valid_obj = $valid->getObj();
            $id = $this->db->insert($this->table, $this->last_valid_obj);
            
            // Если у таблицы нет автоинкерента, превращаем 0 в true
            if ($id === '0') {
                $id = true;
            }

            return $id;
        }

        $this->errors = $valid->errors();
        return false;
    }

}

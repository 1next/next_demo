<?php

namespace Core\Traits\Model;
use Core\Exceptions;

trait Tree {

    protected $field_id_parent = 'id_parent';
    protected $field_item_url = 'item_url';
    protected $field_url = 'url';
    protected $field_num_sort = 'num_sort';
    protected $detect_children_sort = 'children_num_sort';

    public function add($fields) {
        $fields[$this->field_url] = $this->make_url($fields[$this->field_id_parent], $fields[$this->field_item_url]);
        
        $max = $this->db->select("SELECT MAX({$this->field_num_sort}) as mx FROM {$this->table}
                           WHERE {$this->field_id_parent}=:id_parent",
                           ['id_parent' => $fields[$this->field_id_parent]])[0]['mx'];

        $fields[$this->field_num_sort] = ($max === null) ? 0 : $max + 1;
        return parent::add($fields);
    }

    public function edit($pk, $fields) {
        if($pk == $fields[$this->field_id_parent]){
            throw new Exceptions\Fatal("It's bad idea trying to choose element as his own parent.");
        }
        
        $down = $this->get_down_branch($pk);
        
        foreach ($down as $item) {
            if($fields[$this->field_id_parent] == $item[$this->pk]){
                throw new Exceptions\Fatal("It's bad idea trying to choose own child element as parent.");
            }
        }
        
        $change_url = false;
        
        $current = $this->get($pk);
        
        if ($current[$this->field_item_url] != $fields[$this->field_item_url] ||
                $current[$this->field_id_parent] != $fields[$this->field_id_parent]
                ) {
            $fields[$this->field_url] = $this->make_url($fields[$this->field_id_parent], $fields[$this->field_item_url]);
            $change_url = true;
        }

        $res = parent::edit($pk, $fields);

        if($res && isset($fields[$this->detect_children_sort])) {
            $this->sort($fields[$this->detect_children_sort]);
        }

        if ($res && $change_url) {
            $this->change_children_url($pk);
        }

        return $res;
    }

    public function sort($ids) {
        $to_sort = explode(',', $ids);
        
        for($i = 0; $i < count($to_sort); $i++) {
            $id = (int)$to_sort[$i];
            $this->db->update($this->table, [$this->field_num_sort => $i], "{$this->pk}=:pk", ['pk' => $id]);
        }

        return true;
    }
    
    public function delete($pk) {
        $map = $this->make_tree_from_db($pk);
        
        $ids_arr = $this->get_array_from_field_values($map, $this->pk);
        $ids_arr[] = $pk;
        $in = implode(',', $ids_arr);
        
        $this->db->delete($this->table, "{$this->pk} in ($in)");
        
        return true;
    }
    
    public function get_children($id_parent) {
        return $this->db->select("SELECT * FROM {$this->table} WHERE {$this->field_id_parent}=:id_parent ORDER BY {$this->field_num_sort} ASC",
            ['id_parent' => $id_parent]);
    }
    
    public function get_down_branch($pk) {
        return $this->down_branch($pk);
    }
    
    public function get_up_branch($pk) {
        return $this->up_branch($pk);
    }
    
    public function get_down_branch_by_url($url) {
        if (!$current = $this->get_by_fieldset([$this->field_url => $url])) {
            return [];
        }

        return $this->down_branch($current[0][$this->pk]);
    }
    
    public function get_up_branch_by_url($url) {
        if (!$current = $this->get_by_fieldset([$this->field_url => $url])) {
            return [];
        }

        return $this->up_branch($current[0][$this->pk]);
    }

    public function make_tree($start_level = 0) {
        $map = $this->make_tree_from_db($start_level);
        return $map;
    }

    protected function make_tree_from_db($start_level = 0) {
        $map = [];
        $level = $this->db->select("SELECT * FROM {$this->table}
                                    WHERE {$this->field_id_parent}=:id_parent
                                    ORDER BY {$this->field_num_sort}, {$this->pk} ASC",
                                    ['id_parent' => $start_level]);

        if (!empty($level)) {
            foreach ($level as $one) {
                $one['children'] = $this->make_tree_from_db($one[$this->pk]);
                $map[] = $one;
            }
        }

        return $map;
    }

    protected function make_url($id_parent, $url) {
        if ($id_parent == 0) {
            return $url;
        }

        $one = $this->get($id_parent);
        return $one[$this->field_url] . '/' . $url;
    }

    protected function change_children_url($id_parent) {
        $children = $this->db->select("SELECT * FROM {$this->table} WHERE {$this->field_id_parent}=:id_parent", ['id_parent' => $id_parent]);

        foreach ($children as $child) {
            $one = [$this->field_url => $this->make_url($child[$this->field_id_parent], $child[$this->field_item_url])];
            $where = "{$this->pk}=:pk";
            $this->db->update($this->table, $one, $where, ['pk' => $child[$this->pk]]);
            $this->change_children_url($child[$this->pk]);
        }
    }
    
    protected function get_array_from_field_values($tree, $field_name) {
        $arr = [];
        
        foreach ($tree as $item) {
            $arr[] = $item[$field_name];
            if (isset($item['children']) && count($item['children']) > 0) {
                $child_arr = $this->get_array_from_field_values($item['children'], $field_name);
                $arr = array_merge($arr, $child_arr);
            }
        }
        
        return $arr;
    }
    
    protected function down_branch($pk) {
        if (!$current = $this->get($pk)) {
            return [];
        }

        return $this->db->select("SELECT * FROM {$this->table} WHERE {$this->field_url} LIKE :field_url",
                                 ['field_url' => $current[$this->field_url] . '/%']);
    }
    
    protected function up_branch($pk) {
        if (!$current = $this->get($pk)) {
            return [];
        }

        if ($current[$this->field_id_parent] == 0) {
            return [];
        }

        $steps = explode('/', $current[$this->field_url]);
        $urls = array();
        
        for ($i = 0; $i < count($steps) - 1; $i++) {
            $dop = ($i > 0) ? ($urls[$i - 1] . '/') : '';
            $urls[$i] = $dop . $steps[$i];
        }
        
        return $this->get_in([$this->field_url => $urls]);
    }
    

    
}

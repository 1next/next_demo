<?php

namespace Core\Traits\Model;

trait Delete_by_fieldset {    
    use Make_fieldset{
        Make_fieldset::make_fieldset as delete_by_fieldset_make_fieldset;
    }
    
    public function delete_by_fieldset($obj) {
        $filter = $this->delete_by_fieldset_make_fieldset($obj);
        $this->db->delete($this->table, $filter['where'], $filter['params']);
        return true;
    }

}

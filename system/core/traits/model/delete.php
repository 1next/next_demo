<?php

namespace Core\Traits\Model;

trait Delete {

    public function delete($pk) {
        $row = $this->get($pk);
        
        if($row == null){
            $this->errors->add('delete', 'Запись не может быть удалена, поскольку она не найдена');
            return false;
        }
        
        $filter = $this->pk_where($pk);
        $this->db->delete($this->table, $filter['where'], $filter['params']);
        
        $row = $this->get($pk);
        
        if($row != null){
            $this->errors->add('delete', 'Неопознанная ошибка. Запись не была удалена из базы.');
            return false;
        }
        
        return true;
    }

}

<?php

namespace Core\Traits\Model;

trait Make_fieldset {

    protected function make_fieldset($obj) {
        $pairs = [];
        $values = [];
        $i = 0;

        foreach ($obj as $key => $value) {
            if (is_array($value)) {
                $inner_pairs = array();

                foreach ($value as $k => $v) {
                    if ($v === null) {
                        $inner_pairs[] = "$k = NULL";
                    } else {
                        $inner_pairs[] = "$k = :field$i";
                        $values["field$i"] = $v;
                        $i++;
                    }
                }

                $pairs[] = ' (' . implode(" AND ", $inner_pairs) . ') ';
            } else {
                if ($value === null) {
                    $pairs[] = "$key = NULL";
                } else {
                    $pairs[] = "$key = :field$i";
                    $values["field$i"] = $value;
                    $i++;
                }
            }
        }

        $where = implode(" OR ", $pairs);
        return ['where' => $where, 'params' => $values];
    }

}

<?php

namespace Core\Traits\Model;
use Core\Exceptions;

trait Delete_by_field {

    public function delete_by_field($field, $data) {
        if (!in_array($field, $this->app->table_rules($this->table_name)['fields'])) {
            throw new Exceptions\Fatal($this->table_name . '/' . 'don\'t have field: ' . $field);
        }

        $params = [];
        $where = '';

        if (is_array($data)) {
            if (empty($data)) {
                return false;
            }

            $where_pairs = [];

            for ($i = 0; $i < count($data); $i++) {
                $f = "field$i";
                $params[$f] = $data[$i];
                $where_pairs[] = "$field=:$f";
            }

            $where = implode(' OR ', $where_pairs);
        }
        else {
            $params['field'] = $data;
            $where = "$field=:field";
        }

        $this->db->delete($this->table, $where, $params);
        return true;
    }

}

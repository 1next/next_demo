<?php

namespace Core\Traits;

trait Sets {

    protected static $sets = [];

    public static function get_variant($type) {
        if (!isset(static::$sets[$type])) {
            static::$sets[$type] = new static($type);
        }

        return static::$sets[$type];
    }

    protected function __construct($type) {
        
    }

    final protected function __clone() {
        
    }

    final protected function __sleep() {
        
    }

    final protected function __wakeup() {
        
    }

}

<?php

namespace Core;

use Core\Events;

/**
 * Класс для генерации шаблонов
 */
class Template {
    use \Core\Traits\Sets;
    
    /**
     * Базовый путь, относительно которого указываем путь до конкретного шаблона
     * @var string 
     */
    protected $base_template_path;
    /**
     * Массив с переменными, которые будут доступны всем шаблонам
     * @var array 
     */
    protected $global_vars;

    /**
     * Создать новый класс для генерации шаблонов
     * @param string $base_template_path Базовый путь, относительно которого указываем путь до конкретного шаблона
     */
    protected function __construct($base_template_path) {
        $this->base_template_path = $base_template_path;
        $this->global_vars = [];
        Events::run('TemplateClass.wantGlobals', $this);
    }

    /**
     * Установить базовый путь
     * @param string $base_template_path Базовый путь, относительно которого указываем путь до конкретного шаблона
     */
    public function set_base($base_template_path) {
        $this->base_template_path = $base_template_path;
    }

    /**
     * Добаваить глобальную переменную для всех шаблонов
     * @param string $name Название
     * @param mixed $value Значение
     */
    public function add_global($name, $value){
        $this->global_vars[$name] = $value;
    }
    
    /**
     * Сгенерировать шаблон
     * @param string $fileName Путь до шаблона
     * @param array $vars Переменные для шаблона
     * @return string Сгенерированный код
     */
    public function render($fileName, $vars = []) {
        extract($this->global_vars);
        extract($vars);
        $path = "{$this->base_template_path}$fileName.php";
        ob_start();
        include $path;
        return ob_get_clean();
    }
}

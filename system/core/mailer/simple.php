<?php

namespace Core\Mailer;

class Simple {

    use \Core\Traits\Singleton;
    
    public $mailer;

    private function __construct() {
        $this->mailer = new Php_mailer(true);
        $this->mailer->IsMail();
        $this->mailer->AltBody = "Если у Вас не отображается данное письмо, пожалуйста, воспользуйтесь браузером, понимающим HTML-версии писем!";
        $this->mailer->WordWrap = 80;
        $this->mailer->IsHTML(true);	
    }
    
    public function send($subject, $to, $msg) {
        $this->mailer->Subject = $subject;
        $this->mailer->ClearAddresses();
        $this->mailer->AddAddress($to);
        $this->mailer->MsgHTML($msg);
        $this->mailer->Send();
        return $this;
    }

}

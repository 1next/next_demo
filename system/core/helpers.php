<?php

namespace Core;

/**
 * Класс с набором вспомогательных фукнций
 */
class Helpers {

    /**
     * Вывести переменную в тегах pre [удобно для отображения массивов]
     * @param mixed $data Любые данные
     * @param bool $type false || default - print_r, true - var_dump
     */
    public static function preprint($data, $type = false) {
        echo '<pre>';
        print (!$type) ? print_r($data, true) : var_dump($data);
        echo '</pre>';
    }

    /**
     * Проверить, является url_page дочерним или идентичным для url
     * @param string $url Базовый url-адрес
     * @param string $url_page Url, предполагаемо дочерний или идентичный
     * @return boolean Дочерний или идентичный
     */
    public static function url_strpos0($url, $url_page) {
        if (strpos($url, $url_page) === 0) {
            $symb = substr($url, strlen($url_page), 1);

            if ($symb == '' || $symb == '/') {
                return true;
            }
        }

        return false;
    }

    /**
     * Склеить пути к файлу/папке
     * @param array $args Массив элементов пути к файлу/папке
     * @return string Пусть к файлу/папке
     */
    public static function join_path($args = []) {
        $paths = [];

        foreach ($args as $arg) {

            foreach (explode('/', $arg) as $item) {
                $paths[] = $item;
            }

        }

        $paths = array_diff($paths, ['']);

        return implode('/', $paths);
    }

    /**
     * Распарсить полное имя класса
     * @param string $classname Имя класса вместе с пространством имён
     * @return array Массив с указанием ['namespace' => Пространство имён, 'classname' => Имя класса]
     */
    public static function parse_classname($classname) {
        $parts = explode('\\', $classname);
        $cnt = count($parts);
        $class = $parts[$cnt - 1];
        unset($parts[$cnt - 1]);
        
        return [
            'namespace' => implode('\\', $parts),
            'classname' => $class,
        ];
    }

    /**
     * Получить все элементы папки по переданной маске
     * @param string $dir Путь до папки, в которой производится поиск
     * @param string $mask Маска для выбора
     * @return array Массив с файлами и вложенными папками
     */
    public static function scandir($dir, $mask = '*'){
        $base_path_length = strlen($dir);
        
        if($dir[$base_path_length - 1] != '/'){
            if($base_path_length != 0){
                $dir .= '/';
                $base_path_length++;
            }
            else{
                $dir .= './';
                $base_path_length += 2;
            }
        }
        
        $items = glob($dir . $mask);
        $res = [];
        
        foreach($items as $item){
            $res[] = substr($item, $base_path_length);
        }
        
        return $res;
    }
    
    /**
     * Генерация случайного цвета в формате HTML
     * @return string Например, '#FF0099'
     */
    public static function random_html_color() {
        return sprintf( '#%02X%02X%02X', rand(0, 255), rand(0, 255), rand(0, 255) );
    }
    
    /**
     * Генерация случайной строки
     * @param int $length Длина строки
     * @return string Сгенерированная строка
     */
    public static function random_string($length = 10) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789';
        $token = '';
        $clen = strlen($chars) - 1;

        while (strlen($token) < $length) {
            $token .= $chars[mt_rand(0, $clen)];
        }
        
        return $token;
    }
}

<?php

namespace Core;

/**
 * Класс для работы с шифруемыми Cookie [которые не нужны для чтения или установки из javascript]
 */
class Cookie {
    /**
     * 
     * @var int Длина строки для хеша названия куки
     */
    public static $crypt_length = 20;
    
    /**
     * Добавить куку
     * @param string $name Название
     * @param string $value Значение
     * @param int $time Дата истечения
     * @param string $path Путь
     */
    public static function push($name, $value, $time = null, $path = '/') {
        if ($time == null) {
            $time = time() + 3600 * 24 * 31;
        }

        setcookie(Crypt::light($name, self::$crypt_length), Crypt::encode($value), $time, $path);
    }

    /**
     * Получить куку
     * @param string $name Название
     * @return string Значение
     */
    public static function read($name) {
        if(!self::has($name)){
            return null;
        }
        
        return Crypt::decode($_COOKIE[Crypt::light($name, self::$crypt_length)]);
    }

    /**
     * Получить и сразу удалить куку
     * @param string $name Название
     * @return string Значение
     */
    public static function slice($name) {
        $value = self::read($name);
        self::push($name, '', time() - 1);
        return $value;
    }

    /**
     * Проверить наличие куки
     * @param string $name Название
     * @return boolean Найдена или нет
     */
    public static function has($name) {
        return isset($_COOKIE[Crypt::light($name, self::$crypt_length)]);
    }

    /**
     * Удалить набор кук
     * @param array $names Названия кук
     */
    public static function kick($names) {
        foreach ($names as $name) {
            self::push($name, '', time() - 1);
        }
    }

}
